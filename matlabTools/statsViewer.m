function varargout = statsViewer(varargin)
    % statsViewer MATLAB code for statsViewer.fig
    %      statsViewer, by itself, creates a new statsViewer or raises the existing
    %      singleton*.
    %
    %      H = statsViewer returns the handle to a new statsViewer or the handle to
    %      the existing singleton*.
    %
    %      statsViewer('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in statsViewer.M with the given input arguments.
    %
    %      statsViewer('Property','Value',...) creates a new statsViewer or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before statsViewer_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to statsViewer_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Edit the above text to modify the response to help statsViewer

    % Last Modified by GUIDE v2.5 20-May-2015 15:07:29

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @statsViewer_OpeningFcn, ...
                       'gui_OutputFcn',  @statsViewer_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
end

% --- Executes just before statsViewer is made visible.
function statsViewer_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to statsViewer (see VARARGIN)

    % Choose default command line output for statsViewer
    handles.output      = hObject;
    
    handles.activeIndex = 0;
    
    handles.data        = [];
    handles.selData     = [];
    
    handles.objString1  = 'Coverage [%]';
    handles.objString2  = 'Task Time [%]';
    handles.objString3  = 'Behavior Score [-]';
    handles.objString4  = 'BTSize [-]';
    
    set(handles.mainWindow,           'WindowKeyPressFcn', @mainWindow_KeyPressFcn);
    
    axInd = [handles.axObj1; handles.axObj2; handles.axObj3; handles.axObj4];
    for objIndex = 1:4
        cla(axInd(objIndex));
    end
    cla(handles.axMain);
    
    % Update handles structure
    guidata(hObject, handles);
end

% --- Outputs from this function are returned to the command line.
function varargout = statsViewer_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;
end
% --- Executes during object creation, after setting all properties.
function edtLogPath_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function edtLogPath_Callback(hObject, eventdata, handles)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                          Callback Functions                       %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Executes on button press in btnLoad.
function btnLoad_Callback(hObject, eventdata, handles)
    handles = loadData(handles);
    guidata(hObject,handles);
end
% --- Executes on button press in btnMakeFigure.
function btnMakeFigure_Callback(hObject, eventdata, handles)

    figure()
        ax = axes();%subplot(121);
            makeParetoPlot(ax, handles);
            title(ax,['Generation ' num2str(handles.activeIndex) ' / ' num2str(size(handles.data,3))])
            
        for i=1:size(handles.data,1)
            figure();
            ax = axes();%subplot(size(handles.data,1),2,i*2);
            plotStats(ax, handles, i,1);
            ax.XLabel.FontSize = 17;
            ax.YLabel.FontSize = 17;
            for j=1:numel(ax.Children)
                ax.Children(j).LineWidth = 2;
            end
        end

end

function mainWindow_KeyPressFcn(hObject, eventdata)
    handles = guidata(hObject);
    if (strcmp(eventdata.Key,'q'))
        close(handles.mainWindow)
        return;
    end
    if (strcmp(eventdata.Key,'v'))
        view(handles.axMain,[0 90]);
    end
         
    % Select the data of the first trial
    index = handles.activeIndex;

    % move the trial index as commanded
    delta = 0;
    if (strcmp(eventdata.Key,'leftarrow'))
        delta = -1;
    elseif (strcmp(eventdata.Key,'rightarrow'))
        delta = 1;
    elseif (strcmp(eventdata.Key,'uparrow'))
        delta = 10;
    elseif (strcmp(eventdata.Key,'downarrow'))
        delta = -10;
    end

    if (delta ~= 0)
        index = index + delta;

        %Ensure that the index is within valid trials
        while (index > size(handles.data,3))
            index = index - size(handles.data,3);
        end
        while (index < 1)
            index = index + size(handles.data,3);
        end

        % Update data
        handles.activeIndex = index;
        
        % Select data & save handles
        handles = selData(handles);
        guidata(hObject,handles);
    end
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                           Helper Functions                        %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function handles = loadData(handles)
    fid = fopen(handles.edtLogPath.String);
    data = fread(fid,'single');
    fclose(fid);

%     nGenerations = data(1)-1;
%     nGenomes     = data(2);
%     nObj         = data(3);

    nGenomes     = data(1);
    nObj         = data(2);
    nGenerations = (numel(data)-2)/(nGenomes*nObj);

%     handles.data = reshape(data(4:end), nObj,nGenomes, nGenerations);
    handles.data = zeros(nObj, nGenomes, nGenerations);
    pos = 3;
    for generation=1:nGenerations
        for genome=1:nGenomes
            for obj=1:nObj
                handles.data(obj, genome, generation) = data(pos);
                pos = pos+1;
            end
        end
    end

    handles.activeIndex = size(handles.data,3);
    
    axInd = [handles.axObj1; handles.axObj2; handles.axObj3; handles.axObj4];
    for objIndex=1:size(handles.data,1)
        plotStats(axInd(objIndex), handles, objIndex);
    end
    
    for objIndex = objIndex+1:4
        cla(axInd(objIndex));
        axInd(objIndex).Visible = 'off';
    end
        
    
    handles = selData(handles);
end
function handles = selData(handles)

    handles.txtDisplay.String = ['Generation ' num2str(handles.activeIndex) ' / ' num2str(size(handles.data,3))];
    handles.selData     = squeeze(handles.data(:,:,handles.activeIndex));
    
    makeParetoPlot(handles.axMain, handles);
end

function handles = plotStats(axHandle, handles, objIndex, plotMin)
    data = squeeze(handles.data(objIndex,:,:));

    avg     = mean(data);
    minVal  = min(data);
    maxVal  = max(data);

    hold(axHandle,'off');

    if (nargin < 4)
        plotMin = 2;
    end

    if (plotMin > 1)
        errorbar(axHandle,1:size(data,2),avg,avg-minVal,maxVal-avg); hold(axHandle,'on');
    end
    if (plotMin > 0)
        plot(axHandle,1:size(data,2),minVal,'b'); hold(axHandle,'on');
    end
    plot(axHandle,1:size(data,2),maxVal,'r'); hold(axHandle,'on');
    plot(axHandle,1:size(data,2),avg,'g');

    xlabel(axHandle,'Generations');        
    eval(['ylabel(axHandle,handles.objString' num2str(objIndex) ');']);

    xlim(axHandle ,[0 size(data,2)]);
%     ylim(axHandle ,[0 1]);
end
function handles = makeParetoPlot(axHandle, handles)
    hold (axHandle,'off');
    dim = size(handles.data,1);
    if (dim == 2)
        scatter(axHandle,handles.selData(1,:),handles.selData(2,:));
        xlabel(axHandle,handles.objString1);
        ylabel(axHandle,handles.objString2);
        
        xL = [0 1];
        xL(1) = min(min(handles.selData(:,1)),xL(1));
        xL(2) = max(max(handles.selData(:,1)),xL(2));
        
        yL = [0 1];
        yL(1) = min(min(handles.selData(:,2)),yL(1));
        yL(2) = max(max(handles.selData(:,2)),yL(2));
        
        xlim(xL);
        ylim(yL);
        
    elseif(dim == 3)
        [az, el] = view(handles.axMain);
        scatter3(axHandle,handles.selData(1,:),handles.selData(2,:),handles.selData(3,:));
        xlabel(axHandle,handles.objString1);
        ylabel(axHandle,handles.objString2);
        zlabel(axHandle,handles.objString3);
        
        view(axHandle,[az el]);
        
        
        xL = [0 1];
        xL(1) = min(min(handles.selData(:,1)),xL(1));
        xL(2) = max(max(handles.selData(:,1)),xL(2));
        
        yL = [0 1];
        yL(1) = min(min(handles.selData(:,2)),yL(1));
        yL(2) = max(max(handles.selData(:,2)),yL(2));
        
        zL = [0 1];
        zL(1) = min(min(handles.selData(:,3)),zL(1));
        zL(2) = max(max(handles.selData(:,3)),zL(2));
        
        xlim(axHandle,xL);
        ylim(axHandle,yL);
        zlim(axHandle,zL);
    else
        [az, el] = view(handles.axMain);
        
        scatter3(axHandle,handles.selData(1,:),handles.selData(2,:),handles.selData(3,:),20,handles.selData(4,:));
        xlabel(axHandle,handles.objString1);
        ylabel(axHandle,handles.objString2);
        zlabel(axHandle,handles.objString3);
        
        view(axHandle,[az el]);
        
        xL = [0 1];
%         xL(1) = min(min(handles.selData(:,1)),xL(1));
%         xL(2) = max(max(handles.selData(:,1)),xL(2));
        
        yL = [0 1];
%         yL(1) = min(min(handles.selData(:,2)),yL(1));
%         yL(2) = max(max(handles.selData(:,2)),yL(2));
        
        zL = [0 1];
%         zL(1) = min(min(handles.selData(:,3)),zL(1));
%         zL(2) = max(max(handles.selData(:,3)),zL(2));

        cL = [0 1.2];
%         cL(1) = min(min(handles.selData(:,4)),cL(1));
%         cL(2) = max(max(handles.selData(:,4)),cL(2));
        
        xlim(axHandle,xL);
        ylim(axHandle,yL);
        zlim(axHandle,zL);
        
        caxis(axHandle,cL)
        colorbar(axHandle);
    end
end
