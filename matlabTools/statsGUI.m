function varargout = statsGUI(varargin)
% STATSGUI MATLAB code for statsGUI.fig
%      STATSGUI, by itself, creates a new STATSGUI or raises the existing
%      singleton*.
%
%      H = STATSGUI returns the handle to a new STATSGUI or the handle to
%      the existing singleton*.
%
%      STATSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STATSGUI.M with the given input arguments.
%
%      STATSGUI('Property','Value',...) creates a new STATSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before statsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to statsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help statsGUI

% Last Modified by GUIDE v2.5 28-Nov-2014 11:41:23

% Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @statsGUI_OpeningFcn, ...
                       'gui_OutputFcn',  @statsGUI_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end
    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
end
% End initialization code - DO NOT EDIT


% --- Executes just before statsGUI is made visible.
function statsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to statsGUI (see VARARGIN)

% Choose default command line output for statsGUI
handles.output = hObject;

handles.envDataFilePath = '';
handles.simDataFilePath = '';
handles.xmlDataFilePath = '';

handles.guiMode         = 0;    % 0 = Go through all the genomes; 1 = Payback mode
handles.playDataIndex   = 0;
handles.playDataStep    = 10;


handles.cGenIndex       = 1;
handles.cMemIndex       = 1;
handles.trialSel        = [];
handles.dataSel         = [];

handles.gens            = [];
handles.mems            = [];
handles.IDs             = [];

handles.xmlDOM          = [];
handles.data            = [];
handles.envData         = [];
handles.statsData       = [];
handles.genNodeCount    = [];
handles.lastGenStats    = [];


handles.dataSetPos_mem      = 1;
handles.dataSetPos_id       = 2;
handles.dataSetPos_time     = 3;
handles.dataSetPos_fitness  = 4;
handles.dataSetPos_posX     = 5;
handles.dataSetPos_posY     = 6;
handles.dataSetPos_posZ     = 7;
handles.dataSetPos_vel      = 8;
handles.dataSetPos_psi      = 9;
handles.dataSetPos_psiT     = 10;
handles.dataSetPos_velCmd   = 11;
handles.dataSetPos_psiDotCmd= 12;
handles.dataSetPos_distEst  = 13;

handles.dataSetPos_velEst   = 14;
handles.dataSetPos_senD0ID  = 15;
handles.dataSetPos_senD1    = 16;
handles.dataSetPos_senD1ID  = 17;
handles.dataSetPos_distTrue = 18;
handles.dataSetPos_senV1    = 19;

%             logData.push_back((float)ctrl[0]); //10
%             logData.push_back((float)ctrl[1]); //11
%             logData.push_back((float)ctrl[2]); //12
%             logData.push_back((float)ctrl[3]); //13
% //            logData.push_back((float)0);
%             
%             logData.push_back((float)sensorData[0]);
%             logData.push_back((float)sensorData[1]);
%             logData.push_back((float)sensorData[2]);
%             logData.push_back((float)sensorData[3]);
%             logData.push_back((float)0);

set(handles.mainWindow,           'WindowKeyPressFcn', @mainWindow_KeyPressFcn);
set(handles.statsAxesMax,         'XTickLabel',[]);
% Update handles structure

handles.simDataPathEdit.String = '/Users/tszabo/Documents/CollegeStuff/MSc/Thesis/Models/Cpp/Swarmolution/build/Debug/posData.log';
handles.envDataPathEdit.String = '/Users/tszabo/Documents/CollegeStuff/MSc/Thesis/Models/Cpp/Swarmolution/build/Debug/envData.log';
handles.xmlDataPathEdit.String = '/Users/tszabo/Documents/CollegeStuff/MSc/Thesis/Models/Cpp/Swarmolution/build/Debug/evolutionStats.xml';

guidata(hObject, handles);

% UIWAIT makes statsGUI wait for user response (see UIRESUME)
% uiwait(handles.mainWindow);
end

% --- Outputs from this function are returned to the command line.
function varargout = statsGUI_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;
end

function envDataPathEdit_Callback(hObject, eventdata, handles)
end

% --- Executes during object creation, after setting all properties.
function envDataPathEdit_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

function simDataPathEdit_Callback(hObject, eventdata, handles)
end

% --- Executes during object creation, after setting all properties.
function simDataPathEdit_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

% --- Executes on button press in loadDataBtn.
function loadDataBtn_Callback(hObject, eventdata, handles)
    handles.simDataFilePath = get(handles.simDataPathEdit,'string');
    handles.xmlDataFilePath = get(handles.xmlDataPathEdit,'string');
    handles.envDataFilePath = get(handles.envDataPathEdit,'string');
    
    handles.cMemIndex = 1;
    
    handles = loadSimData(handles);
    if (~isempty(handles.data))
        handles.trialSel   = (handles.data(:,handles.dataSetPos_mem) == handles.mems(handles.cMemIndex));
        while (~any(handles.trialSel))
            handles.cMemIndex   = handles.cMemIndex+1;
            handles.trialSel     = (handles.data(:,1) == handles.mems(handles.cMemIndex));
        end
        handles.dataSel = handles.trialSel;

        handles = makePlots(handles);
    end    
    guidata(hObject, handles);
end


function xmlDataPathEdit_Callback(hObject, eventdata, handles)
end

% --- Executes during object creation, after setting all properties.
function xmlDataPathEdit_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end

% --- Executes on button press in prevMemBtn.
function prevMemBtn_Callback(hObject, eventdata, handles)
    handles.cMemIndex = handles.cMemIndex-1;
    if (handles.cMemIndex == 0)
        handles.cMemIndex = numel(handles.mems);
    end
    
    handles.trialSel   = (handles.data(:,handles.dataSetPos_mem) == handles.mems(handles.cMemIndex));
    while (~any(handles.trialSel))
        handles.cMemIndex   = handles.cMemIndex+1;
        handles.trialSel       = (handles.data(:,1) == handles.mems(handles.cMemIndex));
    end
    handles.dataSel     = handles.trialSel;
    
    handles = makePlots(handles);
    guidata(hObject, handles);
end

% --- Executes on button press in firstMemBtn.
function firstMemBtn_Callback(hObject, eventdata, handles)
    handles.cMemIndex = 1;
    
    handles.trialSel   = (handles.data(:,handles.dataSetPos_mem) == handles.mems(handles.cMemIndex));
    while (~any(handles.trialSel))
        handles.cMemIndex   = handles.cMemIndex+1;
        handles.trialSel       = (handles.data(:,1) == handles.mems(handles.cMemIndex));
    end
    
    handles.dataSel = handles.trialSel;
    
    handles = makePlots(handles);
    guidata(hObject, handles);
end

% --- Executes on button press in lastMemBtn.
function lastMemBtn_Callback(hObject, eventdata, handles)
    handles.cMemIndex = numel(handles.mems);
    
    handles.trialSel   = (handles.data(:,handles.dataSetPos_mem) == handles.mems(handles.cMemIndex));
    while (~any(handles.trialSel))
        handles.cMemIndex   = handles.cMemIndex+1;
        handles.trialSel       = (handles.data(:,1) == handles.mems(handles.cMemIndex));
    end
    
    handles.dataSel= handles.trialSel;
    
    handles = makePlots(handles);
    guidata(hObject, handles);
end

% --- Executes on button press in nextMemBtn.
function nextMemBtn_Callback(hObject, eventdata, handles)
    handles.cMemIndex = handles.cMemIndex+1;
    if (handles.cMemIndex > numel(handles.mems))
        handles.cMemIndex = 1;
    end
    
    handles.trialSel   = (handles.data(:,handles.dataSetPos_mem) == handles.mems(handles.cMemIndex));
    while (~any(handles.trialSel))
        handles.cMemIndex   = handles.cMemIndex+1;
        handles.trialSel     = (handles.data(:,1) == handles.mems(handles.cMemIndex));
    end
    
    handles.dataSel = handles.trialSel;
    
    handles = makePlots(handles);
    guidata(hObject, handles);
end

% --- Executes on button press in openAsFigBtn.
function openAsFigBtn_Callback(hObject, eventdata, handles)
    if (isempty(handles.data))
        return;
    end
    makePlotCurrentFig(handles);
end
% --- Executes on button press in statsFigureBtn.
function statsFigureBtn_Callback(hObject, eventdata, handles)
    if (isempty(handles.statsData))
        return;
    end
    makeStatsPlot(handles);
end
% --- Executes on button press in saveGenomeDataBtn.
function saveGenomeDataBtn_Callback(hObject, eventdata, handles)
    if (isempty(handles.data))
        return;
    end
    
    if (eventdata.Source.Value == 0)
        handles.guiMode = 0;
        
        handles.firstMemBtn.Enable  = 'on';
        handles.lastMemBtn.Enable   = 'on';
        handles.nextMemBtn.Enable   = 'on';
        handles.prevMemBtn.Enable   = 'on';
        handles.statsFigureBtn.Enable  = 'on';
        handles.openAsFigBtn.Enable = 'on';
        
        handles.dataSel = handles.trialSel;
        handles = makePlots(handles);
    else
        handles.guiMode = 1;
        handles.firstMemBtn.Enable  = 'off';
        handles.lastMemBtn.Enable   = 'off';
        handles.nextMemBtn.Enable   = 'off';
        handles.prevMemBtn.Enable   = 'off';
        handles.statsFigureBtn.Enable  = 'off';
        handles.openAsFigBtn.Enable = 'off';
        
        handles.playDataIndex = 2;
        timeVec = unique(handles.data(handles.trialSel,handles.dataSetPos_time));
        tS = ismember(handles.data(:,handles.dataSetPos_time), timeVec(1:handles.playDataIndex));
        handles.dataSel = logical(handles.trialSel .* tS);
        handles = makePlots(handles);
    end
    guidata(hObject,handles);
end
% --- Executes on key press with focus on mainWindow and none of its controls.
function mainWindow_KeyPressFcn(hObject, eventdata)
    handles = guidata(hObject);
    if (strcmp(eventdata.Key,'q'))
        close(handles.mainWindow)
        return;
    end
    
    if (isempty(handles.data))
        return;
    end
    
    if (handles.guiMode == 0)
        if (strcmp(eventdata.Key,'rightarrow'))
            nextMemBtn_Callback(hObject, eventdata, handles);
            return;
        end
        if (strcmp(eventdata.Key,'leftarrow'))
            prevMemBtn_Callback(hObject, eventdata, handles);
            return;
        end
    elseif (handles.guiMode == 1)
        if (strcmp(eventdata.Key,'rightarrow'))
            
            timeVec = unique(handles.data(handles.trialSel,handles.dataSetPos_time));
            
            if (isempty(eventdata.Modifier))
                handles.playDataIndex = handles.playDataIndex + handles.playDataStep;
            else
                handles.playDataIndex = handles.playDataIndex + round(handles.playDataStep/10);
            end
            if (handles.playDataIndex > numel(timeVec))
                handles.playDataIndex = numel(timeVec);
            end
            
            tS = ismember(handles.data(:,handles.dataSetPos_time), timeVec(1:handles.playDataIndex));
            handles.dataSel = logical(handles.trialSel .* tS);
            
            handles = makePlots(handles);
            
        elseif (strcmp(eventdata.Key,'leftarrow'))
            
            timeVec = unique(handles.data(handles.trialSel,handles.dataSetPos_time));
            
            if (isempty(eventdata.Modifier))
                handles.playDataIndex = handles.playDataIndex - handles.playDataStep;
            else
                handles.playDataIndex = handles.playDataIndex - round(handles.playDataStep/10);
            end
            if (handles.playDataIndex < 2)
                handles.playDataIndex = 2;
            end
            
            tS = ismember(handles.data(:,handles.dataSetPos_time), timeVec(1:handles.playDataIndex));
            handles.dataSel = logical(handles.trialSel .* tS);
            
            handles = makePlots(handles);
        elseif (strcmp(eventdata.Key,'uparrow'))
            timeVec = unique(handles.data(handles.trialSel,handles.dataSetPos_time));
            
            if (isempty(eventdata.Modifier))
                handles.playDataIndex = handles.playDataIndex + handles.playDataStep*10;
            else
                handles.playDataIndex = handles.playDataIndex + round(handles.playDataStep);
            end
            if (handles.playDataIndex > numel(timeVec))
                handles.playDataIndex = numel(timeVec);
            end
            
            tS = ismember(handles.data(:,handles.dataSetPos_time), timeVec(1:handles.playDataIndex));
            handles.dataSel = logical(handles.trialSel .* tS);
            
            handles = makePlots(handles);
        elseif (strcmp(eventdata.Key,'downarrow'))
            timeVec = unique(handles.data(handles.trialSel,handles.dataSetPos_time));
            
            if (isempty(eventdata.Modifier))
                handles.playDataIndex = handles.playDataIndex - handles.playDataStep*10;
            else
                handles.playDataIndex = handles.playDataIndex - round(handles.playDataStep);
            end
            if (handles.playDataIndex < 1)
                handles.playDataIndex = 1;
            end
            
            tS = ismember(handles.data(:,handles.dataSetPos_time), timeVec(1:handles.playDataIndex));
            handles.dataSel = logical(handles.trialSel .* tS);
            
            handles = makePlots(handles);
        end
    end
    
    if (strcmp(eventdata.Key,'v'))
        view(handles.mainAxes,[0 90]);
    end
    guidata(hObject,handles);
end

function handles = loadSimData(handles)

% Read Environment data
fID = fopen(handles.envDataFilePath);
if (fID ~= -1)
    data = fread(fID,'single');
    fclose(fID);

    pos      = 1;
    memIndex = 1;

    envHeaderSize = 6;
    while (pos < numel(data))
        handles.envData(memIndex).ID        = data(pos);
        numVx                               = data(pos+1);
        handles.envData(memIndex).simTime   = data(pos+2);
        handles.envData(memIndex).resX      = data(pos+3);
        handles.envData(memIndex).resY      = data(pos+4);
        handles.envData(memIndex).genMode   = data(pos+envHeaderSize-1);

        vertData = zeros(numVx*2,1);
        for i=(pos+6):(pos+envHeaderSize+numVx*2-1)
            vertData(i-pos-(envHeaderSize-1)) = data(i);
        end
        % Because Matlab is column major & the data is row major
        handles.envData(memIndex).vertices  = reshape(vertData,2,numVx)';   

        handles.envData(memIndex).map       = [];
        pos = pos+numVx*2+envHeaderSize;
        memIndex = memIndex+1;
    end
end
% Read coverage map data
fID = fopen('../build/Debug/mapData.log');
if (fID ~= -1)
    data = fread(fID,'single');
%     data = fread(fID,'uint16');
    fclose(fID);

    pos = 1;
    memIndex = -1;
    while (pos < numel(data))
        mem = data(pos);
        sX = data(pos+1);
        sY = data(pos+2);
        N = sX*sY;

        for i=1:numel(handles.envData)
            if (mem == handles.envData(i).ID)
                memIndex = i;
                break;
            end
        end

        mapData = data( (pos+3):(pos+3+N-1) );
        handles.envData(memIndex).map = reshape(mapData,sY,sX)';

        pos = pos + N+3;
        memIndex = -1;
    end
end
% Read simulation data from RAW file
fID     = fopen(handles.simDataFilePath,'r');
if (fID ~= -1)
    data    = fread(fID,'single');
    fclose(fID);
    
    nDataPts = 19;
    while (mod(numel(data),nDataPts) ~= 0)
        nD = numel(data) - mod(numel(data),nDataPts);
        disp('There is missing data! Dropping the last data point');
        data = data(1:nD,:);
    end

    nR = int32(numel(data)/nDataPts);
    handles.data = reshape(data,nDataPts,nR)';
    
    handles.mems = unique(handles.data(:,1));
    handles.IDs  = unique(handles.data(:,2));
end
% Read XML data & stats
    xmlDOM = xmlread(handles.xmlDataFilePath);
    % GeneticAlgorithm Node
    gaNodes = xmlDOM.getElementsByTagName('GeneticAlgorithm');
    if (gaNodes.getLength ~= 1)
        error(['Expecting 1 GeneticAlgorithm node, found: ' num2str(gaNodes.getLength)]);
    end
    gaNode = gaNodes.item(0); clear('gaNodes');

    % Statistics Node
    statsNodes = gaNode.getElementsByTagName('Statistics');
    if (statsNodes.getLength ~= 1)
        error(['Expecting 1 Statistics node, found: ' num2str(statsNodes.getLength)]);
    end
    statsNode = statsNodes.item(0); clear('statsNodes');

    numObjFcnVals = statsNode.getAttribute('numObjFcns');
    if (isempty(numObjFcnVals))
        error('Statistics: can''t find the number of objective functions');
    else
        numObjFcnVals = str2double(numObjFcnVals.toCharArray);
    end

    % Statistics-Generations nodes
    genNodes = statsNode.getElementsByTagName('Generation');
    if (genNodes.getLength == 0)
        error('Expecting at least a generation to be present in the statistics. Found none!');
    end

    %Objective function values
%     handles.statsData = zeros(genNodes.getLength,numObjFcnVals,2);
    handles.statsData = zeros(genNodes.getLength,1,2);
    for sg=0:genNodes.getLength-1
        genNode = genNodes.item(sg);
        
        attr = genNode.getAttribute('meanPareto');
        handles.statsData(sg+1,1,1) = 100*str2double(attr.toCharArray');
        attr = genNode.getAttribute('bestPareto');
        handles.statsData(sg+1,1,2) = 100*str2double(attr.toCharArray');
%         objFcns = genNode.getElementsByTagName('Objective');
%         if (objFcns.getLength ~= numObjFcnVals)
%             error(['Expecting ' num2str(numObjFcnVals) ' objective function values, found ' num2str(sg+1)]);
%         end
% 
%         for ofIndex = 0:objFcns.getLength-1
%             ofNode = objFcns.item(ofIndex);
% 
% %             d = ofNode.getAttribute('max');
% %             if (d.isEmpty)
% %                 error('Can''t find max value of Objective function %d from Generation %d',ofIndex+1,gs+1);
% %             end;
%             handles.statsData(sg+1,ofIndex+1,1) = str2double(d.toCharArray');
%             d = ofNode.getAttribute('mean');
%             if (d.isEmpty)
%                 error('Can''t find max value of Objective function %d from Generation %d',ofIndex+1,gs+1);
%             end;
%             handles.statsData(sg+1,ofIndex+1,2) = str2double(d.toCharArray');
%         end
    end
    genomesNodes = gaNode.getElementsByTagName('Genomes');
    if (genomesNodes.getLength ~= 1)
        error(['Expecting one Genomes node, found: ' num2str(gaNode.getLength)]);
    end
    genomesNode = genomesNodes.item(0); clear('genomesNodes');
    
    genomes = genomesNode.getElementsByTagName('Genome');
    if (genomes.getLength == 0)
        error('Expecting at least one genome to be present. Found none!');
    end
    
    for g=0:genomes.getLength-1
        genome = genomes.item(g);
        
        objFcnsNode = genome.getElementsByTagName('ObjectiveFunctions').item(0);
        
        dataCount = str2double(objFcnsNode.getAttribute('dataCount').toCharArray);
        
        objFcns = objFcnsNode.getElementsByTagName('ObjectiveFunction');
        
        % Create stats table of the last generation
        if (isempty(handles.lastGenStats))
            handles.lastGenStats = zeros(genomes.getLength, objFcns.getLength);
        end
        
        for o=0:objFcns.getLength-1
            handles.lastGenStats(g+1,o+1) = str2double(objFcns.item(o).getAttribute('value').toCharArray)/dataCount;
        end
    end
    
    
    colorsMax = hsv(size(handles.statsData,2));
    colorsMean = hsv(size(handles.statsData,2)) - 0.5;
    colorsMean(colorsMean < 0) = 0;
    cla(handles.statsAxesMax);
    cla(handles.statsAxesMean);
    for i=1:size(handles.statsData,2)
        plot(handles.statsAxesMax,  (squeeze(handles.statsData(:,i,1))), 'color',colorsMax(i,:)); hold(handles.statsAxesMax,'on');
        plot(handles.statsAxesMax, (squeeze(handles.statsData(:,i,2))), 'color',colorsMean(i,:)); hold(handles.statsAxesMean,'on');
%         plot(handles.statsAxesMean, (squeeze(handles.statsData(:,i,2))), 'color',colors(i,:)); hold(handles.statsAxesMean,'on');
    end
    
    xlabel(handles.statsAxesMean,'Generations [-]','FontSize',14);
    ylabel(handles.statsAxesMean,'F mean [%]','FontSize',14);
    ylabel(handles.statsAxesMax,'F max [%]','FontSize',14);
    xlim(handles.statsAxesMax,[0 size(handles.statsData,1)]);
    xlim(handles.statsAxesMean,[0 size(handles.statsData,1)]);
    
    l = ylim(handles.statsAxesMax);
    ylim(handles.statsAxesMax,[0 max(100,l(2))]);
    l = ylim(handles.statsAxesMean);
    ylim(handles.statsAxesMean,[0 max(100,l(2))]);
end

function handles = makePlots(handles)
    if (isempty(handles.data))
        return;
    end
    
    colors  = hsv(numel(handles.IDs));
    cla(handles.mainAxes,       'reset');
    cla(handles.psiDotAxes,     'reset');
    cla(handles.vRefAxes,       'reset');
    cla(handles.fitnessAxes,    'reset');
    cla(handles.droneDistAxes,  'reset');
    cla(handles.altRefAxes,     'reset');
    
    hold(handles.mainAxes,      'on');
    hold(handles.psiDotAxes,    'on');
    hold(handles.vRefAxes,      'on');
    hold(handles.fitnessAxes,   'on');
    hold(handles.droneDistAxes, 'on');
    hold(handles.altRefAxes,    'on');

%   Create Environment Map
    env = [];
    
    for i=1:numel(handles.envData)
        if ( (handles.envData(i).ID) == handles.mems(handles.cMemIndex))
            env = handles.envData(i);
            break;
        end
    end
    
    if (isempty(env))
        error('plotData: Environment data not found');
    end
    
% Create (empty) coverage map based on the data from the environment
    % dimensions
    pointMax = [max(env.vertices(:,1)) max(env.vertices(:,2))];
    pointMin = [min(env.vertices(:,1)) min(env.vertices(:,2))];
    
    mW = round((pointMax(1) - pointMin(1))/env.resX);
    mH = round((pointMax(2) - pointMin(2))/env.resY);
    coverageMap = zeros(mW,mH);
    
    [XP,YP] = meshgrid(pointMin(1):env.resX:pointMax(1),pointMin(2):env.resY:pointMax(2));
    XP = reshape(XP,numel(XP),1);
    YP = reshape(YP,numel(YP),1);
    
    inPoly = inpolygon( XP, YP, env.vertices(:,1), env.vertices(:,2));
    
    XP = XP(inPoly);
    YP = YP(inPoly);
    
    XP = (XP - pointMin(1))/env.resX;
    YP = (YP - pointMin(2))/env.resY;
    
    XP(XP == 0) = 1;
    YP(YP == 0) = 1;
    coverageMap(sub2ind(size(coverageMap),XP,YP)) = 1;
    
% Select & plot the data
    timeVec         = unique(handles.data(handles.dataSel,handles.dataSetPos_time));
    if (numel(handles.IDs) < 2)
        dists = [0 0];
    else
        dists = zeros(numel(unique(handles.data(handles.dataSel,handles.dataSetPos_time))),nchoosek(numel(handles.IDs),2));
    end

    arrowSize = 0.3;
    distCounter = 1;
    for i=1:numel(handles.IDs)
        sel = logical( handles.dataSel .* (handles.data(:,handles.dataSetPos_id) == handles.IDs(i)) );

        if (~any(sel))
            continue;
        end

        fS = find(sel == 1,1,'first');
        fE = find(sel == 1,1,'last');

        mPos = unique([...
            round((handles.data(sel,handles.dataSetPos_posX) - pointMin(1))/env.resX) ...
            round((handles.data(sel,handles.dataSetPos_posY) - pointMin(2))/env.resY) ...
            ],'rows');

        mPos(mPos < 1) = 1;
        mPos(mPos(:,1) > size(coverageMap,1),1) = size(coverageMap,1);
        mPos(mPos(:,2) > size(coverageMap,2),2) = size(coverageMap,2);
        coverageMap(sub2ind(size(coverageMap),mPos(:,1),mPos(:,2))) = 2;

        for j=(i+1):numel(handles.IDs)
            sel2 = logical( handles.dataSel .* (handles.data(:,handles.dataSetPos_id) == handles.IDs(j)) );
            [~,~,d] = cart2sph(handles.data(sel,handles.dataSetPos_posX) - handles.data(sel2,handles.dataSetPos_posX), ...
                                          handles.data(sel,handles.dataSetPos_posY) - handles.data(sel2,handles.dataSetPos_posY), ...
                                          handles.data(sel,handles.dataSetPos_posZ) - handles.data(sel2,handles.dataSetPos_posZ));
                                     
                                      
            dists(:,distCounter) = d(1:size(dists,1));
                                      
            distCounter = distCounter+1;                                      
        end
        
        
        plot3(   handles.mainAxes,handles.data(sel,handles.dataSetPos_posX),handles.data(sel,handles.dataSetPos_posY),handles.data(sel,handles.dataSetPos_posZ),'color',colors(i,:)); hold(handles.mainAxes,'on');
        scatter3(handles.mainAxes,handles.data(fS ,handles.dataSetPos_posX),handles.data(fS ,handles.dataSetPos_posY),handles.data(fS ,handles.dataSetPos_posZ),50,colors(i,:),'x');
        scatter3(handles.mainAxes,handles.data(fE ,handles.dataSetPos_posX),handles.data(fE ,handles.dataSetPos_posY),handles.data(fE ,handles.dataSetPos_posZ),50,colors(i,:),'o');
        quiver3(handles.mainAxes,handles.data(fE ,handles.dataSetPos_posX),handles.data(fE ,handles.dataSetPos_posY),handles.data(fE ,handles.dataSetPos_posZ),...
            cos(handles.data(fE ,handles.dataSetPos_psi))*arrowSize, sin(handles.data(fE ,handles.dataSetPos_psi))*arrowSize, 0,   ...
            'color',colors(i,:));

%         plot(handles.psiDotAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_psiDotCmd)*(180/pi),'color',colors(i,:)); 
        plot(handles.psiDotAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,19),'color',colors(i,:)); 

%         plot(handles.vRefAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_velCmd),'color',colors(i,:)); 
%         plot(handles.vRefAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_velCmd),'color',colors(i,:)); 
        plot(handles.vRefAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_vel),'color',colors(i,:)); 

%         plot(handles.altRefAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_dAltCmd),'color',colors(i,:)); 
%         plot(handles.altRefAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_senD0),'color',colors(i,:)); 
        plot(handles.altRefAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_velCmd),'color',colors(i,:)); 
        
        plot(handles.droneDistAxes,handles.data(sel,handles.dataSetPos_time), handles.data(sel,handles.dataSetPos_distEst)/5,'color',colors(i,:));  hold on
        plot(handles.droneDistAxes,handles.data(sel,handles.dataSetPos_time), handles.data(sel,handles.dataSetPos_distTrue)/5,'--','color',colors(i,:));  hold on
        
        plot(handles.fitnessAxes,handles.data(sel,handles.dataSetPos_time), handles.data(sel,handles.dataSetPos_velEst),'--','color',colors(i,:));  hold on
        
    end
    
    tEnd = max(handles.data(handles.dataSel,handles.dataSetPos_time));

    % Main Axes

% Plot the coverage map
    extr = max(max(abs([pointMin; pointMax])));
    axes(handles.mainAxes);
    fill3(1.1*[-extr -extr extr extr -extr]',1.1*[-extr extr extr -extr -extr]',[0 0 0 0 0]',[0.8 0.8 0.8]);
%     [xMark, yMark] = meshgrid([-extr extr],[-extr extr]);
%     surf(handles.mainAxes,xMark,yMark,0*xMark+1);

    if (handles.showCovMap.Value)
        [xMark, yMark] = meshgrid(pointMin(1):env.resX:pointMax(1),pointMin(2):env.resY:pointMax(2));
        surf(handles.mainAxes,xMark, yMark, 0*xMark, coverageMap','EdgeColor','none','LineStyle','none','FaceLighting','phong');
    end
    
% Plot the area limits
    lims = env.vertices;
    lims = [lims 0.01+zeros(size(lims,1),1)];
    lims = [lims; lims(1,:)];
    plot3(handles.mainAxes,lims(:,1),lims(:,2),lims(:,3),'LineWidth',2,'color','k');

    coverage = 100*(sum(sum(env.map == 2)))/(sum(sum(env.map == 2)) + sum(sum(env.map == 1)));
    if (abs(tEnd - env.simTime) < 0.1)
        titleStr = sprintf('Member: %d/%d - SimTime: %.1f - Coverage: %.2f%%',handles.mems(handles.cMemIndex),numel(handles.mems),tEnd, coverage);
        
        set(handles.mainFigTitle,'backgroundcolor',[0.8400    1    0.8400]);
    else
        titleStr = sprintf('Member: %d/%d - SimTime: %.1f/%.1f - Coverage: %.2f%% - CRASH',handles.mems(handles.cMemIndex),numel(handles.mems),tEnd, env.simTime, coverage);
        set(handles.mainFigTitle,'backgroundcolor',[1    0.8400    0.8400]);
    end
    set(handles.mainFigTitle,'string',titleStr);

    xlabel(handles.mainAxes,'X [m]','FontSize',14);
    ylabel(handles.mainAxes,'Y [m]','FontSize',14);
    view(handles.mainAxes,[90 90]);
    
    xlim(handles.mainAxes,1.1*[-extr extr]);
    ylim(handles.mainAxes,1.1*[-extr extr]);
    zlim(handles.mainAxes,[0 5]);
    
    view(handles.mainAxes,[0 90]);
    grid(handles.mainAxes,'on');
        
    % Psi-dot axes
%     ylim(handles.psiDotAxes,[-pi pi]);
%     ylim(handles.psiDotAxes,1.1*[-180 180]);
    xlim(handles.psiDotAxes,[0 tEnd]);
    
    
    xlabel(handles.psiDotAxes,'Time [s]','FontSize',14);
    ylabel(handles.psiDotAxes,'Psi Dot [deg/s]','FontSize',14);

    % Vref axes
%     ylim(handles.vRefAxes,1.1*[-0.5 0.5]);
    xlim(handles.vRefAxes,[0 tEnd]);
    
    ylabel(handles.vRefAxes,'Vel [m/s]','FontSize',14);
    xlabel(handles.vRefAxes,'Time [s]','FontSize',14);

    % dAlt ref axes
%     ylim(handles.altRefAxes,[0 1]);
    xlim(handles.altRefAxes,[0 tEnd]);
    
%     ylabel(handles.altRefAxes,'dAlt [m]','FontSize',14);
    ylabel(handles.altRefAxes,'vCmd [m/s]','FontSize',14);
    xlabel(handles.altRefAxes,'Time [s]','FontSize',14);
    plot(handles.altRefAxes,[0 tEnd],[0 0],'--k');
    plot(handles.altRefAxes,[0 tEnd],[0.5 0.5],'--k');
    plot(handles.altRefAxes,[0 tEnd],[0.2 0.2],'--k');
    
    % Fitness axes
%     plot(handles.fitnessAxes,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_fitness)*100,'color',colors(i,:));
%     ylim(handles.fitnessAxes,[0 100]);
    xlim(handles.fitnessAxes,[0 tEnd]);
    
    ylabel(handles.fitnessAxes,'Rel Velocity [m/s]','FontSize',14);
%     ylabel(handles.fitnessAxes,'Fitness [%]','FontSize',14);
    xlabel(handles.fitnessAxes,'Time [s]','FontSize',14);
    grid(handles.fitnessAxes,'on');
    legend(handles.fitnessAxes,'0','1','2','3');
    
%     % Drone-Drone distance axes
%     if (numel(handles.IDs) < 2)
%         plot(handles.droneDistAxes,[timeVec(1) timeVec(end)], [0 0]); hold on
%         ylim(handles.droneDistAxes,[0 6]);
%         xlim(handles.droneDistAxes,[0 max(handles.data(handles.dataSel,handles.dataSetPos_time))]);
%         xlabel(handles.droneDistAxes,'Time [s]','FontSize',14);
%         ylabel(handles.droneDistAxes,'D-D dist [m]','FontSize',14);
%     else
%         plot(handles.droneDistAxes,timeVec, [min(dists,[],2) mean(dists,2)]); hold on
%         plot(handles.droneDistAxes,[min(handles.data(handles.dataSel,handles.dataSetPos_time)) max(handles.data(handles.dataSel,handles.dataSetPos_time))], [1 1],'--m');
%         plot(handles.droneDistAxes,[min(handles.data(handles.dataSel,handles.dataSetPos_time)) max(handles.data(handles.dataSel,handles.dataSetPos_time))], [0.1 0.1],'--r');
%         ylim(handles.droneDistAxes,[0 6]);
%         xlim(handles.droneDistAxes,[0 max(handles.data(handles.dataSel,handles.dataSetPos_time))]);
%         xlabel(handles.droneDistAxes,'Time [s]','FontSize',14);
%         ylabel(handles.droneDistAxes,'D-D dist [m]','FontSize',14);
%     end
    ylabel(handles.droneDistAxes,'Distance Estimates [m]','FontSize',14);
    grid(handles.droneDistAxes,'on');
    ylim(handles.droneDistAxes,[0 1.2]);
    xlim(handles.droneDistAxes,[0 tEnd]);
    
    hold(handles.mainAxes,'off');
    hold(handles.psiDotAxes,'off');
    hold(handles.vRefAxes,'off');
    hold(handles.fitnessAxes,'off');
    hold(handles.droneDistAxes,'off');
    hold(handles.altRefAxes,'off');
end

function makePlotCurrentFig(handles)
   makeReportPlot(handles);
   return;
%   Create Environment Map
    env = [];
    
    for i=1:numel(handles.envData)
        if ( (handles.envData(i).ID) == handles.mems(handles.cMemIndex))
            env = handles.envData(i);
            break;
        end
    end
    
    if (isempty(env))
        error('plotData: Environment data not found');
    end
    colors  = hsv(numel(handles.IDs));
    pointMax = [max(env.vertices(:,1)) max(env.vertices(:,2))];
    pointMin = [min(env.vertices(:,1)) min(env.vertices(:,2))];
    
    mW = round((pointMax(1) - pointMin(1))/env.resX);
    mH = round((pointMax(2) - pointMin(2))/env.resY);
    coverageMap = zeros(mW,mH);
    
    [XP,YP] = meshgrid(pointMin(1):env.resX:pointMax(1),pointMin(2):env.resY:pointMax(2));
    XP = reshape(XP,numel(XP),1);
    YP = reshape(YP,numel(YP),1);
    
    inPoly = inpolygon( XP, YP, env.vertices(:,1), env.vertices(:,2));
    
    XP = XP(inPoly);
    YP = YP(inPoly);
    
    XP = (XP - pointMin(1))/env.resX;
    YP = (YP - pointMin(2))/env.resY;
    
    XP(XP == 0) = 1;
    YP(YP == 0) = 1;
    coverageMap(sub2ind(size(coverageMap),XP,YP)) = 1;

% Select & plot the data
    timeVec         = unique(handles.data(handles.dataSel,handles.dataSetPos_time));
    if (numel(handles.IDs) < 2)
        dists = [0 0];
    else
        dists = zeros(numel(unique(handles.data(handles.dataSel,handles.dataSetPos_time))),nchoosek(numel(handles.IDs),2));
    end
    
% Set up figure
    fig = figure();
    
    figSize = get(fig,'Position');
    figSize(1) = figSize(1) - 0.75*figSize(3);
    figSize(3) = 1.5*figSize(3);
    set(fig,'Position',figSize);
    
    Smain   = subplot(1,3,1);
    Spsi    = subplot(3,3,2);
    Svel    = subplot(3,3,5);
    Salt    = subplot(3,3,8);
    Sdd     = subplot(2,3,3);
    Sfit    = subplot(2,3,6);
    set(Smain,'Position',[0.05 0.1 0.3 0.82])
    
    distCounter = 1;
    
% Process the individual drones
    for i=1:numel(handles.IDs)
        sel = logical( handles.dataSel .* (handles.data(:,handles.dataSetPos_id) == handles.IDs(i)) );

        fS = find(sel == 1,1,'first');
        fE = find(sel == 1,1,'last');
    % Mark the coverage of the individual drones on the coverage map
        mPos = unique([...
            round((handles.data(sel,handles.dataSetPos_posX) - pointMin(1))/env.resX) ...
            round((handles.data(sel,handles.dataSetPos_posY) - pointMin(2))/env.resY) ...
            ],'rows');

        mPos(mPos == 0) = 1;
        coverageMap(sub2ind(size(coverageMap),mPos(:,1),mPos(:,2))) = 2;

    % Plot the track of the drones
        hold(Smain,'on');
        plot3(   Smain,handles.data(sel,handles.dataSetPos_posX),handles.data(sel,handles.dataSetPos_posY),handles.data(sel,handles.dataSetPos_posZ),'color',colors(i,:)); hold(handles.mainAxes,'on');
        scatter3(Smain,handles.data(fS ,handles.dataSetPos_posX),handles.data(fS ,handles.dataSetPos_posY),handles.data(fS ,handles.dataSetPos_posZ),50,colors(i,:),'x');

    % Calculate the distances from this drone to all other drones
        for j=(i+1):numel(handles.IDs)
            sel2 = logical( handles.dataSel .* (handles.data(:,handles.dataSetPos_id) == handles.IDs(j)) );
            [~,~,d] = cart2sph(handles.data(sel,handles.dataSetPos_posX) - handles.data(sel2,handles.dataSetPos_posX), ...
                                          handles.data(sel,handles.dataSetPos_posY) - handles.data(sel2,handles.dataSetPos_posY), ...
                                          handles.data(sel,handles.dataSetPos_posZ) - handles.data(sel2,handles.dataSetPos_posZ));
                                     
                                      
            dists(:,distCounter) = d(1:size(dists,1));
                                      
            distCounter = distCounter+1;                                      
        end

    % Plot the flight data
%         plot(Spsi,handles.data(sel,handles.dataSetPos_time),max(min(handles.data(sel,handles.dataSetPos_psiDotCmd)*(180/pi),30),-30),'color',colors(i,:)); 
        plot(Spsi,handles.data(sel,handles.dataSetPos_time),max(min(handles.data(sel,handles.dataSetPos_psi)*(180/pi),30),-30),'color',colors(i,:)); 
        hold(Spsi,'on');

%         plot(Svel,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_velCmd),'color',colors(i,:)); 
        plot(Svel,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_vel),'color',colors(i,:)); 
        hold(Svel,'on');

%         plot(Salt,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_dAltCmd),'color',colors(i,:)); 
        hold(Salt,'on');
        
%         plot(Sfit,handles.data(sel,handles.dataSetPos_time),handles.data(sel,handles.dataSetPos_fitness)*100,'color',colors(i,:));
    end
    
    
    tEnd = max(handles.data(handles.dataSel,handles.dataSetPos_time));
    
% Plot the coverage map
    extr = max(max(abs([pointMin; pointMax])));
    [xMark, yMark] = meshgrid([-extr extr],[-extr extr]);
    surf(Smain,xMark,yMark,0*xMark);

    if (handles.showCovMap.Value)
        [xMark, yMark] = meshgrid(pointMin(1):env.resX:pointMax(1),pointMin(2):env.resY:pointMax(2));
        surf(Smain,xMark, yMark, 0*xMark, coverageMap','EdgeColor','none','LineStyle','none','FaceLighting','phong');
    end
    
% Plot the area limits
    lims = env.vertices;
    lims = [lims 0.01+zeros(size(lims,1),1)];
    lims = [lims; lims(1,:)];
    plot3(Smain,lims(:,1),lims(:,2),lims(:,3),'LineWidth',5,'color','k');
    %colorbar('peer',Smain);
    
% Main plot title
    coverage = 100*sum(sum(coverageMap == 2))/(sum(sum(coverageMap == 2)) + sum(sum(coverageMap == 1)));
    if (abs(tEnd - env.simTime) < 0.1)
        titleStr = sprintf('M: %d/%d - Time: %.1f - Cover: %.2f%%',handles.mems(handles.cMemIndex),numel(handles.mems),tEnd, coverage);
        C = [0 0.5 0];
    else
        titleStr = sprintf('M: %d/%d - Time: %.1f/%.1f - Cover: %.2f%% - CRASH',handles.mems(handles.cMemIndex),numel(handles.mems),tEnd, env.simTime, coverage);
        C = [0.5 0 0];
    end
    
% Beauty things: axis labels, limits, etc.
    title(Smain,titleStr,'color',C);
    xlabel(Smain,'Y [m]','FontSize',14);
    ylabel(Smain,'X [m]','FontSize',14);

    view(Smain,[0 90]);
    xlim(Smain,[-extr extr]);
    ylim(Smain,[-extr extr]);
    zlim(Smain,[0 5]);
    hold(Smain,'off');

        
    ylim(Spsi,1.1*[-30 30]);
    hold(Spsi,'off');
    xlim(Spsi,[0 tEnd]);
    xlabel(Spsi,'Time [s]','FontSize',14);
    ylabel(Spsi,'Psi [deg]','FontSize',14);


    ylim(Svel,1.1*[-0.5 0.5]);
    hold(Svel,'off');
    xlim(Svel,[0 tEnd]);
    ylabel(Svel,'Vel [m/s]','FontSize',14);
    xlabel(Svel,'Time [s]','FontSize',14);
    

    ylim(Salt,1.1*[-1.5 1.5]);
    hold(Salt,'off');
    xlim(Salt,[0 tEnd]);
    ylabel(Salt,'dAlt [m]','FontSize',14);
    xlabel(Salt,'Time [s]','FontSize',14);

    
    hold(Sfit,'on');
    plot(Sfit,[0 tEnd],[100 100],'--k');
    ylim(Sfit,[0 110]);
    xlim(Sfit,[0 tEnd]);
    ylabel(Sfit,'Fitness [%]','FontSize',14);
    xlabel(Sfit,'Time [s]','FontSize',14);
    hold(Sfit,'off');
    
    hold(Sdd,'off');
    if (numel(handles.IDs) < 2)
        plot(Sdd,[timeVec(1) timeVec(end)], dists);
    else
        plot(Sdd,timeVec, [min(dists,[],2) mean(dists,2)]);    
    end
%     hold(Sdd,'on');
%     plot(Sdd,[min(handles.data(dataSel,handles.dataSetPos_time)) max(handles.data(dataSel,handles.dataSetPos_time))], [1 1],'--m');
%     plot(Sdd,[min(handles.data(dataSel,handles.dataSetPos_time)) max(handles.data(dataSel,handles.dataSetPos_time))], [0.1 0.1],'--r');
%     ylim(Sdd,[0 6]);
%     xlim(Sdd,[0 max(handles.data(dataSel,handles.dataSetPos_time))]);
%     xlabel(Sdd,'Time [s]','FontSize',14);
%     ylabel(Sdd,'D-D dist [m]','FontSize',14);
    plot(Sdd,timeVec, [min(dists,[],2) mean(dists,2)]); hold(Sdd,'on');
    plot(Sdd,[min(handles.data(handles.dataSel,handles.dataSetPos_time)) max(handles.data(handles.dataSel,handles.dataSetPos_time))], [1 1],'--m');
    plot(Sdd,[min(handles.data(handles.dataSel,handles.dataSetPos_time)) max(handles.data(handles.dataSel,handles.dataSetPos_time))], [0.1 0.1],'--r');
    ylim(Sdd,[0 6]);
    xlim(Sdd,[0 max(handles.data(handles.dataSel,handles.dataSetPos_time))]);
    xlabel(Sdd,'Time [s]','FontSize',14);
    ylabel(Sdd,'D-D dist [m]','FontSize',14);
end

function makeStatsPlot(handles)
    figure();
    colors = hsv(size(handles.statsData,2));
    
    Hmax    = subplot(211);
    Hmean   = subplot(212);
    
    for i=1:size(handles.statsData,2)
        plot(Hmax,  (squeeze(handles.statsData(:,i,1))), 'color',colors(i,:)); hold(handles.statsAxesMax,'on');
        plot(Hmean, (squeeze(handles.statsData(:,i,2))), 'color',colors(i,:)); hold(handles.statsAxesMean,'on');
    end
    
    xlabel(Hmean,'Generations [-]','FontSize',14);
    ylabel(Hmean,'F mean [%]','FontSize',14);
    ylabel(Hmax,'F max[%]','FontSize',14);
    xlim(Hmax,[0 size(handles.statsData,1)]);
    xlim(Hmean,[0 size(handles.statsData,1)]);
    
    l = ylim(Hmax);
    ylim(Hmax,[0 max(100,l(2))]);
    l = ylim(Hmean);
    ylim(Hmean,[0 max(100,l(2))]);
    
%     for i=1:size(handles.statsData,2)
%         plot(Hmax,  100*(squeeze(handles.statsData(:,i,1))), 'color',colors(i,:)); hold(Hmax,'on');
%         plot(Hmean, 100*(squeeze(handles.statsData(:,i,2))), 'color',colors(i,:)); hold(Hmean,'on');
%     end
%     
%     xlabel(Hmean,'Generations [-]','FontSize',17);
%     ylabel(Hmean,'Fmean [%]','FontSize',17);
%     xlim(Hmean,[0 size(handles.statsData,1)]);
%     ylim(Hmean,[0 100]);
%     
%     ylabel(Hmax,'Fmax [%]','FontSize',17);
%     xlim(Hmax,[0 size(handles.statsData,1)]);
%     ylim(Hmax,[0 100]);
%     H = legend(Hmax,'Coverage','Penalty','location','SouthEast');
%     set(H,'FontSize',17);
end


% --- Executes on button press in paretoBtn.
function paretoBtn_Callback(hObject, eventdata, handles)
    % hObject    handle to paretoBtn (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
%     
%     if (isempty(handles.lastGenStats))
%         return;
%     end
    
    if (size(handles.lastGenStats,2) > 4)
        error('Can''t display Pareto front of more than 3 dimenstions');
    end
    
    figure();
    if (size(handles.lastGenStats,2) == 1)
        plot(handles.lastGenStats);
    end
    
    if (size(handles.lastGenStats,2) == 2)

%         x = -5:0.1:5;
%         [X,Y,Z] = meshgrid(x,x,x);
%         
%         data = zeros(numel(x),numel(x),3);
%         data(:,:,1) = X;
%         data(:,:,2) = Y;
%         data(:,:,3) = Z;
%         
%         f1 = zeros(numel(x),numel(x));
%         f2 = zeros(numel(x),numel(x));
%         
%         for i=1:3
%             f2 = f2 + (abs(data(:,:,i))).^0.8 + 5*sin(data(:,:,i).^3);
%             if (i == 3)
%                 continue;
%             end
%                 
%             f1 = f1 - 10*exp(-0.2*sqrt(data(:,:,i).^2 + data(:,:,i+1).^2));
%         end
        
        
        scatter(...
            (handles.lastGenStats(:,1)),...
            (handles.lastGenStats(:,2)),...
            'rx');hold on
%         plot(f1,f2,'k','LineWidth',2);  hold on
        xlim([0 1]);
        ylim([0 1]);
        
    end
    if (size(handles.lastGenStats,2) == 3)
%         subplot(1,2,1)
        scatter3(...
            (handles.lastGenStats(:,1)),...
            (handles.lastGenStats(:,2)),...
            (handles.lastGenStats(:,3)),...
            'r');
        
        L = ylim();
        ylim([min(L(1),0) max(L(2),1)]);
        L = xlim();
        xlim([min(L(1),0) max(L(2),1)]);
        L = zlim();
        zlim([min(L(1),0) max(L(2),1)]);
        
        xlabel('SimTime Score [-]');
        ylabel('TurnRate Score [-]');
        zlabel('Tree Size Score [-]');
        return;
        subplot(3,2,2)
            scatter(...
                (handles.lastGenStats(:,1)),...
                (handles.lastGenStats(:,2))...
            );
            ylim([0 1]);
            xlim([0 1]);
            
            xlabel('Distance [-]');
            ylabel('TurnRate [-]');
        subplot(3,2,4)
            scatter(...
                (handles.lastGenStats(:,1)),...
                (handles.lastGenStats(:,3))...
            );
            ylim([0 1]);
            xlim([0 1]);
            
            xlabel('Distance [-]');
            ylabel('TreeSize [-]');
        subplot(3,2,6)
            scatter(...
                (handles.lastGenStats(:,2)),...
                (handles.lastGenStats(:,3))...
            );
            ylim([0 1]);
            xlim([0 1]);
            
            xlabel('TurnRate [-]');
            ylabel('TreeSize [-]');
        
    end
    if (size(handles.lastGenStats,2) == 4)
        scatter(...
            (handles.lastGenStats(:,1)),...
            (handles.lastGenStats(:,2)),...
...             (handles.lastGenStats(:,3)),...
            20*ones(size(handles.lastGenStats,1),1), ...
            (handles.lastGenStats(:,4))...
            );
        
        xlabel('Velocity Score[-]');%,'FontSize',17);
        ylabel('Turn Rate Score [-]');%,'FontSize',17);
        zlabel('Score Product [-]');%,'FontSize',17);
        xlim([0 1]);
        ylim([0 1]);
        zlim([0 1]);
        caxis([0 1]);
        colorbar
        
    end
    
%     title('Multi-Objective optimization: Pareto Front','FontSize',17);
end


% --- Executes on button press in showCovMap.
function showCovMap_Callback(hObject, eventdata, handles)
% hObject    handle to showCovMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showCovMap
end

function makeReportPlot(handles)

    figure();
    colors  = hsv(numel(handles.IDs));
    
    lineTypeLeg = 1;
    for i=1:numel(handles.IDs)
        sel = logical( handles.dataSel .* (handles.data(:,handles.dataSetPos_id) == handles.IDs(i)) );

        if (~any(sel))
            continue;
        end

        data = handles.data(sel,:);
        Sdef = data(:,handles.dataSetPos_velCmd) == 1;
        
        fS = find(sel == 1,1,'first');
        fE = find(sel == 1,1,'last');
        
        markerSize = 550;
        scatter3(handles.data(fS ,handles.dataSetPos_posX),handles.data(fS ,handles.dataSetPos_posY),handles.data(fS ,handles.dataSetPos_posZ),markerSize,colors(i,:),'x','MarkerFaceColor','flat','HandleVisibility','off'); hold on
        scatter3(handles.data(fE ,handles.dataSetPos_posX),handles.data(fE ,handles.dataSetPos_posY),handles.data(fE ,handles.dataSetPos_posZ),markerSize,colors(i,:),'o','MarkerFaceColor','flat','HandleVisibility','off'); hold on

        
        segLim = [1; find(diff(Sdef) ~= 0); size(data,1)];
        

        
%         if (lineTypeLeg == 1)
%             scatter3(-100, -100, -100,'kx','DisplayName','Start'); hold on
%             scatter3(-100, -100, -100,'ko','DisplayName','End'); hold on
%             plot3(0,0,0,'k-.','DisplayName','Normal Behavior');
%             plot3(0,0,0,'k','DisplayName','Avoidance Behavior');
%             lineTypeLeg = 0;
%         end
        
        legMarker = 1;
        for p=1:numel(segLim)-1;
            S = segLim(p):segLim(p+1);
            if (mean(data(S,handles.dataSetPos_velCmd)) >  0.9)
                    plot3(   data(S,handles.dataSetPos_posX),data(S,handles.dataSetPos_posY),data(S,handles.dataSetPos_posZ),'color',colors(i,:),'HandleVisibility','off');
            else
                if (legMarker == 1)
                    plot3(   data(S,handles.dataSetPos_posX),data(S,handles.dataSetPos_posY),data(S,handles.dataSetPos_posZ),'color',colors(i,:),'DisplayName',['MAV ' num2str(i)],'LineWidth',2);
                    legMarker = 0;
                else
                    plot3(   data(S,handles.dataSetPos_posX),data(S,handles.dataSetPos_posY),data(S,handles.dataSetPos_posZ),'color',colors(i,:),'HandleVisibility','off','LineWidth',3);
                end
            end
        end
    end
    
    env = [];
    
    for i=1:numel(handles.envData)
        if ( (handles.envData(i).ID) == handles.mems(handles.cMemIndex))
            env = handles.envData(i);
            break;
        end
    end
    
    lims = env.vertices;
    lims = [lims 0.01+zeros(size(lims,1),1)];
    lims = [lims; lims(1,:)];
    plot3(lims(:,1),lims(:,2),lims(:,3),'LineWidth',2,'color','k','HandleVisibility','off');
    
    xlabel('X [m]','FontSize',14);
    ylabel('Y [m]','FontSize',14);
    
    L = 3.2;
    xlim([-L L]);
    ylim([-L L]);
%     axis equal
%     legend show
    view([0 90]);
    grid off
    
end