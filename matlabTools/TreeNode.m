classdef TreeNode < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        children        = {};
        
        boxSize         = 10;
        spacingFactorX  = 1.2;
        spacingFactorY  = 2;
        nodeName        = '';
        
        attributes      = {};
        
        parent;
        
        Xdata           = [];
        Ydata           = [];
        
        drawnHandle     = [];
    end
    
    properties(Constant)
        Success = 1;
        Failure = 0;
    end
    
    methods
        function obj = TreeNode(xmlNode, parent)
            if (~isa(xmlNode,'org.apache.xerces.dom.DeferredElementImpl'))
               throw(MException('TreeNode:constructor', 'TreeNode constructor requires an XML Node handle'));
            end
            
            if(nargin < 2)
                parent = [];
            end
            
            obj.parent = parent;
            obj.nodeName = char(xmlNode.getNodeName);
            
            obj.children = TreeNode.empty();
            if (strcmp(obj.nodeName,'BTSequence'))
                child = xmlNode.getFirstChild;
                while (~isempty(child))
                    cName = char(child.getNodeName);
                    if (any(ismember(cName,'#')) == 0)
                        childNode = TreeNode(child, obj);
                        obj.children(end+1) = childNode;
                    end
                    child = child.getNextSibling;
                end
                
                angs = linspace(-pi,pi,100);
                obj.Xdata = (obj.boxSize/2)*cos(angs);
                obj.Ydata = (obj.boxSize/2)*sin(angs);
            elseif (strcmp(obj.nodeName,'BTSelect'))
                child = xmlNode.getFirstChild;
                while (~isempty(child))
                    cName = char(child.getNodeName);
                    if (any(ismember(cName,'#')) == 0)
                        childNode = TreeNode(child, obj);
                        obj.children(end+1) = childNode;
                    end
                    child = child.getNextSibling;
                end
                
                angs = linspace(-pi,pi,100);
                obj.Xdata = (obj.boxSize/2)*cos(angs);
                obj.Ydata = (obj.boxSize/2)*sin(angs);
            elseif (strcmp(obj.nodeName,'BTConditionC'))               
                obj.attributes{1} = char(xmlNode.getAttribute('compareType'));
                obj.attributes{2} = str2double(xmlNode.getAttribute('dataIndex'));
                obj.attributes{3} = str2double(xmlNode.getAttribute('compareTo'));
                
                obj.Xdata = [-obj.boxSize/2 -obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2];
                obj.Ydata = [-obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2 -obj.boxSize/2];
            elseif (strcmp(obj.nodeName,'BTConditionP'))
                obj.attributes{1} = char(xmlNode.getAttribute('compareType'));
                obj.attributes{2} = str2double(xmlNode.getAttribute('dataIndexFirst'));
                obj.attributes{3} = str2double(xmlNode.getAttribute('dataIndexSecond'));
                
                obj.Xdata = [-obj.boxSize/2 -obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2];
                obj.Ydata = [-obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2 -obj.boxSize/2];
            elseif (strcmp(obj.nodeName,'BTSetC'))
                obj.attributes{1} = char(xmlNode.getAttribute('setType'));
                obj.attributes{2} = str2double(xmlNode.getAttribute('dataIndex'));
                obj.attributes{3} = str2double(xmlNode.getAttribute('value'));
                
                obj.Xdata = [-obj.boxSize/2 -obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2];
                obj.Ydata = [-obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2 -obj.boxSize/2];
            elseif (strcmp(obj.nodeName,'BTSetP'))
                obj.attributes{1} = char(xmlNode.getAttribute('setType'));
                obj.attributes{2} = str2double(xmlNode.getAttribute('dataIndex'));
                obj.attributes{3} = str2double(xmlNode.getAttribute('value'));
                
                obj.Xdata = [-obj.boxSize/2 -obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2];
                obj.Ydata = [-obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2 -obj.boxSize/2];
            elseif (strcmp(obj.nodeName,'BTStop'))                
                obj.Xdata = [-obj.boxSize/2 -obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2];
                obj.Ydata = [-obj.boxSize/2 obj.boxSize/2 obj.boxSize/2 -obj.boxSize/2 -obj.boxSize/2];
            end
        end
        function childCount = getChildCount(obj)
            childCount = 1;
            for i=1:numel(obj.children)
                childCount = childCount + obj.children(i).getChildCount();
            end
        end
        function nodeDepth = getNodeDepth(obj)
            p = obj.parent;
            nodeDepth = 1;
            while (~isempty(p))
                nodeDepth = nodeDepth + 1;
                p = p.parent;
            end
        end
        function nodeCount = getNumberOfNodesAtDepth(obj, depth, currentNodeCount)
            if(nargin < 3)
                currentNodeCount = 0;
            end
            
            nodeCount = currentNodeCount;
            if (obj.getNodeDepth() == depth)
                nodeCount = nodeCount+1;
            end
            
            for i=1:numel(obj.children)
                nodeCount = obj.children(i).getNumberOfNodesAtDepth(depth,nodeCount);
            end
        end
        
        function depth = getTreeDepth(obj, currentDepth)
            if (nargin < 2)
                currentDepth = 1;
            end
            
            depth = currentDepth;
            passOnDepth = currentDepth+1;
            for i=1:numel(obj.children)
                cDepth = obj.children(i).getTreeDepth(passOnDepth);
                if (cDepth > depth)
                    depth = cDepth;
                end
            end
        end
        function [maxWidth] = getTreeWidth(obj)
            [maxWidth,~] = obj.getDrawGridSize();
        end
        function [gridX, gridY] = getDrawGridSize(obj)
            gridY = obj.getTreeDepth();
            gridX = 1;
            
            myDepth = obj.getNodeDepth();
            for i=1:gridY
                nodeCount = obj.getNumberOfNodesAtDepth(i+myDepth);
                if(nodeCount > gridX)
                    gridX = nodeCount;
                end
            end
        end
        
        function treeSize = getTreeSizeX(obj)
            treeSize = 0;
            spacing = obj.spacingFactorX*obj.boxSize;
            
            for i=1:numel(obj.children)
                treeSize = treeSize + obj.children(i).getTreeSizeX();
            end
            if (treeSize < spacing)
                treeSize = spacing;
            end
        end
        function [] = drawSelf(obj, ax, xPos, yPos, fontSize)
            hold(ax,'on');
            
            textUnitType = 'data';
            faceAlpha = 0.5;
            if (strcmp(obj.nodeName,'BTSequence'))
                obj.drawnHandle = fill(obj.Xdata+xPos,obj.Ydata+yPos,[0.9 0.7 0.9],'FaceAlpha',faceAlpha);
                textObject = text(xPos,yPos,'\rightarrow','FontSize',fontSize*2);
                textObject.HorizontalAlignment = 'center';
                textObject.Units = textUnitType;
                
            elseif (strcmp(obj.nodeName,'BTSelect'))
                obj.drawnHandle = fill(obj.Xdata+xPos,obj.Ydata+yPos,[0.53 0.90 0.90],'FaceAlpha',faceAlpha);
                textObject = text(xPos,yPos,'?','FontSize',fontSize*2);
                textObject.HorizontalAlignment = 'center';
                textObject.Units = textUnitType;
                
            elseif (strcmp(obj.nodeName,'BTConditionC'))
                obj.drawnHandle = fill(obj.Xdata+xPos,obj.Ydata+yPos,[0.44 0.89 0],'FaceAlpha',faceAlpha);
 
                if (obj.attributes{2} == 0)
                    ioName  = 'd';
                    val     = (obj.attributes{3}+1)*2.5;
                    
                elseif (obj.attributes{2} == 1)
                    ioName  = 'V';
                    val     = obj.attributes{3}*0.5;
                    
                elseif (obj.attributes{2} == 2)
                    ioName  = '$\dot{\Psi}$';
                    val     = obj.attributes{3}*30;
                    
                elseif (obj.attributes{2} == 3)
                    ioName  = '$\Psi_{A}$';
                    val     = obj.attributes{3}*120;
                    
                elseif (obj.attributes{2} == 4)
                    ioName = 'AE';
                    val     = obj.attributes{3};
                elseif (obj.attributes{2} == 10)
                    ioName = '$\dot{d}$';
                    val     = obj.attributes{3};
                else
                    return;
%                     ioName = 'ERR';
%                     val = obj.attributes{3};
                end
                
                if (strcmp(obj.attributes{1},'moreThan'))
                    str = sprintf('%s $>$ %.2f',ioName, val);
                else
                    str = sprintf('%s $<$ %.2f',ioName, val);                    
                end
                
                textObject = text(xPos,yPos,str,'FontSize',fontSize, 'interpreter','latex');
                textObject.HorizontalAlignment = 'center';
                textObject.Units = textUnitType;
                
            elseif (strcmp(obj.nodeName,'BTConditionP'))
                obj.drawnHandle = fill(obj.Xdata+xPos,obj.Ydata+yPos,[0.38 0.50 0.12],'FaceAlpha',faceAlpha);
                if (strcmp(obj.attributes{1},'lessThan'))
                    str = sprintf('?\n[%d] < [%d]',obj.attributes{2}, obj.attributes{3});
                else
                    str = sprintf('?\n[%d] > [%d]',obj.attributes{2}, obj.attributes{3});
                end
                textObject = text(xPos,yPos,str,'FontSize',fontSize);
                textObject.HorizontalAlignment = 'center';
                textObject.Units = textUnitType;
                
            elseif (strcmp(obj.nodeName,'BTSetC'))
                obj.drawnHandle = fill(obj.Xdata+xPos,obj.Ydata+yPos,[0.90 0.23 0.23],'FaceAlpha',faceAlpha);

                if (obj.attributes{2} == 0)
                    ioName = 'V';
                    val = obj.attributes{3}*0.5;
                    
                elseif (obj.attributes{2} == 1)
                    ioName = '$\dot{\Psi}$';
                    val = obj.attributes{3}*30;
                    
                elseif (obj.attributes{2} == 2)
                    ioName = '$\Psi_{A}$';
                    val = obj.attributes{3}*180;
                    
                elseif (obj.attributes{2} == 3)
                    ioName = 'AE';
                    val = obj.attributes{3};
                else
                    return;
%                     ioName = 'ERR';
%                     val = obj.attributes{3};
                end
                str = sprintf('%s = %.2f',ioName, val);
                
                textObject = text(xPos,yPos,str,'FontSize',fontSize, 'interpreter','latex');
                textObject.HorizontalAlignment = 'center';
                textObject.Units = textUnitType;
                
            elseif (strcmp(obj.nodeName,'BTSetP'))
                obj.drawnHandle = fill(obj.Xdata+xPos,obj.Ydata+yPos,[0.90 0.23 0.23],'FaceAlpha',faceAlpha);
                
                if (strcmp(obj.attributes{1},'absolute'))
                    str = sprintf('!P\n[%d] = %d',obj.attributes{2}, obj.attributes{3});
                else
                    str = sprintf('!P\n[%d] += %d',obj.attributes{2}, obj.attributes{3});
                end
                textObject = text(xPos,yPos,str,'FontSize',fontSize);
                textObject.HorizontalAlignment = 'center';
                textObject.Units = textUnitType;
            elseif (strcmp(obj.nodeName,'BTStop'))
                obj.drawnHandle = fill(obj.Xdata+xPos,obj.Ydata+yPos,[1 0.13 0.13],'FaceAlpha',faceAlpha);
                
                str = sprintf('STOP');
                
                textObject = text(xPos,yPos,str,'FontSize',fontSize);
                textObject.HorizontalAlignment = 'center';
                textObject.Units = textUnitType;
            end
        end
        function [ownX, treeSizeX] = draw(obj, ax, xPos, yPos, fontSize)
            
            [ownX, treeSizeX] = drawTree(obj, ax, xPos, yPos, fontSize);
        end
        function [ownX, treeSizeX] = drawTree(obj, ax, xPos, yPos, fontSize)
            
            spacingY    = obj.spacingFactorY*obj.boxSize;
            if (nargin < 5)                
                fontSize = 10;
            end
            if (nargin < 4)
                yPos = 0;
            end
            if (nargin < 3)
                xPos = 0;
            end
            
            treeSizeX   = obj.getTreeSizeX();
            treeSizeY   = obj.getTreeDepth()*spacingY;
            
            if (nargin < 2)
                ax = axes();
                xlim([0 treeSizeX]);
                ylim([-treeSizeY 0]);
                fill(...
                    [...
                        0 0 treeSizeX treeSizeX...
                    ],...
                    [...
                    spacingY/2 -treeSizeY -treeSizeY spacingY/2 ...
                    ],[1 1 1],'LineStyle','none');
                
            end
            ax.Visible = 'off';            
            ax.Color = 'none';
            ax.YTick = [];
            ax.XTick = [];
            ax.XColor = 'none';
            ax.YColor = 'none';

            ownX = treeSizeX/2 + xPos;
            obj.drawSelf(ax, ownX, yPos, fontSize);
            
            childPosX = xPos;
            for i=1:numel(obj.children)
                [childX, childOffset] = obj.children(i).drawTree(ax,childPosX,yPos - spacingY, fontSize);
                plot([ownX childX],[yPos-obj.boxSize/2,yPos - spacingY+obj.boxSize/2],'k');
                
                childPosX = childPosX + childOffset;
            end
        end
        function [workspace, status] = tick(obj,workspace)
            if (strcmp(obj.nodeName,'BTSequence'))
                for i=1:numel(obj.children)
                    cState = obj.children(i).tick(workspace);
                    if (cState ~= obj.Success)
                        status = cState;
                        return;
                    end
                end
                status = obj.Success;
                return;
                
            elseif (strcmp(obj.nodeName,'BTSelect'))
                for i=1:numel(obj.children)
                    cState = obj.children(i).tick(workspace);
                    if (cState ~= obj.Failure)
                        status = cState;
                        return;
                    end
                end
                status = obj.Failure;
                return;
                
            elseif (strcmp(obj.nodeName,'BTConditionC'))               
                if (strcmp(obj.attributes{1},'lessThan'))
                    if (workspace(obj.attributes{2}) < obj.attributes{3})
                        status = obj.Success;
                        return
                    end
                else
                    if (workspace(obj.attributes{2}) >= obj.attributes{3})
                        status = obj.Success;
                        return
                    end
                end
                status = obj.Failure;
                return;
                
            elseif (strcmp(obj.nodeName,'BTConditionP'))
                if (strcmp(obj.attributes{1},'lessThan'))
                    if (workspace(obj.attributes{2}) < workspace(obj.attributes{3}))
                        status = obj.Success;
                        return
                    end
                else
                    if (workspace(obj.attributes{2}) >= workspace(obj.attributes{3}))
                        status = obj.Success;
                        return
                    end
                end
                status = obj.Failure;
                return;
                
            elseif (strcmp(obj.nodeName,'BTSetC'))
%                 obj.attributes{1} = char(xmlNode.getAttribute('setType'));
%                 obj.attributes{2} = str2double(xmlNode.getAttribute('dataIndex'));
%                 obj.attributes{3} = str2double(xmlNode.getAttribute('value'));

                workspace(obj.attributes{2}) = obj.attributes{3};
            end
        end
    end
end

