xmlDataFile = '../Build/Debug/BT_iROS.xml';

xmlDataFile = '../Build/Debug/worksWithoutVelFilter/Genomes/BTGenome_4_mod.xml';
xmlDataFile = '../Build/Debug/res_05_06_2015/Genomes/Genome_7_mod.xml';
xmlDataFile = '../Build/Debug/res_05_06_2015/Genomes/Genome_19.xml';
% xmlDataFile = '../Build/Debug/BT_ideal.xml';3

figure(101)
ax1 = axes();
cla(ax1);
% xmlDataFile = '../tmp/BT1.xml';
xmlDOM = xmlread(xmlDataFile);

btRoot = xmlDOM.getFirstChild();
btRoot = btRoot.getFirstChild();
btRoot = btRoot.getNextSibling();
btRoot = btRoot.getNextSibling();
treeRoot = btRoot.getNextSibling();

while (strcmp(treeRoot.getNodeName,'Tree') == 0)
    treeRoot = treeRoot.getNextSibling();
end

nodeRoot = treeRoot.getFirstChild();
while (strcmp(nodeRoot.getNodeName(),'BTSelect') == 0)
    nodeRoot = nodeRoot.getNextSibling();
end

BTree = TreeNode(nodeRoot);
BTree.drawTree(ax1);

%%
figure(102)
ax2 = axes();
xmlDataFile = '../tmp/BT2.xml';
xmlDOM = xmlread(xmlDataFile);

btRoot = xmlDOM.getFirstChild();
btRoot = btRoot.getFirstChild();
btRoot = btRoot.getNextSibling();
btRoot = btRoot.getNextSibling();
treeRoot = btRoot.getNextSibling();

while (strcmp(treeRoot.getNodeName,'Tree') == 0)
    treeRoot = treeRoot.getNextSibling();
end

nodeRoot = treeRoot.getFirstChild();
while (strcmp(nodeRoot.getNodeName(),'BTSelect') == 0)
    nodeRoot = nodeRoot.getNextSibling();
end

BTree = TreeNode(nodeRoot);
BTree.drawTree(ax2);