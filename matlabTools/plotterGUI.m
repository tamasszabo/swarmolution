function varargout = plotterGUI(varargin)
    % PLOTTERGUI MATLAB code for plotterGUI.fig
    %      PLOTTERGUI, by itself, creates a new PLOTTERGUI or raises the existing
    %      singleton*.
    %
    %      H = PLOTTERGUI returns the handle to a new PLOTTERGUI or the handle to
    %      the existing singleton*.
    %
    %      PLOTTERGUI('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in PLOTTERGUI.M with the given input arguments.
    %
    %      PLOTTERGUI('Property','Value',...) creates a new PLOTTERGUI or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before plotterGUI_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to plotterGUI_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLESc

    % Edit the above text to modify the response to help plotterGUI

    % Last Modified by GUIDE v2.5 08-Jun-2015 10:47:58

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @plotterGUI_OpeningFcn, ...
                       'gui_OutputFcn',  @plotterGUI_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:} );
    end
    % End initialization code - DO NOT EDIT
end

% --- Executes just before plotterGUI is made visible.
function plotterGUI_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to plotterGUI (see VARARGIN)

    % Choose default command line output for plotterGUI
    handles.output = hObject;

    handles.selStrings      = {'Load Data!'};
    handles.areaVertices    = [];
    handles.numDrones       = 0;
    
    handles.trialIDs        = [];
    handles.activeIndex     = [];
    
    handles.logData         = [];
    handles.trialData       = [];
    handles.activeData      = [];
    handles.endTime         = 100000;
    
    handles.axLimits        = [];
    
    handles.selData1.String = handles.selStrings;
    handles.selData2.String = handles.selStrings;
    handles.selData3.String = handles.selStrings;
    handles.selData4.String = handles.selStrings;
    handles.selData5.String = handles.selStrings;
    handles.selData6.String = handles.selStrings;
    
%     handles.logType         = 'old';
    handles.logType         = 'new';
    
    % Update handles structure
    guidata(hObject, handles);

    if (~strcmp(handles.logType,'new'))
         handles.chkAvoidance.Enable = 'off';
         handles.chkAvoidance.Value = 0;
    end
    set(handles.mainWindow,           'WindowKeyPressFcn', @mainWindow_KeyPressFcn);
    % UIWAIT makes plotterGUI wait for user response (see UIRESUME)
    % uiwait(handles.mainWindow);
end

% --- Outputs from this function are returned to the command line.
function varargout = plotterGUI_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;
end
function edtDataFile_Callback(hObject, eventdata, handles)
end
function edtDataFile_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function edtEnvFile_Callback(hObject, eventdata, handles)
end
function edtEnvFile_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end


function btnLoad_Callback(hObject, eventdata, handles)
    handles = loadData(handles);
    guidata(hObject,handles);
end

% --- Executes during object creation, after setting all properties.
function selData1_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to selData1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData2_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData3_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData4_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData5_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData6_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData12_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData22_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData32_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData42_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData52_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
function selData62_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
end
% --- Executes on selection change in selData1.
function selData1_Callback(hObject, eventdata, handles)
    % hObject    handle to selData1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: contents = cellstr(get(hObject,'String')) returns selData1 contents as cell array
    %        contents{get(hObject,'Value')} returns selected item from selData1
    plotTimeHist(handles.axData1,handles,handles.selData1.Value,handles.selData12.Value);
end

function selData2_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData2,handles,handles.selData2.Value,handles.selData22.Value);
end
function selData3_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData3,handles,handles.selData3.Value,handles.selData32.Value);
end
function selData4_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData4,handles,handles.selData4.Value,handles.selData42.Value);
end
function selData5_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData5,handles,handles.selData5.Value,handles.selData52.Value);
end
function selData6_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData6,handles,handles.selData6.Value,handles.selData62.Value);
end

function selData12_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData1,handles,handles.selData1.Value,handles.selData12.Value);
end
function selData22_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData2,handles,handles.selData2.Value,handles.selData22.Value);
end
function selData32_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData3,handles,handles.selData3.Value,handles.selData32.Value);
end
function selData42_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData4,handles,handles.selData4.Value,handles.selData42.Value);
end
function selData52_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData5,handles,handles.selData5.Value,handles.selData52.Value);
end
function selData62_Callback(hObject, eventdata, handles)
    plotTimeHist(handles.axData6,handles,handles.selData6.Value,handles.selData62.Value);
end

% --- Executes on button press in btnPlayback.
function btnPlayback_Callback(hObject, eventdata, handles)
    % hObject    handle to btnPlayback (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hint: get(hObject,'Value') returns toggle state of btnPlayback
    
    if (handles.btnPlayback.Value)
        handles.endTime = 3;
    else
        handles.endTime = 10000;
    end
    handles = selData(handles);
    guidata(hObject,handles);
end
% --- Executes on button press in btnMakePlot.
function btnMakePlot_Callback(hObject, eventdata, handles)
    figure();
        ax = subplot(1,2,1);
            plotTrack(ax, handles);
            
        ax = subplot(3,4,3);
            plotTimeHist(ax,handles,handles.selData1.Value,handles.selData12.Value);
        ax = subplot(3,4,4);
            plotTimeHist(ax,handles,handles.selData2.Value,handles.selData22.Value);
        ax = subplot(3,4,7);
            plotTimeHist(ax,handles,handles.selData3.Value,handles.selData32.Value);
        ax = subplot(3,4,8);
            plotTimeHist(ax,handles,handles.selData4.Value,handles.selData42.Value);
        ax = subplot(3,4,11);
            plotTimeHist(ax,handles,handles.selData5.Value,handles.selData52.Value);
        ax = subplot(3,4,12);
            plotTimeHist(ax,handles,handles.selData6.Value,handles.selData62.Value);
end
function btnMakeSeparatePlots_Callback(hObject, eventdata, handles)
    figure();
        ax = axes();
            plotTrack(ax, handles);
            
    figure();
        ax = subplot(3,2,1);
            plotTimeHist(ax,handles,handles.selData1.Value,handles.selData12.Value);
        ax = subplot(3,2,2);
            plotTimeHist(ax,handles,handles.selData2.Value,handles.selData22.Value);
        ax = subplot(3,2,3);
            plotTimeHist(ax,handles,handles.selData3.Value,handles.selData32.Value);
        ax = subplot(3,2,4);
            plotTimeHist(ax,handles,handles.selData4.Value,handles.selData42.Value);
        ax = subplot(3,2,5);
            plotTimeHist(ax,handles,handles.selData5.Value,handles.selData52.Value);
        ax = subplot(3,2,6);
            plotTimeHist(ax,handles,handles.selData6.Value,handles.selData62.Value);
end
% --- Executes on key press with focus on mainWindow and none of its controls.
function mainWindow_KeyPressFcn(hObject, eventdata)
    handles = guidata(hObject);
    if (strcmp(eventdata.Key,'q'))
        close(handles.mainWindow)
        return;
    end
    if (strcmp(eventdata.Key,'v'))
        view(handles.mainAxes,[0 90]);
    end
    
    if (strcmp(eventdata.Key,'p'))
        handles.btnPlayback.Value = ~handles.btnPlayback.Value;
        btnPlayback_Callback(hObject, eventdata, handles);
    end
    
    needUpdate = 0;
    if (handles.btnPlayback.Value) % Playback mode
        endTime =  handles.endTime;
        delta = 0;
        if (strcmp(eventdata.Key,'leftarrow'))
            delta = -1;
        elseif (strcmp(eventdata.Key,'rightarrow'))
            delta = 1;
        elseif (strcmp(eventdata.Key,'uparrow'))
            delta = 10;
        elseif (strcmp(eventdata.Key,'downarrow'))
            delta = -10;
        end
        
        if (delta ~= 0)
            if (isempty(eventdata.Modifier))
                endTime = endTime + delta;
            else
                endTime = endTime + delta/10;
            end
            handles.endTime = min(max(endTime, 1), max(handles.trialData(:,1)));
            
            needUpdate = 1;
        end
         
    else % Trial move mode
        % Select the data of the first trial
        index = handles.activeIndex;
        
        % move the trial index as commanded
        delta = 0;
        
        if (strcmp(eventdata.Key,'leftarrow'))
            delta = -1;
        elseif (strcmp(eventdata.Key,'rightarrow'))
            delta = 1;
        elseif (strcmp(eventdata.Key,'uparrow'))
            delta = 10;
        elseif (strcmp(eventdata.Key,'downarrow'))
            delta = -10;
        end
        
        if (delta ~= 0)
            index = index + delta;

            %Ensure that the index is within valid trials
            while (index > numel(handles.trialIDs))
                index = index - numel(handles.trialIDs);
            end
            while (index < 1)
                index = index + numel(handles.trialIDs);
            end

            % Update data
            handles.activeIndex = index;

            needUpdate = 1;
        end
    end
    
    if (needUpdate)
        handles = selData(handles);
        guidata(hObject,handles);
    end
end
% --- Executes on button press in chkAvoidance.
function chkAvoidance_Callback(hObject, eventdata, handles)
% hObject    handle to chkAvoidance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkAvoidance
    updatePlots(handles);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function handles = loadData(handles)
    handles.endTime             = 100000;
    handles.btnPlayback.Value   = 0;
    
    % Load Environment data
    fID = fopen(handles.edtEnvFile.String);
    if (fID == -1)
        handles.selStrings = {'N/A'};
        handles.txtOut.String = 'Unable to load file';
        return;
    end
    data = fread(fID,'single');
    fclose(fID);
    
    pos     = 1;
    envID   = 1;
    
    while (pos < numel(data))
        
        %Trial (environment) ID
        handles.env(envID).ID = data(pos);
        pos     = pos+1;
        
        % Number of drones
        handles.numDrones = data(pos);
        pos     = pos+1;
        
        % Number of performance metrics
        nPerf   = data(pos);
        pos     = pos+1;
        
        handles.env(envID).performanceMetrics = zeros(nPerf,1);
        
        % The performance metrics
        for i=1:nPerf
            handles.env(envID).performanceMetrics(i)   = data(pos);
            pos     = pos+1;
        end
        
        % Arena verice count
        numV    = data(pos);
        pos     = pos+1;
        
        % The arena verices
        for i=1:numV
            handles.env(envID).X(i) = data(pos);
            pos = pos+1;
            
            handles.env(envID).Y(i) = data(pos);
            pos = pos+1;
        end
        
        envID = envID+1;
    end
    
   
    % Load the position log data
    fID = fopen(handles.edtDataFile.String);
    if (fID == -1)
        handles.selStrings = {'N/A'};
        handles.txtOut.String = 'Unable to load file';
        return;
    end
    data = fread(fID,'single');
    fclose(fID);
    
    % Reshape it & save
    if (strcmp(handles.logType,'new'))
        numCols = 15 + 2*(handles.numDrones-1);     % For new logs that log the raw BT output
    else
        numCols = 12 + 2*(handles.numDrones-1);     % For old logs without direct BT output logging
    end
    numRows = numel(data)/numCols;
    handles.logData = [reshape(data,numCols,numRows)' zeros(numRows,2)];   % Matlab's Column major, but the data is row major
    
    % Get the unique trial IDs
    handles.trialIDs = unique(handles.logData(:,1));
    
    % Calculate the true minimum distances for the drones
    for i=1:numel(handles.trialIDs)
        trialID = handles.trialIDs(i);
        
        trialSel = handles.logData(:,1) == trialID;
        
        
        IDs = unique(handles.logData(:,3));
        for j=1:numel(IDs)
            droneSel = logical((handles.logData(:,3) == IDs(j)).*trialSel);
            
            trueDist = zeros(sum(droneSel),numel(IDs)-1);
            estDist = zeros(sum(droneSel),numel(IDs)-1);
            
            oIndex = 1;
            otherCnt = 1;
            for o=1:numel(IDs)
                if (o == j)
                    continue;
                end
                
                otherSel = logical((handles.logData(:,3) == IDs(o)).*trialSel);
                trueDist(:,oIndex) = sqrt( ...
                    (handles.logData(droneSel,4) - handles.logData(otherSel,4)).^2 + ...
                    (handles.logData(droneSel,5) - handles.logData(otherSel,5)).^2 + ...
                    (handles.logData(droneSel,6) - handles.logData(otherSel,6)).^2 ...
                    );
                
                
                estDist(:,oIndex) = handles.logData(droneSel, 8 + (numel(IDs)-1) + otherCnt);
                oIndex = oIndex+1;
                otherCnt = otherCnt+1;
            end
            if (numel(IDs) == 1)
                handles.logData(droneSel,end-1)= 0;
                handles.logData(droneSel,end  )= 0;
            else
                handles.logData(droneSel,end-1)= min(trueDist,[],2);
                handles.logData(droneSel,end  )= min(estDist,[],2);
            end
        end
    end
    
    
    % Create the popup menu strings
    handles.selStrings      = cell(numCols - 3,1);
    pos = 1;
    handles.selStrings{pos}   = 'Position X [m]';           % Pos 4
    handles.axLimits(pos,:)   = [-4 4];
    pos = pos+1;
    handles.selStrings{pos}   = 'Position Y [m]';           % Pos 5
    handles.axLimits(pos,:)   = [-4 4];
    pos = pos+1;
    handles.selStrings{pos}   = 'Position Z [m]';           % Pos 6
    handles.axLimits(pos,:)   = [0 2];
    pos = pos+1;
    handles.selStrings{pos}   = 'Velocity Heading [rad]';   % Pos 7
    handles.axLimits(pos,:)   = [-pi pi];
    pos = pos+1;
    
    handles.selStrings{pos}   = 'True Velocity [m/s]';      % Pos 8
    handles.axLimits(pos,:)   = [0 1];
    pos = pos+1;
    
    for i=1:(handles.numDrones-1)                           % Pos 9 -> 9+(numDrones-1)
        handles.selStrings{pos}   = ['RSSI ' num2str(i) '[dB]'];
        handles.axLimits(pos,:)   = [-80 -50];
        pos = pos+1;
    end
    
    for i=1:(handles.numDrones-1)
        handles.selStrings{pos}   = ['Distance Estimate ' num2str(i) ' [m]'];
        handles.axLimits(pos,:)   = [0 5];
        pos = pos+1;
    end
    handles.selStrings{pos}   = 'Turn Rate [rad/s]';
    handles.axLimits(pos,:)   = [-40*pi/180 40*pi/180];
    pos = pos+1;
    handles.selStrings{pos}   = 'Velocity Cmd [m/s]';
    handles.axLimits(pos,:)   = [-1 1];
    pos = pos+1;
    handles.selStrings{pos}   = 'Heading Cmd [rad]';
    handles.axLimits(pos,:)   = [-pi pi];
    pos = pos+1;
    handles.selStrings{pos}   = 'Altitude Cmd [m]';
    handles.axLimits(pos,:)   = [0 2];
    pos = pos+1;
    
% The raw BT output is not available in the older logs
    if (strcmp(handles.logType,'new'))
        handles.selStrings{pos}   = 'BTOut 0 [-]';
        handles.axLimits(pos,:)   = [-1 1];
        pos = pos+1;
        handles.selStrings{pos}   = 'BTOut 1 [-]';
        handles.axLimits(pos,:)   = [-1 1];
        pos = pos+1;
        handles.selStrings{pos}   = 'BTOut 2 [-]';
        handles.axLimits(pos,:)   = [-1 1];
        pos = pos+1;
    end
    handles.selStrings{pos}   = 'Smallest Distance [m]';
    handles.axLimits(pos,:)   = [0 5];
    pos = pos+1;
    handles.selStrings{pos}   = 'Min Estimate [m]';
    handles.axLimits(pos,:)   = [0 5];
    pos = pos+1;
    handles.selStrings{pos}   = 'None';
    handles.axLimits(pos,:)   = [0 0];
    
    % Update the toolbox strings
    handles.selData1.String = handles.selStrings;
    handles.selData2.String = handles.selStrings;
    handles.selData3.String = handles.selStrings;
    handles.selData4.String = handles.selStrings;
    handles.selData5.String = handles.selStrings;
    handles.selData6.String = handles.selStrings;
    handles.selData12.String = handles.selStrings;
    handles.selData22.String = handles.selStrings;
    handles.selData32.String = handles.selStrings;
    handles.selData42.String = handles.selStrings;
    handles.selData52.String = handles.selStrings;
    handles.selData62.String = handles.selStrings;
    
    handles.selData1.Value = find(~cellfun('isempty',strfind(handles.selStrings,'True Velocity')));
    handles.selData12.Value = numel(handles.selStrings);
    
    handles.selData4.Value = find(~cellfun('isempty',strfind(handles.selStrings,'Velocity Heading')));
    handles.selData42.Value = numel(handles.selStrings);
    
    handles.selData2.Value = find(~cellfun('isempty',strfind(handles.selStrings,'Velocity Cmd')));
    handles.selData22.Value = numel(handles.selStrings);
    
    handles.selData5.Value = find(~cellfun('isempty',strfind(handles.selStrings,'Heading Cmd')));
    handles.selData52.Value = numel(handles.selStrings);
    
    handles.selData3.Value = find(~cellfun('isempty',strfind(handles.selStrings,'Min Estimate')));
    handles.selData32.Value = find(~cellfun('isempty',strfind(handles.selStrings,'Smallest Distance')));
    
    handles.selData6.Value = find(~cellfun('isempty',strfind(handles.selStrings,'Turn Rate')));
    handles.selData62.Value = numel(handles.selStrings);
    
    
    % Select the data of the first trial
    handles.activeIndex = 1;
    handles = selData(handles);
end

function plotTrack(axHandle, handles)
    env = [];
    for i=1:numel(handles.env)
        if (handles.env(i).ID == handles.trialIDs(handles.activeIndex))
            env = handles.env(i);
            break;
        end
    end
    limsX = [min(env.X) max(env.X)]*1.1;
    limsY = [min(env.Y) max(env.Y)]*1.1;
    
    hold(axHandle,'off');
    plot3(axHandle, env.X, env.Y,0*env.X, 'k', 'LineWidth',2); hold(axHandle,'on');
    xlim(axHandle,limsX);
    ylim(axHandle,limsY);
    view(axHandle,[0 90]);
    xlabel(axHandle,'X [m]');
    ylabel(axHandle,'Y [m]');
    
    IDs = unique(handles.activeData(:,2));
    C   = hsv(numel(IDs));
    
    for i=1:numel(IDs)
        S = handles.activeData(:,2) == IDs(i);
        pStart = find(S==1,1,'first');
        pEnd   = find(S==1,1,'last');
        
        plot3(axHandle, handles.activeData(S,3), handles.activeData(S,4), handles.activeData(S,5), 'color', C(i,:));
        if (handles.chkAvoidance.Value)
            Savoid = logical(S & ((handles.activeData(:,end-2) ~= 0) | (handles.activeData(:,end-3) ~= 0) | (handles.activeData(:,end-4) ~= 1)) );
            scatter3(axHandle, handles.activeData(Savoid,3), handles.activeData(Savoid,4), handles.activeData(Savoid,5), 20*ones(sum(Savoid),1), repmat(C(i,:),sum(Savoid),1),'o','filled');
        end        
        
        scatter3(axHandle,handles.activeData(pStart,3),handles.activeData(pStart,4),handles.activeData(pStart,5),50,C(i,:),'x');
        scatter3(axHandle,handles.activeData(pEnd,3),handles.activeData(pEnd,4),handles.activeData(pEnd,5),500,C(i,:),'o','filled');
    end
end
function plotTimeHist(axHandle,handles,index1,index2)    
    IDs = unique(handles.activeData(:,2));
    C   = hsv(numel(IDs));
    
    hold(axHandle,'off');
    index1 = index1+2;
    index2 = index2+2;
    for i=1:numel(IDs)
        S = handles.activeData(:,2) == IDs(i);
        plot(axHandle, handles.activeData(S,1), handles.activeData(S,index1),'color',C(i,:));
        hold(axHandle,'on');
        if(index2 ~= (numel(handles.selStrings)+2))
            plot(axHandle, handles.activeData(S,1), handles.activeData(S,index2),'--','color',C(i,:));
        end
    end
    xlabel(axHandle,'Time [s]');
    xlim(axHandle,[min(handles.activeData(S,1)) max(handles.activeData(S,1))]);
    
    if(index2 ~= (numel(handles.selStrings)+2))
        ylabel(axHandle,[handles.selStrings{index1-2} '/' handles.selStrings{index2-2}]);
        yL = [min(handles.axLimits(index1-2,1), handles.axLimits(index2-2,1)) max(handles.axLimits(index1-2,2), handles.axLimits(index2-2,2))];
        ylim(axHandle,yL*1.1);
    else
        ylabel(axHandle,handles.selStrings{index1-2});
        ylim(axHandle,handles.axLimits(index1-2,:)*1.1);
    end
end
function updatePlots(handles)
    plotTrack(handles.axMain, handles);

    plotTimeHist(handles.axData1,handles,handles.selData1.Value,handles.selData12.Value);
    plotTimeHist(handles.axData2,handles,handles.selData2.Value,handles.selData22.Value);
    plotTimeHist(handles.axData3,handles,handles.selData3.Value,handles.selData32.Value);
    plotTimeHist(handles.axData4,handles,handles.selData4.Value,handles.selData42.Value);
    plotTimeHist(handles.axData5,handles,handles.selData5.Value,handles.selData52.Value);
    plotTimeHist(handles.axData6,handles,handles.selData6.Value,handles.selData62.Value);
       
    env = [];
    for i=1:numel(handles.env)
        if (handles.env(i).ID == handles.trialIDs(handles.activeIndex))
            env = handles.env(i);
            break;
        end
    end
    
    text = sprintf('Trial %d/%d \t\t\t SimTime: %.2f/300 s\n',handles.trialIDs(handles.activeIndex) , numel(handles.trialIDs),max(handles.activeData(handles.activeData(:,2) == 1,1)));
    for i=1:numel(env.performanceMetrics)
        text = sprintf('%s   obj%d: %.2f', text, i, env.performanceMetrics(i));
    end
    handles.txtOut.String = text;
    
%     handles.txtOut.String = ['Trial ' num2str(handles.activeIndex) '/' num2str(numel(handles.trialIDs)) '; SimTime: ' num2str(max(handles.activeData(handles.activeData(:,2) == 1,1))) '/300 s'];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function handles = selData(handles)
    S = handles.logData(:,1) == handles.trialIDs(handles.activeIndex);
    handles.trialData        = handles.logData(S,2:end);
    
    S = handles.trialData(:,1) < handles.endTime;
    handles.activeData       = handles.trialData(S,1:end);
    
    updatePlots(handles);
end
