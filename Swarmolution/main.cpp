//
//  main.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include <iostream>

#include "supportFunctions.h"

#include "EA/Core/NSGAII.h"
#include "EA/NSGA-II/TreeGenomes/BTGenome/NSGAIIBTGenome.h"
#include "AI/Models/BehaviorTrees/BehaviorTree.h"

#include "Simulator/Simulators/DroneArena/DroneArena.h"



int main(int argc, const char * argv[])
{
//    genBTCode("BT_iROS.xml", "BTStructure_manual");
//    genBTCode("worksWithoutVelFilter/Genomes/BTGenome_4_mod.xml", "BTStructure_evolved");
//    genBTCode("res_05_06_2015/Genomes/Genome_7_mod.xml", "BTAlternative");
//    return 0;
    
    testFcn();
    return 0;
////    evalBT("Genomes/Genome_6.xml", "evData.log", "evEnv.log", 100);
////    return 0;
//    
//    std::vector<const char*> fNames;
//    fNames.push_back("BT_empty.xml");
//    fNames.push_back("BT_iROS.xml");
//    fNames.push_back("worksWithoutVelFilter/Genomes/BTGenome_4.xml");
//    fNames.push_back("res_05_06_2015/Genomes/Genome_7.xml");
//    
//    compareBTs(fNames, "statsData_1000.m", 1000);
//    return 0;
    
    RandomEngine rand;
    WorkEngine   worker;
    worker.start(8);
    
    Logger statsLogger("evolutionStats.log");
    
    NSGAII* ea = new NSGAII(&rand, &worker, &statsLogger);
    
    if (!ea->loadResults("evolutionResults.xml", "evolutionStats.log"))
    {
        S_LOG("Results could not be loaded. Creating new EA!\n");
        delete ea;
        ea = new NSGAII(&rand, &worker, NSGAII::Objective::Maximize, 0.1, 0.001, 0.75, 10, 5, &statsLogger);
        ea->init<NSGAIIBTGenome>(100);
    }
    else
        S_LOG("Previous results loaded.\n");
    
    ea->evolve(150);
    ea->saveResults("evolutionResults.xml");
    statsLogger.dump();
    
    
    std::deque<NSGAIIGenome*> pareto = ea->getParetoGenomes(10);
    sort(pareto.begin(), pareto.end(), NSGAIIGenome::crowdedComparison);
    S_LOG("Pareto font size: %d\n", (int)pareto.size());
    
    
    Logger envLogger("envLog.log");
    Logger dataLogger("dataLog.log");

    unsigned int memCnt = 0;
    for (unsigned int i = 0; i < 1; ++i)
    {
        for (std::deque<NSGAIIGenome*>::iterator g = pareto.begin(); g != pareto.end(); ++g)
        {
            S_LOG("Trial %d ...\n",memCnt+1);
            
            BehaviorTree *BT = dynamic_cast<BehaviorTree*>(*g);
            
            std::string fName("Genomes/Genome_");
            fName.append(std::to_string(memCnt));
            fName.append(".xml");
            
            BT->save(fName.c_str());
            
          //DroneArena(randomSeed, ai, dataLogger,trialID, numDrones, shape = Shape::fixed, vertexCount = 0, wallLength = 6);
            DroneArena sim(i, BT, &dataLogger, memCnt++, 3);
            
            sim.runSim(0.01, 300);
            sim.logEnvironmentShape(&envLogger);
        }
    }
    
    envLogger.dump();
    dataLogger.dump();
    
    return 0;
}
