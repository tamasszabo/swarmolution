//
//  NSGAIIGenome.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAIIGenome.h"
#include "../NSGA-II/TreeGenomes/BTGenome/NSGAIIBTGenome.h"

////////////////////////////////////////////////////////////////////////
// Constgructors & Destructors
////////////////////////////////////////////////////////////////////////
NSGAIIGenome::NSGAIIGenome(std::string genomeName, unsigned int generationID):
m_genomeName(genomeName),
m_generationID(generationID),
m_numEvaluations(0)
{
}

NSGAIIGenome::NSGAIIGenome(const NSGAIIGenome &other):
m_genomeName(other.m_genomeName),
m_nDomination(other.m_nDomination),
m_dominationSet(other.m_dominationSet),
m_frontIndex(other.m_frontIndex),
m_crowdingDistance(other.m_crowdingDistance),
m_generationID(other.m_generationID),
m_numEvaluations(other.m_numEvaluations),
m_objectiveSums(other.m_objectiveSums)
{}
NSGAIIGenome::~NSGAIIGenome()
{}

void            NSGAIIGenome::setGenerationIndex(unsigned int generationID) { m_generationID = generationID; }

////////////////////////////////////////////////////////////////////////
// Public wrapers for private virtual members
////////////////////////////////////////////////////////////////////////
void             NSGAIIGenome::mutate(RandomEngine *rand, float pMutate, float pMacro)
{
    m_genomeLock.lock();
    mutateGenome(rand, pMutate, pMacro);
    m_genomeLock.unlock();
}
void            NSGAIIGenome::crossover(RandomEngine *rand, float pCross, NSGAIIGenome *other, NSGAIIGenome *&child1, NSGAIIGenome *&child2)
{
    m_genomeLock.lock();
    crossoverGenome(rand, pCross, other, child1, child2, m_generationID);
    m_genomeLock.unlock();
}
void            NSGAIIGenome::evaluate(std::seed_seq::result_type randomSeed)
{
    m_genomeLock.lock();
    evaluateGenome(randomSeed);
    m_genomeLock.unlock();
}
void            NSGAIIGenome::reset()
{
    m_numEvaluations = 0;
    for (std::vector<float>::iterator it = m_objectiveSums.begin(); it != m_objectiveSums.end(); ++it)
        *it = 0;
}
////////////////////////////////////////////////////////////////////////
// Objective Function Accessors
////////////////////////////////////////////////////////////////////////
unsigned int    NSGAIIGenome::getNumberOfObjectives() { return (unsigned int)m_objectiveSums.size(); }
float           NSGAIIGenome::getObjectiveValue(unsigned int objIndex) { return m_objectiveSums[objIndex]/m_numEvaluations; }
std::vector<float> NSGAIIGenome::getObjectiveSums() { return m_objectiveSums; }
unsigned int    NSGAIIGenome::getNumEvaluations() { return m_numEvaluations; }

////////////////////////////////////////////////////////////////////////
// Domination Operators
////////////////////////////////////////////////////////////////////////
void            NSGAIIGenome::setFrontIndex(unsigned int frontIndex) { m_frontIndex = frontIndex; }
unsigned int    NSGAIIGenome::getFrontIndex() { return m_frontIndex; }
unsigned int    NSGAIIGenome::getDC() { return m_nDomination; }
void            NSGAIIGenome::cleanDC() { m_nDomination = 0; m_dominationSet.clear(); }

void            NSGAIIGenome::incDC() { m_nDomination++; }
void            NSGAIIGenome::decDC() { m_nDomination--; }

void            NSGAIIGenome::addDominated(NSGAIIGenome* dominated) { m_dominationSet.push_back(dominated); }
void            NSGAIIGenome::decDominated()
{
    for (std::deque<NSGAIIGenome*>::iterator it = m_dominationSet.begin(); it != m_dominationSet.end(); ++it)
        (*it)->decDC();
}

////////////////////////////////////////////////////////////////////////
// Crowding Distance Operators
////////////////////////////////////////////////////////////////////////
void            NSGAIIGenome::setCrowdingDistance(float crowdingDistance) { m_crowdingDistance = crowdingDistance; }
float           NSGAIIGenome::getCrowdingDistance() { return m_crowdingDistance; }
void            NSGAIIGenome::incCrowdingDistance(float increment) { m_crowdingDistance += increment; }

////////////////////////////////////////////////////////////////////////
// Static Comparison Operators
////////////////////////////////////////////////////////////////////////
bool NSGAIIGenome::dominates(NSGAIIGenome *a, NSGAIIGenome *b, bool largerBetter)
{
    if (largerBetter)
    {
        bool hasBetter = false;
        for (unsigned int i=0; i < a->m_objectiveSums.size(); ++i)
        {
            if ( (a->m_objectiveSums[i]/a->m_numEvaluations) < (b->m_objectiveSums[i]/b->m_numEvaluations) )
                return false;
            
            if ( (a->m_objectiveSums[i]/a->m_numEvaluations) > (b->m_objectiveSums[i]/b->m_numEvaluations) )
                hasBetter = true;
        }
        
        return hasBetter;
    }
    
    bool hasBetter = false;
    for (unsigned int i=0; i < a->m_objectiveSums.size(); ++i)
    {
        if ( (a->m_objectiveSums[i]/a->m_numEvaluations) > (b->m_objectiveSums[i]/b->m_numEvaluations) )
            return false;
        
        if ( (a->m_objectiveSums[i]/a->m_numEvaluations) < (b->m_objectiveSums[i]/b->m_numEvaluations) )
            hasBetter = true;
    }
    
    return hasBetter;
}
bool NSGAIIGenome::crowdedComparison(NSGAIIGenome *a, NSGAIIGenome *b)
{
    if(a->m_frontIndex == b->m_frontIndex)
        return (a->m_crowdingDistance > b->m_crowdingDistance);
    
    return (a->m_frontIndex < b->m_frontIndex);
}
bool NSGAIIGenome::compareObjective(NSGAIIGenome *a, NSGAIIGenome *b, unsigned int objectiveIndex, bool largerBetter)
{
    if (largerBetter)
    {
        return ((a->m_objectiveSums[objectiveIndex]/a->m_numEvaluations) >
                (b->m_objectiveSums[objectiveIndex]/b->m_numEvaluations));
    }
    
    return ((a->m_objectiveSums[objectiveIndex]/a->m_numEvaluations) <
            (b->m_objectiveSums[objectiveIndex]/b->m_numEvaluations));
}
void    NSGAIIGenome::saveGenome(pugi::xml_node &root)
{
    pugi::xml_node genomeRoot = root.append_child("Genome");
    genomeRoot.append_attribute("name") = m_genomeName.c_str();
    genomeRoot.append_attribute("origin") = m_generationID;
    genomeRoot.append_attribute("nEval") = m_numEvaluations;
    for (int i=0; i < m_objectiveSums.size(); ++i)
    {
        std::string attrName("obj");
        attrName.append(std::to_string(i));
        genomeRoot.append_attribute(attrName.c_str()) = m_objectiveSums[i];
    }
    
    saveGenomeAttributes(genomeRoot);
}
NSGAIIGenome* NSGAIIGenome::loadGenome(pugi::xml_node root)
{
    if (strcmp(root.name(),"Genome"))
        return NULL;
 
    NSGAIIGenome *genome = NULL;
    if (!strcmp(root.attribute("name").as_string(), "BT"))
        genome = new NSGAIIBTGenome(root.attribute("origin").as_uint());
    
    
    if (genome != NULL)
    {
        genome->m_numEvaluations = root.attribute("nEval").as_uint(1); // Defaults to 1
        
        float obj;
        do
        {
            std::string objAttrName("obj");
            objAttrName.append(std::to_string(genome->m_objectiveSums.size()));

            obj = root.attribute(objAttrName.c_str()).as_float(NAN);
            if (!isnan(obj))
                genome->m_objectiveSums.push_back(obj);
            
        }
        while (!isnan(obj));
        pugi::xml_node data = root.first_child();
        if (!genome->loadGenomeAttributes(data))
        {
            delete genome;
            genome = NULL;
        }
    }    
    return genome;
}