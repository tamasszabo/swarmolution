//
//  NSGAIIGenome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__NSGAIIGenome__
#define __Swarmolution__NSGAIIGenome__

#include <stdio.h>
#include <string>

#include "../../Tools/MultiCoreWorkEngine/WorkEngine.h"
#include "../../Tools/RandomEngine/RandomEngine.h"
#include "../../Tools/Libraries/pugixml-1.4/src/pugixml.hpp"

class NSGAIIGenome
{
    
protected:
    std::string                     m_genomeName;
    
    std::vector<float>              m_objectiveSums;
    unsigned int                    m_numEvaluations;
    unsigned int                    m_generationID;
    
    std::mutex                      m_genomeLock;
    
    unsigned int                    m_nDomination;
    std::deque<NSGAIIGenome*>       m_dominationSet;
    unsigned int                    m_frontIndex;
    
    float                           m_crowdingDistance;
    
    
public:
    NSGAIIGenome(std::string genomeName, unsigned int generationID);
    NSGAIIGenome(const NSGAIIGenome &other);
    virtual ~NSGAIIGenome();
    
    void            setGenerationIndex(unsigned int generationID);
    

public: // public wrapers for private virtual members
    void            mutate(RandomEngine *rand, float pMutate, float pMacro);
    void            crossover(RandomEngine *rand, float pCross, NSGAIIGenome *other, NSGAIIGenome *&child1, NSGAIIGenome *&child2);
    void            evaluate(std::seed_seq::result_type randomSeed);
    
    virtual void    reset();
    
    void    saveGenome(pugi::xml_node &root);
    
public: // public virtual functions
    virtual void    init(RandomEngine *random)                                                                                                                          = 0;
    virtual NSGAIIGenome* clone()                                                                                                                                       = 0;
    virtual void    saveGenomeAttributes(pugi::xml_node &root)
        = 0;
    virtual bool    loadGenomeAttributes(pugi::xml_node &root)
    = 0;
    
protected: // protected virtual members
    virtual void    mutateGenome(RandomEngine *rand, float pMutate, float pMacro)                                                                                       = 0;
    virtual void    crossoverGenome(RandomEngine *rand, float pCross, NSGAIIGenome *other, NSGAIIGenome *&child1, NSGAIIGenome *&child2, unsigned int generationID)     = 0;
    virtual void    evaluateGenome(std::seed_seq::result_type randomSeed)                                                                                               = 0;
    
public: // Objective function accessors
    unsigned int    getNumberOfObjectives();
    float           getObjectiveValue(unsigned int objIndex);
    std::vector<float> getObjectiveSums();
    unsigned int    getNumEvaluations();
    
    
public: // Domination operators
    void            setFrontIndex(unsigned int frontIndex);
    unsigned int    getFrontIndex();
    unsigned int    getDC();
    void            cleanDC();
    
    void            incDC();
    void            decDC();
    
    void            addDominated(NSGAIIGenome* dominated);
    void            decDominated();
    
public: // Crowding distance operators
    void            setCrowdingDistance(float crowdingDistance);
    float           getCrowdingDistance();
    void            incCrowdingDistance(float increment);
    
public:// Static comparison operators
    static bool     dominates(NSGAIIGenome *a, NSGAIIGenome *b, bool largerBetter);
    static bool     crowdedComparison(NSGAIIGenome *a, NSGAIIGenome *b);
    static bool     compareObjective(NSGAIIGenome *a, NSGAIIGenome *b, unsigned int objectiveIndex, bool largerBetter);
    
    static NSGAIIGenome* loadGenome(pugi::xml_node root);
};

////////////////////////////////////////////////////////////////////////
// Task definition for evaluating the genomes with the WorkEngine
////////////////////////////////////////////////////////////////////////
class EvaluateGenome: public Task
{
private:
    NSGAIIGenome*                               m_genome;
    std::vector<std::seed_seq::result_type>     m_randomSeeds;
public:
    EvaluateGenome(NSGAIIGenome *genome, std::vector<std::seed_seq::result_type> randomSeeds):
    m_genome(genome),
    m_randomSeeds(randomSeeds)
    {
    }
    ~EvaluateGenome()
    {
    }
    void perform()
    {
        for (std::vector<std::seed_seq::result_type>::iterator it = m_randomSeeds.begin(); it != m_randomSeeds.end(); ++it)
            m_genome->evaluate(*it);
    }
};

#endif /* defined(__Swarmolution__NSGAIIGenome__) */
