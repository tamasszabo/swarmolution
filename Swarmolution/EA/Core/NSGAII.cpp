//
//  NSGAII.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAII.h"
#include "../../Tools/Libraries/pugixml-1.4/src/pugixml.hpp"

#include "debug.h"

#include <stdio.h>
#include <fstream>

////////////////////////////////////////////////////////////////////////
// Constructors & Destructor
////////////////////////////////////////////////////////////////////////
NSGAII::NSGAII(RandomEngine *random, WorkEngine *worker, Logger *statsLogger):
m_randomEngine(random),
m_workEngine(worker),
m_objective(Invalid),
m_microMutationProbability(0),
m_macroMutationProbability(0),
m_crossoverProbability(0),
m_tournamentSize(0),
m_numGenomeEvaluations(0),
m_statsLogger(statsLogger),
m_evalSeeds(),
m_seedAge(0)
{
}
NSGAII::NSGAII(RandomEngine *random, WorkEngine *worker, Objective objective, float microMutationProbability, float macroMutationProbability, float crossoverProbability, unsigned int tournamentSize, unsigned int numGenomeEvaluations, Logger *statsLogger):
m_randomEngine(random),
m_workEngine(worker),
m_objective(objective),
m_microMutationProbability(microMutationProbability),
m_macroMutationProbability(macroMutationProbability),
m_crossoverProbability(crossoverProbability),
m_tournamentSize(tournamentSize),
m_numGenomeEvaluations(numGenomeEvaluations),
m_statsLogger(statsLogger),
m_evalSeeds(numGenomeEvaluations,0),
m_seedAge(0)
{
    for (int i = 0; i < numGenomeEvaluations; ++i)
        m_evalSeeds[i] = random->getNewRandomSeed();
}
NSGAII::~NSGAII()
{
    while (m_Parents.size() > 0)
    {
        delete m_Parents.back();
        m_Parents.pop_back();
    }
    while (m_Offsprings.size() > 0)
    {
        delete m_Offsprings.back();
        m_Offsprings.pop_back();
    }
}

////////////////////////////////////////////////////////////////////////
// Public member: Evolve
////////////////////////////////////////////////////////////////////////
void    NSGAII::evolve(unsigned int maxNumGenerations)
{
    if (m_objective == Invalid)
        throw("Invalid optimiziation. Did your load() fail?");
    
    maxNumGenerations += m_currentGenerationIndex;
    
    // Iterate through maxNumGenerations unless haltCondition() returns true
    while ( (m_currentGenerationIndex < maxNumGenerations) && !haltCondition() )
    {
        ++m_currentGenerationIndex;
        
        if ( (m_currentGenerationIndex%10) == 1)
        {
            saveResults("evProgress.xml");
            if (m_statsLogger != NULL)
                m_statsLogger->dump(true); // Write the status log, but keep the data
        }
        
        // Only save evolution statistics if there is a logger for it!
        if (m_statsLogger != NULL)
        {
            if (m_currentGenerationIndex == 1)
            {
                std::vector<float> logHeader(2,0);
                logHeader[0] = m_Parents.size();
                logHeader[1] = m_Parents.front()->getObjectiveSums().size();
                
                m_statsLogger->log(logHeader);
            }            
            for (std::deque<NSGAIIGenome*>::iterator mem = m_Parents.begin(); mem != m_Parents.end(); ++mem)
            {
                std::vector<float> memObj((*mem)->getObjectiveSums());
                
                // Divide the objective value sums by the number of genome evaluations
                for (std::vector<float>::iterator it = memObj.begin(); it != memObj.end(); ++it)
                    *it = (*it)/ ((float)(*mem)->getNumEvaluations());
                
                m_statsLogger->log(memObj);
            }
        }
        
        
        // Create a deque with all Parent and Offspring genomes
        std::deque<NSGAIIGenome*> allGenomes;
        allGenomes.insert(allGenomes.end(), m_Parents.begin(), m_Parents.end());
        allGenomes.insert(allGenomes.end(), m_Offsprings.begin(), m_Offsprings.end());
        
        if (m_seedAge == 0) // Only evaluate the parents if there is a new seed
        {
            // Clear the stats of the genomes
            for (std::deque<NSGAIIGenome*>::iterator genome = allGenomes.begin(); genome != allGenomes.end(); ++genome)
                (*genome)->reset();
            
            // Create evaluation tasks for the genomes
            //
            for (std::deque<NSGAIIGenome*>::iterator genome = allGenomes.begin(); genome != allGenomes.end(); ++genome)
                m_workEngine->push_back(new EvaluateGenome(*genome, m_evalSeeds));
        }
        else // If the seed is _not_ new, only evaluate the population
        {            
            // Create evaluation tasks for the genomes
            for (std::deque<NSGAIIGenome*>::iterator genome = m_Offsprings.begin(); genome != m_Offsprings.end(); ++genome)
                m_workEngine->push_back(new EvaluateGenome(*genome, m_evalSeeds));
        }
        // Wait for the worker to finish (also, work in this thread!)
        m_workEngine->waitUntilDone(true);
        
        // Increase the seed age
        ++m_seedAge;
        
        std::deque<NSGAIIGenome*> duplicates;
        separateDuplicates(allGenomes, duplicates);

        m_currentFronts.clear();
        // Assign all genomes to the different nondominated fronts (std::deque<std::deque<NSGAIIGenome*>> m_currentFronts)
        assignToFronts(allGenomes);
        // Assign all duplicate genomes to new (higher) fronts
        assignToFronts(duplicates);
        
        unsigned int frontCounter = (unsigned int)m_currentFronts.size();
        if ( (m_currentFronts.size() < 10) || (m_seedAge >= 5 ) )
        {
            m_seedAge = 0;
            S_LOG("Seeds Changed.\n");
            for (unsigned int i=0; i < m_numGenomeEvaluations; ++i)
                m_evalSeeds[i] = m_randomEngine->getNewRandomSeed();
        }
        
        // Create new Parents
        std::deque<NSGAIIGenome*> newParents;
        while (newParents.size() < m_populationSize)
        {
            // Get the best (least dominated) front from the deque & remove it from the list to add to the parent population
            std::deque<NSGAIIGenome*> bestFront = m_currentFronts.front();
            m_currentFronts.pop_front();
            
            if ( (newParents.size() + bestFront.size()) <= m_Parents.size() )
            {
                // If this front fits into the new parent population, add it
                newParents.insert(newParents.end(), bestFront.begin(), bestFront.end());
            }
            else
            {
                // The entire front doesn't fit, so sort it & add as much as possible
                sort(bestFront.begin(), bestFront.end(), NSGAIIGenome::crowdedComparison);
                
                // Add the genomes that fit into the parent population
                size_t numInsert = m_Parents.size() - newParents.size();
                newParents.insert(newParents.end(), bestFront.begin(), bestFront.begin()+numInsert);
                
                // Delete the rest of the front, since these won't make it to the next generation
                for (std::deque<NSGAIIGenome*>::iterator g = bestFront.begin()+numInsert; g != bestFront.end(); ++g)
                    delete (*g);
            }
        }
        // Make newParents the current parents
        m_Parents.swap(newParents);
        
        // Delete the rest of the fronts, since they won't make it to the next generation either
        for (std::deque<std::deque<NSGAIIGenome*>>::iterator front = m_currentFronts.begin(); front != m_currentFronts.end(); ++front)
        {
            for (std::deque<NSGAIIGenome*>::iterator g = front->begin(); g != front->end(); ++g)
                delete *g;
        }
        
        // Clean up
        m_currentFronts.clear(); // All of them have been assigned
        m_Offsprings.clear();    // Either moved to the parent population or deleted
        
        // Create the new generation
        createNewGeneration();
        
        S_LOG("Generation %d done. Front count: %d. \n",m_currentGenerationIndex, frontCounter);
    }
}
std::deque<NSGAIIGenome*>   NSGAII::getParetoGenomes(unsigned int nEval)
{
    if (isinf(nEval)) // If nEval is INFINITY, the population will be evaluated m_numGenomeEvaluations times
        nEval = m_numGenomeEvaluations;
    
    std::deque<NSGAIIGenome*> allPop;
    allPop.insert(allPop.end(), m_Parents.begin(), m_Parents.end());
    allPop.insert(allPop.end(), m_Offsprings.begin(), m_Offsprings.end());
    
    if (nEval > 0)
    {
        for (std::deque<NSGAIIGenome*>::iterator g = allPop.begin(); g != allPop.end(); ++g)
            (*g)->reset();

        m_evalSeeds.resize(nEval);
        
        for (unsigned int eCnt = 0; eCnt < nEval; ++eCnt)
            m_evalSeeds[eCnt] = m_randomEngine->getNewRandomSeed();
            
        for (std::deque<NSGAIIGenome*>::iterator genome = allPop.begin(); genome != allPop.end(); ++genome)
            m_workEngine->push_back(new EvaluateGenome(*genome, m_evalSeeds));
        
        m_workEngine->waitUntilDone(true);
    }
    
    assignToFronts(allPop);
    
    return m_currentFronts.front();
}
void NSGAII::saveResults(const char *fileName)
{
    pugi::xml_document doc;
    pugi::xml_node rootNode = doc.append_child("NSGAII");

    if (m_objective == Objective::Minimize)
        rootNode.append_attribute("objective")  = "Minimize";
    else if (m_objective == Objective::Maximize)
        rootNode.append_attribute("objective")  = "Maximize";
    
    rootNode.append_attribute("genCount")       = m_currentGenerationIndex;
    rootNode.append_attribute("popSize")        = m_populationSize;
    rootNode.append_attribute("numEval")        = m_numGenomeEvaluations;
    rootNode.append_attribute("tournamentSize") = m_tournamentSize;
    
    if (m_statsLogger != NULL)
        rootNode.append_attribute("logStats")       = true;
    
    rootNode.append_attribute("pCross")         = m_crossoverProbability;
    rootNode.append_attribute("pMMicro")        = m_microMutationProbability;
    rootNode.append_attribute("pMMacro")        = m_macroMutationProbability;
    
    pugi::xml_node archive = rootNode.append_child("Archive");
    for (std::deque<NSGAIIGenome*>::iterator child = m_Parents.begin(); child != m_Parents.end(); ++child)
        (*child)->saveGenome(archive);

    pugi::xml_node population = rootNode.append_child("Population");
    for (std::deque<NSGAIIGenome*>::iterator child = m_Offsprings.begin(); child != m_Offsprings.end(); ++child)
        (*child)->saveGenome(population);
    
    doc.save_file(fileName);
}
bool NSGAII::loadResults(const char* fileName, const char* statsName)
{
    pugi::xml_document xml;
    if (xml.load_file(fileName).status != pugi::xml_parse_status::status_ok)
        return false;
    
    pugi::xml_node doc = xml.child("NSGAII");
    if (!strcmp(doc.attribute("objective").as_string(),"Minimize"))
        m_objective = Minimize;
    else if (!strcmp(doc.attribute("objective").as_string(),"Maximize"))
        m_objective = Maximize;
    else
    {
        m_objective = Invalid;
        return  false;
    }
    
    m_currentGenerationIndex    = doc.attribute("genCount").as_uint(NAN);
    m_populationSize            = doc.attribute("popSize").as_uint(NAN);
    m_numGenomeEvaluations      = doc.attribute("numEval").as_uint(NAN);
    m_tournamentSize            = doc.attribute("tournamentSize").as_uint(NAN);
    
    m_crossoverProbability      = doc.attribute("pCross").as_float(NAN);
    m_microMutationProbability  = doc.attribute("pMMicro").as_float(NAN);
    m_macroMutationProbability  = doc.attribute("pMMacro").as_float(NAN);
    
    if (
        (isnan(m_currentGenerationIndex))   ||
        (isnan(m_populationSize))           ||
        (isnan(m_numGenomeEvaluations))     ||
        (isnan(m_tournamentSize))           ||
        (isnan(m_crossoverProbability))     ||
        (isnan(m_microMutationProbability)) ||
        (isnan(m_macroMutationProbability))
        )
    {
        m_objective = Invalid;
        return  false;
    }
    
    pugi::xml_node archive      = doc.child("Archive");
    pugi::xml_node population   = doc.child("Population");
    
    NSGAIIGenome* G = NULL;

    // Load the archive
    for (pugi::xml_node genome = archive.child("Genome"); genome; genome = genome.next_sibling("Genome"))
    {
        G = NSGAIIGenome::loadGenome(genome);
        if (G == NULL)
        {
            S_LOG("Failed to load a genome! Aborting load.\n");
            m_objective = Invalid;
            while (m_Parents.size() > 0)
            {
                delete m_Parents.back();
                m_Parents.pop_back();
            }
            return false;
        }
        
        m_Parents.push_back(G);
    }
    
    // Load the population
    for (pugi::xml_node genome = population.child("Genome"); genome; genome = genome.next_sibling("Genome"))
    {
        G = NSGAIIGenome::loadGenome(genome);
        if (G == NULL)
        {
            S_LOG("Failed to load a genome! Aborting load.\n");
            m_objective = Invalid;
            while (m_Parents.size() > 0)
            {
                delete m_Parents.back();
                m_Parents.pop_back();
            }
            while (m_Offsprings.size() > 0)
            {
                delete m_Offsprings.back();
                m_Offsprings.pop_back();
            }
            return false;
        }
        
        m_Offsprings.push_back(G);
    }
    
    // If specified, load the evolution stats
    if ( (m_statsLogger != NULL) && (strcmp(statsName,"") != 0) )
    {
        std::ifstream statsLog;
        
        // Check the file size
        statsLog.open(statsName, std::ifstream::in | std::ios::binary);
        if (statsLog)
        {
            statsLog.seekg(0, std::ios::end);
            unsigned long long fileLen = statsLog.tellg()/sizeof(float);
            
            
            unsigned int numObjectives = m_Parents.front()->getNumberOfObjectives();
            unsigned long long expectedFileSize = m_populationSize*(m_currentGenerationIndex)*numObjectives + 2; // The header contains 3 floats!
            
            if (fileLen == expectedFileSize) // Check if the ammount of data is what we'd expect
            {
                statsLog.seekg(0, std::ios::beg);
                
                float statsData[fileLen];
                statsLog.read((char*)&statsData, fileLen*sizeof(float)); // read the stats log into the buffer
                
                if ( // Check if the header corresponds to the data in the XML
                    (statsData[0] == m_Parents.size()) &&
                    (statsData[1] == m_Parents.front()->getNumberOfObjectives())
                   )
                {
                    
                    std::vector<float> tmpHead(statsData, statsData+2);
                    m_statsLogger->log(tmpHead);
                    
                    unsigned long long dataPos = 2;
                    
                    while (dataPos < fileLen)
                    {
                        std::vector<float> tmp(statsData+dataPos, statsData+dataPos+numObjectives);
                        m_statsLogger->log(tmp);

                        dataPos += numObjectives;
                    }
                }
                else
                    S_LOG("Couldn't load evolution stats!\n");
                    
            }
            else
                S_LOG("Couldn't load evolution stats!\n");
            
        }
        else
            S_LOG("Couldn't load evolution stats!\n");
    }
    
    // Re-evaluate the genomes
    m_evalSeeds.resize(m_numGenomeEvaluations);
    for (int i = 0; i < m_numGenomeEvaluations; ++i)
        m_evalSeeds[i] = m_randomEngine->getNewRandomSeed();
    
    // Create a deque with all Parent and Offspring genomes
    std::deque<NSGAIIGenome*> allGenomes;
    allGenomes.insert(allGenomes.end(), m_Parents.begin(), m_Parents.end());
    allGenomes.insert(allGenomes.end(), m_Offsprings.begin(), m_Offsprings.end());
    
    // Clear the stats of the genomes
    for (std::deque<NSGAIIGenome*>::iterator genome = allGenomes.begin(); genome != allGenomes.end(); ++genome)
        (*genome)->reset();
    
    // Create evaluation tasks for the genomes
    //
    for (std::deque<NSGAIIGenome*>::iterator genome = allGenomes.begin(); genome != allGenomes.end(); ++genome)
        m_workEngine->push_back(new EvaluateGenome(*genome, m_evalSeeds));

    // Wait for the worker to finish (also, work in this thread!)
    m_workEngine->waitUntilDone(true);
    
    return true;
}
////////////////////////////////////////////////////////////////////////
// Protected member functions
////////////////////////////////////////////////////////////////////////
void NSGAII::separateDuplicates(std::deque<NSGAIIGenome *> &genomes, std::deque<NSGAIIGenome *> &duplicates)
{
    
    std::deque<NSGAIIGenome*> uniqueGenomes; // Temp storage for the unique genomes
    
    unsigned int nObj = genomes.front()->getNumberOfObjectives();
    
    // Remove duplicates (i.e. solutions with identical performance) from the front and move them to the lowest front
    for (std::deque<NSGAIIGenome*>::iterator genome = genomes.begin(); genome != genomes.end(); ++genome)
    {
        bool duplicate = false;
        for (std::deque<NSGAIIGenome*>::iterator other = uniqueGenomes.begin(); ((other != uniqueGenomes.end()) && !duplicate); ++other)
        {
            // Check all objectives to see if there is at least one objective with a different value
            bool hasDifferent = false;
            for (unsigned int obj = 0; obj < nObj; ++obj)
            {
                // If the there is one, mark it as different and break the search
                if ((*genome)->getObjectiveValue(obj) != (*other)->getObjectiveValue(obj))
                { hasDifferent = true; obj = nObj; }
            }
            
            // If ALL objectives are identical, than <genome> is a duplicate of <other> in a performance sense
            if (!hasDifferent)
            { duplicate = true; break; }
        }
        
        // If <genome> is not a duplicate, add it to the unique front, otherwise add it to the duplicates collection
        if (!duplicate)
            uniqueGenomes.push_back(*genome);
        else
            duplicates.push_back(*genome);
    }
    
    genomes.swap(uniqueGenomes);
}
void NSGAII::assignToFronts(std::deque<NSGAIIGenome*> &genomes)
{
    
    // Clean up all leftover sorting results from before
    for (std::deque<NSGAIIGenome*>::iterator it = genomes.begin(); it != genomes.end(); ++it)
        (*it)->cleanDC();
    
    // This is the sorting direction
    bool largerIsBetter = (m_objective == Objective::Maximize);
    
    // Calculate the domination counts & construct the domination sets
    for (std::deque<NSGAIIGenome*>::iterator g1 = genomes.begin(); g1 != genomes.end(); ++g1)
    {
        for (std::deque<NSGAIIGenome*>::iterator g2 = g1+1; g2 != genomes.end(); ++g2)
        {
            // If g1 dominates g2:
            if (NSGAIIGenome::dominates(*g1, *g2, largerIsBetter) )
            {
                (*g1)->addDominated(*g2);
                (*g2)->incDC();
            }
            // If g2 dominates g1
            else if (NSGAIIGenome::dominates(*g2, *g1, largerIsBetter) )
            {
                (*g2)->addDominated(*g1);
                (*g1)->incDC();
            }
        }
    }
    
    // While there are still genomes not assigned to a front
    unsigned int frontIndex = (unsigned int)m_currentFronts.size(); // Start the front count from the 
    
    while (genomes.size() > 0)
    {
        // Create the current front
        std::deque<NSGAIIGenome*> currentFront;
        
        for (std::deque<NSGAIIGenome*>::iterator it = genomes.begin(); it != genomes.end();)
        {
            // Check for genomes that have a domination count of zero
            if ((*it)->getDC() == 0)
            {
                // Add the genome to the current front and remove it from the
                (*it)->setFrontIndex(frontIndex);
                currentFront.push_back(*it);
                it = genomes.erase(it);
            }
            else
            { ++it; }
        }
        
        // Decrease the domination count for each genome dominated by the genomes in the current fronts
        for (std::deque<NSGAIIGenome*>::iterator it = currentFront.begin(); it != currentFront.end(); ++it)
            (*it)->decDominated();
        
        // Calculate the crowding distance in the current
        calculateCrowdingDistance(currentFront);
        
        // Add the current front to the non-domination fronts. Add it to the back of the queue, so that the first front is the best one
        m_currentFronts.push_back(currentFront);
        
        // Increase the front counter
        ++frontIndex;
    }
}
void NSGAII::calculateCrowdingDistance(std::deque<NSGAIIGenome*> &theFront)
{
    // Initialize all crowding distances to zero
    for (std::deque<NSGAIIGenome*>::iterator it = theFront.begin(); it != theFront.end(); ++it)
        (*it)->setCrowdingDistance(0);
    
    // The number of objective functions
    unsigned int nObj = theFront.front()->getNumberOfObjectives();
    
    // Go through all objective functions
    for (unsigned int m=0; m < nObj; ++m)
    {
        // Sort the genomes
        sort(theFront.begin(), theFront.end(), bind(NSGAIIGenome::compareObjective, std::placeholders::_1, std::placeholders::_2, m, false)); // the sorting order is fixed to increasing here, so the crowding distance is never negative!
        
        // Set large crowding distances to the boundaries
        theFront.front()->setCrowdingDistance(INFINITY);
        theFront.back()->setCrowdingDistance(INFINITY);
        
        // The front is too small (at most two members)
        if (theFront.size() < 3)
            continue;
        
        // Calculate the scaling factor of the objective
        float scale = fabs(theFront.front()->getObjectiveValue(m) - theFront.back()->getObjectiveValue(m));
        
        if (scale == 0)
            continue;
        
        // Calculate the crowding distance for all remaining members
        for (std::deque<NSGAIIGenome*>::iterator it = theFront.begin()+1; it != theFront.end()-1; ++it)
        {
            float tmp = (fabs((*(it+1))->getObjectiveValue(m) - (*(it-1))->getObjectiveValue(m)))/scale;
            (*it)->incCrowdingDistance(tmp);
        }
    }
}
void NSGAII::createNewGeneration()
{
    // Create the new generation
    while (m_Offsprings.size() < m_populationSize)
    {
        // Select the parents via tournament selection
        NSGAIIGenome *mother = tournamentSelect();
        NSGAIIGenome *father = tournamentSelect();
        
        while (mother == father) // <= Yes, I know. This is quite weird...
            father = tournamentSelect();
        
        // Create the children via crossover
        NSGAIIGenome *child1, *child2;
        mother->crossover(m_randomEngine, m_crossoverProbability, father, child1, child2);
        
        // Mutate the children
        child1->mutate(m_randomEngine, m_microMutationProbability, m_macroMutationProbability);
        child2->mutate(m_randomEngine, m_microMutationProbability, m_macroMutationProbability);
        
        // Update the children's generation index
        child1->setGenerationIndex(m_currentGenerationIndex);
        child2->setGenerationIndex(m_currentGenerationIndex);
        
        // Add them to the new generation
        m_Offsprings.push_back(child1);
        m_Offsprings.push_back(child2);
//        delete child2;

    }
}
NSGAIIGenome* NSGAII::tournamentSelect()
{
    std::deque<NSGAIIGenome*> tournament;
    
    // Create the tournament from random genomes from the pool
    while (tournament.size() < m_tournamentSize)
        tournament.push_back(m_Parents[m_randomEngine->getUniformInt(0, m_populationSize-1)]);
    
    // Sort the tournament pool based on crowding-distance & domination
    sort(tournament.begin(), tournament.end(), NSGAIIGenome::crowdedComparison);
    
    // The first option is always the best
    return tournament.front();
}