//
//  NSGAII.h
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__NSGAII__
#define __Swarmolution__NSGAII__

#include <stdio.h>
#include <deque>

#include "../../Tools/Debug/consoleMacros.h"
#include "../../Tools/RandomEngine/RandomEngine.h"
#include "../../Tools/MultiCoreWorkEngine/WorkEngine.h"
#include "../../Tools/Loggers/Logger.h"

#include "NSGAIIGenome.h"

#include "debug.h"

class NSGAII
{
public:
    enum Objective
    {
        Invalid,
        Minimize,
        Maximize
    };
    
protected:
    // Genome storages
    std::deque<NSGAIIGenome*>               m_Parents;
    std::deque<NSGAIIGenome*>               m_Offsprings;
    unsigned int                            m_populationSize;
    std::vector<std::seed_seq::result_type> m_evalSeeds;
    unsigned int                            m_seedAge;
    
    // Semi-temporary storage of the nondominated fronts
    std::deque<std::deque<NSGAIIGenome*>>   m_currentFronts;
    
    // Algorithm Parameters
    Objective                               m_objective;
    float                                   m_microMutationProbability;
    float                                   m_macroMutationProbability;
    float                                   m_crossoverProbability;
    unsigned int                            m_tournamentSize;
    
    unsigned int                            m_numGenomeEvaluations;
    unsigned int                            m_currentGenerationIndex;
    
    // Tools
    RandomEngine                            *m_randomEngine;
    WorkEngine                              *m_workEngine;
    
    // Data saving
//    std::deque<std::deque<std::vector<float>>>  m_evolutionStatistics;
    Logger                                      *m_statsLogger;
    
public:
    NSGAII(RandomEngine *random, WorkEngine *worker, Logger *statsLogger = NULL);
    NSGAII(RandomEngine *random, WorkEngine *worker, Objective objective, float microMutationProbability, float macroMutationProbability, float crossoverProbability, unsigned int tournamentSize, unsigned int numGenomeEvaluations=1, Logger *statsLogger = NULL);
    virtual ~NSGAII();

    //////////////////////////////////////////////////////////////////////
    // Template based init function - must be inline definition in header:
    //        http://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file
    //
    template <typename Genome>
    typename std::enable_if<std::is_base_of<NSGAIIGenome, Genome>::value>::type    init(unsigned int numGenomes)
    {
        m_populationSize = numGenomes;
        
        
        Genome *genome = NULL;
        for (unsigned int i=0; i < numGenomes; ++i)
        {
            genome = new Genome(0);
            genome->init(m_randomEngine);
            
            m_workEngine->push_back(new EvaluateGenome(genome, m_evalSeeds));
            
            m_Parents.push_back(genome);
        }
        // Wait until all the genomes have been evaluated
        m_workEngine->waitUntilDone(true);
        
        // Assign members to fronts (just so that the nondomination ranks are calculted)
        assignToFronts(m_Parents);
        // Rebuild the parent generation from the fronts
        for (std::deque<std::deque<NSGAIIGenome*>>::iterator front = m_currentFronts.begin(); front != m_currentFronts.end(); ++front)
        {
            for (std::deque<NSGAIIGenome*>::iterator g = front->begin(); g != front->end(); ++g)
                m_Parents.push_back(*g);
        }
        
        saveResults("initialPopulation.xml");
        
        // Create the first generation of offsprings
        createNewGeneration();
        
        S_LOG("NSGAII: Initialization done.\n");
    }
    // END Template based init()
    //////////////////////////////////////////////////////////////////////
    
    void                        evolve(unsigned int maxNumGenerations);
    std::deque<NSGAIIGenome*>&  getGenomes() { return m_Parents; }
    std::deque<NSGAIIGenome*>   getParetoGenomes(unsigned int nEval = INFINITY); // If nEval is INFINITY, the population will be evaluated m_numGenomeEvaluations times
    
    void                        saveResults(const char *fileName);
    bool                        loadResults(const char* fileName, const char* statsName = "");
protected:
    
    // Helper functions for organizing the code - in principle, they are not necessary.
    void                        separateDuplicates(std::deque<NSGAIIGenome*> &genomes, std::deque<NSGAIIGenome*> &duplicates);
    void                        assignToFronts(std::deque<NSGAIIGenome*> &genomes);
    void                        calculateCrowdingDistance(std::deque<NSGAIIGenome*> &front);
    
    void                        createNewGeneration();
    NSGAIIGenome*               tournamentSelect();
    
    // Halt Condition function - evaluated at every generation to check if the evolution needs to go on.
    // The default behavior of this is to return false (i.e. move to the next generation)

//    virtual bool                haltCondition() { return debugHalt; }
    virtual bool                haltCondition() { return false; }
};
#endif /* defined(__Swarmolution__NSGAII__) */
