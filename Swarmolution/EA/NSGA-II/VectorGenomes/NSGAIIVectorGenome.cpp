//
//  NSGAIIVectorGenome.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAIIVectorGenome.h"

NSGAIIVectorGenome::NSGAIIVectorGenome(std::string genomeName, unsigned int generationID, std::vector<float> lB, std::vector<float> uB):
NSGAIIGenome(genomeName, generationID),
m_lb(lB),
m_ub(uB),
m_data(lB.size(),0)
{
    if (m_data.size() != m_ub.size())
        printf("Resize failed!\n");
}
NSGAIIVectorGenome::NSGAIIVectorGenome(const NSGAIIVectorGenome &other):
NSGAIIGenome(other),
m_lb(other.m_lb),
m_ub(other.m_ub),
m_data(other.m_data)
{}
NSGAIIVectorGenome::~NSGAIIVectorGenome()
{}

void    NSGAIIVectorGenome::init(RandomEngine *random)
{
    for (int i=0; i < m_data.size(); ++i)
        m_data[i] = random->getStdUniformPosReal()*(m_ub[i] - m_lb[i]) + m_lb[i];
}
void    NSGAIIVectorGenome::saveGenomeAttributes(pugi::xml_node &root)
{
    pugi::xml_node genome = root.append_child("VectorGenome");
    genome.append_attribute("type")     = m_genomeName.c_str();
    genome.append_attribute("dataSize") = (unsigned int)m_data.size();
    
    pugi::xml_node ll = genome.append_child("lowerBound");
    for (unsigned int i=0; i < m_data.size(); ++i)
    {
        std::string name("p");
        name.append(std::to_string(i));
        
        ll.append_attribute(name.c_str()) = m_lb[i];
    }
    pugi::xml_node hl = genome.append_child("upperBound");
    for (unsigned int i=0; i < m_data.size(); ++i)
    {
        std::string name("p");
        name.append(std::to_string(i));
        
        hl.append_attribute(name.c_str()) = m_ub[i];
    }
    pugi::xml_node data = genome.append_child("data");
    for (unsigned int i=0; i < m_data.size(); ++i)
    {
        std::string name("p");
        name.append(std::to_string(i));
        
        data.append_attribute(name.c_str()) = m_data[i];
    }
}
bool    NSGAIIVectorGenome::loadGenomeAttributes(pugi::xml_node &root)
{
    throw("Not implemented!");
    return false;
}
void    NSGAIIVectorGenome::mutateGenome(RandomEngine *rand, float pMutate, float pMacro)
{
    for (int i=0; i < m_data.size(); ++i)
    {
        float P = rand->getStdUniformPosReal();
        
        if (P < pMacro)
        {
            m_data[i] = rand->getStdUniformPosReal()*(m_ub[i] - m_lb[i]) + m_lb[i];
        }
        else if (P < pMutate)
        {
            double mut = rand->getStdUniformReal()*(m_ub[i] - m_lb[i])*0.05;
            
            if ((m_data[i] + mut) < m_lb[i])
                m_data[i] -= mut;
            else if ((m_data[i] + mut) > m_ub[i])
                m_data[i] -= mut;
            else
                m_data[i] += mut;
        }
    }
}
void    NSGAIIVectorGenome::crossoverGenome(RandomEngine *rand, float pCross, NSGAIIGenome *other, NSGAIIGenome *&child1, NSGAIIGenome *&child2, unsigned int generationID)
{
    unsigned int flipPoint = 1;
    if (rand->getStdUniformPosReal() < pCross)
    flipPoint = rand->getUniformInt(0, (int)m_data.size()-1);

    NSGAIIVectorGenome *C1 = dynamic_cast<NSGAIIVectorGenome *>(this->clone());
    NSGAIIVectorGenome *C2 = dynamic_cast<NSGAIIVectorGenome *>(this->clone());

    C1->reset();
    C2->reset();

    NSGAIIVectorGenome *O = dynamic_cast<NSGAIIVectorGenome*>(other);


    float *src1 = &this->m_data.front();
    float *src2 = &O->m_data.front();

    for (int i=0; i < m_data.size(); ++i)
    {
        C1->m_data[i] = *(src1++);
        C2->m_data[i] = *(src2++);
        
        if(i == flipPoint)
        {
            float *tmp = src1;
            src1 = src2;
            src2 = tmp;
        }
    }

    child1 = C1;
    child2 = C2;
}