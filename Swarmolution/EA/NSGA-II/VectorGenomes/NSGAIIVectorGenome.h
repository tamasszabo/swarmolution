//
//  NSGAIIVectorGenome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__NSGAIIVectorGenome__
#define __Swarmolution__NSGAIIVectorGenome__

#include <stdio.h>

#include "../../Core/NSGAIIGenome.h"

#include <vector>

class NSGAIIVectorGenome: public NSGAIIGenome
{
protected:
    std::vector<float>  m_data;
    std::vector<float>  m_lb;
    std::vector<float>  m_ub;
    
public:
    NSGAIIVectorGenome(std::string genomeName, unsigned int generationID, std::vector<float> lB, std::vector<float> uB);
    NSGAIIVectorGenome(const NSGAIIVectorGenome &other);
    virtual ~NSGAIIVectorGenome();
    
public: // public virtual functions
    void    init(RandomEngine *random);
    virtual void    saveGenomeAttributes(pugi::xml_node &root);
    virtual bool    loadGenomeAttributes(pugi::xml_node &root);
    
protected: // protected virtual members
    void    mutateGenome(RandomEngine *rand, float pMutate, float pMacro);
    void    crossoverGenome(RandomEngine *rand, float pCross, NSGAIIGenome *other, NSGAIIGenome *&child1, NSGAIIGenome *&child2, unsigned int generationID);
};
#endif /* defined(__Swarmolution__NSGAIIVectorGenome__) */
