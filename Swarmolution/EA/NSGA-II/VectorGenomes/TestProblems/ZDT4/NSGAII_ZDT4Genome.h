//
//  NSGAII_ZDT4Genome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_NSGAII_ZDT4Genome_h
#define Swarmolution_NSGAII_ZDT4Genome_h

#include "../../NSGAIITestGenome.h"
class NSGAII_ZDT4Genome: public NSGAIITestGenome
{
public:
    NSGAII_ZDT4Genome(unsigned int generationID, vector<float> &lB, vector<float> &uB):
        NSGAIITestGenome(generationID, lB, uB) {}
    
    NSGAII_ZDT4Genome(const NSGAII_ZDT4Genome &other):
        NSGAIITestGenome(other) {}
    
    ~NSGAII_ZDT4Genome()
    {
    }
    Genome*     clone()
        { return new NSGAII_ZDT4Genome(*this); }
    
    void init(RandomEngine *random)
        {
            for (int i=0; i < m_data.size(); ++i)
                m_data[i] = random->getStdUniformPosReal()*(m_ub[i] - m_lb[i]) + m_lb[i];
            
            if(m_data[0] < 0)
                printf("blip!\n");
        }
    
    void  evaluateGenome(std::seed_seq::result_type randomSeed)
        {
            if (m_objectiveSums.size() != 2)
                m_objectiveSums.resize(2);
            
            double g = 1 + 10*(m_data.size()-1);
            for (std::vector<float>::iterator it = m_data.begin()+1; it != m_data.end(); ++it)
                g += ( (*it)*(*it) - 10*cos(4*M_PI*(*it)));
            
            m_objectiveSums[0] += m_data[0];
            m_objectiveSums[1] += g*(1 - sqrt(m_data[0]/g));
            m_numEvaluations++;
        }
    
    void        printMatlabCode(std::string &data)
    {
        for (std::vector<float>::iterator it = m_data.begin(); it != m_data.end(); ++it)
        {
            data.append(std::to_string(*it));
            data.append(" ");
        }
        for (std::vector<float>::iterator it = m_objectiveSums.begin(); it != m_objectiveSums.end(); ++it)
        {
            data.append(std::to_string( (*it)/m_numEvaluations) );
            data.append(" ");
        }
        data.append(" ;...\n");
    }
};
#endif
