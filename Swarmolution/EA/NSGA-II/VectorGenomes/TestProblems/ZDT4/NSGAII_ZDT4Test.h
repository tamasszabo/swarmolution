//
//  NSGAII_ZDT4Test.h
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__NSGAII_ZDT4Test__
#define __Swarmolution__NSGAII_ZDT4Test__

#include <stdio.h>
#include "NSGAII_ZDT4.h"

void run_NSGAII_ZDT4Test();
#endif /* defined(__Swarmolution__NSGAII_ZDT4Test__) */
