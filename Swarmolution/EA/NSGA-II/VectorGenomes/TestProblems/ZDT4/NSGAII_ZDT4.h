//
//  NSGAII_ZDT4.h
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_NSGAII_ZDT4_h
#define Swarmolution_NSGAII_ZDT4_h

#include "../../NSGAIITest.h"
#include "NSGAII_ZDT4Genome.h"

class NSGAII_ZDT4: public NSGAIITest
{
public:
    NSGAII_ZDT4(RandomEngine *random, WorkEngine *work, float mutationP, float macroP, float crossP, unsigned int tournamentSize):
    NSGAIITest(random, work, Objective::Minimize, 1, mutationP, macroP, crossP, tournamentSize)
    {
        
    }
    void    init(unsigned int numGenomes)
    {
        for (int i=0; i < numGenomes; ++i)
        {
            std::vector<float> lb(10,-5);
            std::vector<float> ub(10,5);
            lb[0]=0;
            ub[0]=1;
            
            NSGAII_ZDT4Genome *genome = new NSGAII_ZDT4Genome(0,lb,ub);
            genome->init(m_randomEngine);
            genome->evaluate(0);
            
            m_archiveGenomes.push_back(genome);
        }
    }
    void    printMatlabPlotCode()
    {
        std::string data;
        data.append("clear all\nclose all\nclc\n\n\n");
        data.append("data = [ ...\n");
        for (std::deque<Genome*>::iterator it = m_archiveGenomes.begin(); it != m_archiveGenomes.end(); ++it)
            dynamic_cast<NSGAII_ZDT4Genome*>(*it)->printMatlabCode(data);
        
        data.append("];\n");
        data.append("eps = 0.00001;\n");
        data.append("fprintf(\'The ZDT4 Test from Deb et. al 2002\\n\\n\');\n");
        data.append("fprintf(\'The algorithm found %d unique solutions from the %d. %d are on the pareto front.\\n\',size(unique(data(:,1:3),\'rows\'),1),numel(data(:,1)), sum( (abs(data(:,1) - data(:,2)) < eps).*(abs(data(:,1) - data(:,3)) < eps).*(abs(data(:,1)) < (1/sqrt(3)))));\n\n");
        
        data.append("x = (0:0.01:1)\';\n");
        data.append("x = [x repmat(0*x,1,9)];\n");
        data.append("o1 = x(:,1);\n");
        data.append("g  = 1 + 10*(size(x,2)-1) + sum( x(:,2:end).^2 - 10*cos(x(:,2:end)),2);\n");
        data.append("o2 = g.*(1 - sqrt(x(:,1)./g));\n");
        data.append("plot(o1,o2); hold on\n");
        
        data.append("scatter(data(:,4), data(:,5));\n");
        
        printf("%s",data.c_str());
    }
};
#endif
