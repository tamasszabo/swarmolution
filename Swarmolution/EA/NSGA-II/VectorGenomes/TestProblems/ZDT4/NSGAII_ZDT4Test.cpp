//
//  NSGAII_ZDT4Test.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAII_ZDT4Test.h"

void run_NSGAII_ZDT4Test()
{
    RandomEngine rand;
    WorkEngine work;
    work.start(5);
    
    NSGAII_ZDT4 *ea = new NSGAII_ZDT4(&rand, &work, 0.8, 0, 0.8, 10);
    ea->init(100);
    ea->evolve(2000);
    
    ea->printMatlabPlotCode();
    
    delete ea;
}