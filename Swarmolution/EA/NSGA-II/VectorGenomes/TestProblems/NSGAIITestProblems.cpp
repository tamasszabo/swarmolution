//
//  NSGAIITestProblems.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include <stdio.h>
#include "NSGAIITestProblems.h"

#include "../../../Core/NSGAII.h"
#include "../../../../Tools/RandomEngine/RandomEngine.h"
#include "../../../../Tools/MultiCoreWorkEngine/WorkEngine.h"

#include "VectorTests/NSGAII_SCHGenome.h"
#include "VectorTests/NSGAII_FONGenome.h"
#include "VectorTests/NSGAII_ZDT1Genome.h"

void run_NSGAII_SCHTest(int figID)
{
    RandomEngine rand;
    WorkEngine work;
    work.start(0);

    Logger          statsLogger("evolutionStats.log");
    
    NSGAII ea(&rand, &work, NSGAII::Objective::Minimize, 1, 0.0, 0.8, 10, 1, &statsLogger);

    ea.init<NSGAII_SCHGenome>(100);
    ea.evolve(500);
    
    statsLogger.dump();
    
    std::deque<NSGAIIGenome*> genomes = ea.getGenomes();
    
    std::string data;
    data.append("%% SCH Test\n\n");
    data.append("data = [ ...\n");
    for (std::deque<NSGAIIGenome*>::iterator it = genomes.begin(); it != genomes.end(); ++it)
        dynamic_cast<NSGAII_SCHGenome*>(*it)->printMatlabCode(data);
    
    data.append("];\n");
    data.append("fprintf(\'The SCH Test from Deb et. al 2002\\n\\n\');\n");
    data.append("fprintf(\'Found %d unique solutions from the %d. %d are on the pareto front.\\n\',numel(unique(data(:,1))),numel(data(:,1)), sum( (data(:,1) > 0).*(data(:,1) < 2)));\n\n");
    
    data.append("x = 0:0.01:2;\n\no1 = x.^2; \no2 = (x-2).^2;\n\n");
    
    data.append("figure(");
    data.append(std::to_string(figID));
    data.append(");\n");
    
    data.append("plot(o1,o2); hold on\n");
    data.append("scatter(data(:,2), data(:,3));\n");
    
    printf("%s",data.c_str());
}
void run_NSGAII_FONTest(int figID)
{
    RandomEngine rand;
    WorkEngine work;
    work.start(0);
    
    Logger          statsLogger("evolutionStats.log");
    NSGAII ea(&rand, &work, NSGAII::Objective::Minimize, 0.6, 0.2, 0.8, 10, 1, &statsLogger);
    
    ea.init<NSGAII_FONGenome>(100);
    ea.evolve(500);
    
    statsLogger.dump();
    
    std::deque<NSGAIIGenome*> genomes = ea.getGenomes();
    
    std::string data;
    data.append("%% FON Test\n\n");
    data.append("data = [ ...\n");
    for (std::deque<NSGAIIGenome*>::iterator it = genomes.begin(); it != genomes.end(); ++it)
        dynamic_cast<NSGAII_FONGenome*>(*it)->printMatlabCode(data);
    
    data.append("];\n");
    data.append("fprintf(\'The FON Test from Deb et. al 2002\\n\\n\');\n");
    data.append("fprintf('Found %d unique solutions from the %d.\\n',numel(unique(data(:,1:end-2))),size(data,1));\n");
    data.append("\n");
    data.append("eps = 0.1;\n");
    data.append("S = sum((abs(data(:,1) - data(:,2))<eps).*(abs(data(:,1) - data(:,3))<eps) .* (abs(data(:,1)) < 1/sqrt(3)));\n");
    data.append("fprintf('%d genomes are on the pareto front.\\n\',S);\n");
                
    data.append("x = repmat((-4:0.01:4)',1,3);\n");
    data.append("\n");
    data.append("S = sum((x + (1/sqrt(3))).^2,2);\n");
    data.append("D = sum((x - (1/sqrt(3))).^2,2);\n");
    data.append("\n");
    data.append("o1 = 1 - exp(-S);\n");
    data.append("o2 = 1 - exp(-D);\n");
    data.append("\n");
    data.append("figure(");
    data.append(std::to_string(figID));
    data.append(");\n");
    data.append("plot(o1,o2); hold on\n");
    data.append("scatter(data(:,4), data(:,5));\n");
    
    
    printf("%s",data.c_str());
}
void run_NSGAII_ZDT1Test(int figID)
{
    RandomEngine rand;
    WorkEngine work;
    work.start(0);
    
    Logger          statsLogger("evolutionStats.log");
    NSGAII ea(&rand, &work, NSGAII::Objective::Minimize, 0.8, 0.02, 0.8, 50, 1, &statsLogger);
    
    ea.init<NSGAII_ZDT1Genome>(200);
    ea.evolve(500);
    
    statsLogger.dump();
    std::deque<NSGAIIGenome*> genomes = ea.getGenomes();
    
    std::string data;
    data.append("\n%% ZDT1 Test\n\n");
    data.append("data = [ ...\n");
    for (std::deque<NSGAIIGenome*>::iterator it = genomes.begin(); it != genomes.end(); ++it)
        dynamic_cast<NSGAII_ZDT1Genome*>(*it)->printMatlabCode(data);
    
    data.append("];\n");
    data.append("fprintf(\'The ZDT1 Test from Deb et. al 2002\\n\\n\');\n");
    data.append("fprintf('Found %d unique solutions from the %d.\\n',numel(unique(data(:,1),\'rows\')),numel(data(:,1)));\n");
    data.append("\n");
    data.append("eps = 0.01;\n");
    data.append("S = sum(prod(data(:,2:end-2) < eps,2));\n");
    data.append("fprintf('%d genomes are on the pareto front.\\n\',S);\n");
    
    data.append("x = 0:0.01:1;\n");
    data.append("\n");
    data.append("o1 = x;\n");
    data.append("o2 = 1-sqrt(x);\n");
    data.append("\n");
    data.append("figure(");
    data.append(std::to_string(figID));
    data.append(");\n");
    data.append("plot(o1,o2); hold on\n");
    data.append("scatter(data(:,end-1), data(:,end));\n");
    
    
    printf("%s",data.c_str());
}