//
//  NSGAII_ZDT1.h
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_NSGAII_ZDT1_h
#define Swarmolution_NSGAII_ZDT1_h

#include "../NSGAIITest.h"
#include "NSGAII_ZDT1Genome.h"

class NSGAII_ZDT1: public NSGAIITest
{
public:
    NSGAII_ZDT1(RandomEngine *random, WorkEngine *work, float mutationP, float macroP, float crossP, unsigned int tournamentSize):
    NSGAIITest(random, work, Objective::Minimize, 1, mutationP, macroP, crossP, tournamentSize)
    {
        
    }
    void    init(unsigned int numGenomes)
    {
        for (int i=0; i < numGenomes; ++i)
        {
            std::vector<float> lb(30,0);
            std::vector<float> ub(30,1);
            
            NSGAII_ZDT1Genome *genome = new NSGAII_ZDT1Genome(0,lb,ub);
            genome->init(m_randomEngine);
            genome->evaluate(0);
            
            m_archiveGenomes.push_back(genome);
        }
    }
    void    printMatlabPlotCode()
    {
        std::string data;
        data.append("%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        data.append("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        data.append("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
        data.append("% ZDT 1 Test problem\n");
        data.append("data = [ ...\n");
        for (std::deque<Genome*>::iterator it = m_archiveGenomes.begin(); it != m_archiveGenomes.end(); ++it)
            dynamic_cast<NSGAII_ZDT1Genome*>(*it)->printMatlabCode(data);
        
        data.append("];\n");
        data.append("eps = 0.00001;\n");
        data.append("fprintf(\'The ZDT1 Test from Deb et. al 2002\\n\\n\');\n");
        data.append("fprintf(\'The algorithm found %d unique solutions from the %d.\\n\',size(unique(data(:,1:3),\'rows\'),1),numel(data(:,1)));\n");
//        data.append("fprintf(\'There are %d Pareto optimal solutions.\', ...\n);\n");
//        data.append("sum( (abs(data(:,1) - data(:,2)) < eps).*(abs(data(:,1) - data(:,3)) < eps).*(abs(data(:,1)) < (1/sqrt(3)))));\n\n");
        
        
        data.append("x   = (0:0.01:1)';\n");
        data.append("o1  = x;\n");
        data.append("o2  = 1 - sqrt(x);\n");
        
        
        data.append("plot(o1,o2); hold on\n");
        
        data.append("scatter(data(:,4), data(:,5));\n");
        
        printf("%s",data.c_str());
    }
};
#endif
