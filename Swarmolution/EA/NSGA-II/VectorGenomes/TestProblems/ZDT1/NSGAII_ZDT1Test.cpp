//
//  NSGAII_ZDT1Test.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAII_ZDT1Test.h"

void run_NSGAII_ZDT1Test()
{
    RandomEngine rand;
    WorkEngine work;
    work.start(5);
    
    NSGAII_ZDT1 *ea = new NSGAII_ZDT1(&rand, &work, 0.8, 0, 0.8, 10);
    ea->init(100);
    ea->evolve(2000);
    
    ea->printMatlabPlotCode();
    
    delete ea;
}