//
//  NSGAII_SCHGenome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_NSGAII_SCHGenome_h
#define Swarmolution_NSGAII_SCHGenome_h

#include "../../NSGAIIVectorGenome.h"
class NSGAII_SCHGenome: public NSGAIIVectorGenome
{
public:
    NSGAII_SCHGenome(unsigned int generationID):
    NSGAIIVectorGenome("SCH",generationID, std::vector<float>(1,-1000), std::vector<float>(1,1000))
    {
    }
    
    NSGAII_SCHGenome(const NSGAII_SCHGenome &other):
        NSGAIIVectorGenome(other)
    {}
    
    ~NSGAII_SCHGenome()
    {
    }
    NSGAIIGenome*     clone()
        { return new NSGAII_SCHGenome(*this); }
    
    void init(RandomEngine *random)
        {
            for (int i=0; i < m_data.size(); ++i)
                m_data[i] = random->getStdUniformPosReal()*(m_ub[i] - m_lb[i]) + m_lb[i];
        }
    
    void  evaluateGenome(std::seed_seq::result_type randomSeed)
        {
            if (m_objectiveSums.size() != 2)
                m_objectiveSums.resize(2);
            
            m_objectiveSums[0] += m_data[0]*m_data[0];
            m_objectiveSums[1] += (m_data[0]-2)*(m_data[0]-2);
            m_numEvaluations++;
        }
    
    void        printMatlabCode(std::string &data)
    {
        if ( (m_numEvaluations != 1) || (m_data[0] < m_lb[0]) || (m_data[0] > m_ub[0]) )
            printf("Something's wrong here!\n");
        data.append(std::to_string(m_data[0]));
        data.append("\t");
        data.append(std::to_string(m_objectiveSums[0]/m_numEvaluations));
        data.append("\t");
        data.append(std::to_string(m_objectiveSums[1]/m_numEvaluations));
        data.append("\t");
        data.append(std::to_string(m_generationID));
        data.append(" ;...\n");
    }
};
#endif
