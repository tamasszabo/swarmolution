//
//  NSGAII_ZDT1Genome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 18/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_NSGAII_ZDT1Genome_h
#define Swarmolution_NSGAII_ZDT1Genome_h

#include "../../NSGAIIVectorGenome.h"
class NSGAII_ZDT1Genome: public NSGAIIVectorGenome
{
public:
    NSGAII_ZDT1Genome(unsigned int generationID):
    NSGAIIVectorGenome("ZDT1",generationID, std::vector<float>(30,0), std::vector<float>(30,1))
    {
    }
    
    NSGAII_ZDT1Genome(const NSGAII_ZDT1Genome &other):
    NSGAIIVectorGenome(other)
    {}
    
    ~NSGAII_ZDT1Genome()
    {
    }
    NSGAIIGenome*     clone()
    { return new NSGAII_ZDT1Genome(*this); }
    
    void init(RandomEngine *random)
    {
        for (int i=0; i < m_data.size(); ++i)
            m_data[i] = random->getStdUniformPosReal()*(m_ub[i] - m_lb[i]) + m_lb[i];
    }
    
    void  evaluateGenome(std::seed_seq::result_type randomSeed)
    {
        if (m_objectiveSums.size() != 2)
            m_objectiveSums.resize(2);
        
        float g = 0;
        
        for (std::vector<float>::iterator it = m_data.begin()+1; it != m_data.end(); ++it)
            g += *it;
        g /= (m_data.size()-1);
        g *= 9;
        
        g += 1;
        
        m_objectiveSums[0] += m_data[0];
        m_objectiveSums[1] += g*(1 - sqrt(m_data[0]/g));
        
        m_numEvaluations++;
    }
    
    
    void        printMatlabCode(std::string &data)
    {
        for (std::vector<float>::iterator it = m_data.begin(); it != m_data.end(); ++it)
        {
            data.append(std::to_string(*it));
            data.append("\t");
        }
        for (std::vector<float>::iterator it = m_objectiveSums.begin(); it != m_objectiveSums.end(); ++it)
        {
            data.append(std::to_string((*it)/m_numEvaluations));
            data.append("\t");
        }
        data.append(";...\n");
    }
    
};


#endif
