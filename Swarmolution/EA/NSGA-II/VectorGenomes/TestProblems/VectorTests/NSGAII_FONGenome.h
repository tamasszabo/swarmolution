//
//  NSGAII_FONGenome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 16/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_NSGAII_FONGenome_h
#define Swarmolution_NSGAII_FONGenome_h


#include "../../NSGAIIVectorGenome.h"
class NSGAII_FONGenome: public NSGAIIVectorGenome
{
public:
    NSGAII_FONGenome(unsigned int generationID):
    NSGAIIVectorGenome("FON",generationID, std::vector<float>(3,-4), std::vector<float>(3,4))
    {
    }
    
    NSGAII_FONGenome(const NSGAII_FONGenome &other):
    NSGAIIVectorGenome(other)
    {}
    
    ~NSGAII_FONGenome()
    {
    }
    NSGAIIGenome*     clone()
    { return new NSGAII_FONGenome(*this); }
    
    void init(RandomEngine *random)
    {
        for (int i=0; i < m_data.size(); ++i)
            m_data[i] = random->getStdUniformPosReal()*(m_ub[i] - m_lb[i]) + m_lb[i];
    }
    
    void  evaluateGenome(std::seed_seq::result_type randomSeed)
    {
        if (m_objectiveSums.size() != 2)
            m_objectiveSums.resize(2);
        
        float S = 0;
        float D = 0;
        
        for (std::vector<float>::iterator it = m_data.begin(); it != m_data.end(); ++it)
        {
            S += pow( ((*it) + (1/sqrt(3))),2);
            D += pow( ((*it) - (1/sqrt(3))),2);
        }
        m_objectiveSums[0] += 1 - exp(-S);
        m_objectiveSums[1] += 1 - exp(-D);
        
        m_numEvaluations++;
    }

    
    void        printMatlabCode(std::string &data)
    {
        data.append(std::to_string(m_data[0]));
        data.append("\t");
        data.append(std::to_string(m_data[1]));
        data.append("\t");
        data.append(std::to_string(m_data[2]));
        data.append("\t");
        data.append(std::to_string(m_objectiveSums[0]/m_numEvaluations));
        data.append("\t");
        data.append(std::to_string(m_objectiveSums[1]/m_numEvaluations));
        data.append(" ;...\n");
    }

};
#endif
