//
//  NSGAIITestProblems.h
//  Swarmolution
//
//  Created by Tamas Szabo on 17/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_NSGAIITestProblems_h
#define Swarmolution_NSGAIITestProblems_h

void run_NSGAII_SCHTest(int figID);
void run_NSGAII_FONTest(int figID);
void run_NSGAII_ZDT1Test(int figID);

#endif
