//
//  TreeNodeGenome.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 18/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "TreeNodeGenome.h"
TreeNodeGenome::TreeNodeGenome(TreeNodeGenome *parent, TreeGenomeLimits *limits):
m_Parent(parent),
m_Limits(limits)
{
    
}
TreeNodeGenome::TreeNodeGenome(const TreeNodeGenome &other):
TreeNode(other),
m_Parent(other.m_Parent),
m_Limits(other.m_Limits)
{
    
}
TreeNodeGenome::~TreeNodeGenome()
{
    
}


TreeNodeGenome* TreeNodeGenome::getParent(TreeNodeGenome* querySource)
{
    if (querySource == m_Parent)
    {
        if (m_Parent != NULL)
            throw("blip!");
    }
    return m_Parent;
}
void            TreeNodeGenome::setParent(TreeNodeGenome* parent)   { m_Parent = parent; }
void            TreeNodeGenome::setLimits(TreeGenomeLimits *limits) { m_Limits = limits; }
TreeGenomeLimits* TreeNodeGenome::getLimits() { return m_Limits; }
unsigned int    TreeNodeGenome::getDepth()
{
    unsigned int depthCounter = 0;
    TreeNodeGenome *parent = m_Parent;
    
    // Recursively search up the tree until there is no more parent. That is the root node. The number of steps equals the node's depth
    while (parent != NULL)
    {
        ++depthCounter;
        parent = parent->getParent(this);
        
        if (depthCounter > 100)
            throw("Recursive branch structure!");
    }
    
    return depthCounter;
}
unsigned int TreeNodeGenome::getBranchDepth()
{
    unsigned int d = getDepth();
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        unsigned int cD = dynamic_cast<TreeNodeGenome*>(*it)->getBranchDepth();
        if (cD > d)
            d = cD;
    }
    
    return d;
}
void  TreeNodeGenome::sync(TreeNodeGenome *parent)
{
//    if (m_Parent != parent)
//        printf("BLA\n");
    m_Parent    = parent;
    if (m_Parent != NULL)
        m_Limits    = m_Parent->getLimits();
    
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        dynamic_cast<TreeNodeGenome*>(*it)->sync(this);
}
void TreeNodeGenome::addChildrenToList(std::deque<TreeNodeGenome*> &theList)
{
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        theList.push_back(dynamic_cast<TreeNodeGenome*>(*it));
}

unsigned int TreeNodeGenome::nodeCount()
{
    unsigned int dest = 0;
    countNodes(dest);
    return dest;
}

void TreeNodeGenome::crossover(RandomEngine *random, TreeNodeGenome *other) // <= This crossover operator swaps two random nodes of the trees!
{
    // get a random branch from both trees - start from index 1, since the root node shouldn't cross over
    TreeNodeGenome *branch1 = this->getChildWithIndex(random->getUniformInt(1, nodeCount()-1));
    TreeNodeGenome *branch2 = other->getChildWithIndex(random->getUniformInt(1, other->nodeCount()-1));
    
    // Get the parent nodes of the two branches
    TreeNodeGenome *parent1 = branch1->getParent();
    TreeNodeGenome *parent2 = branch2->getParent();
    
    // Swap the children
    if (parent1->replaceChildWithNode(branch1, branch2) == NULL)
        throw("TreeNodeGenome::crossover: couldn't find child!\n");
    
    if (parent2->replaceChildWithNode(branch2, branch1) == NULL)
        throw("TreeNodeGenome::crossover: couldn't find child!\n");

    branch2->setParent(parent1);
    branch1->setParent(parent2);
}

TreeNodeGenome*  TreeNodeGenome::getChildWithIndex(unsigned int index)
{
    unsigned int childCounter = 0;
    return findChildWithIndex(index, childCounter);
}
TreeNodeGenome*  TreeNodeGenome::replaceChildWithNode(TreeNodeGenome* child, TreeNodeGenome *node)
{
    
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        if ( (*it) == child )
        {
            TreeNodeGenome *retNode = dynamic_cast<TreeNodeGenome*>(*it);
            
            *it = node;
            node->setParent(this);
            node->setLimits(m_Limits);
            
            retNode->setParent(NULL);
            return retNode;
        }
    }
    
    return NULL;
}

void TreeNodeGenome::countNodes(unsigned int &dest)
{
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        dynamic_cast<TreeNodeGenome*>(*it)->countNodes(dest);
    
    ++dest;
}
TreeNodeGenome* TreeNodeGenome::findChildWithIndex(unsigned int index, unsigned int &nodeCounter)
{
    if (index == nodeCounter)
        return this;
    
    ++nodeCounter;
    
    TreeNodeGenome* returnNode = NULL;
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        returnNode = dynamic_cast<TreeNodeGenome*>(*it)->findChildWithIndex(index, nodeCounter);
        if (returnNode != NULL)
            return returnNode;
    }
    
    return NULL;
}