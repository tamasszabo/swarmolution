//
//  TreeNodeGenome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 18/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__TreeNodeGenome__
#define __Swarmolution__TreeNodeGenome__

#include <stdio.h>
#include <deque>


#include "TreeNode.h"
#include "../../../../Tools/RandomEngine/RandomEngine.h"

struct TreeGenomeLimits
{
    unsigned int maxChildren;
    unsigned int maxDepth;
    unsigned int maxNodeCount;
    
    TreeGenomeLimits():
    maxChildren(5),
    maxDepth(5),
    maxNodeCount(100)
    {
    }
    TreeGenomeLimits(const TreeGenomeLimits &other):
    maxChildren(other.maxChildren),
    maxDepth(other.maxDepth),
    maxNodeCount(other.maxNodeCount)
    {
    }
    
    void operator=(const TreeGenomeLimits &other)
    {
        maxChildren     = other.maxChildren;
        maxDepth        = other.maxDepth;
        maxNodeCount    = other.maxNodeCount;
    }
};



class TreeNodeGenome: public virtual TreeNode
{
protected:
    TreeNodeGenome      *m_Parent;
    TreeGenomeLimits    *m_Limits;
    
public:
    TreeNodeGenome(TreeNodeGenome *parent, TreeGenomeLimits *limits);
    TreeNodeGenome(const TreeNodeGenome &other);
    virtual ~TreeNodeGenome();
    
    virtual void                init(RandomEngine *random)                              = 0;
    virtual void                mutate(RandomEngine *rand, float pMutate, float pMacro) = 0;

    
    TreeNodeGenome*             getParent(TreeNodeGenome* querySource = NULL);
    void                        setParent(TreeNodeGenome* parent);
    
    void                        setLimits(TreeGenomeLimits *limits);
    TreeGenomeLimits*           getLimits();
    
    unsigned int                getDepth();
    unsigned int                getBranchDepth();
    
    void                        sync(TreeNodeGenome *parent = NULL);
    virtual bool                clean() { return false;} // Should return true only if the node is removed from the parent!
    
//    bool                        isChild(TreeNodeGenome* child);
//    void                        addChild(TreeNodeGenome* child);
//    TreeNodeGenome*             popChild(TreeNodeGenome* child = NULL);
//    unsigned int                getNumChildren();

    virtual bool                isTheSame(TreeNodeGenome *other, bool strictComparison = false)  = 0;
    void                        addChildrenToList(std::deque<TreeNodeGenome*> &theList);
    
    unsigned int                nodeCount();
    
    void                        crossover(RandomEngine *random, TreeNodeGenome *other);
    
    TreeNodeGenome*             getChildWithIndex(unsigned int index);
    TreeNodeGenome*             replaceChildWithNode(TreeNodeGenome *child, TreeNodeGenome *node);
    
protected:
    void                        countNodes(unsigned int &dest);
    TreeNodeGenome*             findChildWithIndex(unsigned int index, unsigned int &nodeCounter);
    
    
};

#endif /* defined(__Swarmolution__TreeNodeGenome__) */
