//
//  NSGAIIBTGenome.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 21/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAIIBTGenome.h"
#include "Nodes/NSGAIIBTComposites.h"
#include "Nodes/NSGAIIBTLeaves.h"

#include "../../../../Simulator/Simulators/DroneArena/DroneArena.h"

#include "debug.h"

NSGAIIBTGenome::NSGAIIBTGenome(unsigned int generationID):
NSGAIIGenome("BT",generationID),
BehaviorTree(1,3,1),
m_root(NULL)
{
    m_BTLimits.nInputs  =  this->BehaviorTree::getNumIn();
    m_BTLimits.nOutputs =  this->BehaviorTree::getNumOut();
    m_BTLimits.nPars    =  this->BehaviorTree::getNumPar();
    
    m_BTLimits.maxChildren  = 5;
    m_BTLimits.maxNodeCount = 100;
    m_BTLimits.maxDepth     = 5;
}
NSGAIIBTGenome::NSGAIIBTGenome(const NSGAIIBTGenome &other):
NSGAIIGenome(other),
m_BTLimits(other.m_BTLimits),
m_root(dynamic_cast<TreeNodeGenome*>(other.m_root->clone())),
BehaviorTree(other, NULL) // the BT's root will be set on te next line! m_root may be uninitialized here!
{
    BehaviorTree::m_rootNode = dynamic_cast<BTNode*>(m_root);
    m_root->setLimits(&m_BTLimits);
    m_root->sync();
}

NSGAIIBTGenome::~NSGAIIBTGenome()
{
}

void    NSGAIIBTGenome::init(RandomEngine *random)
{
    NSGAIIBTSelect *root = new NSGAIIBTSelect(NULL, &m_BTLimits);
    root->init(random);
    
    // m_root is present in this class both as part of the BehaviorTree class and as a TreeNodeGenome.
    // In principle, they point to the same tree, but under different types. Both of them must be set to root    
    BehaviorTree::m_rootNode = root;
    m_root  = root;
}
NSGAIIBTGenome* NSGAIIBTGenome::clone()
{
    return new NSGAIIBTGenome(*this);
}
void    NSGAIIBTGenome::saveGenomeAttributes(pugi::xml_node &root)
{
    BehaviorTree::saveData(root);
}
bool    NSGAIIBTGenome::loadGenomeAttributes(pugi::xml_node &root)
{
    pugi::xml_node workspaceNode = root.child("BTWorkspace");
    int numIn   = workspaceNode.attribute("nIn").as_int(NAN);
    int numOut  = workspaceNode.attribute("nOut").as_int(NAN);
    int numPar  = workspaceNode.attribute("nPar").as_int(NAN);
    
    if ( isnan(numIn) || isnan(numOut) || isnan(numPar) )
    { S_LOG("Warning: Loaded BT does not specify its IO size! Loading with defaults!\n"); }
    else
    {
        // Re-create the BTWorkspace with the loaded sizes
        delete m_workspace;
        m_workspace = new BTWorkspace(numIn,numOut,numPar);
    }
    pugi::xml_node treeNode = root.child("Tree");
    pugi::xml_node btNode   = treeNode.first_child();
    
    m_root      = getChildFromNode(btNode, NULL, &m_BTLimits);
    m_rootNode  = dynamic_cast<BTNode*>(m_root);
    
    return true;
}
void    NSGAIIBTGenome::mutateGenome(RandomEngine *rand, float pMutate, float pMacro)
{
    m_root->mutate(rand, pMutate, pMacro);
}
void    NSGAIIBTGenome::crossoverGenome(RandomEngine *rand, float pCross, NSGAIIGenome *other, NSGAIIGenome *&child1, NSGAIIGenome *&child2, unsigned int generationID)
{
    NSGAIIBTGenome *O  = dynamic_cast<NSGAIIBTGenome*>(other);
    NSGAIIBTGenome *C1 = this->clone();
    NSGAIIBTGenome *C2 = O->clone();
    
    C1->m_root->sync();
    C2->m_root->sync();
    
//    C1->m_root->crossover(rand, C2->m_root);
    
    std::deque<TreeNodeGenome*> childQueue1;
    std::deque<TreeNodeGenome*> childQueue2;
    
    C1->m_root->addChildrenToList(childQueue1);
    C2->m_root->addChildrenToList(childQueue2);
    
    
    std::deque<TreeNodeGenome*> boundaryNodes1;
    std::deque<TreeNodeGenome*> boundaryNodes2;
    
    TreeNodeGenome *branch1 = NULL;
    TreeNodeGenome *branch2 = NULL;
    
    // At the root, the number of nodes may be different. Always work on the smaller tree and add the remaining nodes as boundary nodes
    if (childQueue1.size() > childQueue2.size())
        childQueue1.swap(childQueue2);
    
    while (childQueue1.size() > 0)
    {
        branch1 = childQueue1.front();
        branch2 = childQueue2.front();
        
        childQueue1.pop_front();
        childQueue2.pop_front();
        
        if (branch1->isTheSame(branch2, true))
        {
            branch1->addChildrenToList(childQueue1);
            branch2->addChildrenToList(childQueue2);
        }
        else
        {
            boundaryNodes1.push_back(branch1);
            boundaryNodes2.push_back(branch2);
        }
    }
    // If there are any more children left in childQueue2, add them to the boundary
    boundaryNodes2.insert(boundaryNodes2.end(), childQueue2.begin(), childQueue2.end());
    
    while (boundaryNodes1.size() > 0)
    {
        // Select the fist boundary branch of the first tree
        branch1 = boundaryNodes1.front();
        boundaryNodes1.pop_front();

//        branch2 = boundaryNodes2.front();
//        boundaryNodes2.pop_front();

        // Select a boundary branch from the second tree
        // Discourage, but allow swaping of branches during crossover. The probability of selecting the corresponding (i.e. first from the list) branch is set by firstBranchSelectProbability
        {
            float firstBranchSelectProbability = 1;
            
            float stepSize = (1-firstBranchSelectProbability)/(boundaryNodes2.size()-1);
            float R = rand->getStdUniformPosReal();
            
            unsigned int childIndex = 0;;
            while (R > firstBranchSelectProbability + stepSize*childIndex)
                ++childIndex;
            
            std::deque<TreeNodeGenome*>::iterator b = boundaryNodes2.begin() + childIndex;
            
            branch2 = *b;
            
            boundaryNodes2.erase(b);
        }
        
        if (rand->getStdUniformPosReal() > pCross)
            continue;
        
        TreeNodeGenome *parent1 = branch1->getParent();
        TreeNodeGenome *parent2 = branch2->getParent();
        
        // Swap the children
        if (parent1->replaceChildWithNode(branch1, branch2) == NULL)
            throw("Crossover: couldn't find child!\n");
        
        if (parent2->replaceChildWithNode(branch2, branch1) == NULL)
            throw("Crossover: couldn't find child!\n");
        
        branch2->setParent(parent1);
        branch1->setParent(parent2);
    }
    
    child1 = C1;
    child2 = C2;
}

TreeNodeGenome* NSGAIIBTGenome::getRandomBTNode(RandomEngine *random, TreeNodeGenome* parent, TreeGenomeLimits *limits)
{
    TreeNodeGenome *ret = NULL;
    unsigned int nodeType = random->getUniformInt(0, 3);
    
    switch(nodeType)
    {
        case 0:
            ret = new NSGAIIBTSelect(parent, limits);
            break;
        case 1:
            ret = new NSGAIIBTSequence(parent, limits);
            break;
        case 2:
            ret = new NSGAIIBTConditionC(parent, limits);
            break;
        case 3:
            ret = new NSGAIIBTSetC(parent, limits);
            break;
//        case 4:
//            ret = new NSGAIIBTConditionP(parent, limits);
//            break;
//        case 5:
//            ret = new NSGAIIBTSetP(parent, limits);
//            break;
        default:
            throw("NSGAIIBTGenome::getRandomBTNode: Type ID out of range");
    }
    return ret;
}
TreeNodeGenome* NSGAIIBTGenome::getChildFromNode(pugi::xml_node &node, TreeNodeGenome *parent, TreeGenomeLimits *limits)
{
    const char* typeName = node.name();
    
    TreeNodeGenome *returnNode = NULL;
    
    if (strcmp(typeName, "BTSelect") == 0)
        returnNode = new NSGAIIBTSelect(parent, limits);
    
    else if (strcmp(typeName, "BTSequence") == 0)
        returnNode = new NSGAIIBTSequence(parent, limits);
    
    else if (strcmp(typeName, "BTConditionC") == 0)
        returnNode = new NSGAIIBTConditionC(parent, limits);
    
    else if (strcmp(typeName, "BTSetC") == 0)
        returnNode = new NSGAIIBTSetC(parent, limits);
    
    if (returnNode == NULL)
        return returnNode;
    
    if (!dynamic_cast<BTNode*>(returnNode)->loadAttributes(node))
    {
        delete returnNode;
        returnNode = NULL;
    }
    return returnNode;
}

void    NSGAIIBTGenome::evaluateGenome(std::seed_seq::result_type randomSeed)
{
    
//  DroneArena(randomSeed, ai, Logger *dLogger, trialID, numDrones, shape, vertexCount, wallLength):
    DroneArena sim(randomSeed, this, NULL, 0, 3, DroneArena::Shape::randomSize, 4, 9);
    sim.runSim(0.02, 300);
       
    
    // Copy the performance metrics as measured by the simulator
    std::vector<float> perfMetrics = sim.getPerformanceMetrics();
    
    if (m_objectiveSums.size() != perfMetrics.size() + 1)
        m_objectiveSums.resize(perfMetrics.size()+1);
    
    bool isValid = (perfMetrics[1] > 0.01); // Make sure that the def behav was used at least 1% of the time. Otherwise discard the genome
    for (int i = 0; i < perfMetrics.size(); ++i)
        m_objectiveSums[i] += perfMetrics[i];

    // The last performance metric is related to the size of the BT
    float sizeObj = 1 - ((double)m_root->nodeCount()-10)/90.f;
    if (sizeObj < 0.0)
        sizeObj = 0.0;
    
    if (sizeObj > 1)
        sizeObj = 1;

    
    m_objectiveSums.back() += sizeObj;
    if (!isValid)
    {
        for (std::vector<float>::iterator it = m_objectiveSums.begin(); it != m_objectiveSums.end(); ++it)
            (*it) = 0;
    }
    // Increase the evaluation count
    ++m_numEvaluations;
}