//
//  NSGAIIBTLeaves.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 21/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAIIBTLeaves.h"
#include "../NSGAIIBTGenome.h"

NSGAIIBTConditionC::NSGAIIBTConditionC(TreeNodeGenome *parent, TreeGenomeLimits *limits):
TreeNode("BTConditionC"),
TreeNodeGenome(parent, limits)
{
    
}
NSGAIIBTConditionC::NSGAIIBTConditionC(const NSGAIIBTConditionC &other):
TreeNode(other),
TreeNodeGenome(other),
BTConditionC(other)
{
    
}
NSGAIIBTConditionC::~NSGAIIBTConditionC()
{
    
}
    
void NSGAIIBTConditionC::init(RandomEngine *rand)
{
    BTGenomeLimits *limits = static_cast<BTGenomeLimits*>(m_Limits);
    
    m_conditionTypeMoreThan = (rand->getStdUniformPosReal() < 0.5);
//    m_conditionTypeMoreThan = false;
    
    m_compareTo             = rand->getStdUniformReal();
    m_dataIndex             = rand->getUniformInt(0, limits->nInputs + limits->nOutputs + limits->nPars-1);
//    m_dataIndex             = rand->getUniformInt(0, limits->nInputs -1);
}
void NSGAIIBTConditionC::mutate(RandomEngine *rand, float pMutate, float pMacro)
{
    float P = rand->getStdUniformPosReal();
    
    if (P < pMacro) // Macro mutation - replace this node with a randomly generated node/branch
    {
        init(rand);
//        TreeNodeGenome *HCC = NSGAIIBTGenome::getRandomBTNode(rand, m_Parent, m_Limits);
//        HCC->init(rand);
//        m_Parent->replaceChildWithNode(this, HCC);
//        
//        delete this;// Warning: this deletes the current object. Any operation after this point will result in failure!
//        return;
    }
    else if (P < pMutate)// Micro Mutation
    {
//        init(rand);
        float inc = 0.1*rand->getStdUniformReal();
        if ( ((m_compareTo + inc) < 1) && ((m_compareTo + inc) > -1) )
            m_compareTo += inc;
        else
            m_compareTo -= inc;
    }
}
bool NSGAIIBTConditionC::isTheSame(TreeNodeGenome *other, bool strictComparison)
{
    return false;
//    NSGAIIBTConditionC *O = dynamic_cast<NSGAIIBTConditionC*>(other);
//    if (O == NULL)      // It's not the same node type
//        return false;
//
//    if (strictComparison)
//    {
//        if (m_compareTo != O->m_compareTo)
//            return false;
//        
//        if (m_dataIndex != O->m_compareTo)
//            return false;
//        
//        if (m_conditionTypeMoreThan != O->m_conditionTypeMoreThan)
//            return false;
//    }
//
//    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
NSGAIIBTConditionP::NSGAIIBTConditionP(TreeNodeGenome *parent, TreeGenomeLimits *limits):
TreeNode("BTConditionP"),
TreeNodeGenome(parent, limits)
{
    
}
NSGAIIBTConditionP::NSGAIIBTConditionP(const NSGAIIBTConditionP &other):
TreeNode(other),
TreeNodeGenome(other),
BTConditionP(other)
{
    
}
NSGAIIBTConditionP::~NSGAIIBTConditionP()
{
    
}

void NSGAIIBTConditionP::init(RandomEngine *rand)
{
    BTGenomeLimits *limits = static_cast<BTGenomeLimits*>(m_Limits);
    
    m_conditionTypeMoreThan = (rand->getStdUniformPosReal() < 0.5);
    m_dataIndexFirst        = rand->getUniformInt(0, limits->nInputs + limits->nOutputs + limits->nPars-1);
    m_dataIndexSecond       = rand->getUniformInt(0, limits->nInputs + limits->nOutputs + limits->nPars-1);
    
    while (m_dataIndexFirst == m_dataIndexSecond)
        m_dataIndexSecond       = rand->getUniformInt(0, limits->nInputs + limits->nOutputs + limits->nPars-1);
}
void NSGAIIBTConditionP::mutate(RandomEngine *rand, float pMutate, float pMacro)
{
    float P = rand->getStdUniformPosReal();
    
    if (P < pMacro) // Macro mutation - replace this node with a randomly generated node/branch
    {
        TreeNodeGenome *HCC = NSGAIIBTGenome::getRandomBTNode(rand, m_Parent, m_Limits);
        HCC->init(rand);
        m_Parent->replaceChildWithNode(this, HCC);
        
        delete this;// Warning: this deletes the current object. Any operation after this point will result in failure!
        return;
    }
    else if (P < pMutate) // Micro Mutation
    {
        BTGenomeLimits *limits = static_cast<BTGenomeLimits*>(m_Limits);
        m_dataIndexSecond       = rand->getUniformInt(0, limits->nInputs + limits->nOutputs + limits->nPars-1);
        
        while (m_dataIndexFirst == m_dataIndexSecond)
            m_dataIndexSecond       = rand->getUniformInt(0, limits->nInputs + limits->nOutputs + limits->nPars-1);
        
//        if (
//            (m_dataIndexFirst > 4) || (m_dataIndexSecond > 4)
//            )
//            printf("BLA!");
    }
}
bool NSGAIIBTConditionP::isTheSame(TreeNodeGenome *other, bool strictComparison)
{
    return false;
//    NSGAIIBTConditionP *O = dynamic_cast<NSGAIIBTConditionP*>(other);
//    if (O == NULL)      // It's not the same node type
//        return false;
//    
//    if (strictComparison)
//    {
//        if (m_dataIndexFirst != O->m_dataIndexFirst)
//            return false;
//        
//        if (m_dataIndexSecond != O->m_dataIndexSecond)
//            return false;
//        
//        if (m_conditionTypeMoreThan != O->m_conditionTypeMoreThan)
//            return false;
//    }
//    
//    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
NSGAIIBTSetC::NSGAIIBTSetC(TreeNodeGenome *parent, TreeGenomeLimits *limits):
TreeNode("BTSetC"),
TreeNodeGenome(parent, limits)
{
}
NSGAIIBTSetC::NSGAIIBTSetC(TreeNodeGenome *parent, TreeGenomeLimits *limits, unsigned int dataIndex, double value):
TreeNode("BTSetC"),
TreeNodeGenome(parent, limits),
BTSetC(dataIndex, value)
{
    
}
NSGAIIBTSetC::NSGAIIBTSetC(const NSGAIIBTSetC &other):
TreeNode(other),
TreeNodeGenome(other),
BTSetC(other)
{
    
}
NSGAIIBTSetC::~NSGAIIBTSetC()
{
    
}

void NSGAIIBTSetC::init(RandomEngine *rand)
{
    BTGenomeLimits *limits = static_cast<BTGenomeLimits*>(m_Limits);
    
    m_setTypeAbsolute       = true; // No incremental SET is allowed!
    m_dataIndex             = rand->getUniformInt(0, limits->nOutputs + limits->nPars-1);
    m_value                 = rand->getStdUniformReal();

}
void NSGAIIBTSetC::mutate(RandomEngine *rand, float pMutate, float pMacro)
{
    float P = rand->getStdUniformPosReal();
    
    if (P < pMacro) // Macro mutation - replace this node with a randomly generated node/branch
    {
        init(rand);
//        TreeNodeGenome *HCC = NSGAIIBTGenome::getRandomBTNode(rand, m_Parent, m_Limits);
//        HCC->init(rand);
//        m_Parent->replaceChildWithNode(this, HCC);
//        
//        delete this;// Warning: this deletes the current object. Any operation after this point will result in failure!
//        return;
    }
    else if (P < pMutate) // Micro Mutation
    {
//        init(rand);
        float inc = 0.1*rand->getStdUniformReal();
        if ( ((m_value + inc) < 1) && ((m_value + inc) > -1) )
            m_value += inc;
        else
            m_value -= inc;
    }
}
bool NSGAIIBTSetC::isTheSame(TreeNodeGenome *other, bool strictComparison)
{
    return false;
//   NSGAIIBTSetC *O = dynamic_cast<NSGAIIBTSetC*>(other);
//    if (O == NULL)      // It's not the same node type
//        return false;
//    
//    if (strictComparison)
//    {
//        if (m_dataIndex != O->m_dataIndex)
//            return false;
//        
//        if (m_value != O->m_value)
//            return false;
//        
//        if (m_setTypeAbsolute != O->m_setTypeAbsolute)
//            return false;
//    }
//    
//    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
NSGAIIBTSetP::NSGAIIBTSetP(TreeNodeGenome *parent, TreeGenomeLimits *limits):
TreeNode("BTSetP"),
TreeNodeGenome(parent, limits)
{
    
}
NSGAIIBTSetP::NSGAIIBTSetP(const NSGAIIBTSetP &other):
TreeNode(other),
TreeNodeGenome(other),
BTSetP(other)
{
    
}
NSGAIIBTSetP::~NSGAIIBTSetP()
{
    
}
    
void NSGAIIBTSetP::init(RandomEngine *rand)
{
    BTGenomeLimits *limits = static_cast<BTGenomeLimits*>(m_Limits);
    
    m_setTypeAbsolute       = true; // No incremental SET is allowed!
    m_dataIndex             = rand->getUniformInt(0, limits->nOutputs + limits->nPars-1);
    m_value                 = rand->getUniformInt(0, limits->nOutputs + limits->nPars-1);

    while (m_dataIndex == m_value)
        m_value                 = rand->getUniformInt(0, limits->nOutputs + limits->nPars-1);
}
void NSGAIIBTSetP::mutate(RandomEngine *rand, float pMutate, float pMacro)
{
    float P = rand->getStdUniformPosReal();
    
    if (P < pMacro) // Macro mutation - replace this node with a randomly generated node/branch
    {
        TreeNodeGenome *HCC = NSGAIIBTGenome::getRandomBTNode(rand, m_Parent, m_Limits);
        HCC->init(rand);
        m_Parent->replaceChildWithNode(this, HCC);
        
        delete this;// Warning: this deletes the current object. Any operation after this point will result in failure!
        return;
    }
    else if (P < pMutate) // Micro Mutation
    {
        BTGenomeLimits *limits = static_cast<BTGenomeLimits*>(m_Limits);
        m_value                 = rand->getUniformInt(0, limits->nOutputs + limits->nPars);
        
        while (m_dataIndex == m_value)
            m_value                 = rand->getUniformInt(0, limits->nOutputs + limits->nPars);
    }
}
bool NSGAIIBTSetP::isTheSame(TreeNodeGenome *other, bool strictComparison)
{
    return false;
    
//    NSGAIIBTSetP *O = dynamic_cast<NSGAIIBTSetP*>(other);
//    if (O == NULL)      // It's not the same node type
//        return false;
//    
//    if (strictComparison)
//    {
//        if (m_dataIndex != O->m_dataIndex)
//            return false;
//        
//        if (m_value != O->m_value)
//            return false;
//        
//        if (m_setTypeAbsolute != O->m_setTypeAbsolute)
//            return false;
//    }
    
    return true;
}