//
//  NSGAIIBTComposites.h
//  Swarmolution
//
//  Created by Tamas Szabo on 21/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__NSGAIIBTComposites__
#define __Swarmolution__NSGAIIBTComposites__

#include "../../Nodes/TreeNodeGenome.h"
#include "../../../../../AI/Models/BehaviorTrees/Nodes/BTComposites.h"

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class NSGAIIBTSelect: virtual public TreeNodeGenome,  virtual public BTSelect
{
public:
    NSGAIIBTSelect(TreeNodeGenome *parent, TreeGenomeLimits *limits);
    NSGAIIBTSelect(const NSGAIIBTSelect &other);
    ~NSGAIIBTSelect();
    
    virtual NSGAIIBTSelect*     clone();
    void                init(RandomEngine *random);
    void                mutate(RandomEngine *rand, float pMutate, float pMacro);
    
    bool                isTheSame(TreeNodeGenome *other, bool strictComparison = false);
    bool                loadAttributes(pugi::xml_node &node);
    
    virtual bool        clean();
};

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class NSGAIIBTSequence: virtual public TreeNodeGenome,  virtual public BTSequence
{
public:
    NSGAIIBTSequence(TreeNodeGenome *parent, TreeGenomeLimits *limits);
    NSGAIIBTSequence(const NSGAIIBTSequence &other);
    ~NSGAIIBTSequence();
    
    NSGAIIBTSequence*   clone();
    void                init(RandomEngine *random);
    void                mutate(RandomEngine *rand, float pMutate, float pMacro);
    
    bool                isTheSame(TreeNodeGenome *other, bool strictComparison = false);
    bool                loadAttributes(pugi::xml_node &node);
    
    virtual bool        clean();
};
#include <stdio.h>



#endif /* defined(__Swarmolution__NSGAIIBTComposites__) */
