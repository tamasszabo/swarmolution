//
//  NSGAIIBTComposites.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 21/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "NSGAIIBTComposites.h"

#include "../NSGAIIBTGenome.h"

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
NSGAIIBTSelect::NSGAIIBTSelect(TreeNodeGenome *parent, TreeGenomeLimits *limits):
TreeNode("BTSelect"),
TreeNodeGenome(parent, limits)
{}
NSGAIIBTSelect::NSGAIIBTSelect(const NSGAIIBTSelect &other):
TreeNode(other),
TreeNodeGenome(other),
BTSelect(other)
{}
NSGAIIBTSelect::~NSGAIIBTSelect()
{}

NSGAIIBTSelect*     NSGAIIBTSelect::clone()
{ return new NSGAIIBTSelect(*this); }

void                NSGAIIBTSelect::init(RandomEngine *random)
{
    int myDepth = getDepth();
    
    while (m_children.size() < m_Limits->maxChildren)
    {
        TreeNodeGenome* newNode = NSGAIIBTGenome::getRandomBTNode(random, this, m_Limits);
        if (myDepth >= m_Limits->maxDepth-1)
        {
            while(dynamic_cast<BTNode*>(newNode)->isComposite())
            {
                delete newNode;
                newNode = NSGAIIBTGenome::getRandomBTNode(random, this, m_Limits);
            }
        }
        newNode->init(random);
        m_children.push_back(newNode);
    }
}
void NSGAIIBTSelect::mutate(RandomEngine *rand, float pMutate, float pMacro)
{
    float P = rand->getStdUniformPosReal();
    
    if ((P < pMacro) && (m_Parent != NULL)) // Macro mutation - replace this node with a randomly generated node/branch
    {
        TreeNodeGenome *HCC = NSGAIIBTGenome::getRandomBTNode(rand, m_Parent, m_Limits);
        HCC->init(rand);
        m_Parent->replaceChildWithNode(this, HCC);
        
        delete this; // Warning: this deletes the current object. Any operation after this point will result in failure!
        return;
    }
    else if (P < pMutate)// Micro Mutation
    {
        int mutationType;
        
        if (getDepth() >= m_Limits->maxDepth-2)
            mutationType= rand->getUniformInt(1, 2);
        else if (m_Parent == NULL)
            mutationType= rand->getUniformInt(0, 1);
        else
            mutationType= rand->getUniformInt(0, 2);
        
        if ( ( ( (mutationType == 0) || (m_children.size() < 2) ) ) && (m_children.size() < m_Limits->maxChildren) ) // Add random (HCC) child
        {
            TreeNodeGenome* newChild = NSGAIIBTGenome::getRandomBTNode(rand, this, m_Limits);
            
            if (getDepth() > m_Limits->maxDepth-1)
            {
                while (dynamic_cast<BTNode*>(newChild)->isComposite())
                {
                    delete newChild;
                    newChild = NSGAIIBTGenome::getRandomBTNode(rand, this, m_Limits);
                }
            }
            
            newChild->sync(this);
            
            int newChildLocation = rand->getUniformInt(0, (int)m_children.size());
            m_children.insert(m_children.begin()+newChildLocation, newChild);
        }
        else if( mutationType == 1) // Swap two children
        {
            int cIndex1 = rand->getUniformInt(0, (int)(m_children.size()-1));
            int cIndex2 = cIndex1;
            while (cIndex1 == cIndex2)
                cIndex2 = rand->getUniformInt(0, (int)(m_children.size()-1));
            
            TreeNode *tmp = m_children[cIndex1];
            m_children[cIndex1] = m_children[cIndex2];
            m_children[cIndex2] = tmp;
        }
        else    // Remove a child
        {
            int rcIndex = rand->getUniformInt(0, (int)m_children.size()-1);
            std::vector<TreeNode*>::iterator it = m_children.begin() + rcIndex;
            
            delete (*it);
            m_children.erase(it);
        }
    }
    
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        dynamic_cast<TreeNodeGenome*>(*it)->mutate(rand, pMutate, pMacro);
}
bool NSGAIIBTSelect::isTheSame(TreeNodeGenome *other, bool strictComparison)
{
    if (m_children.size() != other->getNumChildren()) // The number of children differs => Not the same node!
        return false;

    if (strictComparison)
    {
        if (strcmp(m_nodeTypeName, other->getTypeName()) != 0)
            return false;
    }
    
    return true;
}
bool    NSGAIIBTSelect::loadAttributes(pugi::xml_node &node)
{
    for (pugi::xml_node_iterator childNode = node.begin(); childNode != node.end(); ++childNode)
    {
        m_children.push_back(NSGAIIBTGenome::getChildFromNode((*childNode), this, m_Limits));
        if (m_children.back() == NULL)
        {
            m_children.pop_back();
            return false;
        }
    }
    
    return true;
}

bool  NSGAIIBTSelect::clean()
{
    for (int i = 0; i < m_children.size(); ++i)
    {
        if (dynamic_cast<TreeNodeGenome*>(m_children[i])->clean())
            --i; // Move the iterator back one position as the current node has been removed!
    }
    
    if ( (m_children.size() == 1) && (m_Parent != NULL) )
    {
        m_Parent->replaceChildWithNode(this, dynamic_cast<TreeNodeGenome*>(m_children.front()));
        m_children.pop_back();
        delete this;
        return true;
    }
    if ( (m_children.size() == 0) && (m_Parent != NULL) )
    {
        m_Parent->popChild(this);
        delete this;
        return true;
    }
    
    return false;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
NSGAIIBTSequence::NSGAIIBTSequence(TreeNodeGenome *parent, TreeGenomeLimits *limits):
TreeNode("BTSequence"),
TreeNodeGenome(parent, limits)
{}
NSGAIIBTSequence::NSGAIIBTSequence(const NSGAIIBTSequence &other):
TreeNode(other),
TreeNodeGenome(other),
BTSequence(other)
{}
NSGAIIBTSequence::~NSGAIIBTSequence()
{}

NSGAIIBTSequence*     NSGAIIBTSequence::clone()
{ return new NSGAIIBTSequence(*this); }

void NSGAIIBTSequence::init(RandomEngine *random)
{
    int myDepth = getDepth();
    
    while (m_children.size() < m_Limits->maxChildren)
    {
        TreeNodeGenome* newNode = NSGAIIBTGenome::getRandomBTNode(random, this, m_Limits);
        if (myDepth >= m_Limits->maxDepth-1)
        {
            while(dynamic_cast<BTNode*>(newNode)->isComposite())
            {
                delete newNode;
                newNode = NSGAIIBTGenome::getRandomBTNode(random, this, m_Limits);
            }
        }
        newNode->init(random);
        m_children.push_back(newNode);
    }
}
void NSGAIIBTSequence::mutate(RandomEngine *rand, float pMutate, float pMacro)
{
    float P = rand->getStdUniformPosReal();
    
    if (P < pMacro) // Macro mutation - replace this node with a randomly generated node/branch
    {
        TreeNodeGenome *HCC = NSGAIIBTGenome::getRandomBTNode(rand, m_Parent, m_Limits);
        HCC->init(rand);
        m_Parent->replaceChildWithNode(this, HCC);
        
        delete this;// Warning: this deletes the current object. Any operation after this point will result in failure!
        return;
    }
    else if (P < pMutate)// Micro Mutation
    {
        
        int mutationType;
        
        if (getDepth() >= m_Limits->maxDepth-2)
            mutationType= rand->getUniformInt(1, 2);
        else if (m_Parent == NULL)
            mutationType= rand->getUniformInt(0, 1);
        else
            mutationType= rand->getUniformInt(0, 2);
        
        if ( ( ( (mutationType == 0) || (m_children.size() < 2) ) ) && (m_children.size() < m_Limits->maxChildren) ) // Add random (HCC) child
        {
            TreeNodeGenome* newChild = NSGAIIBTGenome::getRandomBTNode(rand, this, m_Limits);
            
            if (getDepth() > m_Limits->maxDepth-1)
            {
                while (dynamic_cast<BTNode*>(newChild)->isComposite())
                {
                    delete newChild;
                    newChild = NSGAIIBTGenome::getRandomBTNode(rand, this, m_Limits);
                }
            }
            
            newChild->sync(this);
            
            int newChildLocation = rand->getUniformInt(0, (int)m_children.size());
            m_children.insert(m_children.begin()+newChildLocation, newChild);
        }
        else if( mutationType == 1) // Swap two children
        {
            int cIndex1 = rand->getUniformInt(0, (int)(m_children.size()-1));
            int cIndex2 = cIndex1;
            while (cIndex1 == cIndex2)
                cIndex2 = rand->getUniformInt(0, (int)(m_children.size()-1));
            
            TreeNode *tmp = m_children[cIndex1];
            m_children[cIndex1] = m_children[cIndex2];
            m_children[cIndex2] = tmp;
        }
        else    // Remove a child
        {
            int rcIndex = rand->getUniformInt(0, (int)m_children.size()-1);
            std::vector<TreeNode*>::iterator it = m_children.begin() + rcIndex;
            
            delete (*it);
            m_children.erase(it);
        }
    }
    
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        dynamic_cast<TreeNodeGenome*>(*it)->mutate(rand, pMutate, pMacro);
}
bool NSGAIIBTSequence::isTheSame(TreeNodeGenome *other, bool strictComparison)
{
    if (m_children.size() != other->getNumChildren()) // The number of children differs => Not the same node!
        return false;

    if (strictComparison)
    {
        if (strcmp(m_nodeTypeName, other->getTypeName()) != 0)
            return false;
    }
    
    return true;
}
bool    NSGAIIBTSequence::loadAttributes(pugi::xml_node &node)
{
    for (pugi::xml_node_iterator childNode = node.begin(); childNode != node.end(); ++childNode)
    {
        m_children.push_back(NSGAIIBTGenome::getChildFromNode((*childNode), this, m_Limits));
        if (m_children.back() == NULL)
        {
            m_children.pop_back();
            return false;
        }
    }
    
    return true;
}
bool  NSGAIIBTSequence::clean()
{
    for (int i = 0; i < m_children.size(); ++i)
    {
        if (dynamic_cast<TreeNodeGenome*>(m_children[i])->clean())
            --i; // Move the iterator back one position as the current node has been removed!
    }
    
    // There is a single node in the composite
    if ( (m_children.size() == 1) && (m_Parent != NULL) )
    {
        m_Parent->replaceChildWithNode(this, dynamic_cast<TreeNodeGenome*>(m_children.front()));
        m_children.pop_back();
        delete this;
        return true;
    }
    // The node has no children
    if ( (m_children.size() == 0) && (m_Parent != NULL) )
    {
        m_Parent->popChild(this);
        delete this;
        return true;
    }
    
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        // Sequence under Sequence is redundant.
        if (!strcmp((*it)->getTypeName(),m_nodeTypeName))
        {
            NSGAIIBTSequence *child = dynamic_cast<NSGAIIBTSequence*>(*it);
            
            it = m_children.erase(it);
            it = m_children.insert(it, child->m_children.begin(), child->m_children.end());
            
            // Remove the children of the child, so that when child is destroyed, it won't destroy its own children.
            while(child->m_children.size() > 0)
                child->popChild(NULL);
            
            delete child;
        }
    }
    
    return false;
}