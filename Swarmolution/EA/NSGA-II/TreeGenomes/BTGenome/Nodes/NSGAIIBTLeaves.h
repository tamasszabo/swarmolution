//
//  NSGAIIBTLeaves.h
//  Swarmolution
//
//  Created by Tamas Szabo on 21/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__NSGAIIBTLeaves__
#define __Swarmolution__NSGAIIBTLeaves__

#include <stdio.h>
#include "../../Nodes/TreeNodeGenome.h"
#include "../../../../../AI/Models/BehaviorTrees/Nodes/BTLeaves.h"


class NSGAIIBTConditionC: virtual public TreeNodeGenome, virtual public BTConditionC
{
public:
    NSGAIIBTConditionC(TreeNodeGenome *parent, TreeGenomeLimits *limits);
    NSGAIIBTConditionC(const NSGAIIBTConditionC &other);
    ~NSGAIIBTConditionC();
    
    NSGAIIBTConditionC* clone() { return new NSGAIIBTConditionC(*this); }
    
    void                init(RandomEngine *rand);
    void                mutate(RandomEngine *rand, float pMutate, float pMacro);
    
    bool                isTheSame(TreeNodeGenome *other, bool strictComparison = false);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class NSGAIIBTConditionP: virtual public TreeNodeGenome, virtual public BTConditionP
{
public:
    NSGAIIBTConditionP(TreeNodeGenome *parent, TreeGenomeLimits *limits);
    NSGAIIBTConditionP(const NSGAIIBTConditionP &other);
    ~NSGAIIBTConditionP();
    
    NSGAIIBTConditionP* clone() { return new NSGAIIBTConditionP(*this); }
    
    void                init(RandomEngine *rand);
    void                mutate(RandomEngine *rand, float pMutate, float pMacro);
    
    bool                isTheSame(TreeNodeGenome *other, bool strictComparison = false);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class NSGAIIBTSetC: virtual public TreeNodeGenome, virtual public BTSetC
{
public:
    NSGAIIBTSetC(TreeNodeGenome *parent, TreeGenomeLimits *limits);
    NSGAIIBTSetC(TreeNodeGenome *parent, TreeGenomeLimits *limits, unsigned int dataIndex, double value);
    NSGAIIBTSetC(const NSGAIIBTSetC &other);
    ~NSGAIIBTSetC();
    
    NSGAIIBTSetC*       clone() { return new NSGAIIBTSetC(*this); }
    
    void                init(RandomEngine *rand);
    void                mutate(RandomEngine *rand, float pMutate, float pMacro);
    
    bool                isTheSame(TreeNodeGenome *other, bool strictComparison = false);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class NSGAIIBTSetP: virtual public TreeNodeGenome, virtual public BTSetP
{
public:
    NSGAIIBTSetP(TreeNodeGenome *parent, TreeGenomeLimits *limits);
    NSGAIIBTSetP(const NSGAIIBTSetP &other);
    ~NSGAIIBTSetP();
    
    NSGAIIBTSetP*       clone() { return new NSGAIIBTSetP(*this); }
    
    void                init(RandomEngine *rand);
    void                mutate(RandomEngine *rand, float pMutate, float pMacro);
    
    bool                isTheSame(TreeNodeGenome *other, bool strictComparison = false);
};
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//class NSGAIIBTProdP: virtual public TreeNodeGenome, virtual public BTProdP
//{
//};
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//class NSGAIIBTSetProportional: virtual public TreeNodeGenome, virtual public BTSetProportional
//{
//    
//};

#endif /* defined(__Swarmolution__NSGAIIBTLeaves__) */
