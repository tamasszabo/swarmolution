//
//  NSGAIIBTGenome.h
//  Swarmolution
//
//  Created by Tamas Szabo on 21/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__NSGAIIBTGenome__
#define __Swarmolution__NSGAIIBTGenome__

#include <stdio.h>
#include "../../../Core/NSGAIIGenome.h"
#include "../../../../AI/Models/BehaviorTrees/BehaviorTree.h"

#include "../Nodes/TreeNodeGenome.h"

struct BTGenomeLimits: public TreeGenomeLimits
{
    unsigned int nInputs;
    unsigned int nOutputs;
    unsigned int nPars;
    
    BTGenomeLimits()
    {
        
    }
    BTGenomeLimits(const BTGenomeLimits &other):
    TreeGenomeLimits(other),
    nInputs(other.nInputs),
    nOutputs(other.nOutputs),
    nPars(other.nPars)
    {
    }
};

class NSGAIIBTGenome: public NSGAIIGenome, public BehaviorTree
{
protected:
    TreeNodeGenome*     m_root;
    BTGenomeLimits      m_BTLimits;
    
public:
    NSGAIIBTGenome(unsigned int generationID);
    NSGAIIBTGenome(const NSGAIIBTGenome &other);
    virtual ~NSGAIIBTGenome();
    
    
public: // public virtual functions
    void    init(RandomEngine *random);
    NSGAIIBTGenome* clone();

    virtual void    saveGenomeAttributes(pugi::xml_node &root);
    virtual bool    loadGenomeAttributes(pugi::xml_node &root);
    
protected: // protected virtual members
    void    mutateGenome(RandomEngine *rand, float pMutate, float pMacro);
    void    crossoverGenome(RandomEngine *rand, float pCross, NSGAIIGenome *other, NSGAIIGenome *&child1, NSGAIIGenome *&child2, unsigned int generationID);
    void    evaluateGenome(std::seed_seq::result_type randomSeed);
    
    
public:
    static TreeNodeGenome* getRandomBTNode(RandomEngine *random, TreeNodeGenome* parent, TreeGenomeLimits *limits);
    static TreeNodeGenome* getChildFromNode(pugi::xml_node &node, TreeNodeGenome *parent, TreeGenomeLimits *limits);
};

#endif /* defined(__Swarmolution__NSGAIIBTGenome__) */
