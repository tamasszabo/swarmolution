//
//  consoleMacros.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef Swarmolution_consoleMacros_h
#define Swarmolution_consoleMacros_h

#define DEBUG_ENABLED
//#define LOG_DETAIL
//#define LOG_PROGRESS
#define LOG_STATS

#ifdef DEBUG_ENABLED
    #define D_LOG(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
    #define D_LOG(fmt, ...)
#endif  // #ifdef DEBUG_ENABLED

#ifdef LOG_DETAIL
    #define DETAIL_LOG(fmt, ...) printf(fmt, ##__VA_ARGS__)
    #define P_LOG(fmt, ...) printf(fmt, ##__VA_ARGS__)
    #define S_LOG(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
    #define DETAIL_LOG(fmt, ...)

    #ifdef LOG_PROGRESS
        #define P_LOG(fmt, ...) printf(fmt, ##__VA_ARGS__)
        #define S_LOG(fmt, ...) printf(fmt, ##__VA_ARGS__)
    #else
        #define P_LOG(fmt, ...)
        #ifdef LOG_STATS
            #define S_LOG(fmt, ...) {time_t rawtime; struct tm * timeinfo; time (&rawtime); timeinfo = localtime (&rawtime); printf("%d/%d %02d:%02d:%02d: ",timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min, timeinfo->tm_sec); printf(fmt, ##__VA_ARGS__);}
        #else
            #define S_LOG(fmt, ...)
        #endif  // #ifdef LOG_STATS
    #endif  // #ifdef LOG_PROGRESS

#endif  // #ifdef LOG_DETAIL

#endif  // #ifndef Swarmolution_consoleMacros_h
