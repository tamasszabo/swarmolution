//
//  Ray.h
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__Ray__
#define __Swarmolution__Ray__

#include <stdio.h>

#include "Point.h"
#include "Segment.h"

struct Ray
{
    Point  P0;
    gVector direction;

                Ray();
                Ray(Point p0, Point p1);
                Ray(Point p0, gVector dir);
                Ray(Point p0, double dir);
                Ray(Point p0, double az, double el);


    void        invert();
    double      intersect2(Segment S, Point &Ps);
    double      intersect2(Segment S, Point &Ps, double &tS, bool extendSegmentToLine = false);
    double      distance(Segment S);
};
#endif /* defined(__Swarmolution__Ray__) */
