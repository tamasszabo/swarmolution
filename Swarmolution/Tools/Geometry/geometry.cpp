///* 
// * File:   geometry.cpp
// * Author: tszabo
// * 
// * Created on October 2, 2014, 14:35 AM
// * 
// * Most algorithms in this collection are from: http://geomalgorithms.com/
// * 
// */
//
//#include "geometry.h"
//#include <limits>
//#include "consoleMacros.h"
////
////int Line::closestPoint(Point P[], int n)
////// Function taken and modified from http://geomalgorithms.com/
////{
////    // Get coefficients of the implicit line equation.
////    // Do NOT normalize since scaling by a constant
////    // is irrelevant for just comparing distances.
////    float a = P0.y - P1.y;
////    float b = P1.x - P0.x;
////    float c = P0.x * P1.y - P1.x * P0.y;
////
////    // initialize min index and distance to P[0]
////    int mi = 0;
////    float min = a * P[0].x + b * P[0].y + c;
////    if (min < 0) min = -min;     // absolute value
////
////    // loop through Point array testing for min distance to L
////    for (int i=1; i<n; i++) {
////         // just use dist squared (sqrt not  needed for comparison)
////         float dist = a * P[i].x + b * P[i].y  + c;
////         if (dist < 0) dist = -dist;    // absolute value
////         if (dist < min) {      // this point is closer
////              mi = i;              // so have a new minimum
////              min = dist;
////         }
////    }
////    return mi;     // the index of the closest  Point P[mi]
////}
////double Line::distance(Point& P)
////// Function taken and modified from http://geomalgorithms.com/
////{
////     Vector v = P1 - P0;
////     Vector w = P - P0;
////
////     double c1 = w*v;
////     double c2 = v*v;
////     double b = c1 / c2;
////
////     Point Pb = P0 + (v*b);
////     return ((Vector)(P- Pb)).norm();
////}
////
////double Segment::distance(const Point& P)
////// Function taken and modified from http://geomalgorithms.com/
////{
////     Vector v(P0,P1);
////     Vector w(P0,P);
////
////     double c1 = w*v;
////     if ( c1 <= 0 )
////          return (Vector(P0,P)).norm();
////
////     double c2 = v*v;
////     if ( c2 <= c1 )
////          return (Vector(P1,P)).norm();
////
////     double b = c1 / c2;
////     
////     Point Pb = P0 + (v*b);
////     return (Vector(Pb,P)).norm();
////}
////bool Segment::inSegment(const Point& P)
////// Function taken and modified from http://geomalgorithms.com/
////{
////    if (P0.x != P1.x) 
////    {    // S is not  vertical
////        if (P0.x <= P.x && P.x <= P1.x)
////            return 1;
////        
////        if (P0.x >= P.x && P.x >= P1.x)
////            return 1;
////    }
////    else 
////    {    // S is vertical, so test y  coordinate
////        if (P0.y <= P.y && P.y <= P1.y)
////            return 1;
////        
////        if (P0.y >= P.y && P.y >= P1.y)
////            return 1;
////    }
////    return 0;
////}
////int Segment::intersect2(Segment &S2, Point &I0, Point &I1 )
////// Function taken and modified from http://geomalgorithms.com/
////// NOTE: S1 in the comments refers to the current segment!
//////    Input:  finite segment: S2
//////    Output: *I0 = intersect point (when it exists)
//////            *I1 =  endpoint of intersect segment [I0,I1] (when it exists)
//////    Return: 0=disjoint (no intersect)
//////            1=intersect  in unique point I0
//////            2=overlap  in segment from I0 to I1
////{
////    Vector    u = P1    - P0;
////    Vector    v = S2.P1 - S2.P0;
////    Vector    w = P0    - S2.P0;
////    double    D = u.perp2(v);
////
////    // test if  they are parallel (includes either being a point)
////    if (fabs(D) < GEOMETRY_SMALL_NUMBER) {           // S1 and S2 are parallel
////        if (u.perp2(w) != 0 || v.perp2(w) != 0)  {
////            return 0;                    // they are NOT collinear
////        }
////        // they are collinear or degenerate
////        // check if they are degenerate  points
////        double du = u*u;
////        double dv = v*v;
////        if (du==0 && dv==0) {            // both segments are points
////            if (P0 !=  S2.P0)         // they are distinct  points
////                 return 0;
////            I0 = P0;                 // they are the same point
////            return 1;
////        }
////        if (du==0) {                     // S1 is a single point
////            if  (S2.inSegment(P0) == 0)  // but is not in S2
////                 return 0;
////            I0 = P0;
////            return 1;
////        }
////        if (dv==0) {                     // S2 a single point
////            if (inSegment(S2.P0) == 0)  // but is not in S1
////                 return 0;
////            I0 = S2.P0;
////            return 1;
////        }
////        // they are collinear segments - get  overlap (or not)
////        double t0, t1;                    // endpoints of S1 in eqn for S2
////        Vector w2 = P1 - S2.P0;
////        if (v.x != 0) {
////                 t0 = w.x / v.x;
////                 t1 = w2.x / v.x;
////        }
////        else {
////                 t0 = w.y / v.y;
////                 t1 = w2.y / v.y;
////        }
////        if (t0 > t1) {                   // must have t0 smaller than t1
////                 float t=t0; t0=t1; t1=t;    // swap if not
////        }
////        if (t0 > 1 || t1 < 0) {
////            return 0;      // NO overlap
////        }
////        t0 = t0<0? 0 : t0;               // clip to min 0
////        t1 = t1>1? 1 : t1;               // clip to max 1
////        if (t0 == t1) {                  // intersect is a point
////            I0 = S2.P0 +  (v*t0);
////            return 1;
////        }
////
////        // they overlap in a valid subsegment
////        I0 = S2.P0 + (v*t0);
////        I1 = S2.P0 + (v*t0);
////        return 2;
////    }
////
////    // the segments are skew and may intersect in a point
////    // get the intersect parameter for S1
////    double     sI = v.perp2(w) / D;
////    if (sI < 0 || sI > 1)                // no intersect with S1
////        return 0;
////
////    // get the intersect parameter for S2
////    double     tI = u.perp2(w) / D;
////    if (tI < 0 || tI > 1)                // no intersect with S2
////        return 0;
////
////    I0 = P0 + (u*sI);                // compute S1 intersect point
////    return 1;
////}
////int Segment::intersect2(Ray &R, Point& I0)
////{
////    Point p1, p2;
////    p1.x = P0.x;
////    p2.x = P1.x;
////    
////    int onRay1 = R.getY(p1);
////    int onRay2 = R.getY(p2);
////    
////    if (!(onRay1 || onRay2)) { I0 = P0; return 0; }
////    
////    if (!onRay1)
////        p1 = R.P0;
////    if (!onRay2)
////        p2 = R.P0;
////    
////    Segment S(p1,p2);
////    
////    Point P;
////    return intersect2(S, I0,P);
////}
////
////
////int Ray::getX(double& x, const double& y, const double& z)
////{
////    if ( (direction.x != 0) && (direction.y == 0) && (direction.z == 0) && (y == P0.y) && (z == P0.z)) { x = P0.x; return 2;}
////    
////    double c1 = (y - P0.y)/direction.y;
////    double c2 = (z - P0.z)/direction.z;
////    
////    if ( 
////            (((c1 - c2) > GEOMETRY_SMALL_NUMBER) && ((c2 - c1) > GEOMETRY_SMALL_NUMBER)) ||
////            ((c1 != c1) && (c2 != c2))
////       )
////        return 0;
////    
////    if (c1 != c1)
////        x = P0.x + c2*direction.x;
////    else
////        x = P0.x + c1*direction.x;
////        
////    return 1;
////}
////int Ray::getY(const double& x, double& y, const double& z)
////{
////    if ( (direction.x == 0) && (direction.y != 0) && (direction.z == 0) && (x == P0.x) && (z == P0.z)) { y = P0.y; return 2;}
////    
////    double c1 = (x - P0.x)/direction.x;
////    double c2 = (z - P0.z)/direction.z;
////    
////    if ( 
////            (((c1 - c2) > GEOMETRY_SMALL_NUMBER) && ((c2 - c1) > GEOMETRY_SMALL_NUMBER)) ||
////            ((c1 != c1) && (c2 != c2))
////       )
////        
////        return 0;
////    
////    if (c1 != c1)
////        y = P0.y + c2*direction.y;
////    else
////        y = P0.y + c1*direction.y;
////        
////    return 1;
////}
////int Ray::getZ(const double& x, const double& y, double& z)
////{
////    if ( (direction.x == 0) && (direction.y == 0) && (direction.z != 0) && (x == P0.x) && (y == P0.y)) { z = P0.z; return 2;}
////    
////    double c1 = (y - P0.y)/direction.y;
////    double c2 = (x - P0.x)/direction.x;
////    
////    if ( 
////            (((c1 - c2) > GEOMETRY_SMALL_NUMBER) && ((c2 - c1) > GEOMETRY_SMALL_NUMBER)) ||
////            ((c1 != c1) && (c2 != c2))
////       )
////        return 0;
////    
////    if (c1 != c1)
////        z = P0.z + c2*direction.z;
////    else
////        z = P0.z + c1*direction.z;
////        
////    return 1;
////}
////bool Ray::getT(const double& x, const double& y, const double& z, double& T)
////{
////    double c1 = (y - P0.x)/direction.x;
////    double c2 = (y - P0.y)/direction.y;
////    double c3 = (y - P0.z)/direction.z;
////    
////    if (
////            ( ((c1 - c2) > GEOMETRY_SMALL_NUMBER) && ((c2 - c1) > GEOMETRY_SMALL_NUMBER) ) ||
////            ( ((c1 - c3) > GEOMETRY_SMALL_NUMBER) && ((c3 - c1) > GEOMETRY_SMALL_NUMBER) ) ||
////            ( c1 < 0 ) ||
////            (c1 != c1)
////       )
////    { return false; }
////    
////    T = c1;
////    return true;
////}
////
////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Polygon Class Members ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////Polygon::Polygon()
////{
////    this->locked = false;
////}
////Polygon::~Polygon() {}
////
////bool Polygon::addVertex(const Point& pt)
////{
////    if ((pt.z != 0) || locked)
////        return false;
////    
////    vertices.push_back(pt);
////    return true;
////}
////
////void Polygon::lock()
////{
////    
////    simplify();
////    
////    min = vertices.front();
////    max = vertices.front();
////    
////    for (std::vector<Point>::iterator it = vertices.begin() ; it != vertices.end(); ++it)
////    {
////        if (it->x > max.x) { max.x = it->x; }
////        if (it->y > max.y) { max.y = it->y; }
////        
////        if (it->x < min.x) { min.x = it->x; }
////        if (it->y < min.y) { min.y = it->y; }
////    }
////
////    vertices.push_back(vertices.front());
////    
////    locked = true;
////}
////void Polygon::clear()
////{
////    locked = false;
////    vertices.clear();
////}
////
////bool Polygon::enclosingRect(Point& min, Point& max)
////{
////    if (!locked) return false;
////    
////    min = this->min;
////    max = this->max;
////    return true;
////}
////bool Polygon::pointInPoly(const Point& P)
////// Winding number test for a point in a polygon
//////      Input:   P = a point,
//////               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//////      Return:  wn = the winding number (=0 only when P is outside)
////
////{
////    int    wn = 0;    // the  winding number counter
////
////    // loop through all edges of the polygon
////    for (int i=0; i < vertices.size()-1; i++) {   // edge from V[i] to  V[i+1]
////        if (vertices[i].y <= P.y) {          // start y <= P.y
////            if (vertices[i+1].y  > P.y)      // an upward crossing
////                 if ( Line(vertices[i], vertices[i+1]).isLeft(P) > 0)  // P left of  edge
////                     ++wn;            // have  a valid up intersect
////        }
////        else {                        // start y > P.y (no test needed)
////            if (vertices[i+1].y  <= P.y)     // a downward crossing
////                 if (Line( vertices[i], vertices[i+1]).isLeft(P) < 0)  // P right of  edge
////                     --wn;            // have  a valid down intersect
////        }
////    }
////    if (wn == 0) return false;
////    
////    return true;
////}
////
////bool Polygon::simplify()
////{
////    return true;
////}
////
////bool Polygon::minDistance(const Point& pt, double &dist)
////{
//////    if (!pointInPoly(pt)) return false;
////    
////    dist = Vector(max,min).norm();
////    
////    for (std::vector<Point>::iterator it = vertices.begin() ; it != vertices.end()-1; )
////    {
////        Segment S(*it,*(++it));
////        
////        double tmp = S.distance(pt);
////        
////        if (tmp < dist) { dist = tmp; }
////    }
////    return true;
////}
////bool Polygon::maxDistance(const Point& pt, double &dist)
////{
////    if (!pointInPoly(pt)) return false;
////    
////    dist = 0;
////    
////    for (std::vector<Point>::iterator it = vertices.begin() ; it != vertices.end()-1; )
////    {
////        Segment S(*it,*(++it));
////        
////        double tmp = S.distance(pt);
////        
////        if (tmp > dist) { dist = tmp; }
////    }
////    
////    return true;
////}
////bool Polygon::distance(Ray& R, double &dist)
////{
////    if (!pointInPoly(R.P0)) return false;
////    
////    dist = Vector(max,min).norm();
////    for (std::vector<Point>::iterator it = vertices.begin() ; it != vertices.end()-1; )
////    {
////        Segment S(*it,*(++it));
////        double tmp;
////        
////        if (R.distance(S,tmp))
////        {
////            if (tmp < dist)
////                dist = tmp;
////        }
////        
////    }
////    
////    return true;
////}
////double Polygon::getArea()
////{
////    double A = 0;
////    
////    for (int i=0; i < vertices.size()-1; i++)
////        A += (vertices[i].x*vertices[i+1].y - vertices[i+1].x*vertices[i].y);
////    
////    if (A < 0)
////        A = (-1)*A;
////    
////    return A/2;
////}
////void Polygon::printVertices()
////{
////    int vCnt = 0;
////    std::vector<Point>::iterator it = vertices.begin();
////    for (; it != vertices.end()-1; it++)
////    {
////        printf("Point %d: (%.4f, %.4f)\n",vCnt++,it->x,it->y);
////    }
////    printf("Point %d: (%.4f, %.4f)\n",vCnt++,it->x,it->y);
////}
////vector<Point> Polygon::getVertices()
////{
////    return vertices;
////}
////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// PolyMap Class Members ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////
////PolyMap::PolyMap() 
////{
////}
////PolyMap::~PolyMap()
////{
////}
////bool PolyMap::init(Polygon& p, const double step)
////{
////    Point pMax;
////    
////    if (!p.enclosingRect(offset,pMax)) { return false; }
////    
////    this->stepSize = step;
////    
////    nX = ceil((pMax.x - offset.x)/stepSize)+1;
////    nY = ceil((pMax.y - offset.y)/stepSize)+1;
////    
////    N = nX*nY;
////   
////    map.resize(nX);
////    for (int i = 0; i < nX; i++)
////        map[i].resize(nY,0);
////    
////    initCnt = 0;
////    markCnt = 0;
////    Point pt;
////    for (unsigned long i=0; i < nX; i++)
////    {
////        pt.x = i*stepSize + offset.x;
////        for (unsigned long j=0; j < nY; j++)
////        {
////            pt.y = j*stepSize + offset.y;
////            
////            if (!p.pointInPoly(pt))
////            {
////                map[i][j] = 0;
////                initCnt++;
////            }
////            else
////            {
////                map[i][j] = 1;
////            }
////        }
////    }
////    
////    return true;
////}
////bool PolyMap::mark(Point &pt)
////{
////    int i,j;
////    i = floor((pt.x-offset.x) /stepSize);
////    j = floor((pt.y-offset.y) /stepSize);
////    
////    if ( (i >= nX) || (i < 0) )
////    {
//////        D_LOG("PolyMap: mark point X coordinate is out of bounds: (%d >= %d); Point: (%.3f,%.3f)\n",(int)i,(int)nX,pt.x,pt.y);
////        return false;
////    }
////    if ( (j >= nY) || (j < 0) )
////    {
//////        D_LOG("PolyMap: mark point Y coordinate is out of bounds: (%d >= %d); Point: (%.3f,%.3f)\n",(int)j,(int)nY,pt.x,pt.y);
////        return false;
////    }
////
//////    unsigned long markPt = nY*i + j;
//////    if (markPt > N)
//////    {
//////        D_LOG("PolyMap: mark point out of bounds (%d > %d)\n",(int)markPt,(int)N);
//////        return false;
//////    }
////    
////    if (map[i][j] == 2)
////        return true;
////
////    map[i][j] = 2;
////    markCnt++;
////    
////    return true;
////}
////double PolyMap::getCoverage() { return   ((double)markCnt)/((double)(N-initCnt)); }
////
////void PolyMap::printMap()
////{
////    for (int i=0; i < nX; i++)
////    {
////        for (int j = 0; j < nY; j++)
////        {
////            printf("%d ",map[i][j]);
////        }
////        printf("\n");
////    }
////}
