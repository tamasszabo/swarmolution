/* 
 * File:   geometry.h
 * Author: tszabo
 *
 * Created on October 2, 2014, 2:34 PM
 *
 *
 * For many of the algorithms used here, the credit goes to: http://geomalgorithms.com/
 *
 *
 */

#ifndef GEOMETRY_H
#define	GEOMETRY_H

#include <cmath>
#include <vector>

#define GEOMETRY_SMALL_NUMBER 0.00000001

#include "Point.h"
#include "gVector.h"
#include "Polygon.h"


using namespace std;
//
//
//struct Line
//{
//    Point P0;
//    Point P1;
//    
//                Line()                                          {}
//                Line(Point p0, Point p1)                        { P0 = p0; P1 = p1; }
//    
//    int         closestPoint(Point P[], int n);
//    double      distance(Point &P);
//    
//    inline int  isLeft(Point P2 ) { return ( (P1.x - P0.x) * (P2.y - P0.y) - (P2.x - P0.x) * (P1.y - P0.y) ); }
//    
//};
//struct Segment
//{
//    Point P0;
//    Point P1;
//    
//                Segment() {}
//                Segment(Point p0, Point p1)                     { P0 = p0; P1 = p1; }
//    
//    double      length()                                        {return Vector(P0,P1).norm(); }
//    double      distance(const Point &P);
//    bool        inSegment(const Point &P);
//    int         intersect2(Segment &S2, Point &I0, Point &I1 );
//    int         intersect2(Ray &R, Point &I0);
//};
//struct Ray
//{
//    Point  P0;
//    Vector direction;
//    
//                Ray() {}
//                Ray(Point p0, Point p1)                         { P0 = p0; direction = p1-p0;}
//                Ray(Point p0, Vector dir)                       { P0 = p0; direction = dir;}
//                Ray(Point p0, double dir)                       { P0 = p0; direction.setPolar(dir,1); }
//                Ray(Point p0, double az, double el)             { P0 = p0; direction.setSpherical(az,el,1); }
//    
//    int         getX(double &x, const double &y, const double &z);
//    int         getY(const double &x, double &y, const double &z);
//    int         getZ(const double &x, const double &y, double &z);
//    
//    int         getX(Point &pt) { return getX(pt.x, pt.y, pt.z); }
//    int         getY(Point &pt) { return getY(pt.x, pt.y, pt.z); }
//    int         getZ(Point &pt) { return getZ(pt.x, pt.y, pt.z); }
//    
//    bool        getT(const double &x, const double &y, const double &z, double &T);
//    
//    bool        intersect2(Segment S, Segment &result)          { result.P0 = P0; if (S.intersect2(*this,result.P1)) {return true;} else { return false; }}
//    bool        distance(Segment S, double &d)                  { Point P; if (S.intersect2(*this,P)) {d = Vector(P0,P).norm(); return true;} else {d=0; return false; } }
//};
//
//class Polygon
//{
//    vector<Point> vertices;
//    
//    Point max, min;
//    
//    bool locked;
//public:
//    Polygon();
//    ~Polygon();
//    
//    bool        addVertex(const Point &pt);
//    
//    void        lock();
//    void        clear();
//    
//    bool        pointInPoly(const Point &P);
//    
//    bool        minDistance(const Point &pt, double &dist);
//    bool        maxDistance(const Point &pt, double &dist);
//    bool        distance(Ray &R, double &dist);
//    
//    bool        enclosingRect(Point &min, Point &max);
//    
//    bool        simplify();
//    
//    double      getArea();
//    void        printVertices();
//    
//    vector<Point> getVertices();
//};
//
//class PolyMap
//{
////    uint8_t             *map;
//    unsigned long       N;
//
//    vector<vector<uint8_t>> map;
//
//    int                 nX;
//    int                 nY;
//    
//    unsigned long       initCnt;
//    unsigned long       markCnt;
//    double              stepSize;
//    
//    Point               offset;
//    
//public:
//    PolyMap();
//    ~PolyMap();
//    
//    bool                init(Polygon &p, double step);
//    bool                mark(Point &pt);
//    
//    double              getMapStepSize() { return stepSize; }
//    double              getCoverage();
//    void                printMap();
//    
//    int                 getMapSizeX() { return nX; }
//    int                 getMapSizeY() { return nY; }
//    
//    vector<vector<uint8_t>>& getRawMap() { return map; }
//};
#endif	/* GEOMETRY_H */

