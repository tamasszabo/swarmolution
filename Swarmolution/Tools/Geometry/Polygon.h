//
//  Polygon.h
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__Polygon__
#define __Swarmolution__Polygon__

#include <stdio.h>
#include <vector>

#include "Point.h"
#include "Ray.h"


////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Polygon Object Definition /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
class Polygon
{
    std::vector<Point> m_vertices;

    Point m_max, m_min;

    bool m_locked;
public:
    Polygon();
    Polygon(Polygon &other);
    
    ~Polygon();
    
    void        operator=(const Polygon &p);

    bool        addVertex(const Point &pt);
    int         getVertexCount();

    void        lock();
    void        clear();

    bool        pointInPoly(const Point &P);

    double      minDistance(const Point &pt);
    double      minDistance(const Point &pt, Segment &S);
    double      minDistance(const Point &pt, Segment &S, unsigned int &sengmentIndex);
    double      maxDistance(const Point &pt);

    
    double      distance(Ray &R);
    double      distance(Ray &R, Point &impactPoint);
    double      distance(Ray &R, Point &impactPoint, double &ts);
    double      distance(Ray &R, double &ts);
    

    double      rayReflectionAngle(Ray &R, Segment &bounceSegment);
    gVector     rayReflectionVector(Ray &R, Segment &bounceSegment);
    
    bool        enclosingRect(Point &min, Point &max);

    unsigned int isSelfIntersecting();
    bool        simplify();

    double      getArea(bool signedArea = false);
    Point       getCentroid();
    void        scale(double factor);
    
    void        printVertices();
    std::vector<Point>& getVertices();
    
    static double       triangleArea(const Point &P0, const Point &P1, const Point &P2, bool signedArea = true);
    
};
////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// PolyMap Object Definition /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
class PolyMap
{
//    uint8_t             *map;
    unsigned long       m_N;

    std::vector<std::vector<uint8_t>> m_map;

    int                 m_nX;
    int                 m_nY;

    unsigned long       m_initCnt;
    unsigned long       m_markCnt;
    double              m_stepSize;

    Point               m_offset;

public:
    PolyMap();
    ~PolyMap();

    bool                init(Polygon &p, double step);
    bool                mark(Point &pt);

    double              getMapStepSize();
    double              getCoverage();

    int                 getMapSizeX();
    int                 getMapSizeY();

    std::vector<std::vector<uint8_t>>& getRawMap();
};

#endif /* defined(__Swarmolution__Polygon__) */
