//
//  Point.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "Point.h"

            Point::Point()                                         { x = 0; y = 0; z = 0;}
            Point::Point(double X, double Y)                       { x = X; y = Y; z = 0;}
            Point::Point(double X, double Y, double Z)             { x = X; y = Y; z = Z;}
            Point::Point(const Point &orig)                        { x = orig.x; y = orig.y; z = orig.z;}

void        Point::operator=(const Point &pt)                      { x = pt.x; y = pt.y; z = pt.z; }
bool        Point::operator==(const Point &pt)                     { return ( (x == pt.x) && (y == pt.y) && (z == pt.z)); }
bool        Point::operator!=(const Point &pt)                     { return ( (x != pt.x) || (y != pt.y) || (z != pt.z)); }

Point       operator+(const gVector &v, const Point &p)            { return Point(v.x + p.x, v.y + p.y, v.z + p.z); }
Point       operator-(const gVector &v, const Point &p)            { return Point(v.x - p.x, v.y - p.y, v.z - p.z); }
Point       operator+(const Point &p, const gVector &v)            { return Point(v.x + p.x, v.y + p.y, v.z + p.z); }
Point       operator-(const Point &p, const gVector &v)            { return Point(v.x - p.x, v.y - p.y, v.z - p.z); }

gVector      operator-(const Point  &pt, const Point &p)           { return gVector(pt.x-p.x, pt.y-p.y,pt.z-p.z); };
