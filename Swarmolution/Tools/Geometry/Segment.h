//
//  Segment.h
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__Segment__
#define __Swarmolution__Segment__

#include <stdio.h>

#include "Point.h"
struct Segment
{
    Point P0;
    Point P1;
    
    Segment();
    Segment(Point &Pstart, Point &Pend);
    
    double  length();
    double  angle();
    double  distance(const Point &p);
    double  distance2D(Point p);
    
    bool    intersects(Segment S);
    
    bool    isLeft(const Point &p);
};

#endif /* defined(__Swarmolution__Segment__) */
