//
//  Segment.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "Segment.h"

Segment::Segment() {}
Segment::Segment(Point &Pstart, Point &Pend):
P0(Pstart), P1(Pend)
{
}

double  Segment::length()
{
    return (P1-P0).norm();
}
double  Segment::angle()
{
    return (P1-P0).angle();
}
double  Segment::distance(const Point &p)
{
    gVector v = P1 - P0;
    gVector w = p  - P0;
    
    double c1 = w*v;
    if ( c1 <= 0 )
        return (p - P0).norm();
    
    double c2 = v*v;
    if ( c2 <= c1 )
        return (p-P1).norm();
    
    double b = c1 / c2;
    
    gVector vb = b*v;
    
    Point Pb = P0 + vb;
    return (p-Pb).norm();
}
double  Segment::distance2D(Point p)
{
    p.z = 0;
    
    gVector v = P1 - P0;
    gVector w = p  - P0;
    
    double c1 = w*v;
    if ( c1 <= 0 )
        return (p - P0).norm();
    
    double c2 = v*v;
    if ( c2 <= c1 )
        return (p-P1).norm();
    
    double b = c1 / c2;
    
    gVector vb = b*v;
    
    Point Pb = P0 + vb;
    return (p-Pb).norm();
}

bool    Segment::intersects(Segment S)
{
    // Algorithm from: http://algs4.cs.princeton.edu/91primitives/
    Point A = P0;
    Point B = P1;
    Point C = S.P0;
    Point D = S.P1;
    
    double rn = (A.y - C.y)*(D.x - C.x) - (A.x - C.x)*(D.y - C.y);
    double rd = (B.x - A.x)*(D.y - C.y) - (B.y - A.y)*(D.x - C.x);

    double sn = (A.y - C.y)*(B.x - A.x) - (A.x - C.x)*(B.y - A.y);
    double sd = (B.x - A.x)*(D.y - C.y) - (B.y - A.y)*(D.x - C.x);
    
    double r = rn/rd;
    double s = sn/sd;
    
    if (rd == 0)
    {
        if (sd == 0)
            return true;
        
        return false;
    }

// s & r are the fractional distances to the intersection point along the segments between the start & end points.
// The below algorithm does NOT consider the case where the start/end points of the segments coincide, or
// a segment crosses the start/end point of the other one an intersection!
    
    if ( (r > 0) && (r < 1) && (s > 0) && (s < 1) )
        return true;
    
    return false;
}

bool    Segment::isLeft(const Point &p)
{
    return ( ( (P1.x - P0.x) * (p.y - P0.y) - (p.x - P0.x) * (P1.y - P0.y) ) > 0);
}
