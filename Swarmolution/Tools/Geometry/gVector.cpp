//
//  gVector.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include <cmath>

#include "gVector.h"

            gVector::gVector()                                        { x = 0; y = 0; z = 0; }
            gVector::gVector(double X, double Y)                      { x = X; y = Y; z = 0; }
            gVector::gVector(double X, double Y, double Z)            { x = X; y = Y; z = Z; }
            gVector::gVector(const gVector &orig)                      { x = orig.x; y = orig.y; z = orig.z; }
            gVector::gVector(const Point &pt)                         { x = pt.x; y = pt.y; z = pt.z; }
            gVector::gVector(Point p0, Point p1)                      { x = p1.x - p0.x; y = p1.y - p0.y; z = p1.z - p0.z; }

//gVector      gVector::operator+(const gVector &vec)                    { return gVector(x + vec.x, y + vec.y, z + vec.z); }
//gVector      gVector::operator-(const gVector &vec)                    { return gVector(x - vec.x, y - vec.y, z - vec.z); }
//gVector      gVector::operator*(const double &c)                      { return gVector(c*x, c*y, c*z); }
//gVector      gVector::operator*(const int &c)                         { return gVector(c*x, c*y, c*z); }
//double      gVector::operator*(const gVector &vec)                    { return (x*vec.x + y*vec.y + z*vec.z); }
void        gVector::operator=(const gVector &vec)                    { x = vec.x; y = vec.y; z = vec.z; }
void        gVector::operator=(const Point &pt)                      { x = pt.x; y = pt.y; z = pt.z; }

void        gVector::setSpherical(double az)                         { setSpherical(az,0,1);  }
void        gVector::setSpherical(double az, double el)              { setSpherical(az,el,1); }
void        gVector::setSpherical(double az, double el, double r)    { x = r*sin(az)*cos(el); y = r*sin(az)*sin(el); z = r*cos(az); }

void        gVector::setPolar(double theta)                          { setPolar(theta,1); }
void        gVector::setPolar(double theta, double r)                { x = r*cos(theta); y = r*sin(theta); z = 0; }

void        gVector::getSpherical(double &az, double &el, double &r)  const{ r = sqrt(x*x + y*y + z*z); el = atan2(z, r); az = atan2(y,x); }
void        gVector::getPolar(double &theta, double &r)               const{ r = sqrt(x*x + y*y); theta = atan2(y,x); }

void        gVector::normalize()                                     { double N = norm(); x /= N; y /= N; z /= N; }

double      gVector::angle()                                          const{ return atan2(y,x); }
void        gVector::angles(double &az, double &el)                   const{ double dummy; getSpherical(az,el,dummy); }

gVector      gVector::cross(const gVector &vec)                       const{ return gVector(y*vec.z - z*vec.y, z*vec.x - x*vec.z, x*vec.y - y*vec.x); }
double      gVector::norm()                                           const{ return sqrt(x*x + y*y + z*z); }
double      gVector::norm2()                                          const{ return (x*x + y*y + z*z); }
double      gVector::perp2(gVector &v)                                const{ return x*v.y - y*v.x; }


// Operator definitions
gVector      operator+(gVector &a, gVector &b) { return gVector(a.x+b.x,a.y+b.y,a.z+b.z); }
gVector      operator-(gVector &a, gVector &b) { return gVector(a.x-b.x,a.y-b.y,a.z-b.z); }

gVector      operator*(double a, gVector &b)  { return gVector(a*b.x,a*b.y,a*b.z); }
gVector      operator*(gVector &a, double b)  { return gVector(b*a.x,b*a.y,b*a.z); }

gVector      operator*(int a, gVector &b)     { return gVector(a*b.x,a*b.y,a*b.z); }
gVector      operator*(gVector &a, int b)     { return gVector(b*a.x,b*a.y,b*a.z); }

double      operator*(gVector &a, gVector &b) { return (a.x*b.x + a.y*b.y + a.z*b.z); }
