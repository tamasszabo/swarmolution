//
//  Polygon.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "Polygon.h"
#include <cmath>
#include <deque>

////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Polygon Object Definition /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
Polygon::Polygon():
m_locked(false)
{
}
Polygon::Polygon(Polygon &other):
m_locked(other.m_locked),
m_max(other.m_max),
m_min(other.m_min),
m_vertices(other.m_vertices)
{
}
Polygon::~Polygon() {}

void Polygon::operator=(const Polygon &p)
{
    m_vertices  = p.m_vertices;
    m_max       = p.m_max;
    m_min       = p.m_min;
    m_locked    = p.m_locked;
}

bool Polygon::addVertex(const Point& pt)
{
    if ((pt.z != 0) || m_locked)
        return false;

    m_vertices.push_back(pt);
    return true;
}
int Polygon::getVertexCount()
{
    return (int)m_vertices.size();
}
void Polygon::lock()
{
    m_min = m_vertices.front();
    m_max = m_vertices.front();

    for (std::vector<Point>::iterator it = m_vertices.begin() ; it != m_vertices.end(); ++it)
    {
        if (it->x > m_max.x) { m_max.x = it->x; }
        if (it->y > m_max.y) { m_max.y = it->y; }

        if (it->x < m_min.x) { m_min.x = it->x; }
        if (it->y < m_min.y) { m_min.y = it->y; }
    }

    m_vertices.push_back(m_vertices.front());

    m_locked = true;
}
void Polygon::clear()
{
    m_locked = false;
    m_vertices.clear();
}

bool Polygon::enclosingRect(Point& min, Point& max)
{
    if (!m_locked) return false;

    min = m_min;
    max = m_max;
    return true;
}
bool Polygon::pointInPoly(const Point& P)
// Winding number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  wn = the winding number (=0 only when P is outside)

{
    int    wn = 0;    // the  winding number counter

    // loop through all edges of the polygon
    for (unsigned long i=0; i < m_vertices.size()-1; i++) {   // edge from V[i] to  V[i+1]
        if (m_vertices[i].y <= P.y) {          // start y <= P.y
            if (m_vertices[i+1].y  > P.y)      // an upward crossing
                if (Polygon::triangleArea(m_vertices[i], m_vertices[i+1], P, true) > 0)
//                 if ( Line(vertices[i], vertices[i+1]).isLeft(P) > 0)  // P left of  edge
                     ++wn;            // have  a valid up intersect
        }
        else {                        // start y > P.y (no test needed)
            if (m_vertices[i+1].y  <= P.y)     // a downward crossing
                if (Polygon::triangleArea(m_vertices[i], m_vertices[i+1], P, true) < 0)
//                if (Line( vertices[i], vertices[i+1]).isLeft(P) < 0)  // P right of  edge
                     --wn;            // have  a valid down intersect
        }
    }
    if (wn == 0) return false;

    return true;
}
unsigned int Polygon::isSelfIntersecting()
{
    unsigned int intersectionCount = 0;
    
    // If the polygoin is not locked, than it's not closed! Temporarily close it
    if (!m_locked)
        m_vertices.push_back(m_vertices.front());
        

    // A polygon with less than 4 points can't be self intersecting!
    if (m_vertices.size() > 4)
    {
        std::vector<Point>::iterator P1;
        std::vector<Point>::iterator P3;
        for (std::vector<Point>::iterator P0 = m_vertices.begin(); P0 != m_vertices.end() - 1; ++P0)
        {
            P1 = P0+1;
            Segment S0(*P0,*P1);
            for (std::vector<Point>::iterator P2 = P0+1; P2 != m_vertices.end()-1; ++P2)
            {
                P3 = P2+1;
                Segment S1(*P2,*P3);
                if (S0.intersects(S1))
                    intersectionCount++;
            }
        }
    }

    // If the polygoin is not locked, than it's not closed! Remove the closing point
    if (!m_locked)
        m_vertices.pop_back();

    return intersectionCount;
}
bool Polygon::simplify()
{
    if (!m_locked)
        lock();
    
    if (!isSelfIntersecting())
        return true;
    
    std::deque<Point> vertices(m_vertices.begin(), m_vertices.end());
    
    vertices.pop_back();
    
    Polygon dummy;
    
    while (vertices.size() > 0)
    {
        for (unsigned int i = 0; i <= dummy.m_vertices.size(); ++i)
        {
            dummy.m_vertices.insert(dummy.m_vertices.begin()+i, vertices.begin(), vertices.begin()+1);
            
            if (!dummy.isSelfIntersecting())
                { vertices.pop_front(); break; }
            else
                dummy.m_vertices.erase(dummy.m_vertices.begin()+i);
        }
    }
    
    dummy.lock();
    
    m_vertices.clear();
    m_vertices.insert(m_vertices.begin(), dummy.m_vertices.begin(), dummy.m_vertices.end());
    return true;
}

double Polygon::minDistance(const Point& pt)
{
    double dist = gVector(m_max,m_min).norm();

    for (std::vector<Point>::iterator it = m_vertices.begin() ; it != m_vertices.end()-1; )
    {
        Point P0 = *it;
        ++it;
        Point P1 = *it;
        double tmp = Segment(P0,P1).distance2D(pt);
        if (tmp < dist) { dist = tmp; }
    }
    return dist;
}
double Polygon::minDistance(const Point &pt, Segment &S)
{
    double dist = gVector(m_max,m_min).norm();
    
    for (std::vector<Point>::iterator it = m_vertices.begin() ; it != m_vertices.end()-1; )
    {
        Point P0 = *it;
        ++it;
        Point P1 = *it;
        double tmp = Segment(P0,P1).distance2D(pt);
        if (tmp < dist)
        {
            dist = tmp;
            S.P0 = P0;
            S.P1 = P1;
        }
    }
    return dist;
}
double Polygon::minDistance(const Point &pt, Segment &S, unsigned int &segmentIndex)
{
    double dist = gVector(m_max,m_min).norm();
    
    unsigned int segmentCounter = 0;
    for (std::vector<Point>::iterator it = m_vertices.begin() ; it != m_vertices.end()-1; )
    {
        Point P0 = *it;
        ++it;
        Point P1 = *it;
        ++segmentCounter;
        double tmp = Segment(P0,P1).distance2D(pt);
        if (tmp < dist)
        {
            dist = tmp;
            S.P0 = P0;
            S.P1 = P1;
            segmentIndex = segmentCounter;
        }
    }
    return dist;
}
double Polygon::maxDistance(const Point& pt)
{
    double dist = 0;

    for (std::vector<Point>::iterator it = m_vertices.begin() ; it != m_vertices.end()-1; )
    {
        double tmp = Segment(*it,*(++it)).distance2D(pt);
        if (tmp > dist) { dist = tmp; }
    }

    return true;
}
double Polygon::distance(Ray &R, Point &impactPoint, double &ts)
{
    double dist = gVector(m_max,m_min).norm();
    
    Point  tmpPoint;
    double tmpTs;
    
    for (std::vector<Point>::iterator it = m_vertices.begin() ; it != m_vertices.end()-1; )
    {
        double tmp = R.intersect2(Segment(*it,*(++it)), tmpPoint, tmpTs);

        if (tmp < dist)
        {
            dist        = tmp;
            impactPoint = tmpPoint;
            ts          = tmpTs;
        }
    }

    return dist;
}
double      Polygon::distance(Ray &R, Point &impactPoint)
{
    double dummyTs;
    return distance(R, impactPoint, dummyTs);
}
double      Polygon::distance(Ray &R, double &ts)
{
    Point dummyPoint;
    return distance(R, dummyPoint, ts);
}
double      Polygon::distance(Ray &R)
{
    double dummyTs;
    Point dummyPoint;
    return distance(R, dummyPoint, dummyTs);
}


double      Polygon::rayReflectionAngle(Ray &R, Segment &bounceSegment)
{

//    double dist = INFINITY;
    
    Point   impactPoint;
    double  ts = 0;
    double dist = R.intersect2(bounceSegment, impactPoint, ts, true);
    
    if (dist != INFINITY)
    {
        
        double l2 =(bounceSegment.P1-bounceSegment.P0).norm2();
        gVector P0RP0 = (R.P0 - bounceSegment.P0);
        gVector P0P1  = (bounceSegment.P1-bounceSegment.P0);
        
        double tPp = (P0RP0*P0P1)/l2;
        Point Pb = R.P0 + P0P1*(2*(ts-tPp));
        
        
        double returnData = (Pb-impactPoint).angle();
        return returnData;
    }
    
    return NAN;
}
gVector      Polygon::rayReflectionVector(Ray &R, Segment &bounceSegment)
{
    
    //    double dist = INFINITY;
    
    Point   impactPoint;
    double  ts = 0;
    double dist = R.intersect2(bounceSegment, impactPoint, ts, true);
    
    if (dist != INFINITY)
    {
        
        double l2 =(bounceSegment.P1-bounceSegment.P0).norm2();
        gVector P0RP0 = (R.P0 - bounceSegment.P0);
        gVector P0P1  = (bounceSegment.P1-bounceSegment.P0);
        
        double tPp = (P0RP0*P0P1)/l2;
        Point Pb = R.P0 + P0P1*(2*(ts-tPp));
        
        
        return (Pb-impactPoint);
    }
    
    return gVector(NAN,NAN,NAN);
}

double Polygon::getArea(bool signedArea)
{
    double A = 0;

    for (int i=0; i < m_vertices.size()-1; i++)
        A += (m_vertices[i].x*m_vertices[i+1].y - m_vertices[i+1].x*m_vertices[i].y);

    if ((A < 0) && (!signedArea))
        A = (-1)*A;

    return A/2;
}
Point Polygon::getCentroid()
{
    if (!m_locked)
        lock();
    
    if (m_vertices.size() == 2)
        return m_vertices.front();
    
    if (m_vertices.size() == 3)
    {
        Point P = m_vertices.front();
        gVector V(m_vertices[0], m_vertices[1]);
        
        P = P + 0.5*V;
        
        return P;
    }
    
    double Cx = 0;
    double Cy = 0;
    double A  = 0;
    
    std::vector<Point>::iterator ptp1;
    for (std::vector<Point>::iterator pt = m_vertices.begin(); pt != m_vertices.end()-1; ++pt )
    {
        ptp1 = pt + 1;
        
        A   += (pt->x*ptp1->y - ptp1->x*pt->y);
        Cx  += (pt->x + ptp1->x)*(pt->x*ptp1->y - ptp1->x*pt->y);
        Cy  += (pt->y + ptp1->y)*(pt->x*ptp1->y - ptp1->x*pt->y);
    }
 
    A   = A/2;
    Cx /= (6*A);
    Cy /= (6*A);
    
    
    return Point(Cx,Cy,0);
}
void Polygon::scale(double factor)
{
    Point centroid = getCentroid();
    
    Point max(-INFINITY,-INFINITY);
    Point min(INFINITY,INFINITY);
    for (std::vector<Point>::iterator pt = m_vertices.begin(); pt != m_vertices.end(); ++pt )
    {
        pt->x = (pt->x - centroid.x)*factor + centroid.x;
        pt->y = (pt->y - centroid.y)*factor + centroid.y;
        
        if (pt->x > max.x)
            max.x = pt->x;
        if (pt->x < min.x)
            min.x = pt->x;
        if (pt->y > max.y)
            max.y = pt->y;
        if (pt->y < min.y)
            min.y = pt->y;
    }
    m_max = max;
    m_min = min;
}
void Polygon::printVertices()
{
//    int vCnt = 0;
    std::vector<Point>::iterator it = m_vertices.begin();
    for (; it != m_vertices.end()-1; ++it)
    {
        printf("%.4f\t %.4f\n",it->x,it->y);
    }
    printf("%.4f\t %.4f\n",it->x,it->y);
}
std::vector<Point>& Polygon::getVertices()
{
    return m_vertices;
}

double       Polygon::triangleArea(const Point &P0, const Point &P1, const Point &P2, bool signedArea)
{
    if (signedArea)
        return ( (P1.x - P0.x) * (P2.y - P0.y) - (P2.x - P0.x) * (P1.y - P0.y) )/2;
    else
        return fabs(( (P1.x - P0.x) * (P2.y - P0.y) - (P2.x - P0.x) * (P1.y - P0.y) )/2);
}
////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// PolyMap Object Definition /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
PolyMap::PolyMap():
m_N(0), m_nX(0), m_nY(0), m_initCnt(0), m_markCnt(0), m_stepSize(0)
{
    
}
PolyMap::~PolyMap()
{
    
}

bool PolyMap::init(Polygon &p, double step)
{
    Point pMax;

    if (!p.enclosingRect(m_offset,pMax)) { return false; }

    m_stepSize = step;

    m_nX = (int)(ceil((pMax.x - m_offset.x)/m_stepSize)+1);
    m_nY = (int)(ceil((pMax.y - m_offset.y)/m_stepSize)+1);

    m_N = m_nX*m_nY;

    m_map.resize(m_nX);
    for (int i = 0; i < m_nX; i++)
        m_map[i].resize(m_nY,0);

    m_initCnt = 0;
    m_markCnt = 0;
    Point pt;
    for (unsigned long i=0; i < m_nX; i++)
    {
        pt.x = i*m_stepSize + m_offset.x;
        for (unsigned long j=0; j < m_nY; j++)
        {
            pt.y = j*m_stepSize + m_offset.y;

            if (!p.pointInPoly(pt))
            {
                m_map[i][j] = 0;
                m_initCnt++;
            }
            else
            {
                m_map[i][j] = 1;
            }
        }
    }
    
    return true;
}
bool                PolyMap::mark(Point &pt)
{
    int i,j;
    i = (int)floor((pt.x-m_offset.x) /m_stepSize);
    j = (int)floor((pt.y-m_offset.y) /m_stepSize);

    if ( (i >= m_nX) || (i < 0) )
        return false;

    if ( (j >= m_nY) || (j < 0) )
        return false;



    if (m_map[i][j] == 2)
        return true;

    m_map[i][j] = 2;
    m_markCnt++;
    
    return true;
}

double              PolyMap::getMapStepSize() { return m_stepSize; }
double              PolyMap::getCoverage()
{
    return   ((double)m_markCnt)/((double)(m_N-m_initCnt));
}

int                 PolyMap::getMapSizeX() { return m_nX; }
int                 PolyMap::getMapSizeY() { return m_nY; }

std::vector<std::vector<uint8_t>>& PolyMap::getRawMap() { return m_map; }
