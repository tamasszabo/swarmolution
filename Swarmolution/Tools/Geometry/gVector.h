//
//  gVector.h
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__gVector__
#define __Swarmolution__gVector__

#include <stdio.h>
#include "Point.h"

struct Point;

struct gVector
{
    double x;
    double y;
    double z;
    
    gVector();
    gVector(double X, double Y);
    gVector(double X, double Y, double Z);
    gVector(const gVector &orig);
    gVector(const Point &pt);
    gVector(Point p0, Point p1);
    
    void        operator=(const gVector &vec);
    void        operator=(const Point &pt);

    void        setSpherical(double az);
    void        setSpherical(double az, double el);
    void        setSpherical(double az, double el, double r);
    
    void        setPolar(double theta);
    void        setPolar(double theta, double r);
    
    void        getSpherical(double &az, double &el, double &r) const;
    void        getPolar(double &theta, double &r) const;
    
    void        normalize();
    
    double      angle() const;
    void        angles(double &az, double &el) const;
    
    gVector      cross(const gVector &vec) const;
    double      norm() const;
    double      norm2() const;
    double      perp2(gVector &v) const;
};

gVector      operator+(gVector &a, gVector &b);
gVector      operator-(gVector &a, gVector &b);

gVector      operator*(double a, gVector &b);
gVector      operator*(gVector &a, double b);

gVector      operator*(int a, gVector &b);
gVector      operator*(gVector &a, int b);

double      operator*(gVector &a, gVector &b);

#endif /* defined(__Swarmolution__gVector__) */
