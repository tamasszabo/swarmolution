//
//  Ray.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//
#include <cmath>

#include "Ray.h"
#include "geometry.h"

Ray::Ray()
{}
Ray::Ray(Point p0, Point p1):
P0(p0), direction(p0,p1)
{
}
Ray::Ray(Point p0, gVector dir):
P0(p0), direction(dir)
{
    direction.normalize();
}
Ray::Ray(Point p0, double dir):
P0(p0), direction(cos(dir),sin(dir))
{
    
}
Ray::Ray(Point p0, double az, double el):
P0(p0)
{
    direction.setSpherical(az,el,1);
}

void Ray::invert()
{
    direction = direction*(-1);
}

double       Ray::intersect2(Segment S, Point &Ps)
{
    double dummy;
    return intersect2(S, Ps, dummy);
}
double       Ray::intersect2(Segment S, Point &Ps, double &ts, bool extendSegmentToLine)
{
    double angle = direction.angle();

    // Ensure that the angle is between plus-minus pi
    while (angle > M_PI)
        angle -= 2*M_PI;
    while (angle < -M_PI)
        angle += 2*M_PI;

    if (fabs(fabs(angle) - M_PI/2) > GEOMETRY_SMALL_NUMBER)    // Most of the cases - non-vertical lines
    {
        double gamma    = direction.y/direction.x;
        ts       = ((S.P0.y - P0.y) - gamma*(S.P0.x - P0.x))/(gamma*(S.P1.x - S.P0.x) - (S.P1.y - S.P0.y));
        
        if ( ( (ts >= 0) && (ts <= 1) ) || extendSegmentToLine )
        {
            gVector SP0SP1 =(S.P1-S.P0);
            Ps = S.P0 + SP0SP1*ts;
            double tR = (Ps.x - P0.x)/direction.x;
            if (tR > 0)
                return tR;
            else
            {
                Ps = P0;
                return INFINITY;
            }
        }
        else
        {
            Ps = P0;
            return INFINITY;
        }
    }
    else    // The heading is plus-minus pi/2 - singular soultion of the geometry problem
    {
        double dx = S.P1.x - S.P0.x;
        
        // The segment is vertical - Check if the segment is vertical - if yes, the two lines either coincide or they are parallel
        if (fabs(dx) < GEOMETRY_SMALL_NUMBER)
        {
            // The source of the ray is on the segment: the solution is the source of the ray itself - otherwise there is no solution
            if (fabs(S.P0.x - P0.x) < GEOMETRY_SMALL_NUMBER)
            {
                Ps = P0;
                ts = 0;
                return 0;
            }
            else
            {
                Ps = P0;
//                ts = INFINITY;
                return INFINITY;
            }
        }
        // The segment is not vertical - find the intersection point of the ray with the extended segment (line)
        else
        {
            ts = (P0.x - S.P0.x)/dx;
            
            // Check if the intersection point is within the bounds of the segment
            if ( ( (ts < 0) || (ts > 1) ) && !extendSegmentToLine )
            {
                Ps = P0;
                return INFINITY;
            }
            else
            {
                // Calculate the distance (in the y direction as the ray is vertical) to the point of intersection
                double dy = (S.P0.y + (S.P1.y-S.P0.y)*ts) - P0.y;
                
                // Check if the direction has the same sign as the heading (i.e. the point of intersection lies ON the ray and not behind its origin)
                if (angle*dy < 0)
                {
                    Ps = P0;
                    return INFINITY;
                }
                else
                {
                    gVector SP1SP0 = (S.P1-S.P0);
                    Ps = P0 + SP1SP0*ts;
                    return fabs(dy);
                }
            }
        }
    }
    return 0;
}

double        Ray::distance(Segment S)
{
    Point dummy;
    double dummyTS;
    return intersect2(S, dummy, dummyTS);
}
