//
//  Point.h
//  Swarmolution
//
//  Created by Tamas Szabo on 26/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__Point__
#define __Swarmolution__Point__

#include <stdio.h>
#include "gVector.h"

struct gVector;

struct Point
{
    double x;
    double y;
    double z;
    
    Point();
    Point(double X, double Y);
    Point(double X, double Y, double Z);
    Point(const Point &orig);
    
//    Point       operator+(const Vector &v);
//    Point       operator-(const Vector &v);
//    
//    Vector      operator-(const Point  &pt);
    
    void        operator=(const Point &pt);
    bool        operator==(const Point &pt);
    bool        operator!=(const Point &pt);
    
    //    inline double distance(const Point &pt)     { return sqrt( (x-pt.x)*(x-pt.x) + (y-pt.y)*(y-pt.y) + ); }
};

Point       operator+(const gVector &v, const Point &p);
Point       operator+(const Point &p,   const gVector &v);
Point       operator-(const gVector &v, const Point &p);
Point       operator-(const Point &p,   const gVector &v);

gVector     operator-(const Point  &pt, const Point &p);


#endif /* defined(__Swarmolution__Point__) */
