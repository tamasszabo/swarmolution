//
//  RandomEngine.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 13/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "RandomEngine.h"

RandomEngine::RandomEngine():
m_randomEngine(),
m_stdRealPosDist(0,1),
m_stdRealDist(-1,1),
m_stdIntDist(0,9)
{
    m_revertPointSeed = (unsigned int)chrono::system_clock::now().time_since_epoch().count();
    m_randomEngine.seed(m_revertPointSeed);
}

RandomEngine::RandomEngine(unsigned int seed):
m_randomEngine(),
m_stdRealDist(-1,1),
m_stdIntDist(0,9)
{
    m_revertPointSeed = seed;
    m_randomEngine.seed(m_revertPointSeed);
}

RandomEngine::~RandomEngine()
{
}
double RandomEngine::getUniformReal(double low, double high)
{
    
    m_rndMutex.lock();
    uniform_real_distribution<double> dist(low, high);
    double ret = dist(m_randomEngine);
    m_rndMutex.unlock();
    
    return ret;
}
int RandomEngine::getUniformInt(int low, int high)
{
    m_rndMutex.lock();
    uniform_int_distribution<int> dist(low,high);
    int ret = dist(m_randomEngine);
    m_rndMutex.unlock();
    
    return ret;
}
double RandomEngine::getStdUniformReal()
{
    m_rndMutex.lock();
    double ret = m_stdRealDist(m_randomEngine);
    m_rndMutex.unlock();
    
    return ret;
}
double RandomEngine::getStdUniformPosReal()
{
    m_rndMutex.lock();
    double ret = m_stdRealPosDist(m_randomEngine);
    m_rndMutex.unlock();
    
    return ret;
}
int RandomEngine::getStdUniformInt()
{
    m_rndMutex.lock();
    int ret = m_stdIntDist(m_randomEngine);
    m_rndMutex.unlock();
    
    return ret;
}
void RandomEngine::setNewRevertPoint()
{ m_revertPointSeed = (unsigned int)chrono::system_clock::now().time_since_epoch().count(); }
void RandomEngine::revert()
{ m_randomEngine.seed(m_revertPointSeed); }
