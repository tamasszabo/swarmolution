//
//  RandomEngine.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__RandomEngine__
#define __Swarmolution__RandomEngine__

#include <stdio.h>
#include <random>
#include <mutex>

using namespace std;
class RandomEngine {
    
    
    uniform_real_distribution<double>   m_stdRealPosDist;
    uniform_real_distribution<double>   m_stdRealDist;
    uniform_int_distribution<int>       m_stdIntDist;
    
    std::seed_seq::result_type          m_revertPointSeed;
    
    mutex                               m_rndMutex;
public:
    default_random_engine               m_randomEngine;
    
public:
    RandomEngine();
    RandomEngine(std::seed_seq::result_type seed);
    virtual ~RandomEngine();
    
    double      getUniformReal(double low, double high);
    int         getUniformInt(int low, int high);
    
    double      getStdUniformPosReal();
    double      getStdUniformReal();
    int         getStdUniformInt();
    
    void        setNewRevertPoint();
    void        revert();
    
    std::seed_seq::result_type getNewRandomSeed() { return m_randomEngine(); }
private:
    
};
#endif /* defined(__Swarmolution__RandomEngine__) */
