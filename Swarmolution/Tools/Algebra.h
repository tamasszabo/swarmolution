//
//  Algebra.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_Algebra_h
#define Swarmolution_Algebra_h

#include "Libraries/Eigen/Eigen"

#define ALGEBRA_USE_FLOAT

#ifdef ALGEBRA_USE_FLOAT
//    typedef Eigen::VectorXf Vec;
//    typedef Eigen::MatrixXf Mat;

    typedef float FP;
    #pragma message("FP is a typedef for float")
#else
//    typedef Eigen::VectorXd Vec;
//    typedef Eigen::MatrixXd Mat;

    typedef double FP;
    #pragma message("FP is a typedef for double")
#endif // #ifdef ALGEBRA_USE_FLOAT

#endif // #ifndef Swarmolution_Algebra_h
