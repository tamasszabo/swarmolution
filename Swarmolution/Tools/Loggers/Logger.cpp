//
//  Logger.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 13/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "Logger.h"

Logger::Logger()
{}

Logger::Logger(const char* fName, bool logOnWrite):
m_logOnWrite(logOnWrite),m_fName(fName), m_logFile()
{
    if (m_logOnWrite)
        m_logFile.open(fName, ios::out | ios::binary);
}
Logger::~Logger()
{
    if (m_logOnWrite)
        m_logFile.close();
}

void Logger::init(int relativeRate, const char* fName, bool logOnWrite)
{
    m_logOnWrite    = logOnWrite;
    m_fName         = fName;
    if (m_logOnWrite)
        m_logFile.open(fName, ios::out | ios::binary);
}

void Logger::log(std::vector<float> &data)
{
    if (m_logOnWrite)
    {
        m_logFile.write((const char*)data.data(), sizeof(float)*data.size());
        return;
    }
    
    m_bufferLock.lock();
    m_buffer.push_back(data);
    m_bufferLock.unlock();
}
void Logger::log(Logger &other)
{
    if (other.m_logOnWrite)
        return;
    
    if (m_logOnWrite)
    {
        for (std::deque<std::vector<float>>::iterator it = other.m_buffer.begin(); it != other.m_buffer.end(); ++it)
        {
            m_logFile.write((const char*)(*it).data(), sizeof(float)*(*it).size());
        }
        return;
    }
    
    // Lock both mutexes
    m_bufferLock.lock();
    other.m_bufferLock.lock();
    
    // The other logger is empty
    if (other.m_buffer.size() < 1)
    {
        other.m_bufferLock.unlock();
        m_bufferLock.unlock();
        return;
    }
    
    // Ensure that the size of the data contained in the loggers is the same
    if ( (m_buffer.size() == 0) || (m_buffer.front().size() == other.m_buffer.front().size()) )
    {
        m_buffer.insert(m_buffer.end(), other.m_buffer.begin(), other.m_buffer.end());
        other.m_buffer.clear();
    }
    
    // Unlock mutexes
    other.m_bufferLock.unlock();
    m_bufferLock.unlock();
}
void Logger::clear()
{
    m_bufferLock.lock();
    m_buffer.clear();
    m_bufferLock.unlock();
}

void Logger::dump(bool keepData)
{
    if (m_logOnWrite) // There's no need to write to the file anymore!
        return;
    
    m_logFile.open(m_fName, ios::out | ios::binary);
    
    m_bufferLock.lock();

    unsigned long D = 0;
    for (std::deque<std::vector<float>>::iterator it = m_buffer.begin(); it != m_buffer.end(); ++it)
    {
        D += (*it).size();
        m_logFile.write((const char*)(*it).data(), sizeof(float)*(*it).size());
    }
    if (!keepData)
    {
        m_buffer.clear();
        printf("Saved %lu floats.\n",D);
    }

    m_bufferLock.unlock();
    m_logFile.close();
}