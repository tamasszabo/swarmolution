//
//  Logger.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__Logger__
#define __Swarmolution__Logger__

#include <stdio.h>
#include <fstream>
#include <deque>
#include <vector>
#include <mutex>
#include "geometry.h"

class Logger
{
    std::deque<std::vector<float>>  m_buffer;
    
    std::mutex                      m_bufferLock;

    bool                            m_logOnWrite;
    
    std::fstream                    m_logFile;
    const char                      *m_fName;
public:
    Logger();
    Logger(const char* fName, bool logOnWrite = false);
    virtual ~Logger();
    
    void init(int relativeRate, const char* fName, bool logOnWrite = false);
    
    void log(std::vector<float> &data);
    void log(Logger &other);
    
    void clear();
    void dump(bool keepData = false);
private:
    
};

#endif /* defined(__Swarmolution__Logger__) */
