//
//  ThreadedWorker.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 15/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "WorkEngine.h"
#include <unistd.h>

////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////// MultiThreadedWorker function definitions /////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
WorkEngine::WorkEngine()
{
}

WorkEngine::~WorkEngine()
{
    // Remove tasks first and block the threads access to the work queue by locking the mutex - faster destruction
    //  as the threads can finish up their current work without access to new ones
    m_queueLock.lock();
    while (m_workQueue.size() > 0)
    {
        delete m_workQueue.front();
        m_workQueue.pop_front();
    }
    m_queueLock.unlock();
    
    // Destroy the workers
    while (m_workers.size() > 0)
    {
        // Ensure a graceful stop
        m_workers.front()->stop();
        
        delete m_workers.front();
        m_workers.pop_front();
    }
}
void WorkEngine::start(int numThreads)
{
    for (int i=0; i < numThreads; i++)
        addWorker();
}
void WorkEngine::addWorker()
{
    Worker *w = new Worker(&m_workQueue, &m_queueLock);
    while (!w->start())
    {
        w->start();
        usleep(1000);
    }
    
    m_workers.push_back(w);
}
void WorkEngine::stop()
{
    m_queueLock.lock();     // Prevent workers from access to new tasks

    // Stop the workers
    for (std::deque<Worker*>::iterator it = m_workers.begin(); it != m_workers.end(); ++it)
        (*it)->stop();
    
    m_queueLock.unlock();
}
void WorkEngine::push_back(Task* task)
{
    m_workAddLock.lock();
    m_queueLock.lock();
    
    m_workQueue.push_back(task);
    
    m_queueLock.unlock();
    m_workAddLock.unlock();
}
void WorkEngine::push_front(Task *task)
{
    m_workAddLock.lock();
    m_queueLock.lock();
    
    m_workQueue.push_front(task);
    
    m_queueLock.unlock();
    m_workAddLock.unlock();
}

void WorkEngine::waitUntilDone(bool workInThisThread)
{
    // Prevent the work add functions from adding more work
    m_workAddLock.lock();
    
    bool taskQueueEmpty = false;
    while (!taskQueueEmpty)
    {
        // Lock the work queue and check its size. If it's zero, mark queue as empty, unlock the queue and continue
        m_queueLock.lock();
        if (m_workQueue.size() == 0)
        {
            m_queueLock.unlock();
            taskQueueEmpty = true;
            continue;
        }
        
        // If this thread is a non-working thread, release the lock to the workQueue and wait a bit
        if (!workInThisThread)
        {
            m_queueLock.unlock();
            usleep(1000);
            continue;
        }
        
        // If this is a working thread, take the first task and release the lock
        Task* currentTask = m_workQueue.front();
        m_workQueue.pop_front();
        m_queueLock.unlock();
        
        // Perform the task and clean up
        if (currentTask != NULL)
            currentTask->perform();
        
        delete currentTask;
    }
    
    bool workersFinished = false;
    while (!workersFinished)
    {
        workersFinished = true;
        for (std::deque<Worker*>::iterator it = m_workers.begin(); it != m_workers.end(); ++it)
        {
            if ((*it)->getState() == Worker::working)
            {
                workersFinished = false;
                usleep(1000);
                
                break;
            }
        }
        
        
    }
    
    // Release the lock on the work add functions
    m_workAddLock.unlock();
}