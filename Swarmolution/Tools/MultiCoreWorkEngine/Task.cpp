//
//  Task.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 17/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "Task.h"

Task::Task()
{}
Task::~Task()
{
}
void Task::perform()
{
    throw ("Task::perform: Task by itself does not define any work to be done. You need to derive a specific task and override Task::doWork()");
}
