//
//  Task.h
//  Swarmolution
//
//  Created by Tamas Szabo on 17/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__Task__
#define __Swarmolution__Task__

#include <stdio.h>
#include <mutex>

class Task
{
    
public:
    Task();
    virtual ~Task();
    
    virtual void    perform();
};

#endif /* defined(__Swarmolution__Task__) */
