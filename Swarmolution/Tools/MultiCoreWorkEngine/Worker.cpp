//
//  Worker.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 20/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "Worker.h"
#include <unistd.h>

Worker::Worker(std::deque<Task*> *taskList, std::mutex *taskLockMutex):
m_taskList(taskList), m_taskLockMutex(taskLockMutex), m_aliveFlag(false), m_currentTask(NULL), m_state(stopped), m_thread(NULL)
{
    
}
Worker::Worker(Worker &other):
m_taskList(other.m_taskList), m_taskLockMutex(other.m_taskLockMutex), m_aliveFlag(other.m_aliveFlag), m_currentTask(NULL), m_thread(NULL), m_state(stopped)
{
    if (m_aliveFlag == true)
        start();
}
Worker::~Worker()
{
    stop();
}

Worker::WorkerState     Worker::getState()
{
    WorkerState currentState;
    
    m_workerAccessMutex.lock();
    currentState = m_state;
    m_workerAccessMutex.unlock();
    
    return currentState;
}
bool  Worker::start()
{
    if ( (getState() == stoping) && !isAlive() )
        return false;
    
    m_workerAccessMutex.lock();
    m_aliveFlag = true;
    
    if (m_thread == NULL)   // Check that we won't actually start the thread again!
        m_thread    = new std::thread(&Worker::doWork, this);
    
    m_workerAccessMutex.unlock();
    
    return true;
}
void            Worker::stop()
{
    // Signal the thread to stop
    setState(stoping);
    setAliveFlag(false);
    
    if (m_thread != NULL)
    {
        m_thread->join();
        delete m_thread;
        m_thread = NULL;
    }
    
    setState(stopped);
}

void            Worker::setState(Worker::WorkerState newState)
{
    m_workerAccessMutex.lock();
    m_state = newState;
    m_workerAccessMutex.unlock();
}
void Worker::setAliveFlag(bool val)
{
    m_workerAccessMutex.lock();
    m_aliveFlag = val;
    m_workerAccessMutex.unlock();
}
bool Worker::isAlive()
{
    bool val;
    
    m_workerAccessMutex.lock();
    val = m_aliveFlag;
    m_workerAccessMutex.unlock();
    
    return val;
}
void            Worker::doWork()
{
    while (isAlive())
    {
        // Lock mutex for the global taskmanager to check & select task to do
        m_taskLockMutex->lock();
        
        // If there is no task present
        if (m_taskList->size() == 0)
        {
            // There is no task, so release lock
            m_taskLockMutex->unlock();
            
            // Mark worker as waiting
            setState(waiting);
            
            // Wait a bit for a task to appear before checking it again
            usleep(1000);
            
            continue;
        }
        
        // Mark the worker as busy
        setState(working);
        
        // Take the first task in the queue
        m_currentTask =  m_taskList->front();
        m_taskList->pop_front();
        
        // Release the task list for other threads
        m_taskLockMutex->unlock();
        
        // Do the work defined by the task
        if (m_currentTask != NULL)      // Ensure that it's a valid task
            m_currentTask->perform();
        
        // Destroy the finished task
        delete m_currentTask;
        m_currentTask = NULL;
    }
}