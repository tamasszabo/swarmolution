//
//  ThreadedWorker.h
//  Swarmolution
//
//  Created by Tamas Szabo on 15/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__WorkEngine__
#define __Swarmolution__WorkEngine__

#include <stdio.h>
#include <thread>
#include <deque>
#include <mutex>
#include "Worker.h"

class WorkEngine
{    
    std::deque<Task*>           m_workQueue;
    std::mutex                  m_queueLock;
    std::mutex                  m_workAddLock;
    
    std::deque<Worker*>         m_workers;
    
public:
    WorkEngine();
    virtual ~WorkEngine();
    
    void start(int numThreads);
    void addWorker();
    void stop();
    void push_back(Task *task);
    void push_front(Task *task);
    
    void waitUntilDone(bool workInThisThread = false);
    
};
#endif /* defined(__Swarmolution__WorkEngine__) */
