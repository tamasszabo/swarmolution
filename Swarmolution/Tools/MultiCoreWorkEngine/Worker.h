//
//  Worker.h
//  Swarmolution
//
//  Created by Tamas Szabo on 20/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__Worker__
#define __Swarmolution__Worker__

#include <stdio.h>
#include <thread>
#include <mutex>
#include <deque>

#include "Task.h"

class Worker
{
// Public worker state definition
public:
    enum WorkerState
    {
        stopped,
        stoping,
        waiting,
        working
    };
    
// Private members of the worker
private:
    
    std::thread         *m_thread;
    WorkerState         m_state;
    bool                m_aliveFlag;
    
    std::mutex          m_workerAccessMutex;
    Task                *m_currentTask;
    
    std::deque<Task*>   *m_taskList;
    std::mutex          *m_taskLockMutex;
    
// Public interface functions
public:
    Worker(std::deque<Task*> *taskList, std::mutex *taskLockMutex);     // Default  Constructor
    Worker(Worker&      other);                                         // Copy     Constructor

    
    ~Worker();
    
    WorkerState     getState();
    bool            start();
    void            stop();

// Private member functions
private:
    void            setState(WorkerState newState);
    void            setAliveFlag(bool val);
    bool            isAlive();
    void            doWork();
};

#endif /* defined(__Swarmolution__Worker__) */
