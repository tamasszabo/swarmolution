//
//  DisreteMinMaxFilter.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 28/01/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#include "DiscreteMinMaxFilter.h"
#include <cmath>

DiscreteMinMaxFilter::DiscreteMinMaxFilter(mmType filterType, int bufferSize):
DiscreteFilter(bufferSize),m_filterType(filterType)
{}
DiscreteMinMaxFilter::DiscreteMinMaxFilter(mmType filterType, int bufferSize, FP initialValue):
DiscreteFilter(bufferSize, initialValue),m_filterType(filterType)
{}
DiscreteMinMaxFilter::~DiscreteMinMaxFilter()
{}
FP DiscreteMinMaxFilter::getOutput()
{
    FP returnValue;
    if (m_filterType == min)
    {
        returnValue = *m_buffer.begin();
        for (std::vector<FP>::iterator it = m_buffer.begin()+1; it != m_buffer.end(); ++it)
            if ((*it) < returnValue)
                returnValue = (*it);
    }
    else
    {
        returnValue = (*m_buffer.begin());
        for (std::vector<FP>::iterator it = m_buffer.begin()+1; it != m_buffer.end(); ++it)
            if ((*it) > returnValue)
                returnValue = (*it);
    }
    return returnValue;
}