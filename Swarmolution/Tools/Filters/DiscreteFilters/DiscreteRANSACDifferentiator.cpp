//
//  DiscreteLinearRANSAC.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 13/02/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#include "DiscreteRANSACDifferentiator.h"

DiscreteRANSACDifferentiator::DiscreteRANSACDifferentiator(RandomEngine *random, int bufferSize, int maxNumIterations, int minNumDataPointsInBounds, FP thresholdValue, FP timeStep):
DiscreteFilter(bufferSize), m_maxNumInterations(maxNumIterations), m_thresholdValue(thresholdValue), m_minNumDataPointsInBounds(minNumDataPointsInBounds), m_dt(timeStep), m_random(random)
{
}
DiscreteRANSACDifferentiator::~DiscreteRANSACDifferentiator()
{
}

FP DiscreteRANSACDifferentiator::getOutput()
{
    FP diffValue    = 0;
    FP fitError = INFINITY;
    
    for (unsigned int it=0; it < m_maxNumInterations; ++it)
    {
        
        // Get two random points
        int pt1Index = m_random->getUniformInt(0, (int)m_buffer.size()-1);
        int pt2Index = m_random->getUniformInt(0, (int)m_buffer.size()-1);
        while (pt2Index == pt1Index)
            pt2Index = m_random->getUniformInt(0, (int)m_buffer.size()-1);
        
        // Make sure that they are ordered properly: pt1Index is the older, pt2Index is the newer point!
        if (pt2Index > pt1Index)
        {
            int tmp = pt2Index;
            pt2Index = pt1Index;
            pt1Index = tmp;
        }
        FP x1 = ((FP)pt1Index)*m_dt;
        FP x2 = ((FP)pt2Index)*m_dt;
        FP y1 = at(pt1Index);
        FP y2 = at(pt2Index);
        
        std::vector<unsigned int> inlierIndices;
        for (unsigned int i=0; i < m_buffer.size(); ++i)
        {
            if ((i == pt1Index) || (i == pt2Index))
                continue;
            
            FP x0 = i*m_dt;
            FP y0 = at(i);
            
            FP d = ((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - x1*y2)/sqrt( (y2-y1)*(y2-y1) + (x2-x1)*(x2-x1) );
            if (d < m_thresholdValue)
                inlierIndices.push_back(i);
        }
        
        if (inlierIndices.size() > m_minNumDataPointsInBounds)
        {
            inlierIndices.push_back(pt1Index);
            inlierIndices.push_back(pt2Index);
            
            // Parameters of the equation y(x) = a*x + b
            FP a = (y2-y1)/(x2-x1);
            FP b = a*x1 - y1;
            
            // Squared sum error of inliers
            FP err = 0;
            for (std::vector<unsigned int>::iterator index = inlierIndices.begin(); index != inlierIndices.end(); ++index)
            {
                FP x = (*index)*m_dt;
                FP y = at(*index);
                
                err += pow(y - (a*x + b),2);
            }
            
            if (err < fitError)
            {
                fitError = err;
                diffValue = a;
            }
        }
    }
    return diffValue;
}
