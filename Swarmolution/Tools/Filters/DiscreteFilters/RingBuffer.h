//
//  RingBuffer.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/02/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef __Swarmolution__RingBuffer__
#define __Swarmolution__RingBuffer__

#include <stdio.h>
#include <vector>

//template <class T>
//class RingBuffer
//{
//protected:
//    std::vector<T>                      m_buffer;
//    typename std::vector<T>::iterator   m_currentPoint;
//    
//public:
//    RingBuffer(int bufferSize);
//    RingBuffer(int bufferSize, T defaultValue);
//    ~RingBuffer();
//    
////    T operator[](const unsigned int index);
////    T& begin();
////    T& end();
////    
////    void push(T &value);
//};


template <class T> class RingBuffer
{
protected:
    std::vector<T>                      m_buffer;
    typename std::vector<T>::iterator   m_currentIndex;
    
public:
    RingBuffer(int size): m_buffer(size), m_currentIndex(m_buffer.begin()) {}
    RingBuffer(int size, T defaultValue): m_buffer(size, defaultValue), m_currentIndex(m_buffer.begin()) {};
    
    void push(T value)
    {
        *m_currentIndex = value;
        ++m_currentIndex;
        if (m_currentIndex == m_buffer.end())
            m_currentIndex = m_buffer.begin();
    }
    T& operator[] (unsigned int pos)
    {
        if (pos > m_buffer.size())
            pos = pos % m_buffer.size();
        
        ptrdiff_t afterCPTR = (m_buffer.end() - m_currentIndex);
        
        if (pos > afterCPTR)
            return *(m_buffer.begin() + (pos - afterCPTR));
        else
            return *(m_currentIndex + pos);
    }
    T& at(unsigned int pos)
    {
        if (pos > m_buffer.size())
            pos = pos % m_buffer.size();
        
        ptrdiff_t afterCPTR = (m_buffer.end() - m_currentIndex);
        
        if (pos > afterCPTR)
            return *(m_buffer.begin() + (pos - afterCPTR));
        else
            return *(m_currentIndex + pos);
    }
    T& back()
    {
        return (*m_currentIndex);
    }

    T& front()
    {
        if (m_currentIndex == m_buffer.begin())
            return m_buffer.back();
        else
            return *(m_currentIndex-1);
    }
    
};
#endif /* defined(__Swarmolution__RingBuffer__) */
