//
//  DiscreteLinearRANSAC.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/02/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef __Swarmolution__DiscreteRANSACDifferentiator__
#define __Swarmolution__DiscreteRANSACDifferentiator__

#include <stdio.h>
#include "DiscreteFilter.h"
#include "RandomEngine.h"

class DiscreteRANSACDifferentiator: public DiscreteFilter
{
protected:
    unsigned int    m_maxNumInterations;
    unsigned int    m_minNumDataPointsInBounds;
    FP          m_thresholdValue;
    FP          m_dt;
    
    RandomEngine    *m_random;
    
public:
    DiscreteRANSACDifferentiator(RandomEngine *random, int bufferSize, int maxNumIterations, int minNumDataPointsInBounds, FP thresholdValue, FP timeStep);
    virtual ~DiscreteRANSACDifferentiator();
    
    FP getOutput() ;
};

#endif /* defined(__Swarmolution__DiscreteLinearRANSAC__) */
