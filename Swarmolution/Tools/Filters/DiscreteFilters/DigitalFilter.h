//
//  DigitalFilter.h
//  Swarmolution
//
//  Created by Tamas Szabo on 27/05/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_DigitalFilter_h
#define Swarmolution_DigitalFilter_h

#include <deque>
#include "../../Algebra.h"

class DigitalFilter
{
protected:
    std::vector<std::pair<double,double>>   m_coefficients;
    std::deque<std::pair<double,double>>    m_buffer;
    
public:
    DigitalFilter(int order):
    m_buffer(order+1, std::pair<double,double>(0,0))
    {
        
    }
    DigitalFilter(int order, std::vector<std::pair<double,double>> coefficients):
    m_buffer(order+1, std::pair<double,double>(0,0)),
    m_coefficients(coefficients)
    {
    }
    DigitalFilter(const DigitalFilter &other):
    m_buffer(other.m_buffer),
    m_coefficients(other.m_coefficients)
    {
    }
    virtual ~DigitalFilter()
    {
    }
    
    void setCoefficients(std::vector<std::pair<double,double>> coefficients)
    {
        m_coefficients = coefficients;
    }
    FP step(FP input)
    {
        addInput(input);
        return getOutput();
    }
    void addInput(FP input)
    {
        m_buffer.push_front(std::pair<double,double>(input,0));
        m_buffer.pop_back();
    }
    virtual FP getOutput()
    {
        double out = 0;
        
        for (unsigned int i=0; i < m_buffer.size(); ++i)
            out += m_coefficients[i].first*m_buffer[i].first - m_coefficients[i].second*m_buffer[i].second;
        
        m_buffer[0].second = out;
        
        return out;
    }
};

#endif
