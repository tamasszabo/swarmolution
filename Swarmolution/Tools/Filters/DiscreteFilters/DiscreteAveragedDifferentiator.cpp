//
//  DiscreteAveragedDifferentiator.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 28/01/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#include "DiscreteAveragedDifferentiator.h"

DiscreteAveragedDifferentiator::DiscreteAveragedDifferentiator(int bufferSize, FP defaultValue, FP timeStepSize):
DiscreteFilter(bufferSize, defaultValue), m_dt(timeStepSize), m_lastReading(0), m_filterSum(bufferSize*defaultValue)
{
}
DiscreteAveragedDifferentiator::~DiscreteAveragedDifferentiator()
{
    
}

void DiscreteAveragedDifferentiator::addInput(FP input)
{
    FP D = (input - m_lastReading)/m_dt;
    m_lastReading = input;
    
    m_filterSum += (D - back());
    push(D);
}
FP DiscreteAveragedDifferentiator::getOutput()
{
//    m_filterSum = 0;
//    for (std::vector<FP>::iterator it = m_buffer.begin(); it != m_buffer.end(); ++it)
//        m_filterSum += (*it);
    
    return m_filterSum/m_buffer.size();
    
}