//
//  DiscreteFilter.h
//  ;
//
//  Created by Tamas Szabo on 28/01/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef Swarmolution_DiscreteFilter_h
#define Swarmolution_DiscreteFilter_h

#include <vector>
#include "RingBuffer.h"

#include "../../Algebra.h"

class DiscreteFilter: public RingBuffer<FP>
{
protected:
//    std::vector<FP>             m_buffer;
//    std::vector<FP>::iterator   m_bufferIterator;
    
public:
//    DiscreteFilter(): m_buffer(1,0), m_bufferIterator(m_buffer.begin()) {}
//    DiscreteFilter(int bufferSize): m_buffer(bufferSize,0), m_bufferIterator(m_buffer.begin()) {}
//    DiscreteFilter(int bufferSize, FP defaultValue): m_buffer(bufferSize, defaultValue), m_bufferIterator(m_buffer.begin()) {}
//
    DiscreteFilter():RingBuffer(1) {}
    DiscreteFilter(int bufferSize): RingBuffer(bufferSize) {}
    DiscreteFilter(int bufferSize, FP defaultValue): RingBuffer(bufferSize,defaultValue) {}

    
    virtual ~DiscreteFilter() {};
    
    FP step(FP input)
    {
        addInput(input);
        return getOutput();
    }
    virtual void addInput(FP input) { push(input); }
    virtual FP getOutput()          = 0;
};

#endif
