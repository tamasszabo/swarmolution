//
//  FilteredRANSAC.h
//  Swarmolution
//
//  Created by Tamas Szabo on 16/02/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef __Swarmolution__FilteredRANSAC__
#define __Swarmolution__FilteredRANSAC__

#include <stdio.h>
#include "DiscreteRANSACDifferentiator.h"
#include "DiscreteMinMaxFilter.h"

class FilteredRANSACDiff: public DiscreteMinMaxFilter
{
    DiscreteRANSACDifferentiator    m_ransac;
    FP  m_filterSum;
    
public:
    FilteredRANSACDiff(RandomEngine *random, int bufferSize, int maxNumIterations, int minNumDataPointsInBounds, FP thresholdValue, FP timeStep):
    m_ransac(random, bufferSize, maxNumIterations, minNumDataPointsInBounds, thresholdValue, timeStep),
    DiscreteMinMaxFilter(mmType::min, 5, 0)
    {
    }
    virtual void addInput(FP input)
    {
        push(m_ransac.step(input));
    }
};

#endif /* defined(__Swarmolution__FilteredRANSAC__) */
