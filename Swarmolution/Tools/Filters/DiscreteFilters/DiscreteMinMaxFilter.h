//
//  DisreteMinMaxFilter.h
//  Swarmolution
//
//  Created by Tamas Szabo on 28/01/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef __Swarmolution__DiscreteMinMaxFilter__
#define __Swarmolution__DiscreteMinMaxFilter__

#include <stdio.h>
#include "DiscreteFilter.h"

class DiscreteMinMaxFilter: public DiscreteFilter
{
public:
    enum mmType
    {
        min,
        max
    };
protected:
    mmType  m_filterType;
    
public:
    DiscreteMinMaxFilter(mmType filterType, int bufferSize);
    DiscreteMinMaxFilter(mmType filterType, int bufferSize, FP initialValue);
    virtual ~DiscreteMinMaxFilter();
    
    FP getOutput();
    
};
#endif /* defined(__Swarmolution__DiscreteMinMaxFilter__) */
