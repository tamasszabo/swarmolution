//
//  DiscreteAveragedDifferentiator.h
//  Swarmolution
//
//  Created by Tamas Szabo on 28/01/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef __Swarmolution__DiscreteAveragedDifferentiator__
#define __Swarmolution__DiscreteAveragedDifferentiator__

#include <stdio.h>
#include "DiscreteFilter.h"

class DiscreteAveragedDifferentiator: public DiscreteFilter
{
    FP  m_lastReading;
    FP  m_dt;
    FP  m_filterSum;
    
public:
    DiscreteAveragedDifferentiator(int bufferSize, FP defaultValue, FP timeStepSize);
    virtual ~DiscreteAveragedDifferentiator();
    
    void addInput(FP input);
    virtual FP getOutput();
};

#endif /* defined(__Swarmolution__DiscreteAveragedDifferentiator__) */
