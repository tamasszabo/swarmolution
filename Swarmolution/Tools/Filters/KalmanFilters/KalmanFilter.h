//
//  KalmanFilter.h
//  Swarmolution
//
//  Created by Tamas Szabo on 12/01/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef Swarmolution_KalmanFilter_h
#define Swarmolution_KalmanFilter_h

#include <vector>
#include "../../Algebra.h"

template <typename T, int stateSize, int outputSize>
class KalmanFilter
{
protected:
    Eigen::Matrix<T,stateSize,1>           m_x;
    Eigen::Matrix<T,stateSize,stateSize>   m_P;
    Eigen::Matrix<T,stateSize,stateSize>   m_Q;
    Eigen::Matrix<T,outputSize,outputSize> m_R;
    
    Eigen::Matrix<T,outputSize,1>           m_predictedOutput;
public:
    KalmanFilter()
    {
        m_x.fill(0);
        m_P.fill(0);
        m_Q.fill(0);
        m_R.fill(0);
    }
    
    
    virtual ~KalmanFilter()
    {
    }
    void getState(std::vector<T> &state)
    {
        state = std::vector<T>(m_x.data(), m_x.data() + stateSize);
    }
    
    void getPred(std::vector<T> &pred)
    { pred  = std::vector<T>(m_predictedOutput.data(), m_predictedOutput.data() + m_predictedOutput.size()); }
    
    void setState(std::vector<T> newState)
    { m_x   = Eigen::Map<Eigen::Matrix<T,stateSize,1>>(newState.data(),newState.size(),1); }
    
    void setCovariance(std::vector<T> cov)
    { m_P   = Eigen::Map<Eigen::Matrix<T,stateSize,1>>(cov.data(),cov.size(),1).asDiagonal(); }
    
    void setCovariance(Eigen::Matrix<T,stateSize,stateSize> covMat)
    { m_P   = covMat; }
    
    void getCovariance(Eigen::Matrix<T,stateSize,stateSize> &covMat)
    { covMat = m_P; }
    
    void setQ(std::vector<FP> Q)
    { m_Q   = Eigen::Map<Eigen::Matrix<T, stateSize, 1>>(Q.data(),Q.size(),1).asDiagonal(); }
    void setQ(Eigen::Matrix<T,stateSize,stateSize> Q)
    { m_Q   = Q; }
    
    void setR(std::vector<T> R)
    { m_R   = Eigen::Map<Eigen::Matrix<T,outputSize,1>>(R.data(),R.size(),1).asDiagonal(); }
    void setR(Eigen::Matrix<T,outputSize,outputSize> R)
    { m_R   = R; }

//    KalmanFilter(int numStates, int numInputs):
//    m_x(Vec::Zero(numStates)), m_P(Mat::Zero(numStates,numStates)), m_Q(Mat::Zero(numStates,numStates)), m_R(Mat::Zero(numInputs,numInputs))
//    {}
//    
//    
//    virtual ~KalmanFilter() {}
//    void getState(std::vector<double> &state)   { state = std::vector<double>(m_x.data(), m_x.data() + m_x.size()/sizeof(double)); }
//    void getPred(std::vector<double> &pred)     { pred  = std::vector<double>(m_predictedOutput.data(), m_predictedOutput.data() + m_predictedOutput.size()/sizeof(double)); }
//    
//    void setState(std::vector<double> newState) { m_x   = Eigen::Map<Vec>(newState.data()); }
//    
//    void setCovariance(std::vector<double> cov) { m_P   = Eigen::Map<Vec>(cov.data()).asDiagonal(); }
//    void setCovariance(Mat covMat)              { m_P   = covMat; }
//    
//    void setQ(std::vector<double> Q)            { m_Q   = Eigen::Map<Vec>(Q.data()).asDiagonal(); }
//    void setQ(Mat Q)                            { m_Q   = Q; }
//    
//    void setR(std::vector<double> R)            { m_R   = Eigen::Map<Vec>(R.data()).asDiagonal(); }
//    void setR(Mat R)                            { m_R   = R; }
public:
    virtual void step(FP dt, std::vector<FP> inputs, std::vector<FP> readings)     = 0;
    
};

#endif
