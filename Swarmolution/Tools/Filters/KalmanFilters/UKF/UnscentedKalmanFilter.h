////
////  UnscentedKalmanFilter.h
////  Swarmolution
////
////  Created by Tamas Szabo on 27/01/15.
////  Copyright (c) 2015 tszabo. All rights reserved.
////
//
//#ifndef __Swarmolution__UnscentedKalmanFilter__
//#define __Swarmolution__UnscentedKalmanFilter__
//
//#include <stdio.h>
//#include "KalmanFilter.h"
//
//class UnscentedKalmanFilter: public KalmanFilter
//{
//protected:
//    FP m_lambda;
//    
//    
//public:
//    UnscentedKalmanFilter(int numStates, int numInputs);
//    UnscentedKalmanFilter(int numStates, int numInputs, FP lambda);
//    virtual ~UnscentedKalmanFilter();
//    
//    virtual void step(FP dt, std::vector<FP> inputs, std::vector<FP> readings);
//    
//    virtual Vec calcF(FP dt, Vec x, std::vector<FP> input);
//    virtual Vec calcH(FP dt, Vec x, std::vector<FP> input);
//    
//public:
//    static void getSigmaPoints(Mat &weights, Mat &points, Vec state, Mat covMat, FP lambda);
//};
//
//#endif /* defined(__Swarmolution__UnscentedKalmanFilter__) */
