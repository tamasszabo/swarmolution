////
////  UnscentedKalmanFilter.cpp
////  Swarmolution
////
////  Created by Tamas Szabo on 27/01/15.
////  Copyright (c) 2015 tszabo. All rights reserved.
////
//
//#include "UnscentedKalmanFilter.h"
//#include "../../../Libraries/Eigen/Cholesky"
//
//UnscentedKalmanFilter::UnscentedKalmanFilter(int numStates, int numInputs):
//KalmanFilter(numStates,numInputs)
//{
//    m_lambda = 3-numStates;
//}
//UnscentedKalmanFilter::UnscentedKalmanFilter(int numStates, int numInputs, FP lambda):
//KalmanFilter(numStates,numInputs), m_lambda(lambda)
//{
//    
//}
//UnscentedKalmanFilter::~UnscentedKalmanFilter()
//{
//    
//}
//void UnscentedKalmanFilter::step(FP dt, std::vector<FP> inputs, std::vector<FP> readings)
//{
//    Mat gam;
//    Mat Xi_x;
//    
//    // get Sigma points
//    getSigmaPoints(gam, Xi_x, m_x, m_P, m_lambda);
//    
//    Mat Xi_x_kp1_k = Mat::Zero(Xi_x.rows(), Xi_x.cols());
//    
//    for (int i = 0; i < Xi_x.cols(); ++i)
//        Xi_x_kp1_k.col(i) = calcF(dt, Xi_x.col(i), inputs);
//    
//    // Estimate of the new state vector
//    Vec x_kp1_k = gam.replicate(Xi_x_kp1_k.rows(), 1).cwiseProduct(Xi_x_kp1_k).colwise().sum();
//
//    // Estimate of the new covariance matrix
//    Mat tmpX      = Xi_x_kp1_k - x_kp1_k.replicate(1,Xi_x_kp1_k.cols());
//    Mat P_x_kp1_k = m_Q + tmpX*gam.asDiagonal()*tmpX.transpose();
//    
//    // The output equation's Sigma points
//    Mat Xi_h;
//    getSigmaPoints(gam, Xi_h, x_kp1_k, P_x_kp1_k, m_lambda);
//    
//    // Estimate the system's output at the sigma points of the output equation
//    Mat Xi_h_kp1_k = Mat::Zero(m_R.rows(), Xi_x.cols());
//    for (int i = 0; i < Xi_h.cols(); ++i)
//        Xi_h_kp1_k.col(i) = calcH(dt, Xi_h.col(i), inputs);
//
//    // The expected system output at the propagated state
//    Vec h_kp1_k = gam.replicate(Xi_h_kp1_k.rows(), 1).cwiseProduct(Xi_h_kp1_k).colwise().sum();
//
//    // The expected covariance of the popagated state's output
//    Mat tmpH      = Xi_h_kp1_k - h_kp1_k.replicate(1,Xi_h_kp1_k.cols());
//    Mat P_h_kp1_k = m_R + tmpH*gam.asDiagonal()*tmpH.transpose();
//
//    // State-output cross-covariance matrix
//    Mat P_xh_kp1_k= tmpX*gam.asDiagonal()*tmpH.transpose();
//    
//    // Lu decomposition of P_xh_kp1_k - needed for inversion
//    Eigen::FullPivLU<Mat> luOfP_xh_kp1_k = P_xh_kp1_k.fullPivLu();
//    
//    // Kalman Gain matrix
//    Mat K = P_xh_kp1_k*luOfP_xh_kp1_k.inverse();
//    
//    // State and covariance matrix update
//    m_x = x_kp1_k   + K*(Eigen::Map<Vec>(readings.data(), readings.size(), 1) - h_kp1_k);
//    m_P = P_x_kp1_k - K*P_h_kp1_k*K.transpose();
//    
//    m_predictedOutput = h_kp1_k;
//}
//
//Vec UnscentedKalmanFilter::calcF(FP dt, Vec x, std::vector<FP> input)
//{ (void)dt; (void)input; throw("UnscentedKalmanFilter::calcF must be overridden with the proper system dynamics"); return x; }
//
//Vec UnscentedKalmanFilter::calcH(FP dt, Vec x, std::vector<FP> input)
//{ (void)dt; (void)input; throw("UnscentedKalmanFilter::calcF must be overridden with the proper system dynamics"); return x; }
//void UnscentedKalmanFilter::getSigmaPoints(Mat &weights, Mat &points, Vec state, Mat covMat, FP lambda)
//{
//    int n = (int)state.rows();
//    
//    //Sigma point Weights
//    weights.setOnes(1,(2*n+1));
//    weights *= (1/(2*(n+lambda)));
//    weights(0) = lambda/(n+lambda);
//    
//    // The Cholesky decomposition of covMat
//    Mat P_sqrt = covMat.llt().matrixL();
//    
////    Mat tmp = join_rows(join_rows(zeros<vec>(n), P_sqrt), -P_sqrt);
//    Mat tmp;
//    tmp << Vec::Zero(n), P_sqrt, -P_sqrt;
//
//    points = state.replicate(1,(2*n+1)) + tmp;
//    
//}