//
//  ExtendedKalmanFilter.h
//  Swarmolution
//
//  Created by Tamas Szabo on 12/01/15.
//  Copyright (c) 2015 tszabo. All rights reserved.
//

#ifndef __Swarmolution__ExtendedKalmanFilter__
#define __Swarmolution__ExtendedKalmanFilter__

#include <stdio.h>

#include "../../../Libraries/Eigen/LU"

#include "KalmanFilter.h"

template <typename T, unsigned int stateSize, unsigned int inputSize, unsigned int outputSize>
class ExtendedKalmanFilter: public KalmanFilter<T, stateSize,outputSize>
{
    Eigen::Matrix<T,stateSize,stateSize>      m_Fx;
    Eigen::Matrix<T,outputSize, stateSize>    m_Hx;
    
    bool        m_staticDynamics;
public:
    ExtendedKalmanFilter(bool staticDynamics = true):
        m_staticDynamics(staticDynamics)
    {
    }
    virtual ~ExtendedKalmanFilter() {}
    
    void step(T dt, std::vector<T> inputs, std::vector<T> readings)
    {
        {
            // Predict next state
            Eigen::Matrix<T,stateSize,1>  x_kp1_k   = calcF(dt, (KalmanFilter<T,stateSize,outputSize>::m_x), inputs);
            Eigen::Matrix<T,outputSize,1> h_kp1_k  = calcH(dt, x_kp1_k, inputs);
            
            // Linearize dynamics around the new state
            Eigen::Matrix<T,stateSize,stateSize>   Phi;
            Eigen::Matrix<T,stateSize,inputSize>   Gam;
            calcFx(dt, x_kp1_k, inputs, Phi, Gam);
            
            // State variance estimate at the next predicted state
            Eigen::Matrix<T,stateSize,stateSize> P_kp1_k =
                Phi*(KalmanFilter<T,stateSize,outputSize>::m_P)*Phi.transpose() + Gam*(KalmanFilter<T,stateSize,outputSize>::m_Q)*Gam.transpose();
            
            // Estimate the Jacobian of the output at the predicted state
            Eigen::Matrix<T,outputSize,stateSize> Hx_est;
            calcHx(dt, x_kp1_k, inputs, Hx_est);
            
            Eigen::Matrix<T,outputSize,outputSize> Ve    = Hx_est*P_kp1_k*Hx_est.transpose() + (KalmanFilter<T,stateSize,outputSize>::m_R);
            
            Eigen::FullPivLU<Eigen::Matrix<T,outputSize,outputSize>> luOfVe = Ve.fullPivLu();
            
            // Calculate the Kalman gain matrix
            Eigen::Matrix<T,outputSize,outputSize> K = (P_kp1_k*Hx_est.transpose())*luOfVe.inverse();
            
            // Estimate of the new state
            (KalmanFilter<T,stateSize,outputSize>::m_x) = x_kp1_k + K*(Eigen::Map<Eigen::Matrix<T,outputSize,1>>(readings.data(), readings.size(), 1) - h_kp1_k);
            //
            //    // Estimate the new state covariance matrix
            (KalmanFilter<T,stateSize,outputSize>::m_P) =
            (Eigen::Matrix<T,stateSize,stateSize>::Identity(stateSize,stateSize) - K*Hx_est) * P_kp1_k * (Eigen::Matrix<T,stateSize,stateSize>::Identity(stateSize,stateSize)  - K*Hx_est).transpose() + K*(KalmanFilter<T,stateSize,outputSize>::m_R)*K.transpose();
        }
    }
    
private:
    virtual Eigen::Matrix<T,stateSize,1>   calcF( T dt, Eigen::Matrix<T,stateSize,1> x, std::vector<T> input)                                   = 0;
    virtual Eigen::Matrix<T,outputSize,1>   calcH( T dt, Eigen::Matrix<T,stateSize,1> x, std::vector<T> input)                                   = 0;
    virtual void calcFx(T dt, Eigen::Matrix<T,stateSize,1> x, std::vector<T> input, Eigen::Matrix<T,stateSize,stateSize> &Phi, Eigen::Matrix<T,stateSize,inputSize> &Gamma)             = 0;
    virtual void calcHx(T dt, Eigen::Matrix<T,1,1> x, std::vector<T> input, Eigen::Matrix<T,outputSize,stateSize> &Hx) = 0;
//    virtual Eigen::Matrix<T,outputSize,stateSize>   calcHx(T dt, Eigen::Matrix<T,stateSize,1> x, std::vector<T> input)                                   = 0;
    
public:
//    static void c2d(Mat A, Mat B, T dt, Mat &Phi, Mat &Gamma);
};

#endif /* defined(__Swarmolution__ExtendedKalmanFilter__) */
