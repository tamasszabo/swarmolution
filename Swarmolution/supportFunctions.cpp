//
//  supportFunctions.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 01/06/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "supportFunctions.h"

#include "WorkEngine.h"

#include "BehaviorTree.h"
#include "BTComposites.h"
#include "BTLeaves.h"

#include "DroneArena.h"

#include "conflictParameters.h"

#include <vector>


struct dronePerf
{
    unsigned int    btID;
    float           wallSize;
    float           droneSize;
    float           conflictRange;
    float           simTime;
    
    unsigned int    nEval;
    
    unsigned int    nSuccess;
    unsigned long   nConflicts;
    double          defBehavSum;
    double          simTimeSum;
    double          areaCoveredSum;
    double          meanConflictTime;
};

class BTEvalTask: public Task
{
private:
    BehaviorTree*           m_BT;
    
    std::vector<dronePerf>  *m_data;
    std::mutex              *m_dataLock;
    
    dronePerf               perf;

    
public:
    BTEvalTask(BehaviorTree* BT, std::mutex *lock, std::vector<dronePerf> *perfMetrics, unsigned int btID, unsigned int nEval, float wallSize, float simTime):
    m_BT(BT),
    m_dataLock(lock),
    m_data(perfMetrics)
    {
        perf.btID           = btID;
        perf.wallSize       = wallSize;
        perf.droneSize      = DRONE_DRONE_CRASH_RANGE;
        perf.conflictRange  = DRONE_DRONE_CONFLICT_RANGE;
        perf.simTime        = simTime;
        
        perf.nEval          = nEval;
        
        perf.nSuccess       = 0;
        perf.nConflicts     = 0;
        perf.defBehavSum    = 0;
        perf.simTimeSum     = 0;
        perf.areaCoveredSum = 0;
        perf.meanConflictTime = 0;
    }
    ~BTEvalTask()
    {
    }
    void perform()
    {
        for (unsigned int trial = 0; trial < perf.nEval; ++trial)
        {
            DroneArena sim(trial, m_BT, NULL, trial, 3, DroneArena::Shape::fixed, 4, perf.wallSize,false);
            bool simSuccess =sim.runSim(0.01, perf.simTime);
            if (simSuccess)
                perf.nSuccess += 1;
            
            perf.defBehavSum    += sim.getDefBehavRatio();
            perf.simTimeSum     += sim.getSimTime();
            perf.nConflicts     += sim.getConflictCount();
            
            perf.areaCoveredSum += sim.getCoveredArea();
            perf.meanConflictTime += sim.getMeanConflictTime();
        }
        
        m_dataLock->lock();
        m_data->push_back(perf);
        S_LOG("Wall Size: %.2f, Drone Size: %.2f BT: %d\n", perf.wallSize, DRONE_DRONE_CRASH_RANGE, perf.btID);
        m_dataLock->unlock();
    }
};

void compareBTs(std::vector<const char*> fNames, const char* saveName, unsigned int nEval)
{
    std::vector<BehaviorTree*> BTs;

    for (std::vector<const char*>::iterator fName = fNames.begin(); fName != fNames.end(); ++fName)
    {
        BTs.push_back(BehaviorTree::loadFromFile(*fName));
        if (BTs.back() == NULL)
            throw("Can't load BT");
    }
    

    std::vector<std::vector<float>> perfMetrics(fNames.size(),std::vector<float>(3,0));
    std::mutex                      perfLock;
    
    double simTime      = 300;
    
    WorkEngine worker;
    worker.start(8);
    
    std::vector<dronePerf>  data;
    std::mutex              dataLock;
    
    for (unsigned int sizeID = 0; sizeID < 8; ++sizeID)
    {
        DRONE_DRONE_CRASH_RANGE = 0.1+sizeID*0.1;
        for (unsigned int id = 0; id < BTs.size(); ++id)
        {
            for (unsigned int arenaSize = 6; arenaSize < 13; ++arenaSize)
            {
                float wallSize = arenaSize;

                BTEvalTask *task = new BTEvalTask(BTs[id], &dataLock, &data, id, nEval, wallSize, simTime);
                worker.push_back(task);
            }
        }
        worker.waitUntilDone(true);
    }
    std::string results;
    
    results.append(" BT_IDs = { ");
    for (unsigned int id = 0; id < BTs.size(); ++id)
    {
        results.append("'");
        results.append(fNames[id]);
        results.append("', ");
    }
    results.pop_back();
    results.pop_back();
    
    results.append("};");
    results.append("\n");
    
    
    results.append("% dronePerfData structure:\n");
    results.append("% \t 1 droneSize"); results.append("\n");
    results.append("% \t 2 BT_ID"); results.append("\n");
    results.append("% \t 3 wallSize"); results.append("\n");
    results.append("% \t 4 conflictRange"); results.append("\n");
    results.append("% \t 5 maxSimTime"); results.append("\n");
    results.append("% \t 6 nEval"); results.append("\n");
    results.append("% \t 7 nSuccess"); results.append("\n");
    results.append("% \t 8 nConflicts"); results.append("\n");
    results.append("% \t 9 defBehavSum - sum of percentages"); results.append("\n");
    results.append("% \t 10 simTimeSum - sum of all simulation times"); results.append("\n");
    results.append("% \t 11 areaCoveredSum - non-normalized area coverage"); results.append("\n");
    results.append("% \t 12 meanConflictTimeSum"); results.append("\n");
    results.append("\n\n");
    
    results.append("dronePerfData = [ ...\n");
    for (std::vector<dronePerf>::iterator it = data.begin(); it != data.end(); ++it)
    {
        results.append("\t");
        results.append(std::to_string(it->droneSize));
        results.append(",\t");
        results.append(std::to_string(it->btID));
        results.append(",\t");
        results.append(std::to_string(it->wallSize));
        results.append(",\t");
        results.append(std::to_string(it->conflictRange));
        results.append(",\t");
        results.append(std::to_string(it->simTime));
        results.append(",\t");
        results.append(std::to_string(it->nEval));
        results.append(",\t");
        results.append(std::to_string(it->nSuccess));
        results.append(",\t");
        results.append(std::to_string(it->nConflicts));
        results.append(",\t");
        results.append(std::to_string(it->defBehavSum));
        results.append(",\t");
        results.append(std::to_string(it->simTimeSum));
        results.append(",\t");
        results.append(std::to_string(it->areaCoveredSum));
        results.append(",\t");
        results.append(std::to_string(it->meanConflictTime));
        results.append("; ...\n");
    }
    results.append("];\n");
    ofstream mFile;
    mFile.open(saveName);
    mFile << results;
    mFile.close();
    
    S_LOG("Reuslts have been written to: %s\n",saveName);
}

void evalBT(const char* fName, const char* dataLogName, const char* envLogName, unsigned int count)
{
    Logger envLogger(envLogName);
    Logger dataLogger(dataLogName);
    
    BehaviorTree *BT = BehaviorTree::loadFromFile(fName);
    if (BT == NULL)
    {
        S_LOG("Can't load BT from %s\n",fName);
        return;
    }
    
    double avgSimTime = 0;
    for (unsigned int i = 0; i < count; ++i)
    {
        //DroneArena(randomSeed, ai, dataLogger,trialID, numDrones, shape = Shape::fixed, vertexCount = 0, wallLength = 6, faceEachother = true);
        DroneArena sim(i, BT, &dataLogger, i, 3, DroneArena::Shape::fixed, 4, 6, false);
        
        sim.runSim(0.01, 300);
        sim.logEnvironmentShape(&envLogger);
        
        avgSimTime += sim.getSimTime();
        
        S_LOG("Trial %03d. SimEnd: %.2f\n", i, sim.getSimTime());
    }
    
    S_LOG("\n\nAverage sim time: %.2f\n\n", avgSimTime/count);
    envLogger.dump();
    dataLogger.dump();
}

void testFcn()
{
    Logger envLogger("envLog.log");
    Logger dataLogger("dataLog.log");
    
    BehaviorTree *BT1 = BehaviorTree::loadFromFile("BT_iROS.xml");
    BehaviorTree *BT2 = BehaviorTree::loadFromFile("res_05_06_2015/Genomes/Genome_7_mod.xml");
    
    if ( (BT1 == NULL) || (BT2 == NULL) )
    {
        S_LOG("Can't load BTs.\n");
        return;
    }
    
    unsigned int count = 20;
    for (unsigned int i = 0; i < count; ++i)
    {
        //DroneArena(randomSeed, ai, dataLogger,trialID, numDrones, shape = Shape::fixed, vertexCount = 0, wallLength = 6, faceEachother = true);
        DroneArena sim(i, BT1, &dataLogger, i*2, 3, DroneArena::Shape::fixed, 4, 6, false);
        sim.runSim(0.01, 300);
        sim.logEnvironmentShape(&envLogger);
        
        S_LOG("BT1 - Trial %d\n",i);
    }
    for (unsigned int i = 0; i < count; ++i)
    {
        //DroneArena(randomSeed, ai, dataLogger,trialID, numDrones, shape = Shape::fixed, vertexCount = 0, wallLength = 6, faceEachother = true);
        DroneArena sim(i, BT2, &dataLogger, i*2+1, 3, DroneArena::Shape::fixed, 4, 6, false);
        sim.runSim(0.01, 300);
        sim.logEnvironmentShape(&envLogger);
        S_LOG("BT2 - Trial %d\n",i);
    }
    envLogger.dump();
    dataLogger.dump();
}
void genBTCode(const char *srcName, const char *btName)
{
    BehaviorTree *theBT = BehaviorTree::loadFromFile(srcName);
    
    // Add the default behavior to the BT
    BTNode *defBehavRoot = new BTSequence();
    BTNode *tmp;
    tmp = new BTSetC(0,1); defBehavRoot->addChild(tmp);
    tmp = new BTSetC(1,0); defBehavRoot->addChild(tmp);
    tmp = new BTSetC(2,0); defBehavRoot->addChild(tmp);
    
    theBT->getRoot()->addChild(defBehavRoot);
    
    std::string saveNameC(btName);
    saveNameC.append(".c");
    
    std::string saveNameH(btName);
    saveNameH.append(".h");
    
    theBT->generateCode(saveNameH.c_str(), saveNameC.c_str());
    
    delete theBT;
}