//
//  supportFunctions.h
//  Swarmolution
//
//  Created by Tamas Szabo on 01/06/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__supportFunctions__
#define __Swarmolution__supportFunctions__

#include <stdio.h>
#include <vector>

void compareBTs(std::vector<const char*> fNames, const char* saveName, unsigned int nEval);
void evalBT(const char* fName, const char* dataLogName, const char* envLogName, unsigned int count);

void testFcn();

void genBTCode(const char *srcName, const char *btName);
#endif /* defined(__Swarmolution__supportFunctions__) */
