//
//  AICore.h
//  Swarmolution
//
//  Created by Tamas Szabo on 12/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_AICore_h
#define Swarmolution_AICore_h

#include <vector>
#include "../../Tools/Algebra.h"

////////////////////////////////////////////////////////////////////////
// Definition of the AI Input-Output structure.
////////////////////////////////////////////////////////////////////////
struct AIIO
{
    
    std::vector<FP>  in;
    std::vector<FP>  out;
};


////////////////////////////////////////////////////////////////////////
// Definition of the pure virtual AI encoding scructure. Different AI models can derive from this
////////////////////////////////////////////////////////////////////////
class AICore
{
protected:
    bool        m_initialized;
    
public:
    AICore(): m_initialized(false) {}
    AICore(const AICore &orig): m_initialized(orig.m_initialized) {}
    virtual ~AICore() {}
    
    virtual AICore* clone()             = 0;
    virtual void trigger(AIIO &data)    = 0;
    
    virtual unsigned int getNumIn()     = 0;
    virtual unsigned int getNumOut()    = 0;
};

#endif
