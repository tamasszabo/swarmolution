//
//  BehaviorTree.h
//  Swarmolution
//
//  Created by Tamas Szabo on 21/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__BehaviorTree__
#define __Swarmolution__BehaviorTree__

#include <stdio.h>

#include "../../Core/AICore.h"
#include "../../../Tools/RandomEngine/RandomEngine.h"
#include "BTWorkspace.h"
#include "BTNode.h"

class BehaviorTree: public AICore
{
protected:
    BTWorkspace     *m_workspace;

    
    unsigned int    m_nIn;
    unsigned int    m_nOut;
public:

    BTNode          *m_rootNode;
    
    BehaviorTree(int numIn, int numOut, int numPars);
    BehaviorTree(const BehaviorTree &other);
    BehaviorTree(const BehaviorTree &other, BTNode* rootNode);
    
    virtual ~BehaviorTree();
    
    virtual BehaviorTree* clone() { return new BehaviorTree(*this); }
    
    virtual void OnStart();
    
    virtual void saveData(pugi::xml_node &node);
    virtual void save(const char* fName);
    
    virtual bool loadData(pugi::xml_node &node);

    virtual void trigger(AIIO &data);
    
    BTNode*     getRoot();
    
    unsigned int getNumIn()     { return m_nIn; }
    unsigned int getNumOut()    { return m_nOut; }
    unsigned int getNumPar()    { return m_workspace->getNumPar(); }
    
    void   setWorkspacePar(int index, double value);
    
    void   generateCode(const char *headerName, const char *sourceName);
    
    static BehaviorTree* loadFromNode(pugi::xml_node &node);
    static BehaviorTree* loadFromFile(const char* fileName);
};

#endif /* defined(__Swarmolution__BehaviorTree__) */
