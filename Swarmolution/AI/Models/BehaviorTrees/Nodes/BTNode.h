//
//  BTNode.h
//  Swarmolution
//
//  Created by Tamas Szabo on 18/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef Swarmolution_BTNode_h
#define Swarmolution_BTNode_h

#include <vector>
#include <string>

#include "pugixml.hpp"

#include "RandomEngine.h"
#include "BTWorkspace.h"
#include "TreeNode.h"

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// BTNode Class Definition /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
typedef enum {Failure, Success, Running} BTNodeState;

class BTNode: public virtual TreeNode
{
protected:
    bool                m_isComposite;
    
    unsigned long       m_tickCounter;
    bool                m_wasTicked;
public:
                        BTNode(const char* nodeTypeName, bool isComposite);
                        BTNode(const BTNode& other);
    virtual             ~BTNode();
    
    bool                isComposite()   { return m_isComposite; }

    BTNodeState         tick(BTWorkspace* workspace);
    virtual BTNodeState tickFcn(BTWorkspace* workspace)            = 0;
    
    unsigned long       getTickCount();
    void                resetTickCount();
    
    void                resetTickMarker();
    bool                wasTicked() { return m_wasTicked; }
    
    virtual void        save(pugi::xml_node &node);
    virtual bool        load(pugi::xml_node &node, int &nodeIndex);
    
    BTNode*             getChild(unsigned int index);
    BTNode*             getLastChild() { if (m_children.size() == 0) {return NULL;} else {return dynamic_cast<BTNode*>(m_children.back()); }}
    unsigned int        getChildCount();
    
    virtual void        saveAttributes(pugi::xml_node &node)    = 0;
    virtual bool        loadAttributes(pugi::xml_node &node)    = 0;
    
    virtual std::string    getTypeAsString()                                                                                             = 0;
    virtual std::string    genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter) = 0;
    
    void print()
    {
        printf("<%s>\n",m_nodeTypeName);
        for (std::vector<TreeNode*>::iterator c = m_children.begin(); c != m_children.end(); ++c)
            dynamic_cast<BTNode*>(*c)->print();
        
        printf("</%s>\n",m_nodeTypeName);
        
    }

public:
    static BTNode*      getChildFromNode(pugi::xml_node &node);
};
#endif
