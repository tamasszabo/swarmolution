//
//  TreeNode.h
//  Swarmolution
//
//  Created by Tamas Szabo on 21/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef Swarmolution_TreeNode_h
#define Swarmolution_TreeNode_h

#include <vector>
#include <deque>

class TreeNode
{
protected:
    const char*             m_nodeTypeName;;
    std::vector<TreeNode*>   m_children;
    
public:
    TreeNode(const char* typeName): m_nodeTypeName(typeName) {};
    TreeNode(const TreeNode &other):
    m_nodeTypeName(other.m_nodeTypeName)
    {
        for (std::vector<TreeNode*>::const_iterator it = other.m_children.begin(); it != other.m_children.end(); ++it)
            m_children.push_back((*it)->clone());
    }
    virtual ~TreeNode()
    {
        while (m_children.size() > 0)
        {
            delete m_children.back();
            m_children.pop_back();
        }
    }
    virtual TreeNode*     clone() = 0;
    
    
    const char* getTypeName() { return m_nodeTypeName; }
    
    bool isChild(TreeNode *child)
    {
        for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
            if ((*it) == child)
                return true;
        
        return false;
    }
    void addChild(TreeNode* child)
    {
        m_children.push_back(child);
    }
    TreeNode* popChild(TreeNode* child)
    {
        TreeNode *ret = NULL;
        if (child == NULL)
        {
            ret = m_children.back();
            m_children.pop_back();
            return ret;
        }
        
        for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        {
            if ((*it) == child)
            {
                ret = child;
                m_children.erase(it);
                
                return ret;
            }
        }
        
        return NULL;
    }
    unsigned int getNumChildren()
    {
        return (unsigned int)m_children.size();
    }
};

#endif
