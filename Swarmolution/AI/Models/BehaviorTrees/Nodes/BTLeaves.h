//
//  BTLeaves.h
//  Swarmolution
//
//  Created by Tamas Szabo on 20/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__BTLeaves__
#define __Swarmolution__BTLeaves__

#include <stdio.h>
#include "BTNode.h"
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTConditionC: public BTNode
{
protected:
    int     m_dataIndex;
    double  m_compareTo;
    
    bool    m_conditionTypeMoreThan;
public:
                        BTConditionC();
                        BTConditionC(const BTConditionC& other);
    virtual             ~BTConditionC();
    
    virtual BTNodeState     tickFcn(BTWorkspace* workspace);
    virtual BTConditionC*   clone();
    
    std::string         getTypeAsString() { return std::string("tBTConditionC"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
    
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTConditionP: public BTNode
{
protected:
    unsigned int     m_dataIndexFirst;
    unsigned int     m_dataIndexSecond;
    
    bool    m_conditionTypeMoreThan;
public:
                        BTConditionP();
                        BTConditionP(const BTConditionP& other);
    virtual             ~BTConditionP();
    
    virtual BTNodeState     tickFcn(BTWorkspace* workspace);
    virtual BTConditionP*   clone();
    
    std::string         getTypeAsString() { return std::string("tBTConditionP"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTWhileConditionC: public BTConditionC
{
public:
    BTWhileConditionC();
    BTWhileConditionC(const BTWhileConditionC& other);
    virtual             ~BTWhileConditionC();
    
    virtual BTWhileConditionC*  clone() { return new BTWhileConditionC(*this); }
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    
    std::string         getTypeAsString() { return std::string("tBTWhileConditionC"); }
//    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);    // <= Not needed as it is already implemented in BTConditionC
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTWhileConditionP: public BTConditionP
{
public:
    BTWhileConditionP();
    BTWhileConditionP(const BTWhileConditionP& other);
    virtual             ~BTWhileConditionP();
    
    virtual BTWhileConditionP*  clone() { return new BTWhileConditionP(*this); }
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    
    std::string         getTypeAsString() { return std::string("tBTWhileConditionP"); }
//    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter); // <= Not needed as it is already implemented in BTConditionP
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTSetC: public BTNode
{
protected:
    unsigned int     m_dataIndex;
    double  m_value;
    
    bool    m_setTypeAbsolute;
public:
                        BTSetC();
                        BTSetC(const BTSetC &other);
                        BTSetC(unsigned int dataIndex, double value);
    virtual             ~BTSetC();
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    virtual BTSetC*             clone();
    
    std::string         getTypeAsString() { return std::string("tBTSetC"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTSetP: public BTNode
{
protected:
    unsigned int     m_dataIndex;
    unsigned int     m_value;
    
    bool    m_setTypeAbsolute;
public:
    BTSetP();
    BTSetP(const BTSetP &other);
    virtual             ~BTSetP();
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    virtual BTSetP*             clone();
    
    std::string         getTypeAsString() { return std::string("tBTSetP"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTProdP: public BTNode
{
protected:
    int     m_dataIndex1;
    int     m_dataIndex2;
    int     m_dataTo;
    
public:
    BTProdP();
    BTProdP(const BTProdP &other);
    virtual             ~BTProdP();
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    virtual BTProdP*    clone();
    
    std::string         getTypeAsString() { return std::string("tBTProd"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTSetProportional: public BTNode
{
protected:
    int     m_dataIndex;
    int     m_dataTo;
    double  m_G;
    double  m_O;
    
    double  m_lLim;
    double  m_uLim;
    
    bool    m_linear;
public:
    BTSetProportional();
    BTSetProportional(const BTSetProportional &other);
    virtual             ~BTSetProportional();
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    virtual BTSetProportional*  clone();
    
    std::string         getTypeAsString() { return std::string("tBTSetProportional"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTDelay: public BTNode
{
protected:
    int     m_tickCount;
    int     m_numTicks;
public:
                        BTDelay();
                        BTDelay(const BTDelay &other);
    virtual             ~BTDelay();
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    virtual BTDelay*    clone();
    
    std::string         getTypeAsString() { return std::string("tBTDelay"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTDefaultTask: public BTNode
{
protected:
    int     m_tickCount;
    int     m_numTicks;
public:
    BTDefaultTask();
    BTDefaultTask(const BTDefaultTask &other);
    virtual             ~BTDefaultTask();
    
    BTNodeState         tickFcn(BTWorkspace* workspace);
    virtual BTDefaultTask*    clone();
    
    std::string         getTypeAsString() { return std::string("tBTDefaultTask"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    void                saveAttributes(pugi::xml_node &node);
    bool                loadAttributes(pugi::xml_node &node);
};
#endif /* defined(__Swarmolution__BTLeafs__) */
