//
//  BTLeafs.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 20/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "BTLeaves.h"
#include "consoleMacros.h"

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTConditionC::BTConditionC():
TreeNode("BTConditionC"),
BTNode("BTConditionC", false),
m_compareTo(0),
m_conditionTypeMoreThan(false),
m_dataIndex(0)
{
    
}
BTConditionC::BTConditionC(const BTConditionC& other):
TreeNode(other), BTNode(other), m_compareTo(other.m_compareTo), m_conditionTypeMoreThan(other.m_conditionTypeMoreThan), m_dataIndex(other.m_dataIndex)
{
    
}
BTConditionC::~BTConditionC()
{
    
}

BTNodeState    BTConditionC::tickFcn(BTWorkspace *workspace)
{
    if (m_conditionTypeMoreThan)
    {
        if (workspace->getPar(m_dataIndex) > m_compareTo)
            return BTNodeState::Success;
    }
    else
    {
        if (workspace->getPar(m_dataIndex) < m_compareTo)
            return BTNodeState::Success;
    }
    
    return BTNodeState::Failure;
}
BTConditionC* BTConditionC::clone()
{
    return new BTConditionC(*this);
}
std::string    BTConditionC::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_ConditionC");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTConditionC\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataIndex \t\t= ");
    sourceString.append(std::to_string(this->m_dataIndex));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".compareTo \t\t\t= ");
    sourceString.append(std::to_string(this->m_compareTo));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".compareTypeMoreThan \t= ");
    if (this->m_conditionTypeMoreThan)
        sourceString.append("true;");
    else
        sourceString.append("false;");
    sourceLines.push_back(sourceString);
    
    sourceLines.push_back(std::string(""));
    return varName;
}
void    BTConditionC::saveAttributes(pugi::xml_node &node)
{
    if (m_conditionTypeMoreThan)
        node.append_attribute("compareType") = "moreThan";
    else
        node.append_attribute("compareType") = "lessThan";
    
    node.append_attribute("dataIndex") = m_dataIndex;
    node.append_attribute("compareTo") = m_compareTo;
}
bool    BTConditionC::loadAttributes(pugi::xml_node &node)
{
    const char* cmpType = node.attribute("compareType").as_string();
    if (strcmp(cmpType, "moreThan") == 0)
        m_conditionTypeMoreThan = true;
    else if(strcmp(cmpType, "lessThan") == 0)
        m_conditionTypeMoreThan = false;
    else
        return false;

    m_dataIndex = node.attribute("dataIndex").as_int();
    m_compareTo = node.attribute("compareTo").as_double();
    
    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTConditionP::BTConditionP():
TreeNode("BTConditionP"),
BTNode("BTConditionP", false),
m_conditionTypeMoreThan(false),
m_dataIndexFirst(0),
m_dataIndexSecond(0)
{
}
BTConditionP::BTConditionP(const BTConditionP& other):
TreeNode(other), BTNode(other), m_conditionTypeMoreThan(other.m_conditionTypeMoreThan), m_dataIndexFirst(other.m_dataIndexFirst), m_dataIndexSecond(other.m_dataIndexSecond)
{
}
BTConditionP::~BTConditionP()
{
}

BTNodeState    BTConditionP::tickFcn(BTWorkspace* workspace)
{
    if (m_conditionTypeMoreThan)
    {
        if (workspace->getPar(m_dataIndexFirst) > workspace->getPar(m_dataIndexSecond))
            return BTNodeState::Success;
    }
    else
    {
        if (workspace->getPar(m_dataIndexFirst) < workspace->getPar(m_dataIndexSecond))
            return BTNodeState::Success;
    }
    
    return BTNodeState::Failure;
}
BTConditionP* BTConditionP::clone()
{
    return new BTConditionP(*this);
}
std::string    BTConditionP::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_ConditionP");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTConditionP\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataIndex \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataIndexFirst));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".compareTo \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataIndexSecond));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".compareTypeMoreThan \t\t\t= ");
    if (this->m_conditionTypeMoreThan)
        sourceString.append("true;");
    else
        sourceString.append("false;");
    sourceLines.push_back(sourceString);
    
    sourceLines.push_back(std::string(""));
    return varName;
}
void    BTConditionP::saveAttributes(pugi::xml_node &node)
{
    if (m_conditionTypeMoreThan)
        node.append_attribute("compareType") = "moreThan";
    else
        node.append_attribute("compareType") = "lessThan";
    
    node.append_attribute("dataIndexFirst") = m_dataIndexFirst;
    node.append_attribute("dataIndexSecond") = m_dataIndexSecond;
}
bool    BTConditionP::loadAttributes(pugi::xml_node &node)
{
    const char* cmpType = node.attribute("compareType").as_string();
    
    if (strcmp(cmpType, "moreThan") == 0)
        m_conditionTypeMoreThan = true;
    else if(strcmp(cmpType, "lessThan") == 0)
        m_conditionTypeMoreThan = false;
    else
        return false;
    
    m_dataIndexFirst    = node.attribute("dataIndexFirst").as_int();
    m_dataIndexSecond   = node.attribute("dataIndexSecond").as_int();
    
    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTWhileConditionC::BTWhileConditionC():
TreeNode("BTWhileConditionC"),
BTConditionC()
{
}
BTWhileConditionC::BTWhileConditionC(const BTWhileConditionC& other):
TreeNode(other),
BTConditionC(other)
{
    m_nodeTypeName = "BTWhileConditionC";
}
BTWhileConditionC::~BTWhileConditionC()
{
}

BTNodeState    BTWhileConditionC::tickFcn(BTWorkspace *workspace)
{
    if (m_conditionTypeMoreThan)
    {
        if (workspace->getPar(m_dataIndex) > m_compareTo)
            return BTNodeState::Running;
    }
    else
    {
        if (workspace->getPar(m_dataIndex) < m_compareTo)
            return BTNodeState::Running;
    }
    return BTNodeState::Success;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTWhileConditionP::BTWhileConditionP():
TreeNode("BTWhileConditionP"),
BTConditionP()
{
}
BTWhileConditionP::BTWhileConditionP(const BTWhileConditionP& other):
TreeNode(other),
BTConditionP(other)
{
}
BTWhileConditionP::~BTWhileConditionP()
{
}

BTNodeState    BTWhileConditionP::tickFcn(BTWorkspace* workspace)
{
    if (m_conditionTypeMoreThan)
    {
        if (workspace->getPar(m_dataIndexFirst) > workspace->getPar(m_dataIndexSecond))
            return BTNodeState::Running;
    }
    else
    {
        if (workspace->getPar(m_dataIndexFirst) < workspace->getPar(m_dataIndexSecond))
            return BTNodeState::Running;
    }
    
    return BTNodeState::Success;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTSetC::BTSetC():
TreeNode("BTSetC"),
BTNode("BTSetC", false),
m_dataIndex(0),
m_value(0),
m_setTypeAbsolute(true)
{
}
BTSetC::BTSetC(const BTSetC& other):
TreeNode("BTSetC"),BTNode(other), m_dataIndex(other.m_dataIndex), m_value(other.m_value), m_setTypeAbsolute(other.m_setTypeAbsolute)
{
}
BTSetC::BTSetC(unsigned int dataIndex, double value):
TreeNode("BTSetC"),
BTNode("BTSetC", false),
m_dataIndex(dataIndex),
m_value(value),
m_setTypeAbsolute(true)
{
    
}
BTSetC::~BTSetC()
{
}
BTNodeState    BTSetC::tickFcn(BTWorkspace* workspace)
{
    if (m_setTypeAbsolute)
        workspace->setPar(m_dataIndex, m_value);
    else
        workspace->incPar(m_dataIndex, m_value);
    
    return BTNodeState::Success;
}
BTSetC* BTSetC::clone()
{
    return new BTSetC(*this);
}
std::string    BTSetC::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_SetC");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTSetC\t\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataIndex \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataIndex));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".value \t\t\t\t= ");
    sourceString.append(std::to_string(this->m_value));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".setTypeAbsolute \t\t\t= ");
    if (this->m_setTypeAbsolute)
        sourceString.append("true;");
    else
        sourceString.append("false;");
    sourceLines.push_back(sourceString);
    
    sourceLines.push_back(std::string(""));
    return varName;
}
void    BTSetC::saveAttributes(pugi::xml_node &node)
{
    if (!m_setTypeAbsolute)
        node.append_attribute("setType") = "incremental";
    
    node.append_attribute("dataIndex")  = m_dataIndex;
    node.append_attribute("value")      = m_value;
}
bool    BTSetC::loadAttributes(pugi::xml_node &node)
{
    const char* cmpType = node.attribute("setType").as_string("absolute");
    
    if (strcmp(cmpType, "absolute") == 0)
        m_setTypeAbsolute = true;
    else if(strcmp(cmpType, "incremental") == 0)
        m_setTypeAbsolute = false;
    else
        return false;
    
    m_dataIndex = node.attribute("dataIndex").as_int();
    m_value     = node.attribute("value").as_double();
    
    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTSetP::BTSetP():
TreeNode("BTSetP"),
BTNode("BTSetP", false),
m_dataIndex(0),
m_value(0),
m_setTypeAbsolute(true)
{
}
BTSetP::BTSetP(const BTSetP& other):
TreeNode(other), BTNode(other), m_dataIndex(other.m_dataIndex), m_value(other.m_value), m_setTypeAbsolute(other.m_setTypeAbsolute)
{
}
BTSetP::~BTSetP()
{
}
BTNodeState    BTSetP::tickFcn(BTWorkspace* workspace)
{
    if (m_setTypeAbsolute)
        workspace->setPar(m_dataIndex, workspace->getPar(m_value));
    else
        workspace->incPar(m_dataIndex, workspace->getPar(m_value));
    
    return BTNodeState::Success;
}
BTSetP* BTSetP::clone()
{
    return new BTSetP(*this);
}
std::string    BTSetP::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_SetP");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTSetP\t\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataIndex \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataIndex));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".value \t\t\t\t= ");
    sourceString.append(std::to_string(this->m_value));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".setTypeAbsolute \t\t\t= ");
    if (this->m_setTypeAbsolute)
        sourceString.append("true;");
    else
        sourceString.append("false;");
    sourceLines.push_back(sourceString);
    
    sourceLines.push_back(std::string(""));
    return varName;
}
void    BTSetP::saveAttributes(pugi::xml_node &node)
{
    if (!m_setTypeAbsolute)
        node.append_attribute("setType") = "incremental";
    
    node.append_attribute("dataIndex")  = m_dataIndex;
    node.append_attribute("value")      = m_value;
}
bool    BTSetP::loadAttributes(pugi::xml_node &node)
{
    const char* cmpType = node.attribute("setType").as_string("absolute");
    
    if (strcmp(cmpType, "absolute") == 0)
        m_setTypeAbsolute = true;
    else if(strcmp(cmpType, "incremental") == 0)
        m_setTypeAbsolute = false;
    else
        return false;
    
    m_dataIndex = node.attribute("dataIndex").as_int();
    m_value     = node.attribute("value").as_int();
    
    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTProdP::BTProdP():
TreeNode("BTProdP"),
BTNode("BTProdP", false),
m_dataIndex1(0),
m_dataIndex2(0),
m_dataTo(0)
{
}
BTProdP::BTProdP(const BTProdP& other):
TreeNode(other), BTNode(other), m_dataIndex1(other.m_dataIndex1), m_dataIndex2(other.m_dataIndex2), m_dataTo(other.m_dataTo)
{
}
BTProdP::~BTProdP()
{
}
BTNodeState    BTProdP::tickFcn(BTWorkspace* workspace)
{
    if (m_dataTo == -1)
        workspace->setPar(m_dataIndex1 - workspace->getNumIn(), workspace->getPar(m_dataIndex1)*workspace->getPar(m_dataIndex2));
    else
        workspace->setPar(m_dataTo, workspace->getPar(m_dataIndex1)*workspace->getPar(m_dataIndex2));
    
    return BTNodeState::Success;
}
BTProdP* BTProdP::clone()
{
    return new BTProdP(*this);
}
std::string BTProdP::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_SetProportional");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTSetProportional\t\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataIndex1 \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataIndex1));
    sourceString.append(";");
    sourceLines.push_back(sourceString);

    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataIndex2 \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataIndex2));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".writeTo \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataTo));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceLines.push_back(std::string(""));
    return varName;
}
void    BTProdP::saveAttributes(pugi::xml_node &node)
{
    
    node.append_attribute("dataIndex1") = m_dataIndex1;
    node.append_attribute("dataIndex2") = m_dataIndex2;
    
    if (m_dataIndex1 != m_dataTo)
        node.append_attribute("dataTo") = m_dataTo;
}
bool    BTProdP::loadAttributes(pugi::xml_node &node)
{
    
    m_dataIndex1    = node.attribute("dataIndex1").as_int();
    m_dataIndex2    = node.attribute("dataIndex2").as_int();
    m_dataTo        = node.attribute("dataTo").as_int(-1);
    
    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTSetProportional::BTSetProportional():
TreeNode("BTSetProportional"),
BTNode("BTSetProportional", false),
m_dataIndex(0),
m_dataTo(0),
m_G(1),
m_O(0),
m_lLim(-INFINITY),
m_uLim(INFINITY),
m_linear(true)
{
}
BTSetProportional::BTSetProportional(const BTSetProportional& other):
TreeNode(other),
BTNode(other),
m_dataIndex(other.m_dataIndex),
m_dataTo(other.m_dataTo),
m_G(other.m_G), m_O(other.m_O),
m_lLim(other.m_lLim),
m_uLim(other.m_uLim),
m_linear(other.m_linear)
{
}
BTSetProportional::~BTSetProportional()
{
}
BTNodeState    BTSetProportional::tickFcn(BTWorkspace* workspace)
{
    
    double val;
    if (m_linear)
        val = (m_G*workspace->getPar(m_dataIndex) + m_O);
    else
        val = (m_G*(1/workspace->getPar(m_dataIndex)) + m_O);

    if (val > m_uLim)
        val = m_uLim;
    if (val < m_lLim)
        val = m_lLim;
    
    workspace->setPar(m_dataTo, val);
    
    return BTNodeState::Success;
}
BTSetProportional* BTSetProportional::clone()
{
    return new BTSetProportional(*this);
}
std::string BTSetProportional::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_SetProportional");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTSetProportional\t\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataIndex \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataIndex));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".wpDataTo \t\t\t= ");
    sourceString.append(std::to_string(this->m_dataTo));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".G \t\t\t= ");
    sourceString.append(std::to_string(this->m_G));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".O \t\t\t= ");
    sourceString.append(std::to_string(this->m_O));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".lLim \t\t\t= ");
    sourceString.append(std::to_string(this->m_lLim));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".uLim \t\t\t= ");
    sourceString.append(std::to_string(this->m_uLim));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
    
    sourceLines.push_back(std::string(""));
    return varName;
}
void    BTSetProportional::saveAttributes(pugi::xml_node &node)
{
    if (!m_linear)
        node.append_attribute("type") = "inverse";
    
    node.append_attribute("dataIndex")  = m_dataIndex;
    node.append_attribute("writeTo")    = m_dataTo;
    node.append_attribute("G")          = m_G;
    node.append_attribute("O")          = m_O;
    
    if (m_lLim != -INFINITY)
            node.append_attribute("lLim") = m_lLim;
    
    if (m_uLim != INFINITY)
        node.append_attribute("uLim") = m_uLim;
}
bool    BTSetProportional::loadAttributes(pugi::xml_node &node)
{
    const char* cmpType = node.attribute("type").as_string("linear");
    
    if (strcmp(cmpType, "linear") == 0)
        m_linear = true;
    else if(strcmp(cmpType, "inverse") == 0)
        m_linear = false;
    else
        return false;
    
    m_dataIndex = node.attribute("dataIndex").as_int();
    m_dataTo    = node.attribute("writeTo").as_int();
    
    m_G         = node.attribute("G").as_double();
    m_O         = node.attribute("O").as_double();
    
    m_lLim      = node.attribute("lLim").as_double(-INFINITY);
    m_uLim      = node.attribute("uLim").as_double(INFINITY);
    
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTDelay::BTDelay():
TreeNode("BTDelay"),
BTNode("BTDelay", false),
m_tickCount(0),
m_numTicks(0)
{
    
}
BTDelay::BTDelay(const BTDelay &other):
TreeNode(other),
BTNode(other),
m_numTicks(other.m_numTicks),
m_tickCount(other.m_tickCount)
{
    
}
BTDelay::~BTDelay()
{
    
}

BTNodeState BTDelay::tickFcn(BTWorkspace* workspace)
{
    if (++m_tickCount > m_numTicks)
    {
        m_tickCount = 0;
        return BTNodeState::Success;
    }
    
    return BTNodeState::Running;
}
BTDelay* BTDelay::clone()
{
    return new BTDelay(*this);
}
std::string BTDelay::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_Delay");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTDelay\t\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    
    sourceString.clear();
    sourceString.append("   ");
    sourceString.append(varName);
    sourceString.append(".numTicks \t\t\t= ");
    sourceString.append(std::to_string(this->m_numTicks));
    sourceString.append(";");
    sourceLines.push_back(sourceString);
       
    sourceLines.push_back(std::string(""));
    return varName;
}
void BTDelay::saveAttributes(pugi::xml_node &node)
{
    node.append_attribute("numTicks")  = m_tickCount;
}
bool BTDelay::loadAttributes(pugi::xml_node &node)
{
    m_numTicks = node.attribute("numTicks").as_int();
    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTDefaultTask::BTDefaultTask():
TreeNode("BTDelay"),
BTNode("BTDelay", false)
{
    
}
BTDefaultTask::BTDefaultTask(const BTDefaultTask &other):
TreeNode(other),
BTNode(other)
{
    
}
BTDefaultTask::~BTDefaultTask()
{
    
}
    
BTNodeState       BTDefaultTask::tickFcn(BTWorkspace* workspace)
{
    return BTNodeState::Failure;
}
BTDefaultTask*    BTDefaultTask::clone()
{
    return new BTDefaultTask(*this);
}

std::string       BTDefaultTask::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    return std::string();
}
void              BTDefaultTask::saveAttributes(pugi::xml_node &node)
{
    throw("not implemented!");
}
bool              BTDefaultTask::loadAttributes(pugi::xml_node &node)
{
    return false;
}

