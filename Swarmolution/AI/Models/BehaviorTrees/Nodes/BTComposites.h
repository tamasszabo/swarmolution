//
//  BTSelector.h
//  Swarmolution
//
//  Created by Tamas Szabo on 20/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__BTComposites__
#define __Swarmolution__BTComposites__

#include <stdio.h>
#include "BTNode.h"
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTSelect: public BTNode
{
    int                 m_runningIndex;
public:
                        BTSelect();
                        BTSelect(const BTSelect& other);
    virtual             ~BTSelect();
    
    virtual BTNodeState tickFcn(BTWorkspace* workspace);
    virtual BTSelect*   clone();
    
    std::string    getTypeAsString() { return std::string("tBTSelect"); }
    std::string     genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    virtual void        saveAttributes(pugi::xml_node &node);
    virtual bool        loadAttributes(pugi::xml_node &node);
};
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class BTSequence: public BTNode
{
    int                 m_runningIndex;
public:
                        BTSequence();
                        BTSequence(const BTSequence& other);
    virtual             ~BTSequence();
    
    virtual BTNodeState tickFcn(BTWorkspace* workspace);
    virtual BTSequence* clone();
    
    std::string    getTypeAsString() { return std::string("tBTSequence"); }
    std::string     genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter);
protected:
    virtual void        saveAttributes(pugi::xml_node &node);
    virtual bool        loadAttributes(pugi::xml_node &node);
};
class BTWhile: public BTSequence
{
    int                 m_runningIndex;
public:
    BTWhile():BTSequence(), TreeNode( "BTWhile")      {}
    BTWhile(BTSequence& other): TreeNode(other), BTSequence(other){ m_nodeTypeName = "BTWhile"; }
    virtual                         ~BTWhile() {}
    
    virtual BTNodeState tickFcn(BTWorkspace* workspace);
    BTWhile*         clone()    { return new BTWhile(*this); }
    
    std::string    getTypeAsString() { return std::string("tBTWhile"); }
//    std::string     genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter); <= Not needed as it is already implemented in BTSequence!
};
#endif /* defined(__Swarmolution__BTComposites__) */

