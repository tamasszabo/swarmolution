//
//  Leaves.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 20/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include <stdio.h>

#include "BTNode.h"
#include "BTComposites.h"
#include "BTLeaves.h"

BTNode::BTNode(const char* nodeTypeName, bool isComposite):
TreeNode(nodeTypeName),
m_isComposite(isComposite),
m_tickCounter(0)
{
}
BTNode::BTNode(const BTNode& other):
TreeNode(other), m_isComposite(other.m_isComposite), m_tickCounter(0)
{
}
BTNode::~BTNode()
{
}

BTNodeState BTNode::tick(BTWorkspace *workspace)
{
    m_wasTicked = true;
    ++m_tickCounter;
    return tickFcn(workspace);
}
unsigned long BTNode::getTickCount()
{
    return m_tickCounter;
}
void BTNode::resetTickCount()
{
    m_tickCounter = 0;
}
void BTNode::resetTickMarker()
{
    m_wasTicked = false;
    for (std::vector<TreeNode*>::iterator c = m_children.begin(); c != m_children.end(); ++c)
        dynamic_cast<BTNode*>(*c)->resetTickMarker();
}
void BTNode::save(pugi::xml_node &node)
{
    pugi::xml_node thisNode = node.append_child(m_nodeTypeName);
    
    saveAttributes(thisNode);
}

bool BTNode::load(pugi::xml_node &node, int &nodeIndex)
{
    int nodeIndexCounter = 0;
    for (pugi::xml_node thisNode = node.child(m_nodeTypeName); thisNode; thisNode = thisNode.next_sibling() )
    {
        if (nodeIndexCounter < nodeIndex)
            continue;
        
        return loadAttributes(thisNode);
    }
    
    return false;
}
BTNode*      BTNode::getChild(unsigned int index)
{
    return dynamic_cast<BTNode*>(m_children[index]);
}
unsigned int BTNode::getChildCount()
{
    return (unsigned int)m_children.size();
}
BTNode* BTNode::getChildFromNode(pugi::xml_node &node)
{
    const char* typeName = node.name();
    
    BTNode *returnNode = NULL;
    if (strcmp(typeName, "BTSelect") == 0)
        returnNode = new BTSelect();

    else if (strcmp(typeName, "BTSequence") == 0)
        returnNode = new BTSequence();

    else if (strcmp(typeName, "BTWhile") == 0)
        returnNode = new BTWhile();
    
    else if (strcmp(typeName, "BTConditionC") == 0)
        returnNode = new BTConditionC();
    
    else if (strcmp(typeName, "BTConditionP") == 0)
        returnNode = new BTConditionP();
    
    else if (strcmp(typeName, "BTWhileConditionP") == 0)
        returnNode = new BTWhileConditionP();
    
    else if (strcmp(typeName, "BTWhileConditionC") == 0)
        returnNode = new BTWhileConditionC();
    
    else if (strcmp(typeName, "BTSetC") == 0)
        returnNode = new BTSetC();
    
    else if (strcmp(typeName, "BTSetP") == 0)
        returnNode = new BTSetP();

//    else if (strcmp(typeName, "BTDelay") == 0)
//        returnNode = new BTDelay();
    
//    else if (strcmp(typeName, "BTTurnAround") == 0)
//        returnNode = new BTTurnAround();
    
//    else if (strcmp(typeName, "BTStop") == 0)
//        returnNode = new BTStop();
    
//    else if (strcmp(typeName, "BTSetProportional") == 0)
//        returnNode = new BTSetProportional();
    
//    else if (strcmp(typeName, "BTPeakDetector") == 0)
//        returnNode = new BTPeakDetector();

    else if (strcmp(typeName, "BTProdP") == 0)
        returnNode = new BTProdP();
    
    if (returnNode == NULL)
        return returnNode;
    
    if (!returnNode->loadAttributes(node))
    {
        delete returnNode;
        returnNode = NULL;
    }
    return returnNode;
}