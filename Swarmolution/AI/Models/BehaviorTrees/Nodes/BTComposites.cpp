//
//  BTSelector.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 20/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "BTComposites.h"

#define BT_CODEGEN_MAX_NUM_CHILDREN 10

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTSelect::BTSelect():
TreeNode("BTSelect"),
BTNode("BTSelect", true),
m_runningIndex(0)
{}
BTSelect::BTSelect(const BTSelect &other):
TreeNode(other),BTNode(other), m_runningIndex(0)
{
}
BTSelect::~BTSelect()
{}

BTNodeState BTSelect::tickFcn(BTWorkspace* workspace)
{
    // Initialize child iterator to the first child & offset it by the runningIndex
    //  (if if no node returned running, it's zero, so the iterator points to the first child)
    std::vector<TreeNode*>::iterator it = m_children.begin() + m_runningIndex;
    
    for (; it != m_children.end(); ++it)
    {
        BTNodeState nodeState = dynamic_cast<BTNode*>(*it)->tick(workspace);
        if (nodeState == BTNodeState::Success)
        {
            m_runningIndex = 0;
            return BTNodeState::Success;
        }
        else if (nodeState == BTNodeState::Running)
        {
            // Save the vector index of the running child. Since m_children is a vector (continuous memory!) the index is
            //   the difference between the current memory location and the memory location of the first child
            m_runningIndex = (int)(it - m_children.begin());
            return BTNodeState::Running;
        }
    }
    m_runningIndex = 0;
    return BTNodeState::Failure;
}
BTSelect* BTSelect::clone()
{
    return new BTSelect(*this);
}
std::string    BTSelect::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_Select");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTComposite\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    int childIndex = 0;
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        BTNode *child = dynamic_cast<BTNode*>(*it);
        sourceString.clear();
        std::string childName = child->genCode(headerLines, sourceLines, varCounter);
        
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".children[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t\t= &");
        sourceString.append(childName);
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        
        sourceString.clear();
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".childrenTypes[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t= ");
        sourceString.append(child->getTypeAsString());
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        ++childIndex;
    }
    while (childIndex < BT_CODEGEN_MAX_NUM_CHILDREN)
    {
        sourceString.clear();
        
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".children[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t\t= ");
        sourceString.append("NULL");
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        
        sourceString.clear();
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".childrenTypes[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t= ");
        sourceString.append("tBTEmpty");
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        ++childIndex;
    }
    
    sourceLines.push_back(std::string(""));
    return varName;
}

void    BTSelect::saveAttributes(pugi::xml_node &node)
{
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        dynamic_cast<BTNode*>(*it)->save(node);
}
bool    BTSelect::loadAttributes(pugi::xml_node &node)
{
    for (pugi::xml_node_iterator childNode = node.begin(); childNode != node.end(); ++childNode)
    {
        m_children.push_back(getChildFromNode(*childNode));
        if (m_children.back() == NULL)
        {
            m_children.pop_back();
            return false;
        }
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTSequence::BTSequence():
TreeNode("BTSequence"),
BTNode("BTSequence", true),
m_runningIndex(0)
{}
BTSequence::BTSequence(const BTSequence& other):
TreeNode(other),BTNode(other), m_runningIndex(0)
{
}
BTSequence::~BTSequence()
{}

BTNodeState    BTSequence::tickFcn(BTWorkspace* workspace)
{
    // Initialize child iterator to the first child & offset it by the runningIndex
    //  (if if no node returned running, it's zero, so the iterator points to the first child)
    std::vector<TreeNode*>::iterator it = m_children.begin() + m_runningIndex;
    
    for (; it != m_children.end(); ++it)
    {
        BTNodeState nodeState = dynamic_cast<BTNode*>(*it)->tick(workspace);
        if (nodeState == BTNodeState::Failure)
        {
            m_runningIndex = 0;
            return BTNodeState::Failure;
        }
        else if (nodeState == BTNodeState::Running)
        {
            // Save the vector index of the running child. Since m_children is a vector (continuous memory!) the index is
            //   the difference between the current memory location and the memory location of the first child
            m_runningIndex = (int)(it - m_children.begin());
            return BTNodeState::Running;
        }
    }
    m_runningIndex=0;
    return BTNodeState::Success;
}
BTSequence* BTSequence::clone()
{
    return new BTSequence(*this);
}
std::string    BTSequence::genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter)
{
    std::string varName ("Node_Sequence");
    varName.append(std::to_string(varCounter++));
    
    std::string headerString("BTComposite\t");
    headerString.append(varName);
    headerString.append(";");
    headerLines.push_back(headerString);
    
    std::string sourceString;
    int childIndex = 0;
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
    {
        BTNode *child = dynamic_cast<BTNode*>(*it);
        sourceString.clear();
        std::string childName = child->genCode(headerLines, sourceLines, varCounter);
        
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".children[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t\t= &");
        sourceString.append(childName);
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        
        sourceString.clear();
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".childrenTypes[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t= ");
        sourceString.append(child->getTypeAsString());
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        ++childIndex;
    }
    while (childIndex < BT_CODEGEN_MAX_NUM_CHILDREN)
    {
        sourceString.clear();
        
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".children[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t\t= ");
        sourceString.append("NULL");
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        
        sourceString.clear();
        sourceString.append("   ");
        sourceString.append(varName);
        sourceString.append(".childrenTypes[");
        sourceString.append(std::to_string(childIndex));
        sourceString.append("] \t\t= ");
        sourceString.append("tBTEmpty");
        sourceString.append(";");
        
        sourceLines.push_back(sourceString);
        ++childIndex;
    }
    sourceLines.push_back(std::string(""));
    
    return varName;
}
void    BTSequence::saveAttributes(pugi::xml_node &node)
{
    for (std::vector<TreeNode*>::iterator it = m_children.begin(); it != m_children.end(); ++it)
        dynamic_cast<BTNode*>(*it)->save(node);
}
bool    BTSequence::loadAttributes(pugi::xml_node &node)
{
    for (pugi::xml_node_iterator childNode = node.begin(); childNode != node.end(); ++childNode)
    {
        m_children.push_back(getChildFromNode(*childNode));
        if (m_children.back() == NULL)
        {
            m_children.pop_back();
            return false;
        }
    }
    
    return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
BTNodeState    BTWhile::tickFcn(BTWorkspace* workspace)
{
    // Initialize child iterator to the first child & offset it by the runningIndex
    //  (if if no node returned running, it's zero, so the iterator points to the first child)
    std::vector<TreeNode*>::iterator it = m_children.begin() + m_runningIndex;
    
    for (; it != m_children.end(); ++it)
    {
        BTNodeState nodeState = dynamic_cast<BTNode*>(*it)->tick(workspace);
        if (nodeState == BTNodeState::Failure)
        {
            m_runningIndex = 0;
            return BTNodeState::Running;     // There is no failure of a while node - it always returns running when a node fails
        }
        else if (nodeState == BTNodeState::Running)
        {
            // Save the vector index of the running child. Since m_children is a vector (continuous memory!) the index is
            //   the difference between the current memory location and the memory location of the first child
            m_runningIndex = (int)(it - m_children.begin());
            return BTNodeState::Running;
        }
    }
    m_runningIndex=0;
    return BTNodeState::Success;
}