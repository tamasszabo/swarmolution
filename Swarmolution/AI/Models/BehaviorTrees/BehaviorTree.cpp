//
//  BehaviorTree.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 21/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#include "BehaviorTree.h"
#include <fstream>

BehaviorTree::BehaviorTree(int numIn, int numOut, int numPars):
m_rootNode(NULL),
m_nIn(numIn),
m_nOut(numOut)
{
    m_workspace = new BTWorkspace(numIn,numOut,numPars);
}
BehaviorTree::BehaviorTree(const BehaviorTree &other):
AICore(other),
m_rootNode(dynamic_cast<BTNode*>(other.m_rootNode->clone())),
m_nIn(other.m_nIn),
m_nOut(other.m_nOut)
{
    m_workspace = new BTWorkspace(*other.m_workspace);
}
BehaviorTree::BehaviorTree(const BehaviorTree &other, BTNode* rootNode):
AICore(other),
m_rootNode(rootNode),
m_nIn(other.m_nIn),
m_nOut(other.m_nOut)
{
    m_workspace = new BTWorkspace(*other.m_workspace);    
}

BehaviorTree::~BehaviorTree()
{
    delete m_workspace;
    
    if (m_rootNode != NULL)
        delete m_rootNode;
}

void BehaviorTree::OnStart()
{
    // Clean the BTWorkspace by constructing a new one - there may be better options!
   delete m_workspace;
    BTWorkspace *tmp = new BTWorkspace(*m_workspace);
    m_workspace = tmp;
}

void BehaviorTree::saveData(pugi::xml_node &node)
{
    pugi::xml_node btNode = node.append_child("BehaviorTree");
    
    pugi::xml_node workspaceNode = btNode.append_child("BTWorkspace");
    m_workspace->save(workspaceNode);
    
    pugi::xml_node treeNode = btNode.append_child("Tree");
    
    m_rootNode->save(treeNode);
}

void BehaviorTree::save(const char* fName)
{
    pugi::xml_document doc;
    
    // Save the genome-specific data
    saveData(doc);
    
    doc.save_file(fName);
}


void BehaviorTree::trigger(AIIO &data)
{
    for (int i=0; i < m_nIn; i++)
        m_workspace->setIn(i, data.in[i]);
    
    m_rootNode->resetTickMarker();
    m_rootNode->tick(m_workspace);
    
    data.out.clear();
    for (int i=0; i < m_nOut; i++)
        data.out.push_back(m_workspace->getOut(i));
}
BTNode* BehaviorTree::getRoot()
{
    return m_rootNode;
}
void BehaviorTree::setWorkspacePar(int index, double value)
{
    m_workspace->setPar(index, value);
}
bool BehaviorTree::loadData(pugi::xml_node &node)
{
    pugi::xml_node treeNode = node.child("Tree");
    pugi::xml_node btNode   = treeNode.first_child();
    
    m_rootNode = BTNode::getChildFromNode(btNode);
    
    return (m_rootNode != NULL);
}

void  BehaviorTree::generateCode(const char *headerName, const char *sourceName)
{
    std::deque<std::string> headerLines;
    std::deque<std::string> sourceLines;
    unsigned int varCounter = 0;
    
    
    std::string rootName = m_rootNode->genCode(headerLines, sourceLines, varCounter);
    
    std::fstream headerFile(headerName, std::fstream::out | std::fstream::trunc);
    std::fstream sourceFile(sourceName, std::fstream::out | std::fstream::trunc);
    
    std::string dataLine;
    
    
    dataLine.clear(); dataLine.append("#ifndef BTDEFINITION_H");
    headerFile<<dataLine.c_str()<<std::endl;
    
    dataLine.clear(); dataLine.append("#define BTDEFINITION_H");
    headerFile<<dataLine.c_str()<<std::endl<<std::endl;
    
    dataLine.clear(); dataLine.append("#include \"BTStructure.h\"");
    headerFile<<dataLine.c_str()<<std::endl<<std::endl;
    
    //    dataLine.clear(); dataLine.append("#define BTWORKSPACE_NUM_INS "); dataLine.append(std::to_string(m_workspace->getNumRead()));
    //    headerFile<<dataLine.c_str()<<std::endl;
    //
    //    dataLine.clear(); dataLine.append("#define BTWORKSPACE_NUM_PAR "); dataLine.append(std::to_string(m_workspace->getNumPar()));
    //    headerFile<<dataLine.c_str()<<std::endl;
    //
    //    dataLine.clear(); dataLine.append("#define BTWORKSPACE_NUM_OUT "); dataLine.append(std::to_string(m_workspace->getNumWrite()));
    //    headerFile<<dataLine.c_str()<<std::endl<<std::endl;
    
    
    //    dataLine.clear(); dataLine.append("#define BT_MAX_NUM_NODES    "); dataLine.append(std::to_string(5));
    //    headerLines.push_front(dataLine);
    
    headerLines.push_back(std::string("\n#endif"));
    
    for (std::deque<std::string>::iterator it = headerLines.begin(); it != headerLines.end(); ++it)
        headerFile<<(*it).c_str()<<std::endl;
    
    
    dataLine.clear(); dataLine.append("#include \""); dataLine.append(headerName); dataLine.append("\"");
    sourceFile<<dataLine.c_str()<<std::endl;
    
    dataLine.clear(); dataLine.append("#include <stdio.h>");
    sourceFile<<dataLine.c_str()<<std::endl;
    
    dataLine.clear(); dataLine.append("void initBT(BehaviorTree *BT)");
    sourceFile<<dataLine.c_str()<<std::endl<<"{"<<std::endl;
    
    for (std::deque<std::string>::iterator it = sourceLines.begin(); it != sourceLines.end(); ++it)
        sourceFile<<(*it).c_str()<<std::endl;
    
    sourceFile<<"  BT->rootNode = &"<<rootName<<";"<<std::endl;
    sourceFile<<"  BT->rootType = "<<m_rootNode->getTypeAsString()<<";"<<std::endl;
    sourceFile<<"}"<<std::endl;
}


BehaviorTree* BehaviorTree::loadFromNode(pugi::xml_node &node)
{
    pugi::xml_node workspaceNode = node.child("BTWorkspace");
    int numIn   = workspaceNode.attribute("nIn").as_int();
    int numOut  = workspaceNode.attribute("nOut").as_int();
    int numPar  = workspaceNode.attribute("nPar").as_int();
    
    BehaviorTree *BT = new BehaviorTree(numIn,numOut,numPar);
    BT->loadData(node);
 
    if (BT->m_rootNode == NULL)
    {
        delete BT;
        return NULL;
    }
    else
        return BT;
}

BehaviorTree* BehaviorTree::loadFromFile(const char* fileName)
{
    pugi::xml_document doc;
    pugi::xml_parse_result loadResult =doc.load_file(fileName);
    if (loadResult.status != pugi::status_ok)
        return NULL;
    
    pugi::xml_node btNode = doc.child("BehaviorTree");
    return loadFromNode(btNode);
    
}
