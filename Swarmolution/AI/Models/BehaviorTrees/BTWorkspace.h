//
//  BTWorkspace.h
//  Swarmolution
//
//  Created by Tamas Szabo on 18/11/14.
//  Copyright (c) 2014 tszabo. All rights reserved.
//

#ifndef __Swarmolution__BTWorkspace__
#define __Swarmolution__BTWorkspace__

#include <stdio.h>
#include <vector>

#include "pugixml.hpp"
class BTWorkspace
{
protected:
    std::vector<double> m_workspaceData;
    
    int                 m_numIn;
    int                 m_numPar;
    int                 m_numOut;
    
public:
    BTWorkspace(int numIn, int numOut, int numPars): m_numIn(numIn),m_numOut(numOut),m_numPar(numPars),m_workspaceData(numIn+numPars+numOut,0) {}
    BTWorkspace(const BTWorkspace &other): m_numIn(other.m_numIn), m_numOut(other.m_numOut), m_numPar(other.m_numPar), m_workspaceData(other.m_numIn+other.m_numPar+other.m_numOut,0) {};
    
    void    setIn(unsigned int pos, double val)     { if (pos > m_numIn)    { throw("BTWorkspace::setInput: index out of range"); }               m_workspaceData[pos] = val; }
    double  getOut(unsigned int pos)                { if (pos > m_numOut)   { throw("BTWorkspace::getOutput: index out of range"); }              return m_workspaceData[m_numIn+pos]; }
    void    setPar(unsigned int pos, double val)    { if (pos > (m_numOut + m_numPar))   { throw("BTWorkspace::setPar: index out of range"); }    m_workspaceData[m_numIn + pos] = val;
    }
    void    incPar(unsigned int pos, double val)    { if (pos > (m_numOut + m_numPar))   { throw("BTWorkspace::incPar: index out of range"); }    m_workspaceData[m_numIn + pos] += val;
    }
    double  getPar(unsigned int pos)                { if (pos >= m_workspaceData.size())   { throw("BTWorkspace::getPar: index out of range"); }  return m_workspaceData[pos]; }
    
    unsigned int getWorkspaceSize()                 { return (unsigned int)m_workspaceData.size(); }
    
    
    int     getNumIn()                              { return m_numIn; }
    int     getNumRead()                            { return (m_numIn + m_numOut + m_numPar); }
    int     getNumWrite()                           { return (m_numOut + m_numPar); }
    int     getNumPar()                             { return m_numPar; }
    
    void    save(pugi::xml_node &node)
    {
        node.append_attribute("nIn")    = m_numIn;
        node.append_attribute("nOut")   = m_numOut;
        node.append_attribute("nPar")   = m_numPar;
    }
};
#endif /* defined(__Swarmolution__BTWorkspace__) */
