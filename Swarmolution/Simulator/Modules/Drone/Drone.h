//
//  Drone.h
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__Drone__
#define __Swarmolution__Drone__

#include <stdio.h>

#include "../../Core/Module.h"

#include "DroneData.h"

#include "RSSIDistanceSensor.h"
#include "DroneController.h"
#include "SimpleDroneDynamics.h"

#define DRONE_TARGET_REACHED_DISTANCE   0.5

////////////////////////////////////////////////////////////////////////
// The module specific input data type
////////////////////////////////////////////////////////////////////////
class DroneInput: public ModuleData
{
public:
    vector<DroneData> *droneData;
    
public:
    DroneInput()  {}
    ~DroneInput() {}
};

////////////////////////////////////////////////////////////////////////
// The module definition
////////////////////////////////////////////////////////////////////////
class Drone: public Module
{
protected:
    // The drone's ID
    unsigned char               m_ID;
    
    // The submodules of the drone
    RSSIDistanceSensor          m_rssiSensor;
    DroneController             m_droneController;
    SimpleDroneDynamics         m_dynamics;
    
    // IOs of the submodule
    RSSISensorInput             m_rssiSensorInput;
    DroneControllerInput        m_controllerInput;
    SimpleDroneDynamicsInput    m_dynamicsInput;
    
    // Index of the wall that the drone has approached. -1 means none!
    int                         m_wallConflictIndex;
    
    float                       m_heading;
    Point                       m_pos;
    Point                       m_currentTarget;
    std::deque<Point>           m_targets;
    unsigned int                m_targetCounter;
    
public:
    Drone(RandomEngine *random, unsigned char id, AICore *ai, Point P0, FP psi0, unsigned int numAllDrones);
    ~Drone();
    
    void            setTargets(std::deque<Point> targets);
    
    unsigned char   getID();
    
    void            getPos(Point &pos);
    void            getVelocityHeading(FP &psi);
    void            getDroneHeading(FP &psi);
    
    void            getDroneData(DroneData &data);
    unsigned int    getTargetCount() { return m_targetCounter; }
    
    void            getVelocity(gVector &vel);
    void            bounceDrone(const gVector &vel, int wallIndex);
    int             getWallIndex();
    
    AICore*         getAI();
    const AIIO*     getAIIO() const;
    
    DroneControllerInput* getControlInput();
    float           getDefBehavRatio();
    
    void            logDroneData(std::vector<float> &data);
    
protected:
    bool            triggerFcn(const TimeStep &Ts, ModuleData *data = NULL);
};

#endif /* defined(__Swarmolution__Drone__) */
