//
//  DroneData.h
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_DroneData_h
#define Swarmolution_DroneData_h

#include "../../../Tools/Geometry/geometry.h"
#include "../../../Tools/Algebra.h"

////////////////////////////////////////////////////////////////////////
// This struct is used by the environment for passing the locations of the drones
////////////////////////////////////////////////////////////////////////

struct DroneData
{
    unsigned char   ID;
    Point           pos;
    FP              heading;
};

#endif
