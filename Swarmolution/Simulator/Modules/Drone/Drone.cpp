//
//  Drone.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "Drone.h"

// Update rate definitions. Note, that the update rate is defined as: the number of times the parent needs to be triggered for a module to be triggered!
#define RSSI_SENSOR_UPDATE_RATE 5

Drone::Drone(RandomEngine *random, unsigned char id, AICore *ai, Point P0, FP psi0, unsigned int numAllDrones):
Module("Drone", random, 1),
m_ID(id),
m_rssiSensor(random, RSSI_SENSOR_UPDATE_RATE),
m_droneController(random, RSSI_SENSOR_UPDATE_RATE, ai, numAllDrones-1, &m_pos, &m_heading, &m_currentTarget),
m_dynamics(1, random, P0, psi0),
m_wallConflictIndex(-1),
m_heading(psi0),
m_targetCounter(0),
m_pos(P0),
m_currentTarget(Point(0,0))
{
    // Construct the sensor's output vector
    m_rssiSensorInput.rssi.resize(numAllDrones-1); // There should be numAllDrones-1 RSSI readings, since there's no reading to the drone itself

    // The ID of the drone doesn't change, so set the sensor's ID here
    m_rssiSensorInput.myID  = m_ID;
    
    // Link the Sensor's output to the Controller's input
    m_controllerInput.rssi  = &m_rssiSensorInput.rssi;
    
    // Construct the controller's output vector
    m_controllerInput.ctrlAction.resize(4); // There are 3 inputs to the drone: vel, psi, alt + the raw BT velocity output
    
    // Link the controller output to the dynamics input
    m_dynamicsInput.vCmd    = &m_controllerInput.ctrlAction[0];
    m_dynamicsInput.psiCmd  = &m_controllerInput.ctrlAction[1];
    m_dynamicsInput.altCmd  = &m_controllerInput.ctrlAction[2];
    
}
Drone::~Drone()
{
}

void Drone::setTargets(std::deque<Point> targets)
{
    m_targets       = targets;
    m_targetCounter = 0;
    m_currentTarget = m_targets[m_targetCounter];
}

unsigned char   Drone::getID()
{ return m_ID; }

void            Drone::getPos(Point &pos)
{ pos = m_pos; }

void            Drone::getVelocityHeading(FP &psi)
{ m_dynamics.getVelocityHeading(psi); }

void            Drone::getDroneHeading(FP &psi)
{ psi = m_heading; }

void            Drone::getDroneData(DroneData &data)
{ data.ID = m_ID; getPos(data.pos); getDroneHeading(data.heading); }

void            Drone::getVelocity(gVector &vel)
{ m_dynamics.getVelocityVector(vel); }

void            Drone::bounceDrone(const gVector &vel, int wallIndex)
{
    if (wallIndex == m_wallConflictIndex)
        return;
    
    m_wallConflictIndex = wallIndex;
    
    if (m_wallConflictIndex != -1)
    {
        m_dynamics.setVelocityVector(vel);
        m_droneController.enableUpdate(false);
    }
    else
        m_droneController.enableUpdate(true);
}
int Drone::getWallIndex()
{
    return m_wallConflictIndex;
}
AICore* Drone::getAI()
{
    return m_droneController.getAI();
}
const AIIO* Drone::getAIIO() const
{
    return m_droneController.getAIIO();
}
DroneControllerInput* Drone::getControlInput()
{
    return &m_controllerInput;
}
float Drone::getDefBehavRatio()
{
    return m_droneController.getDefBehavRatio();
}
void Drone::logDroneData(std::vector<float> &data)
{
    // WARNING: Data may already contain information! DO NOT CLEAR IT!
    
    Point P;
    m_dynamics.getPos(P);
    
    gVector V;
    m_dynamics.getVelocityVector(V);
    
    FP heading;
    m_dynamics.getDroneHeading(heading);
    
    data.push_back(m_ID);
    data.push_back(P.x);
    data.push_back(P.y);
    data.push_back(P.z);
    data.push_back(heading);
    data.push_back(V.norm());
    
    // The RSSI readings
    data.insert(data.end(), m_rssiSensorInput.rssi.begin(), m_rssiSensorInput.rssi.end());

    // The filter estimates
    m_droneController.getDistanceEstimates(data);
    
    // The commanded turn rate
    data.push_back(m_droneController.getTurnRateCmd());
    
    // The input to the dynamics (i.e. the controller's output)
    data.push_back(*m_dynamicsInput.vCmd);
    data.push_back(*m_dynamicsInput.psiCmd);
    data.push_back(*m_dynamicsInput.altCmd);
    
    // Outputs of the BT
    const AIIO* btIO = m_droneController.getAIIO();
    data.push_back(btIO->out[0]);
    data.push_back(btIO->out[1]);
    data.push_back(btIO->out[2]);
}
bool Drone::triggerFcn(const TimeStep &Ts, ModuleData *data)
{
    if (gVector(m_pos, m_currentTarget).norm() < DRONE_TARGET_REACHED_DISTANCE)
    {
        ++m_targetCounter;
        while (m_targetCounter > m_targets.size())
            m_targets.push_back(Point(m_randomEngine->getStdUniformReal()*3, m_randomEngine->getStdUniformReal()*3));
        
        m_currentTarget = m_targets[m_targetCounter];
        
    }
    
    // Convert the general ModuleData input to the specific input type
    DroneInput * input = dynamic_cast<DroneInput*>(data);
    
    // If the conversion fails, throw an exception. Could also return false, but this contains more information and if it fails once, it will always fail.
    if (input == NULL)
        throw("Drone::triggerFcn: Invalid input data");
    
    // Construct the input of the sensor
    m_rssiSensorInput.droneData = input->droneData;
    m_dynamics.getPos(m_rssiSensorInput.myPos);
    m_dynamics.getDroneHeading(m_rssiSensorInput.myHeading);
    
    // Update the sensor measurements
    m_rssiSensor.trigger(Ts, &m_rssiSensorInput);
    
    // Update the control action - No need to copy sensor inpout to controller output as they have already been linked in the constructor
    m_droneController.trigger(Ts, &m_controllerInput);
    
    // Update the dynamics - No need to copy the controller's output, as it's been done in the constructor
    m_dynamics.trigger(Ts, &m_dynamicsInput);
    
    m_dynamics.getDroneHeading(m_heading);
    m_dynamics.getPos(m_pos);
    
    return true;
}