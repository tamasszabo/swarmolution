//
//  RSSIDistanceSensor.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//


#include "RSSIDistanceSensor.h"

#define HEADING_GAIN_NUM_SPLINES    6
#define HEADING_GAIN_SPLINES_ORDER  3

RSSIDistanceSensor::RSSIDistanceSensor(RandomEngine *random, unsigned int sensorRate):
Sensor("RSSIDistanceSensor", random, sensorRate, (FP)RSSI_SENSOR_NOISE_POWER, (FP)RSSI_SENSOR_RANGE)
{
    FP parData[] =
    {
        //        -64.2048,
        //        1.2657,
        //        3.1939,
        //        16.3413,
        //        18.1105,
        //        0.0000,
        //        -2.3123,
        //        -6.7230,
        //        -6.0424,
        //        0.0000,
        //        -0.8658,
        //        -3.6936,
        //        -4.4562,
        //        0.0000,
        //        -4.1680,
        //        12.6850,
        //        -4.4562,
        //        0.0000,
        //        -4.6275,
        //        13.6474,
        //        -4.9601,
        //        0.0000,
        //        4.0000,
        //        -22.4913,
        //        32.8843,
        //        0.0000,
        -68.7400,
        1.4178,
        -5.3187,
        -40.5642,
        -99.9996,
        -80.7627,
        -0.3700,
        -0.2759,
        3.6367,
        5.0328,
        7.0524,
        9.7781,
        0.2751,
        -0.9892,
        -6.6914,
        7.6466,
        0.2751,
        -0.9892,
        -10.5826,
        51.7043,
        -79.1977,
        38.3884,
        3.1171,
        -24.8292,
        61.1045,
        -45.6060,
    };
    m_modelPar.assign(parData, parData+(2+HEADING_GAIN_NUM_SPLINES*(HEADING_GAIN_SPLINES_ORDER+1)));
}
RSSIDistanceSensor::~RSSIDistanceSensor()
{
}

bool RSSIDistanceSensor::triggerFcn(const TimeStep &Ts, ModuleData *data)
{
    (void)Ts;
    
    // Convert the general ModuleData input to the specific input type
    RSSISensorInput *inputs = reinterpret_cast<RSSISensorInput*>(data);
    
    // If the conversion fails, throw an exception. Could also return false, but this contains more information and if it fails once, it will always fail.
    if (inputs == NULL)
        throw("RSSIDistanceSensor::triggerFcn: Invalid input data");


    // Resize the output vector
    if (inputs->rssi.size() != (inputs->droneData->size()-1))
        inputs->rssi.resize( (inputs->droneData->size()-1) );
    
    for (int i=0; i < inputs->droneData->size(); ++i)
    {
        // No need to calculate RSSI to the drone itself
        if ( (*inputs->droneData)[i].ID == inputs->myID )
            continue;
        
        // The location of the drone in the measurement vector corresponds to its ID. This is so that the filters will always track the right RSSI readings
        unsigned int placeIndex = (*inputs->droneData)[i].ID;
        
        // Since the drone doesn't measure a distance to itself, the ones with a higher ID move up one slot
        if (placeIndex > inputs->myID)
            placeIndex--;
        
        // Vector from my position to the other drone
        gVector relPos(inputs->myPos, (*inputs->droneData)[i].pos);
        
        // The direction angle (in world coordinates) to the other drone
        FP directionAngle = (FP)relPos.angle();
        
        inputs->rssi[placeIndex] = m_modelPar[0]
                            - 10*m_modelPar[1]*log10(relPos.norm())
                            + getSensorGain(inputs->myHeading               - directionAngle + (FP)M_PI)
                            + getSensorGain((*inputs->droneData)[i].heading    - directionAngle       )
                            + m_sensorNoise*((FP)m_randomEngine->getStdUniformReal());
        
//        inputs->rssi[placeIndex] = m_modelPar[0] - 10*m_modelPar[1]*log10(relPos.norm()); // Ideal model without gains or noise
    }
    
    return true;
}


FP RSSIDistanceSensor::getSensorGain(FP relativeHeading)
{
    // Ensure that the relative heading is in the range of [-PI, PI]
    while (relativeHeading > M_PI)
        relativeHeading -= 2*M_PI;
    while (relativeHeading < -M_PI)
        relativeHeading += 2*M_PI;
    
    // Get the index of the first spline coefficient
    unsigned int coeffIndex = 2 + ((unsigned int)(floor(((relativeHeading/(2*M_PI))+0.5)/(1.f/HEADING_GAIN_NUM_SPLINES))*(HEADING_GAIN_SPLINES_ORDER+1)));
    
    FP sensorGain = 0;
    for (int currentOrder = HEADING_GAIN_SPLINES_ORDER; currentOrder >= 0; --currentOrder)
        sensorGain += m_modelPar[coeffIndex++]*pow(relativeHeading, currentOrder);
    
    return 0;
}