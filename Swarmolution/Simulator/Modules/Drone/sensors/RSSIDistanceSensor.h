//
//  RSSIDistanceSensor.h
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__RSSIDistanceSensor__
#define __Swarmolution__RSSIDistanceSensor__

#include <stdio.h>

#include "DroneData.h"

#include "../../../Core/Sensor.h"

#define RSSI_SENSOR_RANGE               5
#define RSSI_SENSOR_NOISE_POWER         3.2

////////////////////////////////////////////////////////////////////////
// The module specific input data type
////////////////////////////////////////////////////////////////////////
class RSSISensorInput: public ModuleData
{
public:
    vector<DroneData>   *droneData;
    unsigned char       myID;
    Point               myPos;
    FP                  myHeading;
    
    vector<char>        rssi;
    
public:
    RSSISensorInput()
    {
    }
    ~RSSISensorInput() {}
};
////////////////////////////////////////////////////////////////////////
// The module definition
////////////////////////////////////////////////////////////////////////
class RSSIDistanceSensor: public Sensor
{
    vector<FP>          m_modelPar;
    
public:
    RSSIDistanceSensor(RandomEngine *random, unsigned int sensorRate);
    ~RSSIDistanceSensor();
    
protected:
    bool        triggerFcn(const TimeStep &Ts, ModuleData *data);
    FP          getSensorGain(FP relativeHeading);
};

#endif /* defined(__Swarmolution__RSSIDistanceSensor__) */
