//
//  DroneController.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 12/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "DroneController.h"
#include "../../../../Tools/Filters/KalmanFilters/EKF/ExtendedKalmanFilter.h"
#include "../../../../Tools/Filters/DiscreteFilters/DiscreteMinMaxFilter.h"

#include "../../../../AI/Models/BehaviorTrees/BehaviorTree.h"
#include "../../../../AI/Models/BehaviorTrees/Nodes/BTComposites.h"
#include "../../../../AI/Models/BehaviorTrees/Nodes/BTLeaves.h"

#define FSL_A_model     -68.8909f
#define FSL_n_model     1.3808f

////////////////////////////////////////////////////////////////////////
// The definition of the distance (EKF) Filter: RSSI->distance
////////////////////////////////////////////////////////////////////////
class DistanceFilter: public ExtendedKalmanFilter<FP,1,1,1>
{
public:
    DistanceFilter():
    ExtendedKalmanFilter<FP,1,1,1>(true)
    {
    }
    ~DistanceFilter() {}
    
protected:
    Eigen::Matrix<FP,1,1>   calcF( FP dt, Eigen::Matrix<FP,1,1> x, std::vector<FP> input)
    {
        (void)dt;
        (void)input;
        return x;
    }
    Eigen::Matrix<FP,1,1>   calcH( FP dt, Eigen::Matrix<FP,1,1> x, std::vector<FP> input)
    {
        (void)dt;
        (void)input;
        
        Eigen::Matrix<FP,1,1>  h(1);
        h(0) = FSL_A_model - 10*FSL_n_model*log10(x(0));
        return h;
    }
    void  calcFx(FP dt, Eigen::Matrix<FP,1,1> x, std::vector<FP> input, Eigen::Matrix<FP,1,1> &Phi, Eigen::Matrix<FP,1,1> &Gamma)
    {
        (void)dt;
        (void)input;
        (void)x;
        
//        Phi     = Mat(1,1);
        Phi(0)  = 1;
        
//        Gamma   = Mat(1,1);
        Gamma(0)= 1;
    }
    void calcHx(FP dt, Eigen::Matrix<FP,1,1> x, std::vector<FP> input, Eigen::Matrix<FP,1,1> &Hx)
    {
        (void)dt;
        (void)input;
        
//        Mat Hx(1,1);
        Hx(0,0) = -(10*FSL_n_model)/(x(0)*log(10));
    }
};
////////////////////////////////////////////////////////////////////////
// Custom BT Node that guides the MAVs to a target
////////////////////////////////////////////////////////////////////////
class BTTarget: public BTNode
{
protected:
    Point   *m_currentPos;
    Point   *m_currentTarget;
    float   *m_currentHeading;
    
public:
    BTTarget(Point *currentPos, Point *currentTarget, float *currentHeading):
        TreeNode("BTTarget"), BTNode("BTTarget", false),
        m_currentPos(currentPos),
        m_currentTarget(currentTarget),
        m_currentHeading(currentHeading)
        {}
        
    BTTarget(const BTTarget &other):
        TreeNode("BTTarget"), BTNode("BTTarget", false), m_currentPos(other.m_currentPos), m_currentHeading(other.m_currentHeading), m_currentTarget(other.m_currentTarget) {}
    virtual             ~BTTarget() {}
    
    virtual BTTarget*   clone() { return new BTTarget(*this); }
    
    std::string         getTypeAsString() { return std::string("tBTTarget"); }
    std::string         genCode(std::deque<std::string> &headerLines, std::deque<std::string> &sourceLines, unsigned int &varCounter) { throw("NOT IMPLEMENTED!"); return std::string(""); }
protected:
    void                saveAttributes(pugi::xml_node &node) {}
    bool                loadAttributes(pugi::xml_node &node) {return false;}
    
public:
    BTNodeState         tickFcn(BTWorkspace* workspace)
    {
        // Algorithm from "A Smooth Control Law for Graceful Motion of Differential Wheeled Mobile Robots in 2D Environment" by JJ Park, B Kuipers (ICRA 2011) - Equation 13 with Theta=0
        
        gVector posToTarget(*m_currentPos, *m_currentTarget);
        

        
        float k1 = 5;
        float k2 = 10;
        
        float targetAngle = *m_currentHeading - posToTarget.angle();

        while(targetAngle > M_PI)
            targetAngle -= 2*M_PI;
        
        while(targetAngle < -M_PI)
            targetAngle += 2*M_PI;
        
        float Vcmd =  (posToTarget.norm())*(1 - (fabs(targetAngle)/M_PI));
        
        
        if (Vcmd > 1)
            Vcmd = 1;
        
        float omega = -(Vcmd*0.5/posToTarget.norm())*(k2*targetAngle + 1+k1*sin(targetAngle));
        
        omega /= (60*M_PI/180);
//        if (omega > 1)
//            omega = 1;
//        if (omega < -1)
//            omega = -1;
//        float psiUpdate = (posToTarget.angle() - (*m_currentHeading))/DRONE_PSI_INCREMENT;
//        
//        if (psiUpdate > 0.1)
//            psiUpdate = 0.1;
//        if (psiUpdate < -0.1)
//            psiUpdate = -0.1;
        
        
        workspace->setPar(0, Vcmd);
        workspace->setPar(1, omega);
        workspace->setPar(2, 0);
        
        return BTNodeState::Success;
    }
};
////////////////////////////////////////////////////////////////////////
// The definition of the controller structure
////////////////////////////////////////////////////////////////////////

DroneController::DroneController(RandomEngine *random, unsigned int moduleRate, AICore *ai, unsigned int numTrackedDrones, Point *dronePos, float *droneHeading, Point *target):
Module("DroneController", random, moduleRate),
m_ai(ai->clone()),
m_updateEnabled(true),
m_psiCmd(0)
{
//     Add the default behavior to the BT
    BTNode *defBehavRoot = new BTSequence();
    BTNode *tmp;
    tmp = new BTSetC(0,1); defBehavRoot->addChild(tmp);
    tmp = new BTSetC(1,0); defBehavRoot->addChild(tmp);
    tmp = new BTSetC(2,0); defBehavRoot->addChild(tmp);
    
//    // Add the default behavior to the BT
//    BTNode *defBehavRoot = new BTTarget(dronePos, target, droneHeading);
    
    dynamic_cast<BehaviorTree*>(m_ai)->getRoot()->addChild(defBehavRoot);
    
    // Initialize the AI's IO
    m_aiIO.in.resize(m_ai->getNumIn());
    m_aiIO.out.resize(m_ai->getNumOut());
    
    // Distance filter parameters
    FP x[]  = {2};
    FP P[]  = {1};
    FP Q[]  = {0.1f};
    FP R[]  = {100};
    
    KalmanFilter<FP,1,1> *distanceFilter = NULL;
    for (int i = 0; i < numTrackedDrones; ++i)
    {
        // Add the RSSI-distance EKF to the list of filters
        distanceFilter = new DistanceFilter();
        
        distanceFilter->setR(std::vector<FP>(R,R + sizeof(R) / sizeof(FP)));
        distanceFilter->setQ(std::vector<FP>(Q,Q + sizeof(Q) / sizeof(FP)));
        distanceFilter->setState(std::vector<FP>(x,x + sizeof(x) / sizeof(FP)));
        distanceFilter->setCovariance(std::vector<FP>(P,P + sizeof(P) / sizeof(FP)));
        
        m_distanceFilters.push_back(distanceFilter);
        
        // Add the min/max filter
        m_prefFilters.push_back(new DiscreteMinMaxFilter(DiscreteMinMaxFilter::max, 5, -78));
    }
}
DroneController::DroneController(const DroneController &other):
Module(other),
m_ai(other.m_ai->clone()),
m_aiIO(other.m_aiIO),
m_updateEnabled(other.m_updateEnabled),
m_psiCmd(other.m_psiCmd)
{
    // Initialize the AI's IO
    m_aiIO.in.resize(m_ai->getNumIn());
    m_aiIO.out.resize(m_ai->getNumOut());
    
    // Distance filter parameters
    FP Q[]  = {0.1f};
    FP R[]  = {100};
    
    KalmanFilter<FP,1,1> *distanceFilter = NULL;
    for (int i = 0; i < other.m_distanceFilters.size(); ++i)
    {
        std::vector<FP> state;
        Eigen::Matrix<FP, 1,1> cov;
        other.m_distanceFilters[i]->getState(state);
        other.m_distanceFilters[i]->getCovariance(cov);
        
        // Add the RSSI-distance EKF to the list of filters
        distanceFilter = new DistanceFilter();
        
        distanceFilter->setR(std::vector<FP>(R,R + sizeof(R) / sizeof(FP)));
        distanceFilter->setQ(std::vector<FP>(Q,Q + sizeof(Q) / sizeof(FP)));
        distanceFilter->setState(state);
        distanceFilter->setCovariance(cov);
        
        m_distanceFilters.push_back(distanceFilter);
        
        // Add the min/max filter
        m_prefFilters.push_back(new DiscreteMinMaxFilter(DiscreteMinMaxFilter::max, 5, -78));
    }
}
DroneController::~DroneController()
{
    // Clean up the clone of the AI
    delete m_ai;
    
    // Clean up the distance filters
    while (m_distanceFilters.size() > 0)
    {
        delete m_distanceFilters.back();
        m_distanceFilters.pop_back();
    }
    
    // Clean up the prefilters
    while(m_prefFilters.size() > 0)
    {
        delete m_prefFilters.back();
        m_prefFilters.pop_back();
    }
    
}
    

bool DroneController::triggerFcn(const TimeStep &Ts, ModuleData *data)
{
    // Convert the general ModuleData input to the specific input type
    DroneControllerInput *input = dynamic_cast<DroneControllerInput*>(data);
    
    // If the conversion fails, throw an exception. Could also return false, but this contains more information and if it fails once, it will always fail.
    if (input == NULL)
        throw("DroneController::triggerFcn: Invalid input data");
    
    FP      smallestDistance    = INFINITY;
    int     smallestDistanceID  = -1;
    // Update the distance estimates to the other drones
    for (int i=0; i < m_distanceFilters.size(); ++i)
    {
        // Step the prefliter
        std::vector<FP> distIO(1, m_prefFilters[i]->step((*input->rssi)[i]));
        
        // Step the distance filter
        m_distanceFilters[i]->step(Ts.dt, std::vector<FP>(0,0), distIO);
        m_distanceFilters[i]->getState(distIO);
        
        // Save the smallest distance estimate & the ID that it belongs to
        if (distIO[0] < smallestDistance)
        {
            smallestDistance    = distIO[0];
            smallestDistanceID  = i;
        }
        
    }
    // Construct AI input
    // AI Input Structure:
    // 0. Distance to the closest drone [m]
    m_aiIO.in[0] = smallestDistance/2.5 - 1;  // <= map the region [0 5] meters to the evolved BT's inoput [-1 1]
    if (m_aiIO.in[0] > 1)
        m_aiIO.in[0] = 1;
    
    // Trigger AI - even if the update itself is disabled!
    m_ai->trigger(m_aiIO);
    
    // Only apply the AI's output if the controller update is enabled
    if (m_updateEnabled)
    {
        // AI output strucure:
        // 0. Velocity Cmd  [-1, 1]
        // 1. Psi Dot Cmd   [-1, 1]
        // 2. Psi incr Cmd  [-1, 1]
        
        for (std::vector<FP>::iterator it = m_aiIO.out.begin(); it != m_aiIO.out.end(); ++it)
        {
            if ((*it) < -1)
                (*it) = -1;
            
            if ((*it) > 1)
                (*it) = 1;
        }

        m_psiCmd += m_aiIO.out[1]*DRONE_MAX_TURN_RATE*Ts.dt + m_aiIO.out[2]*DRONE_PSI_INCREMENT;
        
        // Ensure that m_psiCmd remains in the range [-pi pi]
        while (m_psiCmd < -M_PI)
            m_psiCmd += (2*M_PI);
        
        while (m_psiCmd > M_PI)
            m_psiCmd -= (2*M_PI);        
        
        // The Controller's output structure is:
        // 0. Velocity Cmd          [in range of [-1, 1]
        // 1. Commanded heading     [rad]
        // 2. Commanded altitude    [m]
        
        input->ctrlAction[0]    = m_aiIO.out[0];
        input->ctrlAction[1]    = m_psiCmd;
        input->ctrlAction[2]    = 0; // This is the (constant) altitude where the drones fly at
    }
    
    return true;
}
void DroneController::enableUpdate(bool enable)
{
    m_updateEnabled = enable;
}
AICore* DroneController::getAI()
{
    return m_ai;
}
const AIIO* DroneController::getAIIO() const
{
    return const_cast<const AIIO*>(&m_aiIO);
}
void DroneController::getDistanceEstimates(std::vector<float> &data)
{
    // WARNING: Data may already contain information! DO NOT CLEAR IT!
    
    for (std::vector<KalmanFilter<FP,1,1>*>::iterator it = m_distanceFilters.begin(); it != m_distanceFilters.end(); ++it)
    {
        // Get the reading into a temp vector
        std::vector<FP> tmp;
        (*it)->getState(tmp);
        
        // Push it into the data vector
        data.push_back(tmp.front());
    }
}
FP DroneController::getTurnRateCmd()
{
    return m_aiIO.out[1]*DRONE_MAX_TURN_RATE;
}
float   DroneController::getDefBehavRatio()
{
    BTNode *btRoot      = dynamic_cast<BehaviorTree*>(m_ai)->getRoot();
    BTNode *behavRoot   = btRoot->getLastChild();
    
    unsigned long rootTickCount     = btRoot->getTickCount();
    unsigned long defBehavTickCount = behavRoot->getTickCount();
    
    float rVal = ((double)defBehavTickCount)/((double)rootTickCount);
    return rVal;
}