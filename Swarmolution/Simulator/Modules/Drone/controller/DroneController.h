//
//  DroneController.h
//  Swarmolution
//
//  Created by Tamas Szabo on 12/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__DroneController__
#define __Swarmolution__DroneController__

#include <stdio.h>
#include <vector>
#include "geometry.h"

#include "../../../Core/Module.h"
#include "../../../../AI/Core/AICore.h"
#include "../../../../Tools/Filters/KalmanFilters/KalmanFilter.h"
#include "../../../../Tools/Filters/DiscreteFilters/DiscreteFilter.h"
#include "../../../../Tools/Filters/DiscreteFilters/DigitalFilter.h"

////////////////////////////////////////////////////////////////////////
// The definition of the contoller IO data
////////////////////////////////////////////////////////////////////////
class DroneControllerInput: public ModuleData
{
public:
    vector<char>    *rssi;
    vector<FP>      ctrlAction;
    
public:
    DroneControllerInput()  {}
    ~DroneControllerInput() {}
};

////////////////////////////////////////////////////////////////////////
// The definition of the controller class
////////////////////////////////////////////////////////////////////////

#define DRONE_MAX_TURN_RATE (40*M_PI/180)
#define DRONE_PSI_INCREMENT (180*M_PI/180)

#define DRONE_VELFILTER_TAU 10
class DroneController: public Module
{
protected:
    // AI module & IO
    AICore                          *m_ai;
    AIIO                            m_aiIO;
    
    // Distance filters
    std::vector<KalmanFilter<FP,1,1>*> m_distanceFilters;
    std::vector<DiscreteFilter*>    m_prefFilters;
    
    // Flag for disabling the controller
    bool                            m_updateEnabled;
    
    // Commanded (true) drone heading
    FP                              m_psiCmd;
    
public:
    DroneController(RandomEngine *random, unsigned int moduleRate, AICore *ai, unsigned int numTrackedDrones, Point *dronePos, float *droneHeading, Point *target);
    DroneController(const DroneController &other);
    ~DroneController();
    
    void    enableUpdate(bool enable);
    
    AICore* getAI();
    const AIIO*   getAIIO() const;
    
    // Logging functions
    void    getDistanceEstimates(std::vector<float> &data);
    FP      getTurnRateCmd();
    
    // Performance tools
    float   getDefBehavRatio();
protected:
    bool triggerFcn(const TimeStep &Ts, ModuleData *data);
};
#endif /* defined(__Swarmolution__DroneController__) */
