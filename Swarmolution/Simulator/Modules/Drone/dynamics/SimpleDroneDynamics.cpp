//
//  SimpleDroneDynamics.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "SimpleDroneDynamics.h"

SimpleDroneDynamics::SimpleDroneDynamics(unsigned int rate, RandomEngine *random, Point P0, FP psi0, IntegratorModule::Integrator integrator):
IntegratorModule("SimpleDroneDynamics", random, rate, integrator),
m_psi0(psi0),
m_velCmd(0),
m_currentPsi(0)
{
//    m_A.resize(7, 7);
//    m_B.resize(7, 3);
//    m_input.resize(3,1);
//    m_state.resize(7,1);
    
//    m_A << (-2)*DRONE_DYNAMICS_Z*DRONE_DYNAMICS_W , 0,                                          (-1)*pow(DRONE_DYNAMICS_W,2) ,  0,                                  0,  0,  0,
//            0,                                      (-2)*DRONE_DYNAMICS_Z*DRONE_DYNAMICS_W ,    0,                              (-1)*pow(DRONE_DYNAMICS_W,2) ,      0,  0,  0,
//            1,                                      0,                                          0,                              0,                                  0,  0,  0,
//            0,                                      1,                                          0,                              0,                                  0,  0,  0,
//            0,                                      0,                                          1,                              0,                                  0,  0,  0,
//            0,                                      0,                                          0,                              1,                                  0,  0,  0,
//            0,                                      0,                                          0,                              0,                                  0,  0,  -(1/DRONE_ALT_TAU);
//    
//    
//    m_B << pow(DRONE_DYNAMICS_W,2), 0,                          0,
//            0,                      pow(DRONE_DYNAMICS_W,2),    0,
//            0,                      0,                          0,
//            0,                      0,                          0,
//            0,                      0,                          0,
//            0,                      0,                          0,
//            0,                      0,                          (1/DRONE_ALT_TAU);
//    
//    m_input << 0, 0, 0;
    
    m_A(0,0) = (-2)*DRONE_DYNAMICS_Z*DRONE_DYNAMICS_W;
    m_A(0,1) = 0;
    m_A(0,2) = (-1)*pow(DRONE_DYNAMICS_W,2);
    m_A(0,3) = 0;
    m_A(0,4) = 0;
    m_A(0,5) = 0;
    m_A(0,6) = 0;
    
    m_A(1,0) = 0;
    m_A(1,1) = (-2)*DRONE_DYNAMICS_Z*DRONE_DYNAMICS_W;
    m_A(1,2) = 0;
    m_A(1,3) = (-1)*pow(DRONE_DYNAMICS_W,2);
    m_A(1,4) = 0;
    m_A(1,5) = 0;
    m_A(1,6) = 0;
    
    m_A(2,0) = 1;
    m_A(2,1) = 0;
    m_A(2,2) = 0;
    m_A(2,3) = 0;
    m_A(2,4) = 0;
    m_A(2,5) = 0;
    m_A(2,6) = 0;
    
    m_A(3,0) = 0;
    m_A(3,1) = 1;
    m_A(3,2) = 0;
    m_A(3,3) = 0;
    m_A(3,4) = 0;
    m_A(3,5) = 0;
    m_A(3,6) = 0;
    
    m_A(4,0) = 0;
    m_A(4,1) = 0;
    m_A(4,2) = 1;
    m_A(4,3) = 0;
    m_A(4,4) = 0;
    m_A(4,5) = 0;
    m_A(4,6) = 0;
    
    m_A(5,0) = 0;
    m_A(5,1) = 0;
    m_A(5,2) = 0;
    m_A(5,3) = 1;
    m_A(5,4) = 0;
    m_A(5,5) = 0;
    m_A(5,6) = 0;
    
    m_A(6,0) = 0;
    m_A(6,1) = 0;
    m_A(6,2) = 0;
    m_A(6,3) = 0;
    m_A(6,4) = 0;
    m_A(6,5) = 0;
    m_A(6,6) = -(1/DRONE_ALT_TAU);
    
    //Definition of B
    m_B(0,0) = pow(DRONE_DYNAMICS_W,2);
    m_B(0,1) = 0;
    m_B(0,2) = 0;
    
    m_B(1,0) = 0;
    m_B(1,1) = pow(DRONE_DYNAMICS_W,2);
    m_B(1,2) = 0;
    
    m_B(2,0) = 0;
    m_B(2,1) = 0;
    m_B(2,2) = 0;
    
    m_B(3,0) = 0;
    m_B(3,1) = 0;
    m_B(3,2) = 0;
    
    m_B(4,0) = 0;
    m_B(4,1) = 0;
    m_B(4,2) = 0;
    
    m_B(5,0) = 0;
    m_B(5,1) = 0;
    m_B(5,2) = 0;
    
    m_B(6,0) = 0;
    m_B(6,1) = 0;
    m_B(6,2) = (1/DRONE_ALT_TAU);
    
    // Input vector
    m_input(0,0) = 0;
    m_input(1,0) = 0;
    m_input(2,0) = 0;
    
    // State vector
    m_state << 0, 0,  0, 0, P0.x, P0.y, P0.z;

}
SimpleDroneDynamics::~SimpleDroneDynamics()
{
    
}
void SimpleDroneDynamics::getPos(Point &pos)
{
    pos.x = m_state(4);
    pos.y = m_state(5);
    pos.z = m_state(6);
}
void SimpleDroneDynamics::getVelocityHeading(FP &psi)
{
    psi = atan2(m_state(3), m_state(2));
}
void SimpleDroneDynamics::getDroneHeading(FP &psi)
{
    psi = atan2(m_state(3), m_state(2));
    
    double psiErr = psi-m_currentPsi;
    
    while (psiErr > M_PI)
        psiErr -= 2*M_PI;
    
    while (psiErr < -M_PI)
        psiErr += 2*M_PI;
    
    
    // The velocity heading and the commanded heading are off by more than pi/4 than the drone is flying backwards (the tolerance of pi/4 is needed to account for the inertia during turns)
    if (fabs(psiErr) > (M_PI/4))
    {
        psi += M_PI;
    
        while (psi > M_PI)
            psi -= 2*M_PI;
        
        while (psi < -M_PI)
            psi += 2*M_PI;
    }
}
void SimpleDroneDynamics::getState(std::vector<FP> &state)
{
    if(state.size() != 3)
        state.resize(3);
    
    state[0]    = sqrt(pow(m_state(2),2) + pow(m_state(3),2));  // Velocity
    state[1]    = atan2(m_state(3), m_state(2));                // Heading
    state[2]    = m_state(6);                                   // Altitude
}

void SimpleDroneDynamics::getVelocityVector(gVector &vel)
{
    vel.x = m_state(2);
    vel.y = m_state(3);
    vel.z = (1/DRONE_ALT_TAU)*(m_input(2) - m_state(6));
}
void SimpleDroneDynamics::setVelocityVector(const gVector &vel)
{
    
    // Set the bounce-back heading setpoint:
    //      Since there is no *direct* relation between the flown heading, commanded heading and heading setpoint,
    //      the heading setpoint needs to be updated with the difference ange of the current (flown) heading and the
    //      bounce-back heading.
    m_psi0 += vel.angle() - atan2(m_state(3),m_state(2));
    
    while (m_psi0 > M_PI)
        m_psi0 -= 2*M_PI;
    while (m_psi0 < -M_PI)
        m_psi0 += 2*M_PI;
    
    FP velocityHeading      = vel.angle();
    
    // Rotate the acceleration vector to the new direction
    //        FP currentAcceleration  = sqrt(pow(m_stateVector[0],2) + pow(m_stateVector[1],2));
    m_state[0] = 0;//currentAcceleration*cos(velocityHeading);
    m_state[1] = 0;//currentAcceleration*sin(velocityHeading);
    
    // Rotate the velocity vector to the new direction
    FP currentVelocity  = sqrt(pow(m_state(2),2) + pow(m_state(3),2));
    m_state(2)        = currentVelocity*cos(velocityHeading);
    m_state(3)        = currentVelocity*sin(velocityHeading);

}
void SimpleDroneDynamics::calcDerivatives(Eigen::Matrix<FP,7,1> &out, Eigen::Matrix<FP,7,1> &state, const TimeStep &Ts)
{
    (void)state;
    (void)Ts;
    // state_dot = A*state + B*inputs
    out = m_A*state + m_B*m_input;
}
bool SimpleDroneDynamics::triggerFcn(const TimeStep &Ts, ModuleData *data)
{
    // Convert the general ModuleData input to the specific input type
    SimpleDroneDynamicsInput *inputs = dynamic_cast<SimpleDroneDynamicsInput*>(data);
    
    // If the conversion fails, throw an exception. Could also return false, but this contains more information and if it fails once, it will always fail.
    if (inputs == NULL)
        throw("SimpleDroneDynamics::triggerFcn: Invalid input data");
    
    // Calculate system inputs
    m_velCmd        = (*inputs->vCmd)*DRONE_MAX_V;
    m_currentPsi    = (*inputs->psiCmd) + m_psi0;
    
    m_input(0) = cos(m_currentPsi)*m_velCmd;
    m_input(1) = sin(m_currentPsi)*m_velCmd;
    m_input(2) = (*inputs->altCmd);
    
    // Integrate the system
    integrate(Ts);
    
    
    return true;
}