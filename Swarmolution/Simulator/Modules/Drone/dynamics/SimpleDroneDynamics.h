//
//  SimpleDroneDynamics.h
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__SimpleDroneDynamics__
#define __Swarmolution__SimpleDroneDynamics__

#include <stdio.h>

#include "../../../../Tools/Geometry/geometry.h"
#include "../../../Core/IntegratorModule.h"


//#define DRONE_DYNAMICS_Z            0.7832
//#define DRONE_DYNAMICS_W            2.5838

#define DRONE_DYNAMICS_Z            0.56
#define DRONE_DYNAMICS_W            2.74

#define DRONE_ALT_TAU               0.3

#define DRONE_MAX_V                 0.5
#define DRONE_MAX_PSIDOT            (30*(M_PI/180))

#define DRONE_DEFAULT_ALTITUDE      0

////////////////////////////////////////////////////////////////////////
// The module specific input data type
////////////////////////////////////////////////////////////////////////
class SimpleDroneDynamicsInput: public ModuleData
{
public:
    FP   *vCmd;
    FP   *psiCmd;
    FP   *altCmd;
    
public:
    SimpleDroneDynamicsInput()  {}
    ~SimpleDroneDynamicsInput() {}
};

////////////////////////////////////////////////////////////////////////
// The module definition
////////////////////////////////////////////////////////////////////////
class SimpleDroneDynamics: public IntegratorModule<7>
{
//    Mat         m_A;
//    Mat         m_B;
//    
//    Vec         m_input;

    Eigen::Matrix<FP, 7, 7>  m_A;
    Eigen::Matrix<FP, 7, 3>  m_B;
    
    Eigen::Matrix<FP, 3, 1>  m_input;
    FP       m_psi0;
    
    FP       m_velCmd;
    FP       m_currentPsi;
    
public:
    SimpleDroneDynamics(unsigned int rate, RandomEngine *random, Point P0, FP psi0, IntegratorModule::Integrator integrator = IntegratorModule::Euler);
    ~SimpleDroneDynamics();
    
    void getPos(Point &pos);
    void getVelocityHeading(FP &psi);
    void getDroneHeading(FP &psi);
    void getState(std::vector<FP> &state);
    
    void getVelocityVector(gVector &vel);
    void setVelocityVector(const gVector &vel);
    
protected:
    void calcDerivatives(Eigen::Matrix<FP,7,1> &out, Eigen::Matrix<FP,7,1> &state, const TimeStep &Ts);
    bool triggerFcn(const TimeStep &Ts, ModuleData *data);
};
#endif /* defined(__Swarmolution__SimpleDroneDynamics__) */
