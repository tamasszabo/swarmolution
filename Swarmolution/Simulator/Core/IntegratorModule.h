//
//  IntegratorModule.h
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__IntegratorModule__
#define __Swarmolution__IntegratorModule__

#include <stdio.h>

#include "Module.h"

template <int S>
class IntegratorModule: public Module
{
// Members
public:
    enum Integrator
    {
        Euler,
        RK4
    };
protected:
    Eigen::Matrix<FP,S,1>         m_state;
    Integrator  m_integrator;
    
// Functions
public:
    IntegratorModule(std::string moduleName, RandomEngine *random, unsigned int moduleRate = 1, Integrator integrator = Integrator::RK4):
    Module(moduleName,random,moduleRate),
    m_integrator(integrator)
    {
    }
    
    virtual ~IntegratorModule()
    {
    }
    
protected:
    virtual void calcDerivatives(Eigen::Matrix<FP,S,1> &out, Eigen::Matrix<FP,S,1> &state, const TimeStep &Ts) = 0;
    
    void    integrate(const TimeStep &Ts)
    {
#warning IntegratorModule::integrate should be implemented better, since the integration method does not change during runtime!
        switch(m_integrator)
        {
            case RK4:
                RK4Integrate(Ts);
                break;
            case Euler:
                EulerIntegrate(Ts);
                break;
            default:
                throw("IntegratorModule::integrate: Unknown integration method");
        }
    }
private:
    void    RK4Integrate(const TimeStep &Ts)
    {
        FP h        = getModuleTimeStep(Ts.dt)/2;
        TimeStep tmpTs  = Ts;
        
        tmpTs.time      += h;
        
        Eigen::Matrix<FP,S,1> k1,k2,k3,k4;
        Eigen::Matrix<FP,S,1> t1,t2,t3;
        
        calcDerivatives(k1,m_state,Ts);
        k1              = k1*Ts.dt;
        t1              = m_state + 0.5*k1;
        
        calcDerivatives(k2,t1,tmpTs);
        k2              = k2*Ts.dt;
        t2              = m_state + 0.5*k1;
        
        calcDerivatives(k3,t2,tmpTs);
        k3              = k3*Ts.dt;
        t3              = m_state + k1;
        
        calcDerivatives(k4,t3,Ts);
        k4              = k4*Ts.dt;
        
        m_state         = m_state + (k1 + 2*k2 + 2*k3 + k4)/6;;
    }
    
    
//    template<int S>
    void    EulerIntegrate(const TimeStep &Ts)
    {
        FP dt = getModuleTimeStep(Ts.dt);
        
        Eigen::Matrix<FP,S,1> stateIncrement;
        calcDerivatives(stateIncrement, m_state, Ts);
        
        m_state = m_state + stateIncrement*dt;
    }
    
};

#endif /* defined(__Swarmolution__IntegratorModule__) */
