//
//  Simulator.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__Simulator__
#define __Swarmolution__Simulator__

#include <stdio.h>
#include <vector>

#include "Module.h"
#include "../../Tools/Loggers/Logger.h"



class Simulator
{
public:
    enum SimState { Failed, Running, Success};
protected:
    RandomEngine    m_randomEngine;
    vector<float>  m_performanceMetrics;
    
    float           m_simTime;
    float           m_simEnd;
    
    SimState        m_state;
    
public:
    Simulator(std::seed_seq::result_type randomSeed, unsigned int numPerformanceMetrics):
    m_randomEngine(randomSeed),
    m_performanceMetrics(numPerformanceMetrics,0),
    m_simTime(0),
    m_state(Running)
    {
    }
    virtual ~Simulator() {};
    
    
    bool runSim(float timeStep, float simTime)
    {
        m_simTime = simTime;
        
        // Initialize the time step structure
        TimeStep Ts;
        Ts.dt   = timeStep;
        Ts.time = 0;
    
        // Return of the simulator's step() function.
        while ( (Ts.time < m_simTime) && (m_state == Running) )
        {
            step(Ts);
            m_state = evaluate(Ts);
            Ts.time += Ts.dt;
        }
        
        m_simEnd = Ts.time;
        
        if (m_state == Success)
        {
            m_simTime = Ts.time;
            return true;
        }
        
        if (m_state == Running)
            return true;
        
        return false;
    }
    float getSimTime()      { return m_simEnd; }
    float getMaxSimTime()   { return m_simTime; }
    
    virtual vector<float>&      getPerformanceMetrics()     = 0;
protected:
    virtual void                step(TimeStep &Ts)                        = 0;
    virtual SimState            evaluate(TimeStep &Ts)                    = 0;
};

#endif /* defined(__Swarmolution__Simulator__) */
