//
//  Module.h
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__Module__
#define __Swarmolution__Module__

#include <vector>
#include <string>

#include "../../Tools/Algebra.h"
#include "../../Tools/RandomEngine/RandomEngine.h"
#include "../../Tools/Debug/consoleMacros.h"



// TimeStep Definition
struct TimeStep
{
    FP dt;
    FP time;
};

////////////////////////////////////////////////////////////////////////
// ModuleData - The IO structure of the modules. Each module should derive its own IO structure from this
////////////////////////////////////////////////////////////////////////
class ModuleData
{
public:
    ModuleData() {};
    virtual ~ModuleData() {}
};

////////////////////////////////////////////////////////////////////////
// Module - The core of each simulation module. All modules should derive from this class
////////////////////////////////////////////////////////////////////////
class Module
{
private:
    std::string             m_moduleName;
    
    unsigned int            m_triggerRate;
    int                     m_triggerCounter;
    
protected:
    RandomEngine            *m_randomEngine;

public:
    Module(std::string moduleName, RandomEngine *random, unsigned int moduleRate = 1);
    Module(const Module &other);
    virtual ~Module();
    
    std::string         getName();
    
    float               getModuleTimeStep(float baseRate);
    
    bool                trigger(const TimeStep &Ts, ModuleData *data = NULL);
    
protected:
    virtual bool        triggerFcn(const TimeStep &Ts, ModuleData *data = NULL)            = 0;
};


#endif
