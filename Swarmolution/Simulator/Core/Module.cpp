//
//  Module.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "Module.h"

Module::Module(std::string moduleName, RandomEngine *random, unsigned int moduleRate):
m_moduleName(moduleName),
m_randomEngine(random),
m_triggerRate(moduleRate),
m_triggerCounter(-1)
{
    
}
Module::Module(const Module &other):
m_moduleName(other.m_moduleName),
m_randomEngine(other.m_randomEngine),
m_triggerRate(other.m_triggerRate),
m_triggerCounter(-1)
{
}
Module::~Module()
{
}
std::string Module::getName()
{
    return m_moduleName;
}

float Module::getModuleTimeStep(float baseRate)
{
    // Return the trigger rate in
    return m_triggerRate*baseRate;
}

bool Module::trigger(const TimeStep &Ts, ModuleData *data)
{
    
// Rate scheduling for the module - return true if nothing is to be done
    if (++m_triggerCounter >= m_triggerRate)
        m_triggerCounter = 0;
    
    if (m_triggerCounter != 0)
        return true;
    
// Call the module code
    return triggerFcn(Ts, data);

}