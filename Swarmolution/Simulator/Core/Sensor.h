//
//  Sensor.h
//  Swarmolution
//
//  Created by Tamas Szabo on 11/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__Sensor__
#define __Swarmolution__Sensor__

#include <stdio.h>
#include <vector>

#include "Module.h"


class Sensor: public Module
{
protected:
    FP               m_sensorNoise;
    FP               m_sensorRange;
    
public:
    Sensor(std::string sensorName, RandomEngine *random, unsigned int sensorRate, FP noisePower, FP sensorRange):
    Module(sensorName, random, sensorRate),
    m_sensorRange(sensorRange),
    m_sensorNoise(noisePower)
    {
    }
    virtual ~Sensor() {}

    float                       getRange()      { return m_sensorRange; }
};
#endif /* defined(__Swarmolution__Sensor__) */
