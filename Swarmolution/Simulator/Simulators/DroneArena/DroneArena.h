//
//  DroneArena.h
//  Swarmolution
//
//  Created by Tamas Szabo on 13/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef __Swarmolution__DroneArena__
#define __Swarmolution__DroneArena__

#include <stdio.h>
#include <vector>

#include "../../Core/Simulator.h"
#include "../../../AI/Core/AICore.h"
#include "../../Modules/Drone/Drone.h"

#include "../../../Tools/Geometry/geometry.h"


typedef std::vector<Drone*>::iterator droneIterator;

class DroneArena: public Simulator
{
public:
    enum Shape
    {
        fixed                       = 1,
        randomSize                  = 2,
        randomCircleFixedRadius     = 3,
        randomCircle                = 4
    };
    
protected:
    std::vector<Drone*>         m_drones;
    Polygon                     m_walls;
    PolyMap                     m_map;
    bool                        m_didSomething;
    
// Data logger tools
    unsigned int                m_trialID;
    int                         m_logCountIndex;
    Logger                      *m_dataLogger;
    
// Performance metrics data
//    std::vector<float>          m_primaryTaskTime;
    float                       m_minDistance;
    
    std::vector<unsigned int>       m_conflictCount;
    std::vector<std::vector<bool>>  m_conflictFlag;
    double                          m_conflictTime;
    
    std::vector<unsigned int>   m_crashCount;
public:
    DroneArena(std::seed_seq::result_type randomSeed, AICore* ai, Logger *dataLogger = NULL, unsigned int trialID = 0, unsigned int numDrones = 3, DroneArena::Shape shape = Shape::fixed, unsigned int vertexCount = 0, float wallLength = 6, bool faceEachOther = false);
    ~DroneArena();
    
    void                logEnvironmentShape(Logger *log);
    vector<float>&      getPerformanceMetrics();
    unsigned int        getConflictCount();
    float               getMeanConflictTime();
    float               getDefBehavRatio();
    double              getCoveredArea();
    
protected:
    void step(TimeStep &Ts);
    Simulator::SimState evaluate(TimeStep &Ts);
    
private:    
    void createRectangularWalls(double sideLen);
    void createRandomEnvironmentFromCircle(double vertexCount, double radius);
    void createRandomEnvironmentFromCircle(double vertexCount, double defaultRadius, double radiusDeviation);
};

#endif /* defined(__Swarmolution__DroneArena__) */
