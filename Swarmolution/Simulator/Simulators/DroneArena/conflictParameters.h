//
//  conflictParameters.h
//  Swarmolution
//
//  Created by Tamas Szabo on 01/06/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#ifndef Swarmolution_conflictParameters_h
#define Swarmolution_conflictParameters_h

extern float     DRONE_DRONE_CRASH_RANGE;
extern float     DRONE_DRONE_CONFLICT_RANGE;

#endif
