//
//  DroneArena.cpp
//  Swarmolution
//
//  Created by Tamas Szabo on 13/04/15.
//  Copyright (c) 2015 Tamas Szabo. All rights reserved.
//

#include "DroneArena.h"
#include "../../../AI/Models/BehaviorTrees/BehaviorTree.h"

#include "debug.h"

#include "conflictParameters.h"

float     DRONE_DRONE_CRASH_RANGE             = 0.8;
float     DRONE_DRONE_CONFLICT_RANGE          = 2;

#define     DEFAULT_WALL_DEVIATION_RATE         (1/3)
#define     DRONE_DRONE_MIN_INIT_DISTANCE       2

#define     MAP_RESOLUTION                      0.2


DroneArena::DroneArena(std::seed_seq::result_type randomSeed, AICore* ai, Logger *dLogger, unsigned int trialID, unsigned int numDrones, DroneArena::Shape shape, unsigned int vertexCount, float wallLength, bool faceEachOther):
Simulator(randomSeed, 3),
//Simulator(randomSeed, 2),
m_trialID(trialID),
m_conflictCount(numDrones,0),
m_crashCount(numDrones,0),
m_conflictFlag(numDrones,std::vector<bool>(numDrones,false)),
m_logCountIndex(0),
m_dataLogger(dLogger),
m_didSomething(false),
m_minDistance(INFINITY),
m_conflictTime(0)
{
    // Create the walls of the environment
    switch (shape)
    {
        case fixed:
            createRectangularWalls(wallLength);
            break;
        case randomSize:
            createRectangularWalls(wallLength + DEFAULT_WALL_DEVIATION_RATE*wallLength*m_randomEngine.getStdUniformReal());
            break;
        case randomCircleFixedRadius:
            createRandomEnvironmentFromCircle(vertexCount, wallLength);
            break;
        case randomCircle:
            createRandomEnvironmentFromCircle(vertexCount, wallLength, DEFAULT_WALL_DEVIATION_RATE*wallLength);
            break;
        default:
            throw("PocketDroneArena::contructor: Unhandled area generation type");
    }
    m_map.init(m_walls, MAP_RESOLUTION);
    
    // Initialize the drones
    float maxDistFromOrigin = 0.9*wallLength/2; // Maximum distance of the drones from the origin (center of the arena)
    
    std::vector<Point> initPos;
    Polygon dronesPoly;
    while (initPos.size() < numDrones)
    {
        Point P;
        P.x = maxDistFromOrigin*m_randomEngine.getStdUniformReal();
        P.y = maxDistFromOrigin*m_randomEngine.getStdUniformReal();
        P.z = 0;
        
        bool isValid = true;
        for (std::vector<Point>::iterator it = initPos.begin(); it != initPos.end(); ++it)
        {
            if (gVector(P,*it).norm() < DRONE_DRONE_MIN_INIT_DISTANCE)
            { isValid = false; break; }
        }
        
        if (isValid)
        {
            initPos.push_back(P);
            dronesPoly.addVertex(initPos.back());
        }
    }
    
    dronesPoly.simplify();
    
    if (dronesPoly.isSelfIntersecting())
    {
        S_LOG("Self intersecting polygon! \n Vertices:\n\n");
        dronesPoly.printVertices();
    }
    
    Point centroid = dronesPoly.getCentroid();
    
    while (initPos.size() > 0)
    {
       if (faceEachOther)
            m_drones.push_back(new Drone(&m_randomEngine, m_drones.size(), ai, initPos.back(), gVector(initPos.back(),centroid).angle(), numDrones));
        else
        {
                m_drones.push_back(new Drone(&m_randomEngine, m_drones.size(), ai, initPos.back(), m_randomEngine.getStdUniformReal()*M_PI, numDrones));

        }
        initPos.pop_back();
        
        std::deque<Point> targets;
        Point target;
        
        target.x = m_randomEngine.getStdUniformReal()*2.5;
        target.y = m_randomEngine.getStdUniformReal()*2.5;
        
//        targets.push_back(target);
//        for (unsigned int i=1; i < 100; ++i)
//        {
//            while (gVector(target,targets.back()).norm() < 3)
//            {
//                target.x = m_randomEngine.getStdUniformReal()*2.5;
//                target.y = m_randomEngine.getStdUniformReal()*2.5;
//            }
//            targets.push_back(target);
//        }
        targets.push_back(Point( 2.5,-2.5));
        targets.push_back(Point(-2.5, 2.5));
        targets.push_back(Point( 2.5, 2.5));
        targets.push_back(Point(-2.5,-2.5));
        
        targets.push_back(Point(2.5, 0));
        targets.push_back(Point(0, 2.5));
        targets.push_back(Point(-2.5,0));
        targets.push_back(Point(0,0));
        targets.push_back(Point(-2.5,0));
    
        m_drones.back()->setTargets(targets);
    }
    
}
DroneArena::~DroneArena()
{
    m_walls.clear();
    while (m_drones.size() > 0)
    {
        delete m_drones.back();
        m_drones.pop_back();
    }
}
unsigned int  DroneArena::getConflictCount()
{
    unsigned int cCount = 0;
    for (std::vector<unsigned int>::iterator it = m_conflictCount.begin(); it != m_conflictCount.end(); ++it)
        cCount += *it;
    
    return cCount/2;
}
float DroneArena::getMeanConflictTime()
{
    return m_conflictTime/((m_drones.size()*(m_drones.size()-1))/2);
}
float DroneArena::getDefBehavRatio()
{
    double behavRatioSum = 0;
    for (std::vector<Drone*>::iterator drone = m_drones.begin(); drone != m_drones.end(); ++drone)
        behavRatioSum += (*drone)->getDefBehavRatio();
    
    return behavRatioSum/m_drones.size();
}
double DroneArena::getCoveredArea()
{
    return m_map.getCoverage();
}
void DroneArena::step(TimeStep &Ts)
{
    // Create the data collection needed by the RSSI sensors
    vector<DroneData> droneData;
    droneData.resize(m_drones.size());
    
    for (droneIterator it = m_drones.begin(); it != m_drones.end(); ++it)
    {
        unsigned char id = (*it)->getID();
        
        droneData[id].ID = id;
        (*it)->getPos(droneData[id].pos);
        (*it)->getDroneHeading(droneData[id].heading);
    }
    
    // Propagate drones
    DroneInput droneInputs;
    droneInputs.droneData = &droneData;
    Drone *drone = NULL;
    for (droneIterator it = m_drones.begin(); it != m_drones.end(); ++it)
    {
        drone = *it;
        // Step the drone
        drone->trigger(Ts, &droneInputs);
        
        // Check if the drone is still within the arena
        Point P;
        drone->getPos(P);
        
        // The index of the wall that the drone is in conflict with
        int wallIndex = drone->getWallIndex();
        
        if (m_walls.pointInPoly(P))
        {
            Segment S;
            unsigned int segIndex;
            FP wallDistance = m_walls.minDistance(P, S, segIndex);
            if (wallDistance < 0.1)
            {
                FP droneHeading;
                drone->getVelocityHeading(droneHeading);
                
                Ray R(P, droneHeading);
                gVector bounce = m_walls.rayReflectionVector(R,S);
                if ( (bounce.x == bounce.x) && (bounce.y == bounce.y) && (bounce.z == bounce.z) )
                    drone->bounceDrone(bounce, segIndex);

            }
            else
            { drone->bounceDrone(gVector(), -1); }
        }
        else
        {
            if (wallIndex == -1)
                throw("DroneArena: Drone leaked through the wall!");
        }
    }
    
    
    
    // Log the simulation data
    if (m_dataLogger != NULL)
    {
        if (m_logCountIndex++ == 0) // To limit the logging rate
        {
            for (droneIterator it = m_drones.begin(); it != m_drones.end(); ++it)
            {
                drone = *it;
                std::vector<float> log;
                log.push_back(m_trialID);
                log.push_back(Ts.time);
                drone->logDroneData(log);
             
                m_dataLogger->log(log);
            }
        }
        if (m_logCountIndex > 10) // The number of steps that the logger skips
            m_logCountIndex = 0;
    }
}
vector<float>& DroneArena::getPerformanceMetrics()
{
    if (m_simEnd == 0)
        throw("DroneArena: Can't evaluate performance metrics without running a simulation");
    
    float avgTargets = 0;
    for (std::vector<Drone*>::iterator d = m_drones.begin(); d != m_drones.end(); ++d)
        avgTargets += (*d)->getTargetCount();
    
    avgTargets /= m_drones.size();
    
    m_performanceMetrics[0] = avgTargets;//m_map.getCoverage();
    m_performanceMetrics[1] /= ( m_drones.size()*m_simEnd);
    m_performanceMetrics[2] = m_simEnd/m_simTime;//( (m_drones.size()*(m_drones.size()-1)/2  )*m_simTime);


    return m_performanceMetrics;
}
Simulator::SimState DroneArena::evaluate(TimeStep &Ts)
{
    Point P0, P1;
    bool foundCrash = false;
    
//    unsigned int droneCounter = 0;
    // Go through all drones to check their status
    for (droneIterator d0 = m_drones.begin(); d0 != m_drones.end(); ++d0)
    {
        unsigned char d0ID = (*d0)->getID();
        (*d0)->getPos(P0);
        
        gVector vel;
        (*d0)->getVelocity(vel);
        float V = vel.norm();
        
        if ((*d0)->getAIIO()->out[0] < 0)
            V *= (-1);
        
        // Update performance metrics of the simulation
        BTNode *defNode = dynamic_cast<BehaviorTree*>((*d0)->getAI())->getRoot()->getLastChild();
        if (defNode->wasTicked() && (V > 0.3) )
        {
            m_performanceMetrics[1] += Ts.dt;
            
            // Mark the coverage map
            m_map.mark(P0);
        }
        
        // Check drone-drone distances (conflict & collision detection)
        droneIterator d1 = d0+1; // Pointer to the next drone
        for (; d1 != m_drones.end(); ++d1)
        {
            unsigned char d1ID = (*d1)->getID();
            (*d1)->getPos(P1);
            
            float ddDist = gVector(P0,P1).norm();
            
            float pD = 0;
            if (fabs((*d0)->getAIIO()->out[2]) > 0.005)
                pD = -2;
            else if (ddDist < 2)
                pD = -1;
            else if ( (ddDist > DRONE_DRONE_CONFLICT_RANGE) && (V > 0.2) )
                pD = 1;
            else
                pD = 0;
            
            m_performanceMetrics[2] += pD*Ts.dt;
            
            // Check if the drones are in conflict
            if (ddDist < 2)
            {
                // Mark the drones as "in conflict"
                m_conflictFlag[d0ID][d1ID] = true;
                m_conflictFlag[d1ID][d0ID] = true;
                
                m_conflictTime += Ts.dt;
            }
            else
            {
                // The drone just got out of conflict
                if ((m_conflictFlag[d0ID][d1ID]) && (m_conflictFlag[d1ID][d0ID]))
                {
                    m_conflictCount[d0ID]++;
                    m_conflictFlag[d0ID][d1ID] = false;

                    m_conflictCount[d1ID]++;
                    m_conflictFlag[d1ID][d0ID] = false;
                }
            }
            
            // Check if the drones have crashed - if so, mark that a crash happened, but still go through the rest of the drones for their stats
            if (ddDist < DRONE_DRONE_CRASH_RANGE)
                foundCrash = true;
        }
    }
    
    if (foundCrash)
        return Simulator::Failed;
    
    return Simulator::Running;
}

void    DroneArena::createRectangularWalls(double sideLen)
{
    double S = sideLen/2;
    m_walls.addVertex(Point(-S, -S));
    m_walls.addVertex(Point(-S, S));
    m_walls.addVertex(Point(S , S));
    m_walls.addVertex(Point(S , -S));
    
    m_walls.lock();
}
void    DroneArena::createRandomEnvironmentFromCircle(double vertexCount, double radius)
{
    double defAngle = 2*M_PI/vertexCount;
    
    deque<double> vertX;
    deque<double> vertY;
    
    for (int i=0; i < vertexCount; i++)
    {
        double angle = (i+0.5)*defAngle + 0.3*m_randomEngine.getStdUniformReal();
        m_walls.addVertex(Point(cos(angle)*radius,sin(angle)*radius));
    }
    m_walls.lock();
}
void    DroneArena::createRandomEnvironmentFromCircle(double vertexCount, double defaultRadius, double radiusDeviation)
{
    double defAngle = 2*M_PI/vertexCount;
    
    deque<double> vertX;
    deque<double> vertY;
    
    for (int i=0; i < vertexCount; i++)
    {
        double angle = (i+0.5)*defAngle + 0.3*m_randomEngine.getStdUniformReal();
        double R = m_randomEngine.getUniformReal(defaultRadius - radiusDeviation, defaultRadius + radiusDeviation);
        m_walls.addVertex(Point(cos(angle)*R,sin(angle)*R));
    }
    
    m_walls.lock();
}
void DroneArena::logEnvironmentShape(Logger *log)
{
    std::vector<Point> vertices = m_walls.getVertices();
    
    std::vector<float> data(vertices.size()*2 + m_performanceMetrics.size() + 4, 0);
    
    unsigned int pos=0;
    data[pos++] = m_trialID;
    data[pos++] = m_drones.size();
    
    std::vector<float> perfMetrics = getPerformanceMetrics();
    data[pos++] = perfMetrics.size();
    for (std::vector<float>::iterator it = perfMetrics.begin(); it != perfMetrics.end(); ++it)
        data[pos++] = *it;
    
    data[pos++] = vertices.size();
    
    for (std::vector<Point>::const_iterator pt = vertices.begin(); pt != vertices.end(); ++pt)
    {
        data[pos++] = pt->x;
        data[pos++] = pt->y;
    }
    
    log->log(data);
}