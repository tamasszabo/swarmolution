Swarmolution
=============

Swarmolution is a tool designed to aid the evolutionary development of robot controllers. The main parts of it are:

* Simulator: used for evaluating the performance of candidate controllers
* AI: AI frameworks that can be evolved
* EA: Evolutionary algorithms
* Tools: Miscellaneous tools required by the Simulator and EA, including Kalman Filter implementations, a logger a random engine (wrapper) and a multi-core work engine.

Required Software
-----------------

Swarmolution is developed in C++ based on the C++11 standard.
The only two dependencies are: the Eigen linear algebra library and pugixml XML parser. Both of these are included, under Tools/Libraries.

This software is developed in Xcode under OS X, hence the Swarmolution.xcodeproj directory. While there is no Makefile provided with the project, it should compile on any OS by compiling and linking all source files within the project.